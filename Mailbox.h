// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef MAILBOX_INCLUDED
#define MAILBOX_INCLUDED

#include "main.h"
#include "WatchDog.h"
#include "clipbrd.h"

enum
{
	/* final status - success */
	MBOX_NOT_CHECKED,		
	MBOX_CHECKED,
	MBOX_INTERRUPTED_BY_USER,

	/* final status - error */
	MBOX_CANNOT_CONNECT = 100,
	MBOX_CANNOT_CHECK,
	MBOX_CANNOT_SEND,
	MBOX_NETWORK_FAILED,
	MBOX_SERVER_FAILED,
	MBOX_CONNECTION_WAS_LOST,
	MBOX_SOCKET_ERROR,
	MBOX_INVALID_USER,
	MBOX_INVALID_HOST,
	MBOX_INVALID_PASS,

	/* in progress */
	MBOX_RESOLVING = 1000,
	MBOX_CONNECTING,
	MBOX_WF_USER_RESPONSE,
	MBOX_WF_APOP_RESPONSE,
	MBOX_WF_PASS_RESPONSE,
	MBOX_WF_FIRST_NOOP,
	MBOX_WF_SECOND_NOOP,
	MBOX_WF_UIDL_RESPONSE,
	MBOX_WF_TOPEX_RESPONSE,
	MBOX_WF_LIST_RESPONSE,
	MBOX_WF_TOP_RESPONSE,
	MBOX_WF_RETR_RESPONSE,
	MBOX_DOWNLOADING_DATA,
	MBOX_WF_DELE_RESPONSE,
    MBOX_WF_TOPF_RESPONSE,     //  against pop3 flash server weakness
	MBOX_WF_USER_RESPONSE_SSL,
};

#define STATE_FINAL(s)		((s)<1000)
#define STATE_ERROR(s)		( (100<=(s)) && ((s)<1000) )

enum
{
	ACTION_NULL = -1,
	ACTION_NONE,
	ACTION_SPECIFIC,
	ACTION_DEFAULT,
};

enum
{
	PLAYBACK_DEVICE_NULL = -1,
	PLAYBACK_DEVICE_FILE,
	PLAYBACK_DEVICE_CDA,
};

enum
{
	COMMAND_RUN_NULL = -1,
	COMMAND_RUN_NORMAL,
	COMMAND_RUN_MINIMIZED,
	COMMAND_RUN_MAXIMIZED,
	COMMAND_RUN_HIDDEN,
};

class Tristate
{
	int m_iValue;
	bool m_bDefaultValue;
	
public:

	Tristate( bool bDefaultValue ) : 
		m_bDefaultValue( bDefaultValue ), 
		m_iValue( -1 ) 
	{
	}

	operator bool() const
	{
		return ( IsSet() ? ( m_iValue == 1 ) : m_bDefaultValue ); 
	};

	bool IsSet() const { return m_iValue != -1; }
	void Unset() { m_iValue = -1; }

	Tristate& operator=( bool bNewValue )
	{
		m_iValue = ( bNewValue ? 1 : 0 );
		return *this;
	}
};

class CExcerpt;
template < class BASE_CLASS, class TYPE > class CTypedPtrArray;

enum MailBoxFlags
{
	MBF_NO_APOP		=	0x0001,
	MBF_PASSIVE		=	0x0002,
	MBF_ASK_PASS	=	0x0004,
	MBF_SPEC_TOP	=	0x0008,	// for FLASH box
	MBF_NO_RETR		=	0x0010,	// for gmail
	MBF_SSL			=	0x0020,	
	MBF_FOLDER		=	0x0040,	// included in folder
	MBF_FOLDER_CLOSED=	0x0080, // toggled

	MBF_DEFAULT = 0,
};
class CMailbox : public CAsyncSocket, public CWatchDog
{
	DECLARE_SERIAL( CMailbox );

	HANDLE m_hResolveJob;
	char *m_pResolveBuffer;
	CByteArray m_arrayPacket;

	CUIntArray m_aIndices;
	int m_intOrder;

	// server traits
	CString	 m_sServerGreeting;
	Tristate m_tServerSupportsAPOP;
	Tristate m_tServerSupportsUIDL;
	Tristate m_tServerSupportsBurstWrites;
	ULONG m_ulongIP;

	bool m_bLog;
	CString m_strLog;

	void InitializeServerTraits()
	{
		m_sServerGreeting.Empty();
		m_tServerSupportsAPOP.Unset();
		m_tServerSupportsUIDL.Unset();
		m_tServerSupportsBurstWrites.Unset();
		m_ulongIP = 0;
	}

	CString m_strLastError;
	void AppendToLog( const CString& );

	void RecoverString( CString& );
	void BeginResolveHostByName();
	void ParseString( char *str, int len, int size );
	void OrderAssignedToID( int order, const char *str );
	void FinishReceivingID();
	void SendBurstWriteSenseRequest();
	void SendUidlRequest();
	void SendListRequest();
	void SendTopExRequest();
  void SendTopFRequest();

	void SendInitialTopRequest();
	void SendTopRequest();
	bool SendTopRequest( unsigned uIndex );

	void SendInitialRetrRequest();
	void SendRetrRequest();
	void SendRetrRequest( unsigned uIndex );

	void SendInitialDeleRequest();
	void SendDeleRequest();
	void SendDeleRequest( unsigned uIndex );

	void SendQuitRequest( int intState );


	// overwrite CAsyncSocket::Send
	bool Send( const void* lpBuf, int nBufLen );

	// implement a notification from CAsyncSocket
	void OnClose( int nErrorCode );

public:
	void OnSSLEvent(int);

	void CancelPendingJobs();
	virtual void Check();
	virtual int SuggestIcon();
	virtual BOOL IsFolder(){return FALSE;};
	virtual void GetText(int nCol, CString&);
	virtual void SetSelection(int nState);

	void MarkRead(BOOL b=TRUE);

	void StopChecking();
	void EndResolveHostByName( WPARAM, LPARAM );

	const CString& GetGreeting() const;
	unsigned GetIPAddress() const;
	const Tristate& GetAPOPSupported() const;
	const Tristate& GetUIDLSupported() const;
	const Tristate& GetBurstWritesSupported() const;

	BOOL IsDisabled() const;
	bool IsLoggingEnabled() const;
	void EnableLogging( bool );
	const CString& GetLogString() const;
	CString GetPassword();
	int  GetExtraLines();

	virtual BOOL UpdateUnreadStatus(int* pAll = NULL);	// TRUE, if state was changed
	int		m_nUnread;

	UINT m_uMciID;
	HANDLE m_hCmdID;


	int	m_nStatTime;
	int	m_nStatCur;
	int	m_nStatMax;
	
	CTime m_tmCheckTime;	// for advanced sorting

	int m_intClue;
	int m_intPort;
	DWORD	m_dwFlags;
	BYTE    m_nExtraLines;
	int m_intPoll;
	int m_intElapsed;
	CTime	m_tmLastCheck;
	CString m_strAlias;
	CString m_strPass;
	CString m_strUser;
	CString m_strHost;
	/* shell command parameters */
	int m_intCommand;
	int m_intCommandRun;
	CString m_strCommand;
	/* mci playback parameters */
	int m_intPlayback;
	int m_intPlaybackDevice;
	CString m_strPlayback;
	/* popup message parameter */
	int m_intMessage;

	CTypedPtrArray < CPtrArray, CExcerpt* > m_arrayExcerpt;

	/* arrtibutes */
	int m_intState;
	unsigned m_bitSelected : 1;
	unsigned m_bitCreated : 1;
	unsigned m_bitDeleted : 1;

	/* update info */
	int m_intChanged;
	int IsChanged( int intColumn ) { return m_intChanged & BIT( intColumn ); }
	virtual void Change( int intColumn );

	void SetClue( void ) { m_intClue += (int) time(0); }
	void SetElapsed( BOOL bForced = FALSE );
	void SetAlias( CString &strAlias );
	void SetUser( CString &strUser );
	void SetHost( CString &strHost );
	void SetPass( CString &strPass, BOOL bAsk );
	void SetState( int );
	void SetPort( int intPort );
	void SetPoll( int intPoll );
	void SetFlag(int nFlag, int nSet);
	BOOL InFolder() {return (m_dwFlags & MBF_FOLDER);};
	void MakeInFolder(BOOL bSet) {SetFlag(MBF_FOLDER, bSet); m_pFolder = NULL;};
	BOOL IsClosed() {return (m_dwFlags & MBF_FOLDER_CLOSED)!=0;};
	void CloseFolder(BOOL bSet=TRUE) {SetFlag(MBF_FOLDER_CLOSED, bSet);};

	bool IsSecure() { return ( m_tServerSupportsAPOP.IsSet() ? m_tServerSupportsAPOP : false ); };

	const CString& GetLastErrorDescription() { return m_strLastError; }

	//{{AFX_VIRTUAL(CMailbox)
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

	CMailbox();
	virtual ~CMailbox();
	const CMailbox& operator=( const CMailbox& );
	CMailbox*		m_pFolder;

protected:
#ifdef USE_SSL
	CConnectSoc	m_SSL;
	SSL_CTX		* m_pSSLctx;
    SSL			* m_pSSL;
#endif

	// WatchDog
	void WatchDogTimedOut();

	virtual void OnReceive( int nErrorCode );
	virtual void OnConnect( int nErrorCode );
};
class CMailboxFolder: public CMailbox
{
public:
	DECLARE_SERIAL( CMailboxFolder );
	CMailboxFolder();
	virtual void Serialize(CArchive& ar);

	virtual void GetText(int nCol, CString&);
	virtual BOOL IsFolder(){return TRUE;};
	virtual void Check();
	virtual int SuggestIcon();
	virtual void SetSelection(int nState);
	virtual void Change( int intColumn ) { m_intChanged |= BIT( intColumn ); }
	virtual BOOL UpdateUnreadStatus(int* pAll=NULL);	// TRUE, if state was changed
	
	void ResetBoxes() {m_aBoxes.RemoveAll();}
	void AddBox(CMailbox* p);
	CArray<CMailbox*>	m_aBoxes;
};
// helper to serialize several mailboxes setings
class CMailBoxClip : public CClipBoard
{
public:
	CMailBoxClip();
	~CMailBoxClip();


	BOOL		SaveData(CMailbox** pData, int nCount);
	CMailbox**	GetData(int& nCount);

protected:
	virtual BOOL Serialize(CArchive& ar);
	void Cleanup();
private:
	CTypedPtrArray<CObArray, CMailbox*> m_aData;
};

#endif