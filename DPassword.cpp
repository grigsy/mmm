// DPassword.cpp : implementation file
//

#include "stdafx.h"
#include "magic.h"
#include "DPassword.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DPassword dialog


DPassword::DPassword(BOOL bCnf, LPCTSTR sAlias)
	: CDialog(DPassword::IDD, NULL)
{
	//{{AFX_DATA_INIT(DPassword)
	m_sPassword = _T("");
	m_sOpen = _T("");
	//}}AFX_DATA_INIT
	m_bCnf = bCnf;
	m_sAlias = sAlias;
}


void DPassword::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DPassword)
	DDX_Text(pDX, IDC_PASSWORD, m_sPassword);
	DDX_Text(pDX, IDC_PASSWORD_OPEN, m_sOpen);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DPassword, CDialog)
	//{{AFX_MSG_MAP(DPassword)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_EN_CHANGE(IDC_PASSWORD, OnChangePassword)
	ON_EN_CHANGE(IDC_PASSWORD_OPEN, OnChangePassword)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DPassword message handlers


void DPassword::OnOK() 
{
	UpdateData();
	// check match
	if (m_bCnf && m_sPassword != m_sOpen)
	{
		return;
	}
	CDialog::OnOK();
}

void DPassword::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent == m_nTimer)
	{
		GetDlgItem(IDC_CAPS)->ShowWindow(GetKeyState(VK_CAPITAL) & 1);
	}
	CDialog::OnTimer(nIDEvent);
}

BOOL DPassword::OnInitDialog() 
{
	CDialog::OnInitDialog();

	DlgTranslate(this);
	m_nTimer = SetTimer(12, 300, NULL);

	if (m_bCnf)
	{
		m_sOpen = m_sPassword;
	}
	else
	{
		GetDlgItem(IDC_CNF)->ShowWindow(FALSE);
		GetDlgItem(IDC_PASSWORD_OPEN)->ShowWindow(FALSE);
	}
	UpdateData(FALSE);
	OnChangePassword();
	if (!m_sAlias.IsEmpty())
	{
		CString s;
		GetWindowText(s);
		s += _T(": ");
		s += m_sAlias;
		SetWindowText(s);
	}

	return TRUE;
}

void DPassword::OnDestroy() 
{
	KillTimer(m_nTimer);
	CDialog::OnDestroy();
}

void DPassword::OnChangePassword() 
{
	UpdateData();
	GetDlgItem(IDC_WARN)->ShowWindow((m_bCnf && m_sPassword != m_sOpen));
}
