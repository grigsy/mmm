// diction.h : header file
//
#ifndef _DICTION_H_
#define _DICTION_H_

#ifdef _USE_DICTION_

#include <afxcoll.h>


struct DicData
{
	CString sLang;
	CString sFile;
	CString sAuthor;
	CString sRem;
	DicData& operator=(const DicData& a);
};
typedef CArray<DicData, DicData&> Dictions;

class CDictionary
{
public:
	CDictionary();

	BOOL SetDictionary(LPCTSTR sFile, BOOL bAll=FALSE);
	BOOL DlgTranslate(CWnd* p);
	BOOL CBTranslate(CWnd* p);
	BOOL StrTranslate(CString& s);
	BOOL MenuTranslate(HMENU hm);
	BOOL SheetTranslate(CPropertySheet* p);

	void FindDictions(Dictions& Dics, LPCTSTR sPath=NULL);
	BOOL CheckFile(LPCTSTR, DicData* pData, CString*);
	
	DicData	m_Data;
protected:
	void RemoveCRs(CString& s);
	virtual void AddEntry(CString& sKey, CString& sTrans);
	CMapStringToString	m_Words;
	// nOnlyNew = 1, just news, nOnlyNew=2 - empty news
	BOOL LoadDiction(LPCTSTR sFile, BOOL bAll, int nOnlyNew=0, BOOL bGetData = TRUE);
};
CDictionary* GetDictionary();

inline void DlgTranslate(CWnd* p){ GetDictionary()->DlgTranslate(p);};
inline void StrTranslate(CString& s){ GetDictionary()->StrTranslate(s);};
inline void MenuTranslate(HMENU HM){ GetDictionary()->MenuTranslate(HM);};
inline void SheetTranslate(CWnd* p){ GetDictionary()->SheetTranslate((CPropertySheet*)p);};

#else

#define DlgTranslate(p) p
#define StrTranslate(s) s
#define MenuTranslate(p) p
#define SheetTranslate(s) s

#endif 

#define LOAD_STRING(s, ID) s.LoadString(ID); StrTranslate(s);
#define MAKE_STRING(s, ID) CString s; LOAD_STRING(s, ID);


#endif //_DICTION_H_