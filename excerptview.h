// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
/////////////////////////////////////////////////////////////////////
//
//   File : ExcerptView.cpp
//   Description : message list view
//
//   Modification history ( date, name, description ) : 
//		1.	15.12.2002	Igor Green, mmm3@grigsoft.com
//			Icon for loaded message, OnLoad, OnQuickText
//
//HISTORY_ENTRY_END:1!15.12.2002
/////////////////////////////////////////////////////////////////////

#ifndef EXCERPT_VIEW_INCLUDED
#define EXCERPT_VIEW_INCLUDED

#include "ListViewEx.h"
#include "titletip.h"

class CMailbox;

#define CExcerptsParent CListViewEx

class CExcerptView : public CExcerptsParent
{
	DECLARE_DYNCREATE(CExcerptView)
	CExcerptView();           // protected constructor used by dynamic creation
	static int CALLBACK CompareSubItems( LPARAM, LPARAM, LPARAM );

	unsigned m_bitSortEnabled : 1;
	unsigned m_bitSortRequired : 1;

public:
	void EnableSort( BOOL = TRUE );
	void SortItems();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExcerptView)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

protected:
	void DoProtect(BOOL bProtect);
	void GetColumnData();
	void DoAction(int nAction, int nDefault);
	void GetSelAddresses(CStringArray& asFrom, int nType = IDP_COLUMN_FROM);
	CHeaderColSortMark	m_ColSortGUI;
	CImageList m_imageListSmall;
	virtual ~CExcerptView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual void GetItemColors(int nItem, COLORREF& clrText, 
								COLORREF& clrBk, BOOL bSelected);
	virtual void DrawSubItem(CDC* pDC, int nItem, int nColumn, 
		CRect& rcLabel, int nJustify, BOOL bSelected);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	void DoMarkRead(BOOL bHow, BOOL bRead = TRUE);
	BOOL IsSpamSelection();

	CTitleTip	m_TitleTip;

	// Generated message map functions
protected:
	//{{AFX_MSG(CExcerptView)
	afx_msg void OnUpdateViewReport(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewDisable(CCmdUI* pCmdUI);
	afx_msg void OnViewReport();
	afx_msg void OnEditSelectAll();
	afx_msg void OnUpdateEditSelectAll(CCmdUI* pCmdUI);
	afx_msg void OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEditProperties();
	afx_msg void OnUpdateIfSelectedCount(CCmdUI* pCmdUI);
	afx_msg void OnViewRefresh();
	afx_msg void OnUpdateViewRefresh(CCmdUI* pCmdUI);
	afx_msg void OnQuickDelete();
	afx_msg void OnQuickView();
	afx_msg void OnQuickReply();
	afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDestroy();
	afx_msg void OnStopChecking();
	afx_msg void OnUpdateStopChecking(CCmdUI* pCmdUI);
	afx_msg void OnKeydown(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLoad();
	afx_msg void OnQuicktext();
	afx_msg void OnTextPreview();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnAllRead();
	afx_msg void OnFilters();
	afx_msg void OnFileApplyfilters();
	afx_msg void OnAddFriend();
	afx_msg void OnDomainAdd();
	afx_msg void OnKillSpam();
	afx_msg void OnUpdateKillSpam(CCmdUI* pCmdUI);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnToggleSpam();
	afx_msg void OnUpdateToggleSpam(CCmdUI* pCmdUI);
	afx_msg void OnSelRevert();
	afx_msg void OnSelNotFriends();
	afx_msg void OnProtect();
	afx_msg void OnUpdateProtect(CCmdUI* pCmdUI);
	afx_msg void OnUnprotect();
	afx_msg void OnUpdateUnprotect(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg void OnUpdateIndicatorObjects( CCmdUI *pCmdUI );
	afx_msg void OnUpdateIndicatorContents( CCmdUI *pCmdUI );
	afx_msg LRESULT OnLanguage(WPARAM, LPARAM);
	afx_msg void OnMarkRead();
	afx_msg void OnMarkNew();
	afx_msg void OnMarkSpam();
	afx_msg void OnMarkGood();
	afx_msg void OnListCommand(UINT nCmd);
	afx_msg void OnHeaderEndDrag(NMHDR*pHeader, LRESULT* pResult);
	DECLARE_MESSAGE_MAP()

private:
	void AddItemToList(int nType, CString& sList);
	void ShowContextMenu( const POINT& pt );
};

#endif
