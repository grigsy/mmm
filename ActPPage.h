#if !defined(AFX_ACTPPAGE_H__1B41C86D_3038_4F75_9CB4_B943FBE9EC2C__INCLUDED_)
#define AFX_ACTPPAGE_H__1B41C86D_3038_4F75_9CB4_B943FBE9EC2C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ActPPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CActPPage dialog

class CActPPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CActPPage)

// Construction
public:
	CActPPage();
	~CActPPage();

// Dialog Data
	//{{AFX_DATA(CActPPage)
	enum { IDD = IDD_OPTPAGE_CMD };
	int		m_nActDbl;
	int		m_nActRb;
	int		m_nActRb2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CActPPage)
	public:
	virtual BOOL OnApply();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CActPPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACTPPAGE_H__1B41C86D_3038_4F75_9CB4_B943FBE9EC2C__INCLUDED_)
