// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

/* common messages used by CMagicFrame */
#define VM_TASKBAR_NOTIFICATION		( WM_USER + 1 ) 
#define VM_END_RESOLVE_HOST_BY_NAME	( WM_USER + 2 )

#define VM_START_COMMAND			( WM_USER + 3 )
#define VM_START_PLAYBACK			( WM_USER + 4 )
#define VM_START_MESSAGE			( WM_USER + 5 )
/*
 * wParam is a NewMailCount;
 * lParam is a pointer to CMailbox;
 */

#define VM_UPDATEITEM				( WM_USER + 6 )
/* 
 * wParam is pointer to CObject, i.e. item to be updated, 
 * or 0 if all available items need to be updated
 */
#define VM_LANGUAGE					( WM_USER + 7 )
#define VM_TEST_MESSAGE				( WM_USER + 8 )

#define BIT(c)		(1<<(c))

/* used by CMagicView and CExcerptView */
enum
{
	HINT_UPDATE_ITEM,
	HINT_ENABLE_SORT,
	HINT_DISABLE_SORT,
	HINT_OPTIONS,
};

/* used by CMailbox */
/* use caution changing these - there is code that
   expects these values to correspond to columns */
enum
{
	COLUMN_NULL = -1,
	COLUMN_ALIAS,
	COLUMN_USER,
	COLUMN_HOST,
	COLUMN_MAIL,
	COLUMN_STATE,
	COLUMN_ELAPSED,
	COLUMN_PORT,
	COLUMN_POLL,
	// fake:
	COLUMN_SELECTED,
};

/* used by CExcerpt */
/* use caution changing these - there is code that
   expects these values to correspond to columns */
enum
{
	COLUMN_MBOX,
	COLUMN_FROM,
	COLUMN_TO,
	COLUMN_SUBJ,
	COLUMN_DATE,
	COLUMN_SIZE,
	// fake:
};

enum ReadMode
{
	READ_ALWAYS	=	0,	// do not use unread flag
	READ_VIEW,			// after viewed
	READ_MINIM,			// after window is minimized
	READ_BOTH,			// after view or minimized
};

enum MailFiltersField
{
	MFF_SUBJECT,
	MFF_FROM,
	MFF_TO,
	MFF_CC,
	MFF_HEADER,
	MFF_SIZE,
	MFF_DATE,
};

enum MailFiltersOperation
{
	MFO_EQUAL,		// direct or with wildcard
	MFO_INCLUDE,
	MFO_GREATER,	// for size only
	MFO_NOT_INCL,	// does not include
	MFO_INC_RE,		// include regexp
};

enum MailFiltersAction
{
	MFA_NONE	= 0,
	MFA_ENABLED = 0x0001,		// enable this filter
	MFA_DELETE	= 0x0002,		// delete from server immediately
	MFA_SPAM	= 0x0004,		// mark as spam (in subject, or other?)
	MFA_COLOR	= 0x0008,		// custom color
	MFA_READ	= 0x0010,		// no notification
	MFA_OTHER	= 0x0020,		// continue with next filters
	MFA_FRIEND	= 0x0040,		// message from white list
	MFA_SPAM_MAN= 0x0084,		// manual spam selection (also make as read)
	MFA_PROTECT=  0x0100,		// protect message from deletion
};

enum MailFiltersCombination
{
	MFC_NONE,		// no combination
	MFC_AND,						   
	MFC_OR,
};

enum MagicMailFlags
{
	MMF_LOG_DELETED	=	0x0001,	// keep log of deleted files
	MMF_NO_TOOLBAR	=	0x0002,
	MMF_NO_STATUS	=	0x0004,
	MMF_COLOR_FRIEND=	0x0008,	// special color for friends
	MMF_SINGLE		=	0x0010,	// single instance
	MMF_LOG_FILTER	=	0x0020,	// do not log manually deleted msgs
	MMF_CHECK_DBL	=	0x0040,	// check mail on mailbox dblclk
	MMF_FONT		=	0x0080,	// use custom font
	MMF_MAXIMIZED	=	0x0100,
	MMF_SMART_SORT	=	0x0200,	// sort by check time
	MMF_PROTECT		=	0x0400,	// protect mails from friend
	MMF_JUMPEND		=	0x0800,	// header view will jump to end automatically
	MMF_MASSIVE		=	0x1000,	// header view will jump to end automatically
	
	MMF_DEFAULT = MMF_COLOR_FRIEND|MMF_CHECK_DBL,
};

enum MessageAction
{
	ACT_QVIEW = 1,
	ACT_MENU,
	ACT_HEADER,
	ACT_PROPS,
	ACT_PREVIEW,
	ACT_ASTEXT,
};

inline BOOL GetBoolFlag(DWORD dwFlags, int nMask)
{
	return (dwFlags & nMask)!=0;
}
inline void SetBoolFlag(DWORD& dwFlags, int nMask, int bState)
{
	if (bState)
		dwFlags |= nMask;
	else
		dwFlags &= ~nMask;
}

#define FRIENDS_FILE "friendlst.txt"
#define DELLOG_FILE "msgdeleted.log"


#endif /* MAIN_H_INCLUDED */
