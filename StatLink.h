////////////////////////////////////////////////////////////////
// 1997 Microsoft Systems Journal
// If this code works, it was written by Paul DiLascia.
// If not, I don't know who wrote it.
//
#ifndef _STATLINK_H
#define _STATLINK_H

BOOL CreateStaticLink(CWnd*,UINT,  LPCTSTR sURL, HCURSOR hc);

#endif _STATLINK_H
