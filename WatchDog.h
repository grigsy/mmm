// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// WatchDog.h: interface for the CWatchDog class.
//
//////////////////////////////////////////////////////////////////////

#ifndef WATCH_DOG_H_INCLUDED
#define WATCH_DOG_H_INCLUDED

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CWatchDog 
{

protected:

	CWatchDog();
	virtual ~CWatchDog();

	void CreateWatchDog( UINT uintTimeout );

	void DestroyWatchDog();

	bool IsWatchdogOn();

	virtual void WatchDogTimedOut() = 0;

private:

	void OnTimer();

	// DATA MEMBERS
	UINT	m_uTimerID;
	bool	m_bEnabled;

	// STATIC MEMBERS
	static unsigned s_uInstanceCount;

	static HWND s_hWnd;
	static WNDPROC OldWndProc;

	static LRESULT CALLBACK WndProc( HWND, UINT, WPARAM, LPARAM );
};

#endif // WATCH_DOG_H_INCLUDED
