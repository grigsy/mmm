// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// ExcerptView.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////
//
//   File : ExcerptView.cpp
//   Description : message list view
//
//   Modification history ( date, name, description ) : 
//		1.	15.12.2002	Igor Green, mmm3@grigsoft.com
//			Icon for loaded message, OnLoad, OnQuickText
//		2.	17.12.2002	Igor Green, mmm3@grigsoft.com
//			Added partial text preview
//
//HISTORY_ENTRY_END:2!17.12.2002
/////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Magic.h"
#include "Excerpt.h"
#include "Mailbox.h"
#include "MagicDoc.h"
#include "HeaderPage.h"
#include "MailboxView.h"
#include "ExcerptView.h"
#include "MagicFrame.h"
#include "RawHead.h"
#include "getwinver.h"
#include "dfilters.h"
#include "tools.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExcerptView

#include <locale.h>

IMPLEMENT_DYNCREATE(CExcerptView, CExcerptsParent)

CExcerptView::CExcerptView()
:	m_bitSortEnabled(1),
	m_bitSortRequired(0)
{
	//
	// change the C library locale to user's locale for displaying dates
	//
	CString strLocaleString;
	char szBuffer[1024] = {0};

	VERIFY( GetLocaleInfo( LOCALE_USER_DEFAULT, LOCALE_SENGLANGUAGE, szBuffer, sizeof( szBuffer ) ) );
	strLocaleString = szBuffer;

	strLocaleString += "_";

	VERIFY( GetLocaleInfo( LOCALE_USER_DEFAULT, LOCALE_SABBREVCTRYNAME, szBuffer, sizeof( szBuffer ) ) );
	strLocaleString += szBuffer;

	setlocale( LC_ALL, strLocaleString );
}

CExcerptView::~CExcerptView()
{
}

BEGIN_MESSAGE_MAP(CExcerptView, CExcerptsParent)
	//{{AFX_MSG_MAP(CExcerptView)
	ON_UPDATE_COMMAND_UI( ID_VIEW_REPORT, OnUpdateViewReport )
	ON_UPDATE_COMMAND_UI( ID_VIEW_SMALLICON, OnUpdateViewDisable )
	ON_COMMAND( ID_VIEW_REPORT, OnViewReport )
	ON_COMMAND( ID_EDIT_SELECT_ALL, OnEditSelectAll )
	ON_UPDATE_COMMAND_UI( ID_EDIT_SELECT_ALL, OnUpdateEditSelectAll )
	ON_NOTIFY_REFLECT( LVN_COLUMNCLICK, OnColumnclick )
	ON_NOTIFY_REFLECT( NM_RCLICK, OnRclick )
	ON_COMMAND( ID_EDIT_PROPERTIES, OnEditProperties )
	ON_UPDATE_COMMAND_UI( ID_QUICK_VIEW, OnUpdateIfSelectedCount )
	ON_COMMAND( ID_VIEW_REFRESH, OnViewRefresh )
	ON_UPDATE_COMMAND_UI( ID_VIEW_REFRESH, OnUpdateViewRefresh )
	ON_COMMAND( ID_QUICK_DELETE, OnQuickDelete )
	ON_COMMAND( ID_QUICK_VIEW, OnQuickView )
	ON_COMMAND( ID_QUICK_REPLY, OnQuickReply )
	ON_NOTIFY_REFLECT( NM_DBLCLK, OnDblclk )
	ON_WM_DESTROY()
	ON_COMMAND(ID_STOP_CHECKING, OnStopChecking)
	ON_UPDATE_COMMAND_UI(ID_STOP_CHECKING, OnUpdateStopChecking)
	ON_NOTIFY_REFLECT(LVN_KEYDOWN, OnKeydown)
	ON_COMMAND(ID_LOAD, OnLoad)
	ON_COMMAND(ID_QUICKTEXT, OnQuicktext)
	ON_COMMAND(ID_TEXT_PREVIEW, OnTextPreview)
	ON_WM_MOUSEMOVE()
	ON_COMMAND(IDC_ALL_READ, OnAllRead)
	ON_COMMAND(ID_FILE_APPLYFILTERS, OnFileApplyfilters)
	ON_COMMAND(ID_ADD_FRIEND, OnAddFriend)
	ON_COMMAND(ID_DOMAIN_ADD, OnDomainAdd)
	ON_COMMAND(ID_KILL_SPAM, OnKillSpam)
	ON_UPDATE_COMMAND_UI(ID_KILL_SPAM, OnUpdateKillSpam)
	ON_WM_MOUSEWHEEL()
	ON_COMMAND(ID_TOGGLE_SPAM, OnToggleSpam)
	ON_UPDATE_COMMAND_UI(ID_TOGGLE_SPAM, OnUpdateToggleSpam)
	ON_COMMAND(ID_SEL_REVERT, OnSelRevert)
	ON_COMMAND(ID_SEL_NOT_FRIENDS, OnSelNotFriends)
	ON_COMMAND( ID_EDIT_DELETE, OnQuickDelete )
	ON_UPDATE_COMMAND_UI( ID_VIEW_LIST, OnUpdateViewDisable )
	ON_UPDATE_COMMAND_UI( ID_VIEW_ICON, OnUpdateViewDisable )
	ON_UPDATE_COMMAND_UI( ID_QUICK_REPLY, OnUpdateIfSelectedCount )
	ON_UPDATE_COMMAND_UI( ID_EDIT_PROPERTIES, OnUpdateIfSelectedCount )
	ON_UPDATE_COMMAND_UI( ID_QUICK_DELETE, OnUpdateIfSelectedCount )
	ON_UPDATE_COMMAND_UI( ID_EDIT_DELETE, OnUpdateIfSelectedCount )
	ON_UPDATE_COMMAND_UI( ID_LOAD, OnUpdateIfSelectedCount)
	ON_UPDATE_COMMAND_UI( ID_QUICKTEXT, OnUpdateIfSelectedCount)
	ON_UPDATE_COMMAND_UI( ID_TEXT_PREVIEW, OnUpdateIfSelectedCount)
	ON_COMMAND(ID_PROTECT, OnProtect)
	ON_UPDATE_COMMAND_UI(ID_PROTECT, OnUpdateProtect)
	ON_COMMAND(ID_UNPROTECT, OnUnprotect)
	ON_UPDATE_COMMAND_UI(ID_UNPROTECT, OnUpdateUnprotect)
	//}}AFX_MSG_MAP
	ON_UPDATE_COMMAND_UI( ID_INDICATOR_OBJECTS, OnUpdateIndicatorObjects )
	ON_UPDATE_COMMAND_UI( ID_INDICATOR_CONTENTS, OnUpdateIndicatorContents )
	ON_UPDATE_COMMAND_UI( IDC_MAIL_READ, OnUpdateIfSelectedCount)
	ON_UPDATE_COMMAND_UI( IDC_MAIL_UNREAD, OnUpdateIfSelectedCount)
	ON_COMMAND(IDC_MAIL_READ, OnMarkRead)
	ON_COMMAND(IDC_MAIL_UNREAD, OnMarkNew)
	ON_COMMAND(ID_MARK_SPAM, OnMarkSpam)
	ON_COMMAND(ID_MARK_NOSPAM, OnMarkGood)
	ON_MESSAGE(VM_LANGUAGE, OnLanguage)
	ON_NOTIFY(HDN_ENDDRAG, 0, OnHeaderEndDrag)
	ON_COMMAND_RANGE(ID_LIST_MIN, ID_LIST_MAX, OnListCommand)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExcerptView diagnostics

#ifdef _DEBUG
void CExcerptView::AssertValid() const
{
	CExcerptsParent::AssertValid();
}

void CExcerptView::Dump(CDumpContext& dc) const
{
	CExcerptsParent::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CExcerptView message handlers

void CExcerptView::OnUpdateViewReport(CCmdUI* pCmdUI) 
{
	pCmdUI->SetRadio( true );
}

void CExcerptView::OnUpdateViewDisable(CCmdUI* pCmdUI) 
{
	pCmdUI->SetRadio( false );
	pCmdUI->Enable( false );
}

void CExcerptView::OnViewReport() 
{
//	ModifyStyle( LVS_TYPEMASK, LVS_REPORT );
//	theApp.intEVMode = LVS_REPORT;
}

UINT aColTitles[]=
{
	IDP_COLUMN_MBOX, IDP_COLUMN_FROM, IDP_COLUMN_TO, 
	IDP_COLUMN_SUBJ, IDP_COLUMN_DATE, IDP_COLUMN_SIZE,
};
BOOL CExcerptView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	if( !CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext) ) return FALSE;

	VERIFY( ModifyStyle( 
		LVS_ICON | LVS_SMALLICON | LVS_LIST | LVS_REPORT, 
		LVS_REPORT | LVS_ALIGNLEFT | LVS_ALIGNTOP | LVS_SHOWSELALWAYS ) );

	ASSERT( &GetListCtrl() );
	CListCtrl &list = GetListCtrl();

	list.SetExtendedStyle(LVS_EX_HEADERDRAGDROP);

	CString strColumn;
	LOAD_STRING(strColumn, IDP_COLUMN_MBOX ); list.InsertColumn( 0, strColumn, LVCFMT_LEFT, theApp.intECMBoxWidth, 0 );
	LOAD_STRING(strColumn, IDP_COLUMN_FROM ); list.InsertColumn( 1, strColumn, LVCFMT_LEFT, theApp.intECFromWidth, 1 );
	LOAD_STRING(strColumn, IDP_COLUMN_TO ); list.InsertColumn( 2, strColumn, LVCFMT_LEFT, theApp.intECToWidth, 1 );
	LOAD_STRING(strColumn, IDP_COLUMN_SUBJ ); list.InsertColumn( 3, strColumn, LVCFMT_LEFT, theApp.intECSubjWidth, 2 );
	LOAD_STRING(strColumn, IDP_COLUMN_DATE ); list.InsertColumn( 4, strColumn, LVCFMT_LEFT, theApp.intECDateWidth, 3 );
	LOAD_STRING(strColumn, IDP_COLUMN_SIZE ); list.InsertColumn( 5, strColumn, LVCFMT_RIGHT, theApp.intECSizeWidth, 4 );

	int *anColumns = new int[list.GetHeaderCtrl()->GetItemCount()];
	anColumns[theApp.intECMBoxPos] = 0;
	anColumns[theApp.intECFromPos] = 1;
	anColumns[theApp.intECToPos] = 2;
	anColumns[theApp.intECSubjPos] = 3;
	anColumns[theApp.intECDatePos] = 4;
	anColumns[theApp.intECSizePos] = 5;
	list.GetHeaderCtrl()->SetOrderArray(list.GetHeaderCtrl()->GetItemCount(), anColumns);
	delete[] anColumns;

	SetFullRowSel( TRUE );

	list.SetImageList( &theApp.m_imgMsg, LVSIL_SMALL );

	m_ColSortGUI.Init(IDB_UP, IDB_DOWN);
	m_ColSortGUI.SetSortMark(list, theApp.intEVSortColumn, theApp.intEVSortAscend);

	m_TitleTip.Create(this);

	if (theApp.GetFont())
		SetFont(theApp.GetFont());

	return TRUE;
}

void CExcerptView::GetColumnData()
{
	CListCtrl &list = GetListCtrl();
	LVCOLUMN lvc;
	lvc.mask=LVCF_WIDTH | LVCF_ORDER;
	list.GetColumn(COLUMN_MBOX, &lvc);
	theApp.intECMBoxWidth = lvc.cx;
	theApp.intECMBoxPos = lvc.iOrder;
	list.GetColumn(COLUMN_FROM, &lvc);
	theApp.intECFromWidth = lvc.cx;
	theApp.intECFromPos =  lvc.iOrder;
	list.GetColumn(COLUMN_TO, &lvc);
	theApp.intECToWidth = lvc.cx;
	theApp.intECToPos =  lvc.iOrder;
	list.GetColumn(COLUMN_SUBJ, &lvc);
	theApp.intECSubjWidth = lvc.cx;
	theApp.intECSubjPos =  lvc.iOrder;
	list.GetColumn(COLUMN_DATE, &lvc);
	theApp.intECDateWidth = lvc.cx;
	theApp.intECDatePos =  lvc.iOrder;
	list.GetColumn(COLUMN_SIZE, &lvc);
	theApp.intECSizeWidth = lvc.cx;
	theApp.intECSizePos =  lvc.iOrder;
}

void CExcerptView::OnDestroy() 
{
	GetColumnData();

	CExcerptsParent::OnDestroy();
}

void CExcerptView::OnEditSelectAll() 
{
	CListCtrl &list = GetListCtrl();
	for( int i = list.GetItemCount(); i; --i ) list.SetItemState( i-1, LVIS_SELECTED, LVIS_SELECTED );
}

void CExcerptView::OnUpdateEditSelectAll(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable( GetListCtrl().GetItemCount() );
}

void CExcerptView::OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	if( theApp.intEVSortColumn == pNMListView->iSubItem )
	{
		theApp.intEVSortAscend = !theApp.intEVSortAscend;
	}
	theApp.intEVSortColumn = pNMListView->iSubItem; 


	SortItems();

	m_ColSortGUI.SetSortMark(GetListCtrl(), theApp.intEVSortColumn, theApp.intEVSortAscend);

	*pResult = 0;
}

void CExcerptView::EnableSort( BOOL bEnable )
{
	m_bitSortEnabled = bEnable;

	if( m_bitSortEnabled && m_bitSortRequired )
		SortItems();

	SendMessage( WM_SETREDRAW, m_bitSortEnabled ? true : false, 0 );
}

void CExcerptView::SortItems()
{
	if( !m_bitSortEnabled ) { m_bitSortRequired = 1; return; }

	theApp.DoWaitCursor(1);
	m_bitSortRequired = 0;
	GetListCtrl().SortItems( &CompareSubItems, 0 );
	theApp.DoWaitCursor(-1);
}

int CALLBACK CExcerptView::CompareSubItems( LPARAM lp0, LPARAM lp1, LPARAM /*lp*/ )
{
	ASSERT( lp0 && lp1 );

	CExcerpt &expt0 = *(CExcerpt*) lp0;
	CExcerpt &expt1 = *(CExcerpt*) lp1;

	int intRes = -1;
	if (theApp.m_dwFlags & MMF_SMART_SORT)
	if (expt0.m_bitFilled && expt1.m_bitFilled && expt0.m_tmCheckTime != expt1.m_tmCheckTime)
	{
		if( expt0.m_tmCheckTime  < expt1.m_tmCheckTime) 
			intRes = -1;
		else
			intRes = 1;
		return theApp.intEVSortAscend ? intRes : -intRes;
	}

	switch( theApp.intEVSortColumn )
	{
		case COLUMN_MBOX:
			intRes = expt0.m_pMailbox->m_strAlias.CompareNoCase( expt1.m_pMailbox->m_strAlias );
			break;

		case COLUMN_FROM:
			intRes = expt0.m_strFromName.CompareNoCase( expt1.m_strFromName );
			break;

		case COLUMN_TO:
			intRes = expt0.m_strToName.CompareNoCase( expt1.m_strToName );
			break;

		case COLUMN_SUBJ:
			intRes = expt0.m_strSubject.CompareNoCase( expt1.m_strSubject );
			break;

		case COLUMN_DATE:
			if( expt0.m_tmDate == expt1.m_tmDate ) intRes = 0;
			else if( expt0.m_tmDate < expt1.m_tmDate ) intRes = -1;
			else intRes = 1;
			break;

		case COLUMN_SIZE:
			if( expt0.m_intSize == expt1.m_intSize ) intRes = 0;
			else if( expt0.m_intSize < expt1.m_intSize ) intRes = -1;
			else intRes = 1;
			break;
	}

	return theApp.intEVSortAscend ? intRes : -intRes;
}  
			
void CExcerptView::OnUpdate(CView* , LPARAM lHint, CObject* pHint) 
{
	switch (lHint)
	{
		case HINT_OPTIONS:
			if (theApp.GetFont())
			{
				SetFont(theApp.GetFont());
				m_fntBold.DeleteObject();
				Invalidate();
			}
			return;
		case HINT_DISABLE_SORT:
			EnableSort( FALSE );
			return;
		case HINT_ENABLE_SORT:
			EnableSort();
			return;
		case HINT_UPDATE_ITEM:	// handled below
			break;
		default:
			return;
	}

	if( !pHint ) return;
	CListCtrl &list = GetListCtrl();

	if( pHint->IsKindOf( RUNTIME_CLASS( CExcerpt ) ) )
	{
		CExcerpt &xrpt = *(CExcerpt*) pHint;
		BOOL bInserted = FALSE;

		LV_FINDINFO findInfo = { LVFI_PARAM, 0, (LPARAM) &xrpt };
		int intIndex = list.FindItem( &findInfo );
		if( -1 == intIndex )
		{
			if( xrpt.m_pMailbox->m_bitSelected )
			{
				intIndex = list.InsertItem( 0, xrpt.m_pMailbox->m_strAlias, -1 );
				ASSERT( -1 != intIndex );
				VERIFY( LB_ERR != list.SetItemData( 0, (DWORD) &xrpt ) );
				bInserted = TRUE;
			}
			else return;
		}
		
		if( !xrpt.m_pMailbox->m_bitSelected || xrpt.m_bitDeleted )
		{
			VERIFY( list.DeleteItem( intIndex ) );
			return;
		}

		if( bInserted || xrpt.IsChanged( COLUMN_FROM ) )
		{
			VERIFY( list.SetItemText( intIndex, COLUMN_FROM, xrpt.m_strFromName ) );
		}

		if( bInserted || xrpt.IsChanged( COLUMN_TO ) )
		{
			VERIFY( list.SetItemText( intIndex, COLUMN_TO , xrpt.m_strToName ) );
		}

		/*
		if( bInserted || xrpt.IsChanged( COLUMN_MBOX) )
		{
			if (xrpt.m_pMailbox->UpdateUnreadStatus())
					xrpt.m_pMailbox->Change( COLUMN_ALIAS );	// need redraw
		}
		*/

		if( bInserted || xrpt.IsChanged( COLUMN_SUBJ ) )
		{
			CString sSubj;
			if (xrpt.m_dwFiltered & MFA_SPAM)
				sSubj = "[SPAM] " + xrpt.m_strSubject;
			else
				sSubj = xrpt.m_strSubject;
			VERIFY( list.SetItemText( intIndex, COLUMN_SUBJ, sSubj ) );
		}

		if( bInserted || xrpt.IsChanged( COLUMN_DATE ) )
		{
			CString strTmp = CTime( 1971, 1, 1, 1, 1, 1 ) < xrpt.m_tmDate ?
				xrpt.m_tmDate.Format( _T("%c") ) : _T("...");
			VERIFY( list.SetItemText( intIndex, COLUMN_DATE, strTmp ) );
		}
		int nImage = -1;
		BOOL bHighPr = (xrpt.m_nPriority>0 && xrpt.m_nPriority<3);
		BOOL bAttach = xrpt.m_strContent.Find("multipart/")>=0;
		if (xrpt.m_bitRemoveFromServer)
			nImage = II_DELETE;
		else if (xrpt.m_bitDownloadData || xrpt.m_bitPreviewData)
			nImage = II_LOAD;
		else if ( !xrpt.WasRead() )
			nImage = II_MAILNEW;
		else if ( xrpt.WasRead() && !xrpt.IsSpam())
			nImage = II_MAILOPEN;
		else if (xrpt.IsSpam())
		{
			if (bHighPr && bAttach)
				nImage = II_ATTACH_PRIO;
			else if (bHighPr)
			{
				nImage = II_PRIORITY;
				bHighPr = 0;
			}
			else if (bAttach)
			{
				nImage = II_ATTACH;
				bAttach = 0;
			}
			else if (xrpt.m_bitProtected)
				nImage = II_SECURE;
		}
		//else if ( xrpt.m_bitDownloaded || xrpt.m_bitPreviewLoaded)
		//	nImage = II_MAILOPEN;
		list.SetItem(intIndex, 0, LVIF_IMAGE, NULL, nImage, 0, 0, 0);

		//
		int nOverlay = 0;
		if (nImage != II_LOAD && nImage != II_DELETE)	// do not mix these icons 
		{
			if (bHighPr)
				nOverlay = 1;
			if (bAttach)
				nOverlay += 2;	// 3 is an index for priority+attach image
			if (xrpt.m_bitProtected)
				nOverlay = 4;
			if (nOverlay)
				nOverlay = INDEXTOOVERLAYMASK( nOverlay );
		}
		list.SetItemState( intIndex, nOverlay, LVIS_OVERLAYMASK );

			
		if( bInserted || xrpt.IsChanged( COLUMN_SIZE ) )
		{
			CString strTmp = _T("...");
			if( xrpt.m_intSize ) strTmp.Format( _T("%d"), xrpt.m_intSize );
			VERIFY( list.SetItemText( intIndex, COLUMN_SIZE, strTmp ) );
		}

		if( bInserted || xrpt.IsChanged( theApp.intEVSortColumn ) ) SortItems(); 
	}
	else if( pHint->IsKindOf( RUNTIME_CLASS( CMailbox ) ) )
	{
		CMailbox &mbox = *(CMailbox*) pHint;

		if( mbox.IsChanged( COLUMN_SELECTED ) )
		{
			for( int i = 0; i < mbox.m_arrayExcerpt.GetSize(); ++i )
			{
				OnUpdate( 0, HINT_UPDATE_ITEM, mbox.m_arrayExcerpt[i] );
			}
		}

		if( mbox.IsChanged( COLUMN_ALIAS ) )
		{
			for( int i = 0; i < list.GetItemCount(); ++i )
			{
				CExcerpt &xrpt = *(CExcerpt*) list.GetItemData(i);
				if( xrpt.m_pMailbox == &mbox ) 
				{
					VERIFY( list.SetItemText( i, COLUMN_MBOX, xrpt.m_pMailbox->m_strAlias ) );
				}
			}
		}
	}
	else ASSERT( 0 );
}

void CExcerptView::OnRclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	UNUSED_ALWAYS(pNMHDR);
	UNUSED_ALWAYS(pResult);
	int nDiff = GetTickCount() - m_dwLastRButtonDown;
	if (m_dwLastRButtonDown && nDiff>250 && nDiff<10000)
	{
		DoAction(theApp.intRB2Action, ACT_HEADER);
	}
	else
		DoAction(theApp.intRBAction, ACT_MENU);
}

void CExcerptView::DoAction(int nAction, int nDefault)
{
	m_TitleTip.Hide();
	switch (nAction)
	{
		case ACT_QVIEW:
			SendMessage( WM_COMMAND, ID_QUICK_VIEW );
			break;
		case ACT_MENU:
		{
			POINT pt = {0};
			VERIFY( GetCursorPos( &pt ) );
			ShowContextMenu( pt );
			break;
		}
		case ACT_HEADER:
			theApp.m_nMsgPropPage = 1;
			SendMessage( WM_COMMAND, ID_EDIT_PROPERTIES );
			break;
		case ACT_PROPS:
			SendMessage( WM_COMMAND, ID_EDIT_PROPERTIES );
			break;
		case ACT_PREVIEW:
			SendMessage( WM_COMMAND, ID_TEXT_PREVIEW );
			break;
		case ACT_ASTEXT:
			SendMessage( WM_COMMAND, ID_QUICKTEXT );
			break;
		default:
			if (nDefault>0)
				DoAction(nDefault, 0);
			break;
	}
}


void CExcerptView::OnDblclk(NMHDR* pNMHDR, LRESULT* pResult) 
{
	UNUSED_ALWAYS(pNMHDR);
	DoAction(theApp.intDblAction, ACT_QVIEW);
	*pResult = 0;
}

void CExcerptView::OnEditProperties() 
{
	CTypedPtrArray < CPtrArray, CExcerpt* > arrayExcerpt;
	CListCtrl &list = GetListCtrl();

	int i = -1;
	while( -1 != ( i = list.GetNextItem( i, LVNI_SELECTED ) ) )
	{
		arrayExcerpt.Add( (CExcerpt*) list.GetItemData(i) );
	}
	
	CHeaderPage pageH; 
	CRawHead pageRaw; 

	pageH.Attach( &arrayExcerpt );
	pageRaw.Attach( &arrayExcerpt );

	CPropertySheet sheet( IDP_EXCERPT_PROPERTIES_TITLE, AfxGetMainWnd() );
	sheet.AddPage( &pageH );
	sheet.AddPage( &pageRaw );
	
	if (theApp.m_nMsgPropPage)
	{
		sheet.m_psh.nStartPage = theApp.m_nMsgPropPage;
	}
	theApp.m_nMsgPropPage = 0;

	sheet.m_psh.dwFlags |= PSH_NOAPPLYNOW;

	sheet.DoModal();
}

void CExcerptView::OnUpdateIfSelectedCount(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable( GetListCtrl().GetSelectedCount() );
}

/*
void CExcerptView::OnUpdateIfItemCount(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable( GetListCtrl().GetItemCount() );
}
*/

void CExcerptView::OnViewRefresh() 
{
	CMagicDoc *doc = (CMagicDoc*) GetDocument();
	if( doc ) doc->Check();
}

void CExcerptView::OnStopChecking() 
{
	CMagicDoc *doc = (CMagicDoc*) GetDocument(); 
	if( doc ) doc->StopChecking();
}

void CExcerptView::OnQuickView()
{
	CListCtrl &list = GetListCtrl();

	for( int i = list.GetItemCount(); i; --i )
	{
		if( LVNI_SELECTED == list.GetItemState( i-1, LVNI_SELECTED ) )
		{
			( (CExcerpt*) list.GetItemData( i-1 ) )->QuickView();
		}
	}
}

void CExcerptView::OnQuickReply()
{
	CListCtrl &list = GetListCtrl();

	CString strRcpt;
	CString strSubject;

	int i = list.GetItemCount();
	while( i-- )
	{
		if( LVNI_SELECTED == list.GetItemState( i, LVNI_SELECTED ) )
		{
			CExcerpt &expt = *( (CExcerpt*) list.GetItemData( i ) );
			if( !strRcpt.IsEmpty() ) strRcpt += _T(",");
			if (expt.m_sReplyTo.IsEmpty())
				strRcpt += expt.m_strFrom;
			else
				strRcpt += expt.m_sReplyTo;
			if( strSubject.IsEmpty() ) strSubject = expt.m_strSubject;
			else
			{
				if( strSubject != expt.m_strSubject ) strSubject = _T("...");
			}
		}
	}

	/* mark subject by "Re: "(-ply) if not already marked */
	if( _tcsnicmp( strSubject, _T("Re: "), 4 ) ) strSubject = _T("Re: ") + strSubject;

	CString strCommand;
	strCommand.Format("mailto:%s?subject=%s", strRcpt, strSubject );

	/* launch mailer now! */
	if( (int) ShellExecute( NULL, _T("open"), strCommand, _T(""), _T(""), SW_NORMAL ) < 32 )
	{
		CString strMessage;
		AfxFormatString1( strMessage, IDP_CANNOT_FOLLOW_THE_LINK_1, strCommand );
		AfxMessageBox( strMessage );
	}
}

void CExcerptView::OnUpdateViewRefresh(CCmdUI* pCmdUI) 
{
	CMagicDoc *doc = (CMagicDoc*) GetDocument(); 
	if( doc ) pCmdUI->Enable( doc->m_aMailboxes.GetSize() );
}

void CExcerptView::OnUpdateStopChecking(CCmdUI* pCmdUI) 
{
	CMagicDoc *doc = (CMagicDoc*) GetDocument(); 
	if( doc ) pCmdUI->Enable( doc->m_aMailboxes.GetSize() );
}

void CExcerptView::OnUpdateIndicatorObjects( CCmdUI *pCmdUI )
{
	CListCtrl &list = GetListCtrl();
    pCmdUI->Enable(); 

	UINT uintSelected = list.GetSelectedCount();
	UINT uintTotal = list.GetItemCount();
	UINT uintReported = uintSelected ? uintSelected : uintTotal;

    CString strTmp1, strTmp2, strObjects;
	LOAD_STRING(strTmp1, (uintReported -1 ? IDP_MESSAGES : IDP_MESSAGE ) );
	if( uintSelected )
	{
		LOAD_STRING(strTmp2, IDP_SELECTED );
	}
    strObjects.Format( _T("%d %s %s"), uintReported, strTmp1, strTmp2 );
    pCmdUI->SetText( strObjects ); 
}

void CExcerptView::OnUpdateIndicatorContents( CCmdUI *pCmdUI )
{
	CListCtrl &list = GetListCtrl();
    pCmdUI->Enable();
	int intCount = 0;
	int nLoaded = 0;

	BOOL bNoneSelected = !list.GetSelectedCount();

	for( int i = list.GetItemCount(); i; --i )
	{
		if( bNoneSelected || list.GetItemState( i-1, LVIS_SELECTED ) ) 
		{
			intCount += ( (CExcerpt*) list.GetItemData( i-1 ) )->m_intSize;
			nLoaded += ( (CExcerpt*) list.GetItemData( i-1 ) )->m_nLoadedSize;
		}
	}
	
	CString strContents;
	BytesToString( intCount, strContents );
	if (nLoaded && nLoaded<intCount)
	{
		CString s;
		s.Format("%s [%d]", strContents, nLoaded);
		strContents = s;
	}
    pCmdUI->SetText( strContents ); 
}


void CExcerptView::OnQuickDelete() 
{
	if( theApp.intEVConfirmDelete &&
		( IDYES != AfxMessageBox( IDS_DELETE_CNF, MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 ) ) ) return;

	CListCtrl &list = GetListCtrl();
	for( int i = list.GetItemCount(); i; --i )
	{
		if( list.GetItemState( i-1, LVIS_SELECTED ) ) 
		{
			CExcerpt &xrpt = *( (CExcerpt*) list.GetItemData( i-1 ) );
			if (xrpt.m_bitProtected)
				continue;
			xrpt.m_bitRemoveFromServer = 1;
			xrpt.LogDeleted(NULL);
			xrpt.Change( COLUMN_MBOX );
			xrpt.m_pMailbox->Check();
		}
	}
}

void CExcerptView::OnKeydown(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_KEYDOWN* pLVKeyDown = (LV_KEYDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	m_TitleTip.Hide();
	*pResult = 0;

	switch (pLVKeyDown->wVKey)
	{
		case VK_APPS:
		{
			CListCtrl &list = GetListCtrl();
			
			int iFocusedItem = list.GetNextItem( -1, LVNI_FOCUSED );

			POINT pt = {0};
			if( iFocusedItem != -1 )
			{
				RECT rc = {0};
				list.GetItemRect( iFocusedItem, &rc, LVIR_BOUNDS );

				pt.x = rc.left;
				pt.y = rc.bottom;
			}

			ClientToScreen( &pt );
			ShowContextMenu( pt );
			break;
		}
		case VK_TAB:
		{
			CMagicFrame *frame = (CMagicFrame*) GetParentFrame();
			if( frame )
				frame->SwitchPane();
			break;
		}
		case VK_F2:
		{
			theApp.m_nMsgPropPage = 1;
			SendMessage( WM_COMMAND, ID_EDIT_PROPERTIES );
			break;
		}
		case VK_RETURN:
		{
			DoAction(theApp.intDblAction, ACT_QVIEW);
			break;
		}
		case VK_SPACE:
		{
			CListCtrl &list = GetListCtrl();
			int nRead = -1;
			for( int i = list.GetItemCount(); i; --i )
			{
				if( LVNI_SELECTED == list.GetItemState( i-1, LVNI_SELECTED ) )
				{
					CExcerpt* pEx = ( (CExcerpt*) list.GetItemData( i-1 ) );
					if (!pEx)
						continue;
					if (nRead == -1)
						nRead = (pEx->m_bitWasRead == 0);
					pEx->MarkAsRead(nRead);
				}
			}
			break;
		}
	}
}

void CExcerptView::ShowContextMenu( const POINT& pt ) 
{
	CMenu menuPopup;
	VERIFY( menuPopup.LoadMenu( IDR_EXCERPT_POPUP_MENU ) );

	// set default state
	UINT nID = ID_QUICK_VIEW;
	if (theApp.intDblAction == ACT_PROPS)
		nID = ID_EDIT_PROPERTIES;
	else if (theApp.intDblAction == ACT_PREVIEW)
		nID = ID_TEXT_PREVIEW;
	else if (theApp.intDblAction == ACT_ASTEXT)
		nID = ID_QUICKTEXT;
	else
		nID = 0;

	if (nID)
	{
		MENUITEMINFO info;
		info.cbSize = sizeof( info );
		info.fMask = MIIM_STATE;
		info.fState = MFS_DEFAULT;
		VERIFY( SetMenuItemInfo( menuPopup.m_hMenu, nID, FALSE, &info ) );
	}

	MenuTranslate(menuPopup.GetSubMenu(0)->m_hMenu);
	menuPopup.GetSubMenu(0)->TrackPopupMenu( 
		TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON, 
		pt.x, pt.y, AfxGetMainWnd(), NULL );
}

void CExcerptView::OnLoad() 
{
	CListCtrl &list = GetListCtrl();

	for( int i = list.GetItemCount(); i; --i )
	{
		if( LVNI_SELECTED == list.GetItemState( i-1, LVNI_SELECTED ) )
		{
			( (CExcerpt*) list.GetItemData( i-1 ) )->QuickLoad();
		}
	}
}


void CExcerptView::OnQuicktext() 
{
	CListCtrl &list = GetListCtrl();

	for( int i = list.GetItemCount(); i; --i )
	{
		if( LVNI_SELECTED == list.GetItemState( i-1, LVNI_SELECTED ) )
		{
			( (CExcerpt*) list.GetItemData( i-1 ) )->QuickText();
		}
	}
}


void CExcerptView::OnTextPreview() 
{
	CListCtrl &list = GetListCtrl();

	for( int i = list.GetItemCount(); i; --i )
	{
		if( LVNI_SELECTED == list.GetItemState( i-1, LVNI_SELECTED ) )
		{
			( (CExcerpt*) list.GetItemData( i-1 ) )->QuickPreviewText();
		}
	}
}


void CExcerptView::OnMouseMove(UINT nFlags, CPoint point) 
{
	BOOL bTipShown = FALSE;
	if( nFlags == 0 )
	{
		CRect rcItem;
		int nCol = -1;
		int nItem = GetItemFromPoint(point, nCol, &rcItem);

		if (nItem>=0)
		{
			CString s = GetListCtrl().GetItemText(nItem, nCol);
			CExcerpt* pEx = ( (CExcerpt*) GetListCtrl().GetItemData( nItem ) );
			LOGFONT* pFont = NULL;
			if (pEx && !pEx->WasRead())
				pFont = &m_lfBold;
			CRect rcWnd;
			GetListCtrl().GetClientRect(rcWnd);
			int nMax = rcWnd.right - rcItem.left;
			m_TitleTip.SetMaxLen(nMax);
			m_TitleTip.Show(rcItem, s, 0, 0, pFont);
			bTipShown = TRUE;
		}
	}
	
	if (!bTipShown)
		m_TitleTip.Hide();

	CExcerptsParent::OnMouseMove(nFlags, point);
}

void CExcerptView::GetItemColors(int nItem, COLORREF& clrText, 
								COLORREF& clrBk, BOOL bSelected)
{
	CExcerptsParent::GetItemColors(nItem, clrText, clrBk, bSelected);

	CExcerpt* pEx = ( (CExcerpt*) GetListCtrl().GetItemData( nItem ) );
	// set color for filtered message
	if (pEx->m_dwFiltered & MFA_COLOR)
	{
		if (!bSelected)
			clrText = pEx->m_Color;
	}
	else if (pEx->m_dwFiltered == MFA_FRIEND && 
		(theApp.m_dwFlags & MMF_COLOR_FRIEND))
	{
		if (!bSelected)
			clrText = theApp.m_clrFriends;
	}
}

void CExcerptView::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (!lpDrawItemStruct)
		return;
	int nItem = lpDrawItemStruct->itemID;
	CDC* pDC=CDC::FromHandle(lpDrawItemStruct->hDC);
	CExcerpt* pEx = ( (CExcerpt*) GetListCtrl().GetItemData( nItem ) );
	// select bold font for unread messages
	if (!m_fntBold.m_hObject)
	{
		CFont* pF = GetFont();
		if (pF)
		{
			pF->GetObject(sizeof(LOGFONT), &m_lfBold);
			m_lfBold.lfWeight = 800;
			m_fntBold.CreateFontIndirect(&m_lfBold);
		}
	}
	CFont* pOldF = NULL;
	if (m_fntBold.m_hObject && pEx && !pEx->WasRead())
	{
		pOldF = pDC->SelectObject(&m_fntBold);
	}

	CExcerptsParent::DrawItem(lpDrawItemStruct);

	if (pOldF)
		pDC->SelectObject(pOldF);
}


void CExcerptView::DrawSubItem(CDC* pDC, int nItem, int nColumn, CRect& rcLabel, int nJustify, BOOL bSelected)
{
	CExcerptsParent::DrawSubItem(pDC, nItem, nColumn, rcLabel, nJustify, bSelected);

	if (nColumn != COLUMN_SIZE)
		return;
	
	CExcerpt* pEx = ( (CExcerpt*) GetListCtrl().GetItemData( nItem ) );
	if (!pEx || pEx->m_nLoadedSize==0 || rcLabel.Width()<16)
		return;

	if (pEx->m_nLoadedSize<(pEx->m_intSize-2))	// sometimes 2 bytes are not downloaded?
	{
		int nPrc = (pEx->m_nLoadedSize*100)/pEx->m_intSize;
		CRect rcInvert(rcLabel);
		rcInvert.right = rcInvert.left + (rcLabel.Width()*nPrc)/100;
		pDC->InvertRect(rcInvert);
	}
	else
	{
		int nImage = II_LOADED;
		if (bSelected)
		{
			if (GetFocus()==this)
				theApp.m_imgMsg.SetBkColor(::GetSysColor(COLOR_HIGHLIGHT));
			else
				theApp.m_imgMsg.SetBkColor(::GetSysColor(COLOR_INACTIVECAPTIONTEXT));
		}
		else
			theApp.m_imgMsg.SetBkColor(m_clrTextBk);
		theApp.m_imgMsg.Draw(pDC, nImage, rcLabel.TopLeft(), ILD_NORMAL);
	}
}

LRESULT CExcerptView::OnLanguage(WPARAM, LPARAM)
{
	CString strColumn;
	
	int nCols = sizeof(aColTitles)/sizeof(aColTitles[0]);
	for (int i=0; i<nCols; i++)
	{
		LOAD_STRING(strColumn, aColTitles[i]);
		SetColTitle(i, strColumn);
	}
	return 0;
}

void CExcerptView::OnAllRead() 
{
	CListCtrl &list = GetListCtrl();

	for( int i = list.GetItemCount(); i; --i )
	{
		( (CExcerpt*) list.GetItemData( i-1 ) )->MarkAsRead(TRUE);
	}

}

void CExcerptView::OnMarkRead() 
{
	DoMarkRead(TRUE);
}

void CExcerptView::OnMarkNew() 
{
	DoMarkRead(FALSE);
}

void CExcerptView::OnMarkSpam() 
{
	DoMarkRead(TRUE, FALSE);
}

void CExcerptView::OnMarkGood() 
{
	DoMarkRead(FALSE, FALSE);
}

void CExcerptView::DoMarkRead(BOOL bRead, BOOL bWhat) 
{
	CListCtrl &list = GetListCtrl();

	for( int i = list.GetItemCount(); i; --i )
	{
		if( LVNI_SELECTED == list.GetItemState( i-1, LVNI_SELECTED ) )
		{
			if (bWhat)
				( (CExcerpt*) list.GetItemData( i-1 ) )->MarkAsRead(bRead);
			else
				( (CExcerpt*) list.GetItemData( i-1 ) )->MarkAsSpam(bRead);
		}
	}
}
void CExcerptView::OnFilters() 
{
	DFilters dlg((CMagicDoc*)GetDocument());
	dlg.DoModal();
}


void CExcerptView::OnFileApplyfilters() 
{
	CListCtrl &list = GetListCtrl();

	for( int i = list.GetItemCount(); i; --i )
	{
		( (CExcerpt*) list.GetItemData( i-1 ) )->RecheckByFilters();
	}
}

void CExcerptView::OnHeaderEndDrag(NMHDR* /*pHeader*/, LRESULT* pResult)
{
	RedrawWindow(NULL, NULL, RDW_INTERNALPAINT | RDW_INVALIDATE);
	*pResult = 0;
}

void CExcerptView::GetSelAddresses(CStringArray& asFrom, int nType)
{
	CListCtrl &list = GetListCtrl();

	for( int i = list.GetItemCount(); i; --i )
	{
		if( LVNI_SELECTED == list.GetItemState( i-1, LVNI_SELECTED ) )
		{
			CString sFull;
			if (nType == IDP_COLUMN_FROM)
			{
				sFull = ( (CExcerpt*) list.GetItemData( i-1 ) )->m_strFrom;
				GetAddressesFrom(sFull, asFrom);
			}
			else if (nType == IDP_COLUMN_TO)
			{
				sFull = ( (CExcerpt*) list.GetItemData( i-1 ) )->m_strToName;
				GetAddressesFrom(sFull, asFrom);
			}
			else if (nType == IDP_COLUMN_SUBJ)
			{
				sFull = ( (CExcerpt*) list.GetItemData( i-1 ) )->m_strSubject;
				asFrom.Add(sFull);
			}
		}
	}
}

void AddItemsToList(CStringArray& asFrom, LPCTSTR sFile, BOOL bDomains)
{
	if (asFrom.GetSize()<=0)
		return;
	CString sPath;
	if (!FindLocalFile(sFile, sPath, TRUE))
		return;
	CStdioFile file;
	if (!file.Open(sPath, CFile::modeWrite|CFile::typeText))
		return;
	file.SeekToEnd();
	CStringArray asAdded;	// to avoid adding copies
	for (int i=0; i<asFrom.GetSize(); i++)
	{
		CString sAdd;
		sAdd = asFrom[i];
		if (bDomains)
		{
			int nAt = sAdd.Find('@');
			if (nAt<0)
				continue;
			sAdd.Format("*%s", asFrom[i].Mid(nAt));
		}
		if (sAdd.IsEmpty())
			continue;
		// may be it's already added
		for (int j=0; j<asAdded.GetSize(); j++)
		{
			if (asAdded[j].CompareNoCase(sAdd) == 0)
			{
				sAdd.Empty();
				break;
			}
		}
		if (sAdd.IsEmpty())
			continue;
		asAdded.Add(sAdd);
		if (asAdded.GetSize()==1)
			file.WriteString("\n");
		file.WriteString(sAdd);
		file.WriteString("\n");
	}
	file.Close();
	ShellExecute(NULL, "Open", sPath, NULL, NULL, SW_SHOW);
}

void CExcerptView::OnAddFriend() 
{
	CStringArray asFrom;
	GetSelAddresses(asFrom);
	AddItemsToList(asFrom, FRIENDS_FILE, FALSE);
}

void CExcerptView::OnDomainAdd() 
{
	CStringArray asFrom;
	GetSelAddresses(asFrom);
	AddItemsToList(asFrom, FRIENDS_FILE, TRUE);
}

void CExcerptView::OnListCommand(UINT nCmd)
{
	int nType = -1;
	int nFile = -1;
	if (nCmd<ID_LIST_MIN + 50)	// To
	{
		nType = IDP_COLUMN_TO;
		nFile = nCmd - ID_LIST_MIN - 1;
	}
	else if (nCmd<ID_LIST_MIN + 100)	// From
	{
		nType = IDP_COLUMN_FROM;
		nFile = nCmd - ID_LIST_MIN - 50;
	}
	else if (nCmd<ID_LIST_MIN + 150)	// Subj
	{
		nType = IDP_COLUMN_SUBJ;
		nFile = nCmd - ID_LIST_MIN - 100;
	}
	if (nType<0 || nFile<0 || nFile >= ((CMagicFrame*)AfxGetMainWnd())->m_asFilterLists.GetSize())
		return;

	AddItemToList(nType, ((CMagicFrame*)AfxGetMainWnd())->m_asFilterLists[nFile]);
}

void CExcerptView::AddItemToList(int nType, CString& sList)
{
	CStringArray asFields;

	GetSelAddresses(asFields, nType);
	AddItemsToList(asFields, sList, FALSE);
}

void CExcerptView::OnKillSpam() 
{
	// delete all spam messages
	CMap<CMailbox*, CMailbox*, int, int> map;
	CListCtrl &list = GetListCtrl();
	for( int i = list.GetItemCount(); i; --i )
	{				   
		CExcerpt &xrpt = *( (CExcerpt*) list.GetItemData( i-1 ) );
		if (xrpt.m_dwFiltered & MFA_SPAM)
		{
			if (xrpt.m_bitProtected)
				continue;
			xrpt.m_bitRemoveFromServer = 1;
			xrpt.LogDeleted(NULL);
			xrpt.Change( COLUMN_MBOX );
			map.SetAt(xrpt.m_pMailbox, 1);
		}
	}
	POSITION pos = map.GetStartPosition();
	while( pos != NULL )
	{
		CMailbox* pBox;
		int n;
		map.GetNextAssoc( pos, pBox, n);
		pBox->Check();
	}
}

void CExcerptView::OnUpdateKillSpam(CCmdUI* ) 
{
}

BOOL CExcerptView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	m_TitleTip.Hide();
	return CExcerptsParent::OnMouseWheel(nFlags, zDelta, pt);
}

void CExcerptView::OnToggleSpam() 
{
	UINT nCmd = ID_MARK_NOSPAM;
	if (!IsSpamSelection())
		nCmd = ID_MARK_SPAM;
	PostMessage(WM_COMMAND, nCmd);
}

BOOL CExcerptView::IsSpamSelection()
{
	CListCtrl &list = GetListCtrl();
	BOOL bAnySelected = FALSE;
	for( int i = list.GetItemCount(); i; --i )
	{
		if( LVNI_SELECTED == list.GetItemState( i-1, LVNI_SELECTED ) )
		{
			if ((( (CExcerpt*) list.GetItemData( i-1 ) )->m_dwFiltered & MFA_SPAM) == 0)
			{
				return FALSE;
			}
			bAnySelected = TRUE;
		}
	}
	return bAnySelected;
}
void CExcerptView::OnUpdateToggleSpam(CCmdUI* pCmdUI) 
{
	CListCtrl &list = GetListCtrl();
	int nMax = list.GetItemCount();
	if (nMax == 0)
	{
		pCmdUI->SetCheck(FALSE);
		pCmdUI->Enable(FALSE);
		return;
	}
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(IsSpamSelection());
}

void CExcerptView::OnSelRevert() 
{
	CListCtrl &list = GetListCtrl();
	for( int i = list.GetItemCount()-1; i>=0; --i )
	{
		if( LVNI_SELECTED == list.GetItemState( i, LVNI_SELECTED ) )
		{
			list.SetItemState(i, 0, LVNI_SELECTED );
		}
		else
			list.SetItemState(i, LVNI_SELECTED , LVNI_SELECTED );
	}
}

void CExcerptView::OnSelNotFriends() 
{
	CListCtrl &list = GetListCtrl();
	for( int i = 0; i < list.GetItemCount(); ++i )
	{
		CExcerpt &xrpt = *(CExcerpt*) list.GetItemData(i);
		if( xrpt.m_dwFiltered == MFA_FRIEND) 
			list.SetItemState(i, 0, LVNI_SELECTED );
		else
			list.SetItemState(i, LVNI_SELECTED, LVNI_SELECTED );
	}
}

void CExcerptView::DoProtect(BOOL bProtect)
{
	CListCtrl &list = GetListCtrl();

	for( int i = list.GetItemCount(); i; --i )
	{
		if( LVNI_SELECTED == list.GetItemState( i-1, LVNI_SELECTED ) )
		{
			CExcerpt &xrpt = *( (CExcerpt*) list.GetItemData( i-1 ) );
			xrpt.m_bitProtected = bProtect;
			xrpt.Change( COLUMN_MBOX );
		}
	}
}
void CExcerptView::OnProtect() 
{
	DoProtect(TRUE);
}

void CExcerptView::OnUpdateProtect(CCmdUI* ) 
{
	// TODO: Add your command update UI handler code here
	
}

void CExcerptView::OnUnprotect() 
{
	DoProtect(FALSE);
}

void CExcerptView::OnUpdateUnprotect(CCmdUI* ) 
{
	// TODO: Add your command update UI handler code here
	
}

BOOL CExcerptView::PreTranslateMessage(MSG* pMsg) 
{
	switch (pMsg->message)
	{
		case WM_KEYDOWN:
			{
				if (pMsg->wParam == 'S' || pMsg->wParam == 's')
				{
					PostMessage( WM_COMMAND, ID_TOGGLE_SPAM );
					return TRUE;
				}
			}
		default:
			break;
	}
	return CExcerptsParent::PreTranslateMessage(pMsg);
}
