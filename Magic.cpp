// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// Magic.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Magic.h"
#include "Excerpt.h"
#include "Mailbox.h"
#include "MagicFrame.h"
#include "MagicDoc.h"
#include "MailboxView.h"
#include "ExcerptView.h"
#include "getwinver.h"
#include "htmlhelp.h"
#include "afxpriv.h"
#include "enbitmap.h"
#include "about.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define GET_INT(a)	(a) = theApp.GetProfileInt( _T("settings"), _T(#a), a );
#define GET_STR(a)	(a) = theApp.GetProfileString( _T("settings"), _T(#a), a );
#define GET_BOOL(a)	(a) = ( 0 != theApp.GetProfileInt( _T("settings"), _T(#a), a ) );

#define SET_INT(a)	VERIFY( theApp.WriteProfileInt( _T("settings"), _T(#a), (a) ) );
#define SET_STR(a)	VERIFY( theApp.WriteProfileString( _T("settings"), _T(#a), (a) ) );
#define SET_BOOL(a)	VERIFY( theApp.WriteProfileInt( _T("settings"), _T(#a), (a) ) );

const TCHAR	*cstrMultipleSelection = _T("...");

/////////////////////////////////////////////////////////////////////////////
// CMagicApp

BEGIN_MESSAGE_MAP(CMagicApp, CWinApp)
	//{{AFX_MSG_MAP(CMagicApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_EXPORT, OnExport)
	ON_COMMAND(ID_IMPORT, OnImport)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMagicApp construction

CMagicApp::CMagicApp()
:
	m_hCmdID( 0 ),
	m_uMciID( 0 ),

	intMVMode( LVS_REPORT ),
//	intEVMode( LVS_REPORT ),
	intMVSortColumn( COLUMN_ALIAS ),
	intMVSortAscend( TRUE ),
	intEVSortColumn( COLUMN_FROM ),
	intEVSortAscend( TRUE ),
	intSplitterPos( 100 ),
	intMCAliasWidth( 120 ),
	intMCAliasPos( COLUMN_ALIAS ),
	intMCUserWidth( 60 ),
	intMCUserPos( COLUMN_USER ),
	intMCHostWidth( 120 ),
	intMCHostPos( COLUMN_HOST ),
	intMCMailWidth( 60 ),
	intMCMailPos( COLUMN_MAIL ),
	intMCStatWidth( 120 ),
	intMCStatPos( COLUMN_STATE ),
	intMCElapsedWidth( 60 ),
	intMCElapsedPos( COLUMN_ELAPSED ),
	intMCPortWidth( 60 ),
	intMCPortPos( COLUMN_PORT ),
	intMCPollWidth( 60 ),
	intMCPollPos( COLUMN_POLL ),
	intECMBoxWidth( 120 ),
	intECMBoxPos( COLUMN_MBOX ),
	intECFromWidth( 180 ),
	intECFromPos( COLUMN_FROM ),
	intECToWidth( 180 ),
	intECToPos( COLUMN_TO ),
	intECSubjWidth( 120 ),
	intECSubjPos( COLUMN_SUBJ ),
	intECDateWidth( 120 ),
	intECDatePos( COLUMN_DATE ),
	intECSizeWidth( 60 ),
	intECSizePos( COLUMN_SIZE ),
	rcWnd( 0, 0, 0, 0 ),
	intPlayback( ACTION_NONE ),
	intPlaybackDevice( PLAYBACK_DEVICE_FILE ),
	intCommand( ACTION_NONE ),
	intCommandRun( COMMAND_RUN_NORMAL ),

	intStartAlwaysHidden( FALSE ),
	intCheckImmediately( TRUE ),
	intEVConfirmDelete( TRUE ),
	intPopUpMainWindow( FALSE ),
	intPreviewSize(5),
	intEnableFilters(0),
	intMarkRead(READ_VIEW),
	strFileExtensionForMessages( _T("txt") ), 
	m_nWindowsVersion(WUNKNOWN),
	
	intDblAction( ACT_QVIEW ),
	intRBAction( ACT_MENU ),
	intRB2Action( ACT_HEADER ),
#ifdef USE_SSL
	m_SSL(TRUE),
#endif

	bIsSuspended( false )
{
	m_nMsgPropPage = 0;
	m_dwFlags = MMF_DEFAULT;
	m_nMaxLogSize = 50;
	m_clrFriends = RGB(0, 255, 0);
	m_bUseIni = FALSE;
	m_bExited = FALSE;
	m_strTemp = "%TEMP%";
	m_nEncoding = CP_ACP;
	m_nMassiveBoxes = 2;
	m_nMassivePeriod = 1;
	SetHelpMode(afxHTMLHelp);
}

CMagicApp::~CMagicApp()
{
	// this should fix not saving settings on Windows shutdown
	// when ExitInstance is not called
#ifdef USE_SSL
#endif

	if (!m_bExited)
		SaveSettings(NULL);
}
struct _CharSetCP
{
	int nCharSet;
	int nCodePage;
} aCS[]=
{
	{ANSI_CHARSET, CP_ACP},
	{DEFAULT_CHARSET, CP_ACP},
	{SYMBOL_CHARSET, CP_ACP},
	{GB2312_CHARSET, 936},
	{CHINESEBIG5_CHARSET, 938},
	{OEM_CHARSET, 1},
	{HEBREW_CHARSET, 1255},
	{ARABIC_CHARSET, 1256},
	{GREEK_CHARSET, 1253},
	{VIETNAMESE_CHARSET, 1258},
	{THAI_CHARSET, 874},
	{EASTEUROPE_CHARSET, 1252},
	{RUSSIAN_CHARSET, 1251},
	{BALTIC_CHARSET, 1257},
	{MAC_CHARSET, 2},
};

int CMagicApp::GetEncoding()
{
	if (m_nEncoding != -1)
		return m_nEncoding;
	for (int i=0; i<_countof(aCS); i++)
	{
		if (aCS[i].nCharSet == m_lfMain.lfCharSet)
			return aCS[i].nCodePage;
	}
	return CP_ACP;
}
/////////////////////////////////////////////////////////////////////////////
// The one and only CMagicApp object
CMagicApp theApp;

class CStorageHelper
{
public:
	CStorageHelper(CWinApp* p, LPCTSTR sFile)
	{
		if (!sFile || !p)
		{
			pApp = NULL;
			return;
		}
		pApp = p;
		szRegistry = p->m_pszRegistryKey;
		p->m_pszRegistryKey = NULL;
		szApp = p->m_pszProfileName;
		p->m_pszProfileName = sFile;
	};
	~CStorageHelper()
	{
		if (!pApp)
			return;
		pApp->m_pszRegistryKey = szRegistry;
		pApp->m_pszProfileName = szApp;
	};
protected:
	CWinApp* pApp;
	LPCTSTR	 szRegistry;
	LPCTSTR	 szApp;
};

/////////////////////////////////////////////////////////////////////////////
// CMagicApp initialization
void CMagicApp::LoadSettings(LPCTSTR sFile)
{
	CStorageHelper Helper(this, sFile);

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	GET_INT( intMVMode );
	if (intMVMode == LVS_ICON)
		intMVMode = LVS_REPORT;
//	GET_INT( intEVMode );
	GET_INT( intMVSortColumn );
	GET_INT( intMVSortAscend );
	GET_INT( intEVSortColumn );
	GET_INT( intEVSortAscend );
	GET_INT( intSplitterPos );
	GET_INT( intMCAliasWidth );
	GET_INT( intMCAliasPos );
	GET_INT( intMCUserWidth );
	GET_INT( intMCUserPos );
	GET_INT( intMCHostWidth );
	GET_INT( intMCHostPos );
	GET_INT( intMCMailWidth );
	GET_INT( intMCMailPos );
	GET_INT( intMCStatWidth );
	GET_INT( intMCStatPos );
	GET_INT( intMCElapsedWidth );
	GET_INT( intMCElapsedPos );
	GET_INT( intMCPortWidth );
	GET_INT( intMCPortPos );
	GET_INT( intMCPollWidth );
	GET_INT( intMCPollPos );
	GET_INT( intECMBoxWidth );
	GET_INT( intECMBoxPos );
	GET_INT( intECFromWidth );
	GET_INT( intECFromPos );
	GET_INT( intECToWidth );
	GET_INT( intECToPos );
	GET_INT( intECSubjWidth );
	GET_INT( intECSubjPos );
	GET_INT( intECDateWidth );
	GET_INT( intECDatePos );
	GET_INT( intECSizeWidth );
	GET_INT( intECSizePos );
	GET_INT( rcWnd.left ); 
	GET_INT( rcWnd.top );	
	GET_INT( rcWnd.right ); 
	GET_INT( rcWnd.bottom );
	GET_INT( intPlayback );
	GET_STR( strPlayback );
	GET_INT( intPlaybackDevice );
	GET_INT( intCommand );
	GET_STR( strCommand );
	GET_INT( intCommandRun );		 
	GET_INT( intStartAlwaysHidden );
	GET_INT( intCheckImmediately );
	GET_INT( intEVConfirmDelete );
	GET_INT( intPopUpMainWindow );
	GET_INT( intPreviewSize);
	GET_INT( intEnableFilters );
	GET_INT( intMarkRead);
	GET_STR( strFileExtensionForMessages );
	GET_STR( strApp );
	GET_STR( strLastDiction );
	GET_BOOL( bIsSuspended );
	GET_INT(m_dwFlags);
	GET_INT (m_nMaxLogSize);
	GET_INT(m_clrFriends);
	GET_INT(intDblAction);
	GET_INT(intRBAction);
	GET_INT(intRB2Action);
	GET_STR(m_strTemp);
	GET_INT(m_nEncoding);
	GET_INT (m_nMassivePeriod);
	GET_INT (m_nMassiveBoxes);

	if (m_dwFlags & MMF_FONT)
	{
		CString sMainFont;
		GET_STR( sMainFont );
		LF2String(m_lfMain, sMainFont, FALSE);
	}
}

BOOL CMagicApp::InitInstance()
{
	if (!AfxSocketInit())
	{
		AfxMessageBox( IDP_SOCKETS_INIT_FAILED );
		return FALSE;
	}

	CString sVer;
	GetWinVer(sVer, &m_nWindowsVersion);

	CString sCfg;
	if (FindLocalFile(_T("magic.ini"), sCfg, FALSE))
	{
		if (GetPrivateProfileInt(_T("version"), _T("version"), 0, sCfg) >0 ||
			GetPrivateProfileInt(_T("settings"), _T("m_dwFlags"), -1, sCfg) >=0)	// data was saved here
			m_bUseIni = TRUE;	// load from ini
		// else load from registry and switch to ini afterwards
	}
	else
		sCfg.Empty();

	if (!m_bUseIni)
	{
		SetRegistryKey( _T("Orient Lab") );
	}
	else
	{
		if (m_pszProfileName)
			free((void*)m_pszProfileName);
		m_pszProfileName=_tcsdup(sCfg);

	}
	LoadSettings(NULL);

	// check single instance condition
	if (!m_Single.AmIFirst(_T("MagicMailMonitor3")))
	{
		if (m_dwFlags & MMF_SINGLE)
		{
			m_Single.SendData(1);	// WM_COPYDATA with dwData = 1
			return FALSE;
		}
	}

	AddDocTemplate( new CSingleDocTemplate( 
		IDR_MAINFRAME,
		RUNTIME_CLASS(CMagicDoc),
		RUNTIME_CLASS(CMagicFrame),       // main SDI frame window
		RUNTIME_CLASS(CMailboxView)));
		
	// Enable DDE Execute open
	EnableShellOpen();
	CoInitialize(NULL);
	RegisterShellFileTypes(FALSE);	// FALSE will disable print and ShellNew

	// load last dictionary
	if (strLastDiction.GetLength())
		GetDictionary()->SetDictionary(strLastDiction, TRUE);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	PrepareImages();

	if( intStartAlwaysHidden ) 
		m_nCmdShow = SW_HIDE;

	CString strFile; 
	strFile = GetProfileString( _T("Recent File List"), _T("File1"), strFile );

	m_bReset = FALSE;
	// Dispatch commands specified on the command line
	if( CCommandLineInfo::FileNew != cmdInfo.m_nShellCommand ||
		strFile.IsEmpty() ||
		!OpenDocumentFile( strFile ) )
	{
	 	if( !ProcessShellCommand( cmdInfo ) ) return FALSE;
	}
	if (m_bReset)
		m_pMainWnd->PostMessage(WM_COMMAND, ID_FILE_NEW);

	// Enable drag/drop open
	m_pMainWnd->DragAcceptFiles();
	
	m_Single.Init(m_pMainWnd->m_hWnd);

	if (!m_bUseIni && !sCfg.IsEmpty())
	{
		if (m_pszRegistryKey)
			free((void*)m_pszRegistryKey);
		m_pszRegistryKey = NULL;
		if (m_pszProfileName)
			free((void*)m_pszProfileName);
		m_pszProfileName=_tcsdup(sCfg);
		m_bUseIni = TRUE;
	}

	return TRUE;
}

// App command to run the dialog
void CMagicApp::OnAppAbout()
{
	CAboutDlg dlg;
	dlg.DoModal();
}

void InitImageList(CImageList& img)
{
	CEnBitmap	bmp;
	CWindowDC SDC(NULL);  // screen device context
	int nRasterCaps= SDC.GetDeviceCaps(BITSPIXEL);
	BOOL bExtFile = FALSE;
	int nColors = 8;
	HANDLE hBitmap = NULL;
	if (nRasterCaps>=16)
	{
		CString sBmpFile;
		// try loading another picture
		bExtFile = FindLocalFile("mmm3small.bmp", sBmpFile, FALSE);
		if (bExtFile)
		{
			hBitmap = ::LoadImage(NULL, sBmpFile, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			if (!hBitmap)
				bExtFile = FALSE;
			BITMAP bm;
			int nObj = CEnBitmap::GetFileData(sBmpFile, &bm);
			if (!nObj)
				bExtFile = FALSE;
			else 
				nColors = bm.bmBitsPixel;
		}
	}
	if (hBitmap && (nColors<32 || theApp.m_nWindowsVersion >= W2K))
	{
		bmp.Attach(hBitmap);
	}
	else
	{
		/*if (theApp.m_nWindowsVersion < WNTFIRST)
			bmp.LoadBitmap(IDB_ICONS_98);
		else*/
			bmp.LoadBitmap(IDB_MM_ICONS);
			nColors = 24;
	}
	if (nColors == 32)
	{
		img.Create(16, 16, ILC_COLOR32|ILC_MASK, 1, 1);
		if (bmp.ApplyAlpha(GetSysColor(COLOR_WINDOW)))
			img.Add(&bmp, (CBitmap *)NULL);
		else
			img.Add(&bmp, GetSysColor(COLOR_WINDOW));
	}
	else
	{
		img.Create(16, 16, ILC_COLOR24|ILC_MASK, 1, 1);
		img.Add(&bmp, RGB(255, 0, 255));
	}
	if (img.GetImageCount()<=II_FOLDER)
		img.Add(AfxGetApp()->LoadIcon(IDI_FOLDER));
}
void CMagicApp::PrepareImages()
{
	InitImageList(m_imgMailbox);
	InitImageList(m_imgMsg);

	VERIFY( m_imgMailbox.SetOverlayImage( II_FAIL, 1 ) );
	VERIFY( m_imgMailbox.SetOverlayImage( II_DISABLE, 2 ) );
	VERIFY( m_imgMailbox.SetOverlayImage( II_SECURE, 3 ) );
	
	VERIFY( m_imgMsg.SetOverlayImage( II_PRIORITY, 1 ) );
	VERIFY( m_imgMsg.SetOverlayImage( II_ATTACH, 2 ) );
	VERIFY( m_imgMsg.SetOverlayImage( II_ATTACH_PRIO, 3 ) );
	VERIFY( m_imgMsg.SetOverlayImage( II_SECURE, 4 ) );
}

/////////////////////////////////////////////////////////////////////////////
// CMagicApp commands

void CMagicApp::SaveSettings(LPCTSTR sFile)
{
	CStorageHelper Helper(this, sFile);

	if (m_bUseIni || sFile)	// mark a fact of using ini file
	{
		theApp.WriteProfileInt( _T("version"), _T("version"), 1 );
	}

	SET_INT( intMVMode );
//	SET_INT( intEVMode );
	SET_INT( intMVSortColumn );
	SET_INT( intMVSortAscend );
	SET_INT( intEVSortColumn );
	SET_INT( intEVSortAscend );
	SET_INT( intSplitterPos );
	SET_INT( intMCAliasWidth );
	SET_INT( intMCAliasPos );
	SET_INT( intMCUserWidth );
	SET_INT( intMCUserPos );
	SET_INT( intMCHostWidth );
	SET_INT( intMCHostPos );
	SET_INT( intMCMailWidth );
	SET_INT( intMCMailPos );
	SET_INT( intMCStatWidth );
	SET_INT( intMCStatPos );
	SET_INT( intMCElapsedWidth );
	SET_INT( intMCElapsedPos );
	SET_INT( intMCPortWidth );
	SET_INT( intMCPortPos );
	SET_INT( intMCPollWidth );
	SET_INT( intMCPollPos );
	SET_INT( intECMBoxWidth );
	SET_INT( intECMBoxPos );
	SET_INT( intECFromWidth );
	SET_INT( intECFromPos );
	SET_INT( intECToWidth );
	SET_INT( intECToPos );
	SET_INT( intECSubjWidth );
	SET_INT( intECSubjPos );
	SET_INT( intECDateWidth );
	SET_INT( intECDatePos );
	SET_INT( intECSizeWidth );
	SET_INT( intECSizePos );
	SET_INT( rcWnd.left ); 
	SET_INT( rcWnd.top );	
	SET_INT( rcWnd.right ); 
	SET_INT( rcWnd.bottom ); 
	SET_INT( intPlayback );
	SET_STR( strPlayback );
	SET_INT( intPlaybackDevice );
	SET_INT( intCommand );
	SET_STR( strCommand );
	SET_INT( intCommandRun );
	SET_INT( intStartAlwaysHidden );
	SET_INT( intCheckImmediately );
	SET_INT( intEVConfirmDelete );
	SET_INT( intPopUpMainWindow );
	SET_INT( intPreviewSize);
	SET_INT( intEnableFilters );
	SET_INT( intMarkRead);
	SET_STR( strFileExtensionForMessages );
	SET_STR( strApp );
	SET_STR( strLastDiction );
	SET_BOOL( bIsSuspended );
	SET_INT( m_dwFlags );
	SET_INT( m_nMaxLogSize );
	SET_INT(m_clrFriends);
	SET_INT(intDblAction);
	SET_INT(intRBAction);
	SET_INT(intRB2Action);
	SET_STR(m_strTemp);
	SET_INT(m_nEncoding);
	SET_INT (m_nMassivePeriod);
	SET_INT (m_nMassiveBoxes);

	if (m_dwFlags & MMF_FONT)
	{
		CString sMainFont;
		LF2String(m_lfMain, sMainFont, TRUE);
		SET_STR( sMainFont );
	}
}

BOOL CMagicApp::OnIdle(LONG lCount) 
{
	if( CWinApp::OnIdle(lCount) ) return TRUE;

	if( m_pMainWnd ) m_pMainWnd->SendMessage( VM_UPDATEITEM );

	return FALSE;
}

void BytesToString( int intSize, CString &strSize )
{
	MAKE_STRING(strBytes, IDP_BYTES );
	MAKE_STRING(strKiloBytes, IDP_KILOBYTES );
	MAKE_STRING(strMegaBytes, IDP_MEGABYTES );

	if( 1048576 <= intSize ) 
		strSize.Format
		( 
			_T("%.1f%s (%d %s)"), 
			float(intSize)/1048576., 
			strMegaBytes,
			intSize,
			strBytes 
		);
	else if( 1024 <= intSize ) 
		strSize.Format
		( 
			_T("%.1f%s (%d %s)"), 
			float(intSize)/1024., 
			strKiloBytes,
			intSize,
			strBytes 
		);
	else 
		strSize.Format
		( 
			_T("%d %s"), 
			intSize,
			strBytes
		);
}

int CMagicApp::ExitInstance() 
{
	m_bExited = TRUE;
	SaveSettings(NULL);
	
	
	// default implementation calls SaveStdProfileSettings(), which always updates ini
	// need to set m_pCmdInfo->m_nShellCommand == CCommandLineInfo::AppUnregister
	// to avoid this if list is same
	
	return CWinApp::ExitInstance();
}


HICON CMagicApp::LoadIcon( UINT nIDResource, bool Large ) const
{
	int size = Large ? 32 : 16;
	return (HICON) ::LoadImage
	(
		m_hInstance, 
		MAKEINTRESOURCE( nIDResource ), 
		IMAGE_ICON, 
		size, size, 
		LR_CREATEDIBSECTION | LR_SHARED
	);
}

HBITMAP CMagicApp::LoadBitmap( UINT nIDResource ) const
{
	return (HBITMAP) ::LoadImage
	(
		m_hInstance, 
		MAKEINTRESOURCE( nIDResource ), 
		IMAGE_BITMAP, 
		0, 0, 
		LR_CREATEDIBSECTION | LR_SHARED
	);
}

int CMagicApp::DoMessageBox(LPCTSTR lpszPrompt, UINT nType, UINT nIDPrompt) 
{
	CString s;
	if (lpszPrompt && _tcsstr(lpszPrompt, _T("$") _T("{")))
	{
		s = lpszPrompt;
		StrTranslate(s);
		if (s.GetLength())
			lpszPrompt = s;
	}
	return CWinApp::DoMessageBox(lpszPrompt, nType, nIDPrompt);
}

void CMagicApp::GenDefaultFilters()
{
	m_Filters.SetSize(1);
	CMailFilter filter;
	filter.m_sName		= "Blacklist";
	filter.m_aCnd[0].m_nField		= MFF_FROM;
	filter.m_aCnd[0].m_nOperation = MFO_EQUAL;
	filter.m_aCnd[0].m_sText		= "$blacklst.txt";	// get from file
	filter.m_nCombination = MFC_NONE;
	filter.m_dwAction	= MFA_SPAM;
	filter.m_sMailBox	= "*";				// for all mailboxes
	m_Filters.SetAt(0, filter);
}

LPCTSTR GetHelpFilePath()
{
	return theApp.GetHelpFilePath();
}

void CMagicApp::WinHelp(DWORD dwData, UINT nCmd) 
{
#ifdef USE_HTML_HELP
	if (dwData>HID_BASE_RESOURCE && nCmd == 1)
	{
		::HtmlHelp(AfxGetMainWnd()->m_hWnd, GetHelpFilePath(), 
			HH_HELP_CONTEXT, dwData/*-HID_BASE_RESOURCE*/);
	}
	else if (nCmd == 3)
		::HtmlHelp(AfxGetMainWnd()->m_hWnd, GetHelpFilePath(), HH_DISPLAY_TOC, 0);
	/*
	else if (dwData>HID_BASE_COMMAND && nCmd == 1)
		HtmlHelp(AfxGetMainWnd()->m_hWnd, GetHelpFilePath(), 
			HH_HELP_CONTEXT, dwData-HID_BASE_RESOURCE);
			*/
#else
	CWinApp::WinHelp(dwData, nCmd);
#endif
}

LPCTSTR CMagicApp::GetHelpFilePath()
{
	if (m_sHelpPath.IsEmpty())
	{
		CString s;
		GetModuleFileName(NULL, s.GetBuffer(MAX_PATH+2), MAX_PATH);
		s.ReleaseBuffer();
		TCHAR szDrive[_MAX_DRIVE+1];
		TCHAR szPath[_MAX_PATH+1];
		TCHAR szName[_MAX_PATH+1];
		_tsplitpath_s(s, szDrive, _countof(szDrive), 
			szPath, _countof(szPath), szName, _countof(szName), NULL, 0);
		s.Format(_T("%s%s%s.chm"), szDrive, szPath, szName);
		if (GetFileAttributes(s) != (DWORD)-1)
		{
			m_sHelpPath = s;
		}
		else
		{
			CFileFind find;
			if (find.FindFile(_T("magic.chm")))
			{
				find.FindNextFile();
				m_sHelpPath = find.GetFilePath();
			}
		}
	}
	return m_sHelpPath;
}
void	CMagicApp::SetFont(LOGFONT& lf)
{
	memcpy(&m_lfMain, &lf, sizeof(LOGFONT));
	m_fntMain.DeleteObject();
}
void	CMagicApp::SetDefFont(CFont* pF)
{
	LOGFONT lf;
	pF->GetObject(sizeof(LOGFONT), &lf);
	memcpy(&m_lfDef, &lf, sizeof(LOGFONT));
}
CFont*	CMagicApp::GetFont()
{
	LOGFONT* pLF = &m_lfMain;
	if ((m_dwFlags & MMF_FONT)==0)
		pLF = &m_lfDef;
	if (m_fntMain.m_hObject)
		return &m_fntMain;
	// if not set - do not use
	if (_tcslen(pLF->lfFaceName)<3)
		return FALSE;
	if (!m_fntMain.CreateFontIndirect(pLF))
		return NULL;
	return &m_fntMain;
}

static TCHAR BASED_CODE szFilter[] = _T( "Configuration files (*.ini)|*.ini|All Files (*.*)|*.*||");
void CMagicApp::OnExport() 
{
	CFileDialog dlg(FALSE, _T(".ini"), _T("magic.ini"), 
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST,
		szFilter);
	if (dlg.DoModal()!=IDOK)
		return;
	CString s = dlg.GetPathName();
	SaveSettings(s);
}

void CMagicApp::OnImport() 
{
	CFileDialog dlg(TRUE, _T(".ini"), _T("magic.ini"), 
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT|OFN_FILEMUSTEXIST,
		szFilter);
	if (dlg.DoModal()!=IDOK)
		return;
	CString s = dlg.GetPathName();
	LoadSettings(s);
}
