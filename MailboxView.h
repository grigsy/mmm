// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef MAILBOX_VIEW_INCLUDED
#define MAILBOX_VIEW_INCLUDED

#include "ListViewEx.h"

class CMagicDoc;

class CMailboxView : public CListViewEx
{
	unsigned m_bitSortEnabled : 1;
	unsigned m_bitSortRequired : 1;

	CMailboxView();
	DECLARE_DYNCREATE( CMailboxView )

public:
	void EnableSort( BOOL = TRUE );
	void SortItems();
	CImageList m_imageListSmall;
	CImageList m_imageListNormal;
	CMagicDoc* GetDocument();
	static int CALLBACK CompareSubItems( LPARAM, LPARAM, LPARAM );

	//{{AFX_VIRTUAL(CMailboxView)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnInitialUpdate();
	protected:
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

public:
	virtual ~CMailboxView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	int SelectFolder();
	int FindSelectedMailboxes(CArray<CMailbox*>& ar) ;
	void OnOptionsChange(BOOL bWidth);
	virtual void GetCustomRect(int nItem,CRect& rcItem, int LVIR_x);

	//{{AFX_MSG(CMailboxView)
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu,UINT nIndex,BOOL bSysMenu );
	afx_msg void OnViewIcon();
	afx_msg void OnViewList();
	afx_msg void OnViewReport();
	afx_msg void OnViewSmallicon();
	afx_msg void OnUpdateViewIcon(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewList(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewReport(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewSmallicon(CCmdUI* pCmdUI);
	afx_msg void OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEditSelectAll();
	afx_msg void OnUpdateEditSelectAll(CCmdUI* pCmdUI);
	afx_msg void OnItemChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEditDelete();
	afx_msg void OnUpdateIfSelectedCount(CCmdUI* pCmdUI);
	afx_msg void OnEditProperties();
	afx_msg void OnRename();
	afx_msg void OnMarkRead();
	afx_msg void OnMarkUnread();
	afx_msg void OnEditNew();
	afx_msg void OnViewRefresh();
	afx_msg void OnUpdateIfItemCount(CCmdUI* pCmdUI);
	afx_msg void OnUpdateIfFocusedCount(CCmdUI* pCmdUI);
	afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCommand();
	afx_msg void OnDestroy();
	afx_msg void OnStopChecking();
	afx_msg void OnUpdateStopChecking(CCmdUI* pCmdUI);
	afx_msg void OnKeydown(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnFilters();
	afx_msg void OnMailboxCopy();
	afx_msg void OnUpdateMailboxCopy(CCmdUI* pCmdUI);
	afx_msg void OnMailboxPaste();
	afx_msg void OnUpdateMailboxPaste(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg void OnUpdateIndicatorObjects( CCmdUI *pCmdUI );
	afx_msg void OnUpdateIndicatorContents( CCmdUI *pCmdUI );
	afx_msg LRESULT OnLanguage(WPARAM, LPARAM);
	afx_msg void OnHeaderEndDrag(NMHDR* pHeader, LRESULT* pResult);
	afx_msg void OnSelectFolder(UINT nID);
	afx_msg void OnRemoveFromFolder();
	DECLARE_MESSAGE_MAP()
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);

private:
	CHeaderColSortMark	m_ColSortGUI;
	void ShowContextMenu( const POINT& pt );
	void GetListSettings(void);
};

#ifndef _DEBUG  // debug version in MailboxView.cpp
inline CMagicDoc* CMailboxView::GetDocument()
   { return (CMagicDoc*)m_pDocument; }
#endif

#endif
