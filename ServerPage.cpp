// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// ServerPage.cpp : implementation file
//

#include "stdafx.h"
#include "magic.h"
#include "ServerPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CServerPage property page

IMPLEMENT_DYNCREATE(CServerPage, CPropertyPage)

CServerPage::CServerPage() : CPropertyPage(CServerPage::IDD)
{
	//{{AFX_DATA_INIT(CServerPage)
	m_strAPOPSupported = _T("");
	m_strUIDLSupported = _T("");
	m_strIPAddress = _T("");
	m_strGreeting = _T("");
	m_strBurstWritesSupported = _T("");
	//}}AFX_DATA_INIT
}

CServerPage::~CServerPage()
{
}

void CServerPage::Attach( CArray<CMailbox*> *pMailboxArray )
{
	ASSERT( pMailboxArray );
	m_pMailboxArray = pMailboxArray;
}

void CServerPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CServerPage)
	DDX_Text(pDX, IDC_EDIT_APOP_SUPPORTED, m_strAPOPSupported);
	DDX_Text(pDX, IDC_EDIT_UIDL_SUPPORTED, m_strUIDLSupported);
	DDX_Text(pDX, IDC_EDIT_IP_ADDRESS, m_strIPAddress);
	DDX_Text(pDX, IDC_EDIT_GREETING, m_strGreeting);
	DDX_Text(pDX, IDC_EDIT_BURST_WRITES_SUPPORTED, m_strBurstWritesSupported);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CServerPage, CPropertyPage)
	//{{AFX_MSG_MAP(CServerPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static
CString TristateToString( const Tristate& t )
{
	CString strResult;

	if( t.IsSet() )
	{
		strResult.LoadString( t ? IDS_YES : IDS_NO );
	}
	else
	{
		strResult.LoadString( IDS_UNKNOWN );
	}
	StrTranslate(strResult);

	return strResult;
}

static
CString IPAddressToString( unsigned ip )
{
	CString strResult;

	if( ip )
	{
		strResult.Format
		( 
			"%i.%i.%i.%i", 
			LOBYTE( LOWORD( ip ) ),
			HIBYTE( LOWORD( ip ) ),
			LOBYTE( HIWORD( ip ) ), 
			HIBYTE( HIWORD( ip ) ) 
		);
	}
	else
	{
		strResult.LoadString( IDS_UNKNOWN );
	}
	StrTranslate(strResult);

	return strResult;
}

BOOL CServerPage::OnInitDialog()
{
	if( !CPropertyPage::OnInitDialog() ) return FALSE;
	DlgTranslate(this);
	
	CMailbox &mbox = *( m_pMailboxArray->GetAt( 0 ) );
	m_strGreeting = mbox.GetGreeting();
	m_strIPAddress = IPAddressToString( mbox.GetIPAddress() );
	m_strAPOPSupported = TristateToString( mbox.GetAPOPSupported() );
	m_strUIDLSupported = TristateToString( mbox.GetUIDLSupported() );
	m_strBurstWritesSupported = TristateToString( mbox.GetBurstWritesSupported() );

	for( int i = m_pMailboxArray->GetUpperBound(); i; --i )
	{
		CMailbox &mbox = *( m_pMailboxArray->GetAt( i ) );

		if( ( m_strGreeting != cstrMultipleSelection ) && 
			( m_strGreeting != mbox.GetGreeting() ) )
		{
			m_strGreeting = cstrMultipleSelection;
		}

		if( ( m_strIPAddress != cstrMultipleSelection ) && 
			( m_strIPAddress != IPAddressToString( mbox.GetIPAddress() ) ) ) 
		{
			m_strIPAddress = cstrMultipleSelection;
		}

		if( ( m_strAPOPSupported != cstrMultipleSelection ) && 
			( m_strAPOPSupported != TristateToString( mbox.GetAPOPSupported() ) ) )
		{
			m_strAPOPSupported = cstrMultipleSelection;
		}

		if( ( m_strUIDLSupported != cstrMultipleSelection ) && 
			( m_strUIDLSupported != TristateToString( mbox.GetUIDLSupported() ) ) ) 
		{
			m_strUIDLSupported = cstrMultipleSelection;
		}

		if( ( m_strBurstWritesSupported != cstrMultipleSelection ) && 
			( m_strBurstWritesSupported != TristateToString( mbox.GetBurstWritesSupported() ) ) )
		{
			m_strBurstWritesSupported = cstrMultipleSelection;
		}

	}

	UpdateData( FALSE );
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CServerPage message handlers
