#if !defined(AFX_RAWHEAD_H__5BBFF872_CA04_449A_8623_D1B26974E432__INCLUDED_)
#define AFX_RAWHEAD_H__5BBFF872_CA04_449A_8623_D1B26974E432__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RawHead.h : Header-Datei
//
#include "resrc1.h"
/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CRawHead 

class CExcerpt;
template < class BASE_CLASS, class TYPE > class CTypedPtrArray;

class CRawHead : public CPropertyPage
{
	DECLARE_DYNCREATE(CRawHead)

	CTypedPtrArray < CPtrArray, CExcerpt* > *m_pExcerptArray;
// Konstruktion
public:
	CRawHead();
	~CRawHead();

	void Attach( CTypedPtrArray < CPtrArray, CExcerpt* > * );
	//{{AFX_DATA(CRawHead)
	enum { IDD = IDD_HEADER_RAW };
	CString	m_sHeader;
	BOOL	m_bJumpEnd;
	//}}AFX_DATA


// Überschreibungen
	// Der Klassen-Assistent generiert virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CRawHead)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	void JumpEnd();
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CRawHead)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	afx_msg void OnJumpend();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fugt unmittelbar vor der vorhergehenden Zeile zusatzliche Deklarationen ein.

#endif // AFX_RAWHEAD_H__5BBFF872_CA04_449A_8623_D1B26974E432__INCLUDED_
