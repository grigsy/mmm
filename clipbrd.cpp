#include "stdafx.h"
#include "Magic.h"
#include "clipbrd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CClipBoard::CClipBoard(LPCTSTR szFormatName)
{
    m_iFormatNumber = ::RegisterClipboardFormat(szFormatName);
}

BOOL CClipBoard::CopyToClipboard()
{
	if (!AfxGetMainWnd ()->OpenClipboard ())
		return FALSE;
	if (!::EmptyClipboard ())
	{
		::CloseClipboard ();
		return FALSE;
	}

	int nRet = TRUE;
	try
    {
		CMemFile file;
        CArchive ar(&file, CArchive::store);

        nRet = Serialize(ar);

        ar.Close();

		UINT uiDataSize = (UINT)file.GetLength ();
		LPBYTE lpbData = file.Detach ();

		HGLOBAL clipbuffer = GlobalAlloc(GMEM_DDESHARE|GMEM_MOVEABLE, uiDataSize);
		LPBYTE pShared= (LPBYTE)GlobalLock(clipbuffer);
		if (pShared)
			memcpy(pShared, lpbData, uiDataSize);
		//Put it on the clipboard
		GlobalUnlock(clipbuffer);
		SetClipboardData(m_iFormatNumber, clipbuffer);
		
		free(lpbData);
    }
    catch (...)
    {
		nRet = FALSE;
    }
	CloseClipboard();

    return nRet;
}

//-------------------------------------

BOOL CClipBoard::PasteFromClipboard()
{
    if (!m_iFormatNumber)
		return FALSE;
	if (!AfxGetMainWnd ()->OpenClipboard ())
		return FALSE;

    HANDLE hData = ::GetClipboardData(m_iFormatNumber);
    if (hData == NULL)
    {
        VERIFY(::CloseClipboard());
        return FALSE;
    }
	int nRet = TRUE;
	try
    {
		LPBYTE pData = (LPBYTE)GlobalLock(hData);
		CMemFile file(pData, GlobalSize(hData));
        CArchive ar(&file, CArchive::load);

		nRet = Serialize(ar);

        ar.Close();

		GlobalUnlock(pData);
    }
    catch (...)
    {
		nRet = FALSE;
    }
	CloseClipboard();
	return nRet;
}

//-------------------------------------

BOOL CClipBoard::IsPossiblePaste()
{
    if (!m_iFormatNumber)
		return FALSE;

    ASSERT(m_iFormatNumber > 0xC000u && m_iFormatNumber < 0xFFFFu);

    return ::IsClipboardFormatAvailable(m_iFormatNumber);
}
