// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef MAGIC_INCLUDED
#define MAGIC_INCLUDED

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "main.h"
#include "resrc1.h"
#include "diction.h"
#include "dfilters.h"
#include "oneinst.h"

extern const TCHAR	*cstrMultipleSelection;
void BytesToString( int intSize, CString &strSize );

/////////////////////////////////////////////////////////////////////////////
// CMagicApp:
// See Magic.cpp for the implementation of this class
//

extern
class CMagicApp : public CWinApp
{
public:
	CMagicApp();
	~CMagicApp();

	HANDLE m_hCmdID;
	UINT m_uMciID;

	BOOL	m_bUseIni;	// using ini file for storage
	BOOL	m_bExited;

// Settings
	int		intMVMode;
//	int		intEVMode;
	int		intMVSortColumn;
	int		intMVSortAscend;
	int		intEVSortColumn;
	int		intEVSortAscend;
	int		intSplitterPos;
	int		intMCAliasWidth;	int	intMCAliasPos;
	int		intMCUserWidth;		int intMCUserPos;
	int		intMCHostWidth;		int intMCHostPos;
	int		intMCMailWidth;		int intMCMailPos;
	int		intMCStatWidth;		int intMCStatPos;
	int		intMCElapsedWidth;	int intMCElapsedPos;
	int		intMCPortWidth;		int intMCPortPos;
	int		intMCPollWidth;		int intMCPollPos;
	int		intECMBoxWidth;		int intECMBoxPos;
	int		intECFromWidth;		int intECFromPos;
	int		intECToWidth;		int intECToPos;
	int		intECSubjWidth;		int intECSubjPos;
	int		intECDateWidth;		int intECDatePos;
	int		intECSizeWidth;		int intECSizePos;
	CRect	rcWnd;
	int		intPlayback;
	CString	strPlayback;
	int		intPlaybackDevice;
	int		intCommand;
	CString	strCommand;
	int		intCommandRun;
	int		intStartAlwaysHidden;
	int		intCheckImmediately;
	int		intEVConfirmDelete;
	int		intPopUpMainWindow;
	int		intPreviewSize;	// KB for preview
	int		intMarkRead;	// READ_x
	int		intEnableFilters;

	int		intDblAction;
	int		intRBAction;
	int		intRB2Action;

	bool	bIsSuspended;
	CString strFileExtensionForMessages;
	CString strApp;
	CString strLastDiction;
	CString m_strTemp;

	DWORD	m_dwFlags;		// MMF_xy
	int		m_nMaxLogSize;	// for deleted log
	COLORREF	m_clrFriends;
	int		m_nMsgPropPage;	// page to open in MsgProps
	
	// massive mode: check N boxes every M minutes
	int		m_nMassiveBoxes;
	int		m_nMassivePeriod;

	int		GetEncoding();
	int		m_nEncoding;

	int		m_nWindowsVersion;

	BOOL	m_bReset;

	LOGFONT	m_lfMain;
	LOGFONT	m_lfDef;
	CFont	m_fntMain;
	void	SetFont(LOGFONT& lf);
	void	SetDefFont(CFont* p);
	CFont*	GetFont();

	void LoadSettings(LPCTSTR sFile);	//load save to file support, or default if no sFile specified
	void SaveSettings(LPCTSTR sFile);

	COneInstance	m_Single;

	CArray<CMailFilter, CMailFilter&> m_Filters;

	HICON LoadIcon( UINT nIDResource, bool Large = false ) const;
	HBITMAP LoadBitmap( UINT nIDResource ) const;

	CImageList	m_imgMailbox;
	CImageList	m_imgMsg;
	void PrepareImages();

	void GenDefaultFilters();

	CString	m_sHelpPath;
	LPCTSTR GetHelpFilePath();

#ifdef USE_SSL
	CUsesOpenSSL m_SSL;
#endif
	
	//{{AFX_VIRTUAL(CMagicApp)
	public:
	virtual BOOL InitInstance();
	virtual BOOL OnIdle(LONG lCount);
	virtual int ExitInstance();
	virtual void WinHelp(DWORD dwData, UINT nCmd = HELP_CONTEXT);
	//}}AFX_VIRTUAL
	virtual int DoMessageBox(LPCTSTR lpszPrompt, UINT nType, UINT nIDPrompt);

	//{{AFX_MSG(CMagicApp)
	afx_msg void OnAppAbout();
	afx_msg void OnExport();
	afx_msg void OnImport();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
}
theApp;

LPCTSTR GetHelpFilePath();

enum
{
	II_LOADED, II_MAILNEW, II_MAILOPEN, 
	II_PRIORITY, II_ATTACH, II_ATTACH_PRIO,
	II_LOAD, II_DELETE,

	II_EMPTY, II_FULL,
	II_FAIL, II_DISABLE, II_SECURE,

	II_FOLDER,

	II_COUNTER,	// always last
};

#endif
