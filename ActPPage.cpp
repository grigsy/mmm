// ActPPage.cpp : implementation file
//

#include "stdafx.h"
#include "magic.h"
#include "ActPPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CActPPage property page

IMPLEMENT_DYNCREATE(CActPPage, CPropertyPage)

CActPPage::CActPPage() : CPropertyPage(CActPPage::IDD)
{
	//{{AFX_DATA_INIT(CActPPage)
	m_nActDbl = theApp.intDblAction-1;
	m_nActRb = theApp.intRBAction-1;
	m_nActRb2 = theApp.intRB2Action-1;
	//}}AFX_DATA_INIT
}

CActPPage::~CActPPage()
{
}

void CActPPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CActPPage)
	DDX_CBIndex(pDX, IDC_ACTIONS_DBL, m_nActDbl);
	DDX_CBIndex(pDX, IDC_ACTIONS_RB, m_nActRb);
	DDX_CBIndex(pDX, IDC_ACTIONS_RB2, m_nActRb2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CActPPage, CPropertyPage)
	//{{AFX_MSG_MAP(CActPPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CActPPage message handlers

UINT anActions[] = {IDS_ACT_QVIEW, IDS_ACT_MENU, IDS_ACT_HEADER, IDS_ACT_PROPS,
IDS_ACT_PREVIEW, IDS_ACT_ASTEXT};

BOOL CActPPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	DlgTranslate(this);

	for (int i=0; i<sizeof(anActions)/sizeof(UINT); i++)
	{
		MAKE_STRING(s, anActions[i]);
		SendDlgItemMessage(IDC_ACTIONS_DBL, CB_ADDSTRING, 0, (LPARAM)(LPCTSTR)s);
		SendDlgItemMessage(IDC_ACTIONS_RB, CB_ADDSTRING, 0, (LPARAM)(LPCTSTR)s);
		SendDlgItemMessage(IDC_ACTIONS_RB2, CB_ADDSTRING, 0, (LPARAM)(LPCTSTR)s);
	}
	UpdateData(FALSE);
	return TRUE;
}

BOOL CActPPage::OnApply() 
{
	UpdateData();
	theApp.intDblAction = m_nActDbl + 1;
	theApp.intRBAction = m_nActRb + 1;
	theApp.intRB2Action = m_nActRb2 + 1;

	return CPropertyPage::OnApply();
}
