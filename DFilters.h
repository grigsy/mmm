#if !defined(AFX_DFILTERS_H__D797D756_D7CA_477F_A7A1_E31EA0B7BD1B__INCLUDED_)
#define AFX_DFILTERS_H__D797D756_D7CA_477F_A7A1_E31EA0B7BD1B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DFilters.h : Header-Datei
//
#include "tools.h"

struct CFilterCnd
{
	BYTE	m_nField;		// MFF_*
	BYTE	m_nOperation;	// MFO_*
	CString	m_sText;		// filter text

	void ToText(CString& s);
	void Reset() {m_nField = MFF_SUBJECT; m_nOperation = MFO_EQUAL; m_sText.Empty(); ClearData(); };

	void Prepare();
	void ClearData();
	// data for verifying condition
	CStringArray	m_asData;	
	CUIntArray		m_anLines;	// lines in files for asData entries - for detailed report
	DWORD			m_dwFileSize;
	FILETIME		m_ft;	// last loaded file time - to catch updates
	
	BOOL	LoadFile(LPCTSTR sFile, TCHAR cRem, BOOL bCaseSens);
};
struct CMailFilter
{
	CString	m_sName;		// filter text
	
	enum {nConditions = 2};
	CFilterCnd	m_aCnd[nConditions];
	BYTE		m_nCombination;	// MFC_x

	CString	m_sMailBox;
	COLORREF m_Color;		// if color is used
	DWORD	m_dwAction;		// combination of MFA_*

	CMailFilter();
	CMailFilter(const CMailFilter& a);
	// CArray support
	CMailFilter& operator=(const CMailFilter&);
	void Serialize(CArchive& a);
};
template<> void AFXAPI SerializeElements<CMailFilter>(CArchive& ar, CMailFilter* pElements, INT_PTR nCount);
/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DFilters 

class CMagicDoc;

class DFilters : public CDialog
{
// Konstruktion
public:
	DFilters(CMagicDoc*);   // Standardkonstruktor

	//{{AFX_DATA(DFilters)
	enum { IDD = IDD_FILTERS };
	CCheckListBox m_lbList;
	CComboBox	m_cbMailbox;
	CString	m_sName;
	BOOL	m_bOthers;
	BOOL	m_bColor;
	BOOL	m_bDelete;
	BOOL	m_bMark;
	BOOL	m_bRead;
	BOOL	m_bAllow;
	CString	m_sFilter1;
	CString	m_sFilter2;
	int		m_nCombine;
	BOOL	m_bFriend;
	BOOL	m_bProtect;
	//}}AFX_DATA


	//{{AFX_VIRTUAL(DFilters)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

protected:
	void UpdateColor();
	void Prepare();
	void GetData();
	CArray<CMailFilter, CMailFilter&> m_Filters;
	CMagicDoc*	m_pDoc;	// to get mailbox list
	int		m_nCurSel;

	CColorSample	m_Sample;

	//{{AFX_MSG(DFilters)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeList();
	afx_msg void OnFltAdd();
	afx_msg void OnFltDel();
	afx_msg void OnFltDown();
	afx_msg void OnFltUp();
	afx_msg void OnChangeFilterName();
	afx_msg void OnFltColorSel();
	afx_msg void OnFltColor();
	afx_msg void OnFlt1Brs();
	afx_msg void OnFlt2Brs();
	afx_msg void OnSelendokCombine();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fugt unmittelbar vor der vorhergehenden Zeile zusatzliche Deklarationen ein.

#endif // AFX_DFILTERS_H__D797D756_D7CA_477F_A7A1_E31EA0B7BD1B__INCLUDED_
