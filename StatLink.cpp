////////////////////////////////////////////////////////////////
// 1997 Microsoft Systems Journal
// If this code works, it was written by Paul DiLascia.
// If not, I don't know who wrote it.
//
#include "StdAfx.h"
#include "StatLink.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define CLR_UNVIS	RGB(0,0,255)
#define CLR_VISED	RGB(128,0,128)

class CStaticLink : public CStatic 
{
public:
	CStaticLink(LPCTSTR lpText, HCURSOR hc);
	~CStaticLink() { }

	CString		m_sLink;
	COLORREF	m_color;

protected:
	HCURSOR		m_hCursor;
	CFont		m_font;

	virtual void PostNcDestroy();

	// message handlers
	DECLARE_MESSAGE_MAP()
	afx_msg LRESULT	OnNcHitTest(CPoint point);
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void	OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL	OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
};

BEGIN_MESSAGE_MAP(CStaticLink, CStatic)
	ON_WM_NCHITTEST()
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_LBUTTONDOWN()
	ON_WM_SETCURSOR()
END_MESSAGE_MAP()

CStaticLink::CStaticLink(LPCTSTR lpText, HCURSOR hc)
{
	m_sLink= lpText;
	m_color = CLR_UNVIS;				// not visited yet
	m_hCursor=hc;
}

LRESULT CStaticLink::OnNcHitTest(CPoint )
{
	return HTCLIENT;
}

HBRUSH CStaticLink::CtlColor(CDC* pDC, UINT nCtlColor)
{
	nCtlColor;
	ASSERT(nCtlColor == CTLCOLOR_STATIC);
	DWORD dwStyle = GetStyle();
	
	HBRUSH hbr = NULL;
	if ((dwStyle & (SS_CENTER|SS_RIGHT))==SS_CENTER)
	{
		// this is a text control: set up font and colors
		if (!(HFONT)m_font) 
		{
			// first time init: create font
			LOGFONT lf;
			GetFont()->GetObject(sizeof(lf), &lf);
			lf.lfUnderline = TRUE;
			m_font.CreateFontIndirect(&lf);
		}

		// use underline font and visited/unvisited colors
		pDC->SelectObject(&m_font);
		pDC->SetTextColor(m_color);
		pDC->SetBkMode(TRANSPARENT);

		hbr = (HBRUSH)::GetStockObject(HOLLOW_BRUSH);
	}
	return hbr;
}
HINSTANCE Navigate(LPCTSTR sURL)
{
	if (!sURL || !*sURL )
		return NULL;
	return  ShellExecute(0, _T("open"), sURL, 0, 0, SW_SHOWNORMAL);
}

void CStaticLink::OnLButtonDown(UINT , CPoint )
{
	if (m_sLink.IsEmpty()) {		// if URL/filename not set..
		GetWindowText(m_sLink);	// ..get it from window text
		if (m_sLink.IsEmpty())
			return;
	}

	HINSTANCE h = Navigate(m_sLink);
	if ((UINT)h > 32) 						 // success!
	{
		m_color = CLR_VISED;				 // change color
		Invalidate();							 // repaint 
	}
}

BOOL CStaticLink::OnSetCursor(CWnd* , UINT , UINT )
{
	if (m_hCursor)
	{
		::SetCursor(m_hCursor);
		return TRUE;
	}
	return FALSE;
}

void CStaticLink::PostNcDestroy()
{
	delete this;
}
BOOL CreateStaticLink(CWnd* p, UINT ID, LPCTSTR sURL, HCURSOR hc)
{
	if (!p)
		return FALSE;
	CString s(sURL);
	if (s.IsEmpty())	// get URL from control text
	{
		p->GetDlgItemText(ID, s);
		int nSlash = s.Find(_T("//"));
		if (nSlash>=0)
		{
			CString sLabel = s.Left(nSlash);
			p->SetDlgItemText(ID, sLabel);
			s = s.Mid(nSlash+2);
			// is it mail?
			if (s.Find('@')>0)
				s = "Mailto:" + s;
		}
		
	}
	CStaticLink* pL = new CStaticLink(s, hc);
	pL->SubclassDlgItem(ID, p);
	return TRUE;
}
