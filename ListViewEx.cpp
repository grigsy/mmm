// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// ListVwEx.cpp : implementation of the CListViewEx class
//
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) 1992-1996 Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#include "stdafx.h"
#include "ListViewEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CListViewEx

IMPLEMENT_DYNCREATE(CListViewEx, CListView)

BEGIN_MESSAGE_MAP(CListViewEx, CListView)
	//{{AFX_MSG_MAP(CListViewEx)
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
	ON_MESSAGE(LVM_SETIMAGELIST, OnSetImageList)
	ON_MESSAGE(LVM_SETTEXTCOLOR, OnSetTextColor)
	ON_MESSAGE(LVM_SETTEXTBKCOLOR, OnSetTextBkColor)
	ON_MESSAGE(LVM_SETBKCOLOR, OnSetBkColor)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CListViewEx construction/destruction

CListViewEx::CListViewEx()
{
	m_bFullRowSel=FALSE;
	m_bClientWidthSel=TRUE;

	m_cxClient=0;
	m_cxStateImageOffset=0;

	m_clrText=::GetSysColor(COLOR_WINDOWTEXT);
	m_clrTextBk=::GetSysColor(COLOR_WINDOW);
	m_clrBkgnd=::GetSysColor(COLOR_WINDOW);
	
	m_dwLastRButtonDown = 0;
}

CListViewEx::~CListViewEx()
{
}

BOOL CListViewEx::PreCreateWindow(CREATESTRUCT& cs)
{
	// default is report view and full row selection
	cs.style&=~LVS_TYPEMASK;
	cs.style|=LVS_REPORT | LVS_OWNERDRAWFIXED;
	m_bFullRowSel=TRUE;

	return(CListView::PreCreateWindow(cs));
}

BOOL CListViewEx::SetFullRowSel(BOOL bFullRowSel)
{
	// no painting during change
	LockWindowUpdate();

	m_bFullRowSel=bFullRowSel;

	BOOL bRet;

	if(m_bFullRowSel)
		bRet=ModifyStyle(0L,LVS_OWNERDRAWFIXED);
	else
		bRet=ModifyStyle(LVS_OWNERDRAWFIXED,0L);

	// repaint window if we are not changing view type
	if(bRet && (GetStyle() & LVS_TYPEMASK)==LVS_REPORT)
		Invalidate();

	// repaint changes
	UnlockWindowUpdate();

	return(bRet);
}

BOOL CListViewEx::GetFullRowSel()
{
	return(m_bFullRowSel);
}

/////////////////////////////////////////////////////////////////////////////
// CListViewEx drawing
void CListViewEx::GetItemColors(int , COLORREF& clrText, 
								COLORREF& clrBk, BOOL bSelected)
{
	BOOL bFocus=(GetFocus()==this);
	if (bSelected)
	{
		clrText = ::GetSysColor(COLOR_HIGHLIGHTTEXT);
		clrBk = bFocus ? ::GetSysColor(COLOR_HIGHLIGHT) : ::GetSysColor(COLOR_3DFACE);
	}
}

// offsets for first and other columns
#define OFFSET_FIRST	2
#define OFFSET_OTHER	3
void CListViewEx::GetCustomRect(int nItem,CRect& rcItem, int LVIR_x)
{
	GetListCtrl().GetItemRect(nItem,rcItem,LVIR_x);
}

void CListViewEx::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CListCtrl& ListCtrl=GetListCtrl();
	int nColumns=ListCtrl.GetHeaderCtrl()->GetItemCount();
	int nColumn=0;
	int nOffset=OFFSET_FIRST;
	CDC* pDC=CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rcColumn;
	CRect rcItem(lpDrawItemStruct->rcItem);
	UINT uiFlags=ILD_TRANSPARENT;
	UINT nJustify=DT_LEFT;
	CImageList* pImageList;
	int nItem=lpDrawItemStruct->itemID;
	BOOL bFocus=(GetFocus()==this);
	COLORREF clrTextSave(0), clrBkSave(0);
	COLORREF clrImage=m_clrBkgnd;
	static _TCHAR szBuff[MAX_PATH];

	// get item data
	LV_ITEM lvi;
	lvi.mask= LVIF_IMAGE | LVIF_STATE;
	lvi.iItem=nItem;
	lvi.iSubItem=0;
	lvi.pszText=0;
	lvi.cchTextMax=0;
	lvi.stateMask=0xFFFF;		// get all state flags
	ListCtrl.GetItem(&lvi);

	BOOL bSelected=(bFocus || (GetStyle() & LVS_SHOWSELALWAYS)) && lvi.state & LVIS_SELECTED;
	bSelected=bSelected || (lvi.state & LVIS_DROPHILITED);

	LVCOLUMN lvc;
	lvc.mask=LVCF_FMT | LVCF_WIDTH | LVCF_ORDER;
	CRect rcAllLabels;
	ListCtrl.GetItemRect(nItem,rcAllLabels,LVIR_BOUNDS);

	// set colors if item is selected
	COLORREF clrText = GetSysColor(COLOR_WINDOWTEXT);
	COLORREF clrBk = GetSysColor(COLOR_WINDOW);
	GetItemColors(nItem, clrText, clrBk, bSelected);


	CBrush brush(clrBk);
	pDC->FillRect(rcAllLabels, &brush);

	clrTextSave = pDC->SetTextColor(clrText);
	clrBkSave   = pDC->SetBkColor(clrBk);

	// set color and mask for the icon
	if(lvi.state & LVIS_CUT)
	{
		clrImage = m_clrBkgnd;
		uiFlags |= ILD_SELECTED;
	}
	else if(bSelected)
	{
		clrImage = ::GetSysColor(COLOR_HIGHLIGHT);
		uiFlags |= ILD_SELECTED;
	}

	int nLastColRight = 0;
	for(int nIndex=0; nIndex<nColumns; nIndex++)
	{
		nColumn = ListCtrl.GetHeaderCtrl()->OrderToIndex(nIndex);
		ListCtrl.GetColumn(nColumn,&lvc);
		ListCtrl.GetHeaderCtrl()->GetItemRect(nColumn, &rcColumn);
		rcItem.left=rcColumn.left+nOffset;
		rcItem.right=rcColumn.right-nOffset;

		if (nIndex > 0)
		{
			switch(lvc.fmt & LVCFMT_JUSTIFYMASK)
			{
			case LVCFMT_RIGHT:
				nJustify=DT_RIGHT;
				break;
			case LVCFMT_CENTER:
				nJustify=DT_CENTER;
				break;
			default:
				nJustify=DT_LEFT;
				break;
			}
		}

		if (nColumn == 0)
		{
			// draw state icon
			UINT nStateImageMask=lvi.state & LVIS_STATEIMAGEMASK;
			if(nStateImageMask)
			{
				int nImage=(nStateImageMask>>12)-1;
				pImageList=ListCtrl.GetImageList(LVSIL_STATE);
				if(pImageList)
					pImageList->Draw(pDC,nImage,CPoint(rcItem.left,rcItem.top),ILD_TRANSPARENT);
			}

			// draw normal and overlay icon
			CRect rcIcon;
			GetCustomRect(nItem, rcIcon, LVIR_ICON);

			pImageList=ListCtrl.GetImageList(LVSIL_SMALL);
			if(pImageList)
			{
				UINT nOvlImageMask=lvi.state & LVIS_OVERLAYMASK;
				if(rcItem.left<rcItem.right-1)
					ImageList_DrawEx(pImageList->m_hImageList,lvi.iImage,pDC->m_hDC,rcIcon.left,rcIcon.top,16,16,m_clrBkgnd,clrImage,uiFlags | nOvlImageMask);
			}

			// draw item label
			GetCustomRect(nItem,rcItem,LVIR_LABEL);
			rcItem.right-=m_cxStateImageOffset;
			rcItem.left += 2;
			nLastColRight = rcItem.right;
		}
		else
		{
			rcItem.right = nLastColRight + rcItem.Width() + 2*nOffset;
			rcItem.left = nLastColRight + nOffset*2;
			nLastColRight = rcItem.right;
		}
		
		DrawSubItem(pDC, nItem, nColumn, rcItem, nJustify, bSelected);
		nOffset = OFFSET_OTHER;
	}

	// draw focus rectangle if item has focus
	if(lvi.state & LVIS_FOCUSED && bFocus)
	{
		pDC->DrawFocusRect(rcAllLabels);
	}

	// set original colors if item was selected
	//if(bSelected)
	{
		pDC->SetTextColor(clrTextSave);
		pDC->SetBkColor(clrBkSave);
	}
}
int CListViewEx::GetItemText(int nItem, int nColumn, LPTSTR pBuf, int nLen)
{
	if (m_bUseMapping)
	{
		nColumn = GetVisibleColIndex(nColumn);
		if (nColumn<0)
			return 0;
	}
	if (nColumn>0)
	{
		return GetListCtrl().GetItemText(nItem,nColumn,pBuf, nLen);
	}
	// main item text
	LV_ITEM lvi;
	lvi.mask=LVIF_TEXT;
	lvi.iItem=nItem;
	lvi.iSubItem=0;
	lvi.pszText=pBuf;
	lvi.cchTextMax=nLen;
	lvi.stateMask=0;		// get all state flags
	GetListCtrl().GetItem(&lvi);
	return strlen(pBuf);
}

void CListViewEx::DrawSubItem(CDC* pDC, int nItem, int nColumn, CRect& rcLabel, int nJustify, BOOL )
{
	static _TCHAR szBuff[MAX_PATH];
	
	if (GetItemText(nItem, nColumn, szBuff,sizeof(szBuff)) > 0)
	{
		pDC->DrawText(szBuff,-1,rcLabel, nJustify | DT_SINGLELINE | DT_NOPREFIX | DT_VCENTER | DT_END_ELLIPSIS );
	}
}

void CListViewEx::RepaintSelectedItems()
{
	CListCtrl& ListCtrl=GetListCtrl();
	CRect rcItem, rcLabel;

// invalidate focused item so it can repaint properly

	int nItem=ListCtrl.GetNextItem(-1,LVNI_FOCUSED);

	if(nItem!=-1)
	{
		ListCtrl.GetItemRect(nItem,rcItem,LVIR_BOUNDS);
		ListCtrl.GetItemRect(nItem,rcLabel,LVIR_LABEL);
		rcItem.left=rcLabel.left;

		InvalidateRect(rcItem,FALSE);
	}

// if selected items should not be preserved, invalidate them

	if(!(GetStyle() & LVS_SHOWSELALWAYS))
	{
		for(nItem=ListCtrl.GetNextItem(-1,LVNI_SELECTED);
			nItem!=-1; nItem=ListCtrl.GetNextItem(nItem,LVNI_SELECTED))
		{
			ListCtrl.GetItemRect(nItem,rcItem,LVIR_BOUNDS);
			ListCtrl.GetItemRect(nItem,rcLabel,LVIR_LABEL);
			rcItem.left=rcLabel.left;

			InvalidateRect(rcItem,FALSE);
		}
	}

// update changes 

	UpdateWindow();
}

/////////////////////////////////////////////////////////////////////////////
// CListViewEx diagnostics

#ifdef _DEBUG

void CListViewEx::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);

	dc << "m_bFullRowSel = " << (UINT)m_bFullRowSel;
	dc << "\n";
	dc << "m_cxStateImageOffset = " << m_cxStateImageOffset;
	dc << "\n";
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CListViewEx message handlers

LRESULT CListViewEx::OnSetImageList(WPARAM wParam, LPARAM lParam)
{
	if((int)wParam==LVSIL_STATE)
	{
		int cx, cy;

		if(::ImageList_GetIconSize((HIMAGELIST)lParam,&cx,&cy))
			m_cxStateImageOffset=cx;
		else
			m_cxStateImageOffset=0;
	}

	return(Default());
}

LRESULT CListViewEx::OnSetTextColor(WPARAM , LPARAM lParam)
{
	m_clrText = (COLORREF)lParam;
	return(Default());
}

LRESULT CListViewEx::OnSetTextBkColor(WPARAM , LPARAM lParam)
{
	m_clrTextBk = (COLORREF)lParam;
	return(Default());
}

LRESULT CListViewEx::OnSetBkColor(WPARAM , LPARAM lParam)
{
	m_clrBkgnd = (COLORREF)lParam;
	return(Default());
}

void CListViewEx::OnSize(UINT nType, int cx, int cy) 
{
	m_cxClient=cx;
	CListView::OnSize(nType, cx, cy);
}

void CListViewEx::OnPaint() 
{
	// in full row select mode, we need to extend the clipping region
	// so we can paint a selection all the way to the right
	if(m_bClientWidthSel && (GetStyle() & LVS_TYPEMASK)==LVS_REPORT && GetFullRowSel())
	{
		CRect rcAllLabels;
		GetListCtrl().GetItemRect(0,rcAllLabels,LVIR_BOUNDS);

		if(rcAllLabels.right<m_cxClient)
		{
			// need to call BeginPaint (in CPaintDC c-tor)
			// to get correct clipping rect
			CPaintDC dc(this);

			CRect rcClip;
			dc.GetClipBox(rcClip);

			rcClip.left=min(rcAllLabels.right-1,rcClip.left);
			rcClip.right=m_cxClient;

			InvalidateRect(rcClip,FALSE);
			// EndPaint will be called in CPaintDC d-tor
		}
	}

	CListView::OnPaint();
}

void CListViewEx::OnSetFocus(CWnd* pOldWnd) 
{
	CListView::OnSetFocus(pOldWnd);

	// check if we are getting focus from label edit box
	if(pOldWnd!=NULL && pOldWnd->GetParent()==this)
		return;

	// repaint items that should change appearance
	if(m_bFullRowSel && (GetStyle() & LVS_TYPEMASK)==LVS_REPORT)
		RepaintSelectedItems();
}

void CListViewEx::SetColTitle(int nIdx, LPCTSTR sTitle)
{
	CListCtrl &list = GetListCtrl();

	LVCOLUMN lc;
	lc.mask = LVCF_TEXT/*|LVCF_SUBITEM*/;
	lc.iSubItem = nIdx;
	lc.pszText = (LPTSTR)sTitle;
	list.SetColumn( nIdx, &lc );
}

void CListViewEx::OnKillFocus(CWnd* pNewWnd) 
{
	CListView::OnKillFocus(pNewWnd);

	// check if we are losing focus to label edit box
	if(pNewWnd!=NULL && pNewWnd->GetParent()==this)
		return;

	// repaint items that should change appearance
	if(m_bFullRowSel && (GetStyle() & LVS_TYPEMASK)==LVS_REPORT)
		RepaintSelectedItems();
}

int CListViewEx::GetItemFromPoint(const CPoint& pt, int& nSubItem, CRect* pRect)
{
	LVHITTESTINFO ht;
	ht.pt = pt;
	int nItem = GetListCtrl().SubItemHitTest(&ht);
	if (nItem<0 || (ht.flags & LVHT_ONITEMLABEL)==0)
		return -1;
	nSubItem = ht.iSubItem;
	if (!pRect)
		return nItem;
	// 1st column can have icon
	CRect rcFull;
	GetListCtrl().GetItemRect(nItem, rcFull, LVIR_BOUNDS);
	CRect rcLabel;
	GetListCtrl().GetItemRect(nItem, rcLabel, LVIR_LABEL);
	CRect rc(rcFull);
	if (ht.iSubItem==0)
	{
		rc = rcLabel;
		rc.left -= 3;
	}
	else
	{
		CRect rcItem;
		GetListCtrl().GetHeaderCtrl()->GetItemRect(ht.iSubItem, rcItem);
		rc.left = rcItem.left + 1;
		rc.right = rcItem.right;
		rc.top--;
		rc.bottom++;
	}
	*pRect = rc;
	return nItem;
}

//----------------------------------------------------------------

#ifndef HDF_SORTUP 
#define HDF_SORTUP 0x0400 
#endif 

#ifndef HDF_SORTDOWN 
#define HDF_SORTDOWN 0x0200 
#endif 


CHeaderColSortMark::CHeaderColSortMark()
{
	m_nColSorted = -1;
	m_bXP = FALSE;
}

void CHeaderColSortMark::Init(UINT nUpBmp, UINT nDownBmp)
{
	m_nUpImage = nUpBmp;
	m_nDownImage = nDownBmp;

	OSVERSIONINFOEX info;
	ZeroMemory(&info, sizeof(OSVERSIONINFOEX));
	info.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	if(GetVersionEx((OSVERSIONINFO*)&info))
		m_bXP = ((info.dwMajorVersion == 5 && info.dwMinorVersion == 1) || info.dwMajorVersion>5);
}

void CHeaderColSortMark::SetSortMark(CListCtrl& list, int nCol, int nAsc)
{
	if (nCol != m_nColSorted && m_nColSorted>=0)
		RemoveMark(list, m_nColSorted);
	HDITEM HeaderItem; 
	ZeroMemory(&HeaderItem, sizeof(HeaderItem));
	HeaderItem.mask = HDI_FORMAT; 
	CHeaderCtrl* HeaderCtrl = list.GetHeaderCtrl(); 
	HeaderCtrl->GetItem(nCol, &HeaderItem); 
	if (HeaderItem.hbm != 0)
	{ 
		DeleteObject(HeaderItem.hbm); 
		HeaderItem.hbm = 0; 
	} 
	if (1) // need manifest for this to work
	{
		HeaderItem.mask = HDI_FORMAT | HDI_BITMAP;
		HeaderItem.fmt |= HDF_BITMAP;
		if ((HeaderItem.fmt & HDF_RIGHT) == 0)
			HeaderItem.fmt |= HDF_BITMAP_ON_RIGHT;
		HeaderItem.hbm = (HBITMAP)LoadImage(AfxGetInstanceHandle(),
			MAKEINTRESOURCE(nAsc ? m_nUpImage : m_nDownImage), IMAGE_BITMAP, 0, 0, LR_LOADMAP3DCOLORS);
	}
	else
	{
		HeaderItem.fmt &= ~(HDF_SORTUP | HDF_SORTDOWN);	
		HeaderItem.fmt |= nAsc ? HDF_SORTUP : HDF_SORTDOWN;	
	}
	HeaderCtrl->SetItem(nCol, &HeaderItem);
	m_nColSorted = nCol;
}

void CHeaderColSortMark::RemoveMark(CListCtrl& list, int nCol)
{
	HDITEM HeaderItem; 
	ZeroMemory(&HeaderItem, sizeof(HeaderItem));
	HeaderItem.mask = HDI_FORMAT | HDI_BITMAP; 
	CHeaderCtrl* HeaderCtrl = list.GetHeaderCtrl(); 
	HeaderCtrl->GetItem(nCol, &HeaderItem);
    HeaderItem.fmt &= ~(HDF_BITMAP | HDF_BITMAP_ON_RIGHT); 
    if(m_bXP)
		HeaderItem.fmt &= ~(HDF_SORTUP | HDF_SORTDOWN);
    if (HeaderItem.hbm != 0)
    {
       DeleteObject(HeaderItem.hbm); 
       HeaderItem.hbm = 0; 
    }
    HeaderCtrl->SetItem(nCol, &HeaderItem);
}
int		CListViewEx::GetVisibleColIndex(int nCol)
{
	return nCol;	// not implemented yet
}

void CListViewEx::OnRButtonDown(UINT nFlags, CPoint point) 
{
	m_dwLastRButtonDown = GetTickCount();
	CListView::OnRButtonDown(nFlags, point);
}

void CListViewEx::OnRButtonUp(UINT nFlags, CPoint point) 
{
	CListView::OnRButtonUp(nFlags, point);
	m_dwLastRButtonDown = 0;
}

BOOL CListViewEx::OnEraseBkgnd(CDC* pDC) 
{
	CRect rc;
    pDC->GetBoundsRect(&rc,0);

    int code=pDC->SaveDC();
    long lCount=GetListCtrl().GetItemCount();
    for(int i=0;i<lCount;i++){
        GetListCtrl().GetItemRect(i,rc,LVIR_BOUNDS);
        pDC->ExcludeClipRect(rc);
    }

    CListView::OnEraseBkgnd(pDC);
    
    pDC->RestoreDC(code);
	return TRUE;

}
