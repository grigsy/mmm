#include "stdafx.h"
#include "UsesOpenSSL.h"

#include <openssl/err.h>
#include <openssl/ssl.h>   


#pragma comment(lib, "ssleay32.lib")
#pragma comment(lib, "libeay32.lib")

static HANDLE *s_pLocks = 0;

enum Threading 
{
   SingleThread,
   MultiThread,
   Unset
} s_threading = Unset;

///////////////////////////////////////////////////////////////////////////////
// Static helper functions...
///////////////////////////////////////////////////////////////////////////////

static void LockingCallback(
   int mode, 
   int type, 
   const char *file, 
   int line);

static bool ThreadingSetup();

static void ThreadingCleanup();

///////////////////////////////////////////////////////////////////////////////
// CUsesOpenSSL
///////////////////////////////////////////////////////////////////////////////

CUsesOpenSSL::CUsesOpenSSL(
   bool multiThreadedUse)
   : m_weOwnLocks(multiThreadedUse ?  ThreadingSetup() : false)
{
   SSL_load_error_strings();
	
   SSL_library_init();
}
      
CUsesOpenSSL::~CUsesOpenSSL()
{
   if (m_weOwnLocks)
   {
      ThreadingCleanup();
   }
   
   ERR_free_strings();
}

///////////////////////////////////////////////////////////////////////////////
// Static helper functions...
///////////////////////////////////////////////////////////////////////////////

static bool ThreadingSetup()
{
   bool ok = false;

   if (!s_pLocks)
   {
   	s_pLocks = (HANDLE *)malloc(CRYPTO_num_locks() * sizeof(HANDLE));

      for (int i = 0; i < CRYPTO_num_locks(); i++)
	   {
         s_pLocks[i] = ::CreateMutex(
            0,                         // Security attributes
            FALSE,                     // Initially owned
            0);                        // name
      }

	   CRYPTO_set_locking_callback(LockingCallback);

      ok = true;
   }
   
   return ok;
}

static void ThreadingCleanup()
{
   if (s_pLocks)
   {
	   CRYPTO_set_locking_callback(0);

	   for (int i = 0; i < CRYPTO_num_locks(); i++)
      {
		   CloseHandle(s_pLocks[i]);
      }

	   free(s_pLocks);

      s_pLocks = 0;
   }
}

static void LockingCallback(
   int mode, 
   int type, 
   const char* /*file*/, 
   int /*line*/)
{
	if (mode & CRYPTO_LOCK)
   {
      ::WaitForSingleObject(s_pLocks[type],INFINITE);
   }
	else
	{
      ::ReleaseMutex(s_pLocks[type]);
	}
}

///////////////////////////////////////////////////////////////////////////////
// End of file
///////////////////////////////////////////////////////////////////////////////
