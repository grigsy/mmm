#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

#include "OpenSSLConnectorBase.h"

class CAsyncConnector : public COpenSSLConnectorBase
{
   public :

      struct IConnectorCallback
      {
         virtual void WriteData(
            BYTE *pData,
            int dataLength) = 0;

         virtual void OnDataAvailable() = 0; 
      };

      CAsyncConnector(
         SSL_CTX *pContext,
         IConnectorCallback &sink);
      
      virtual ~CAsyncConnector();

      size_t Receive(
         BYTE *pData,
         size_t dataLength);

      void Write(
         BYTE *pData,
         size_t dataLength);

      void ReadCompleted(
         BYTE *pData,
         size_t dataLength);


   protected :

      void ProcessData();

   private :


      virtual void GetPendingOperations(
         bool &dataToRead, 
         bool &dataToWrite);

      virtual void PerformRead();

      virtual void PerformWrite() ;

      virtual void OnDataToWrite(
         BYTE *pData,
         size_t dataLength);

      virtual void OnDataToRead(
         BYTE *pData,
         size_t dataLength);

      virtual void GetNextReadDataBuffer(
         BYTE **ppBuffer,
         size_t &bufferSize);

      virtual void GetNextWriteDataBuffer(
         BYTE **ppBuffer,
         size_t &bufferSize);

   public:
      enum { BUFFER_SIZE = 1024 };
   private:
      struct Buffer
      {
         size_t used;
         BYTE data[BUFFER_SIZE];
         Buffer *pNext;

         Buffer(
            BYTE *pData,
            size_t dwDataLength);

         void ConsumeAndRemove(
            size_t bytesUsed);
      };

      struct BufferList
      {
         Buffer *pHead;
         Buffer *pTail;

         BufferList() : pHead(0), pTail(0) {}

         bool Empty()
         {
            return (pHead == 0);
         }
      };

      void UseData(
         BufferList &list,
         Buffer *pBuffer,
         size_t result);

      void AppendBuffer(
         BufferList &list,
         Buffer *pBuffer);

      Buffer *GetNextBuffer(
         BufferList &list);

      void PutbackBuffer(
         BufferList &list,
         Buffer *pBuffer);

      BYTE m_buffer[BUFFER_SIZE];
      
      size_t m_bytesAvailable;

      BufferList m_pendingWriteList;

      BufferList m_pendingReadList;

      IConnectorCallback &m_sink;

      // No copies do not implement
      CAsyncConnector(const CAsyncConnector &rhs);
      CAsyncConnector &operator=(const CAsyncConnector &rhs);
};

///////////////////////////////////////////////////////////////////////////////
// End of file
///////////////////////////////////////////////////////////////////////////////

