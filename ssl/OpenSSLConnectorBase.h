#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

#include <openssl/ssl.h>   

class COpenSSLConnectorBase
{
   public :
      
      void ExpectConnect();

      void ExpectAccept();

   protected :

      COpenSSLConnectorBase(
         SSL_CTX *pContext);

      virtual ~COpenSSLConnectorBase();

      void RunSSL();

      size_t DataToWrite(
         BYTE *pData,
         size_t dataLength);

      size_t DataToRead(
         BYTE *pData,
         size_t dataLength);

   private :

      virtual void GetPendingOperations(
         bool &dataToRead, 
         bool &dataToWrite) = 0;

      virtual void PerformRead() = 0;

      virtual void PerformWrite() = 0;

      virtual void OnDataToWrite(
         BYTE *pData,
         size_t dataLength) = 0;

      virtual void OnDataToRead(
         BYTE *pData,
         size_t dataLength) = 0;

      virtual void GetNextReadDataBuffer(
         BYTE **ppBuffer,
         size_t &bufferSize) = 0;

      virtual void GetNextWriteDataBuffer(
         BYTE **ppBuffer,
         size_t &bufferSize) = 0;

      void SendPendingData();

      void HandleError(
         int result);

      bool m_readRequired;

      SSL *m_pConnection;

      BIO *m_pIn;
      BIO *m_pOut;

      // No copies do not implement
      COpenSSLConnectorBase(const COpenSSLConnectorBase &rhs);
      COpenSSLConnectorBase &operator=(const COpenSSLConnectorBase &rhs);
};


///////////////////////////////////////////////////////////////////////////////
// End of file
///////////////////////////////////////////////////////////////////////////////

