#ifndef LIST_VIEW_EX_INCLUDED
#define LIST_VIEW_EX_INCLUDED
// ListVwEx.h : interface of the CListViewEx class
//
// This class provedes a full row selection mode for the report
// mode list view control.
//
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) 1992-1996 Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

class CListViewEx : public CListView
{
	DECLARE_DYNCREATE(CListViewEx)

// Construction
public:
	CListViewEx();

// Attributes
protected:
	BOOL m_bFullRowSel;

public:
	BOOL SetFullRowSel(BOOL bFillRowSel);
	BOOL GetFullRowSel();

	BOOL m_bClientWidthSel;

	int		GetItemFromPoint(const CPoint& pt, int& nSubItem, CRect* pRect);

// Overrides
protected:
	CFont		m_fntBold;	// for unread msgs
	LOGFONT		m_lfBold;

	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	
	virtual void DrawSubItem(CDC* pDC, int nItem, int nColumn, 
		CRect& rcLabel, int nJustify, BOOL bSelected);
	
	void SetColTitle(int nIdx, LPCTSTR sTitle);
	virtual int GetItemText(int nItem, int nColumn, LPTSTR pBuf, int nLen);
	virtual void GetItemColors(int nItem, COLORREF& clrText, 
								COLORREF& clrBk, BOOL bSelected);
	virtual void GetCustomRect(int nItem,CRect& rcItem, int LVIR_x);

	//{{AFX_VIRTUAL(CListViewEx)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CListViewEx();
#ifdef _DEBUG
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	void RepaintSelectedItems();

// Implementation - client area width
	int m_cxClient;

	BOOL	m_bUseMapping;
	int		GetVisibleColIndex(int nCOl);	// for columns map

// Implementation - state icon width
	int m_cxStateImageOffset;
	afx_msg LRESULT OnSetImageList(WPARAM wParam, LPARAM lParam);

// Implementation - list view colors
	COLORREF m_clrText;
	COLORREF m_clrTextBk;
	COLORREF m_clrBkgnd;
	afx_msg LRESULT OnSetTextColor(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetTextBkColor(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetBkColor(WPARAM wParam, LPARAM lParam);

protected:
	DWORD	m_dwLastRButtonDown;
	//{{AFX_MSG(CListViewEx)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

// helper class for displaying sort mark
class CHeaderColSortMark
{
public:
	CHeaderColSortMark();

	void Init(UINT nUpBmp, UINT nDownBmp);
	void SetSortMark(CListCtrl& pCtrl, int nCol, int nAsc);

protected:
	void	RemoveMark(CListCtrl& pCtrl, int nCol);

	int		m_nColSorted;	// need to know it to remove prev mark
	BOOL	m_bXP;			// XP requires a bit different approach
	UINT	m_nUpImage;
	UINT	m_nDownImage;
};

#endif
