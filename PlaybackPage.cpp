// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// PlaybackPage.cpp : implementation file
//

#include "stdafx.h"
#include "magic.h"
#include "Mailbox.h"
#include "MagicDoc.h"
#include "MagicFrame.h"
#include "MailboxView.h"
#include "PlaybackPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPlaybackPage property page

IMPLEMENT_DYNCREATE(CPlaybackPage, CPropertyPage)

CPlaybackPage::CPlaybackPage() : CPropertyPage(CPlaybackPage::IDD)
{
	m_pMailboxArray = 0;

	//{{AFX_DATA_INIT(CPlaybackPage)
	m_intPlayback = -1;
	m_intPlaybackDevice = -1;
	//}}AFX_DATA_INIT
}

CPlaybackPage::~CPlaybackPage()
{
}

void CPlaybackPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPlaybackPage)
	DDX_Text(pDX, IDC_EDIT_PLAYBACK, m_strPlayback);
	DDX_CBIndex(pDX, IDC_COMBO_PLAYBACK, m_intPlayback);
	DDX_CBIndex(pDX, IDC_COMBO_PLAYBACK_DEVICE, m_intPlaybackDevice);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPlaybackPage, CPropertyPage)
	//{{AFX_MSG_MAP(CPlaybackPage)
	ON_BN_CLICKED(IDC_BROWSE, OnBrowse)
	ON_BN_CLICKED(IDC_TEST, OnTest)
	ON_CBN_SELCHANGE(IDC_COMBO_PLAYBACK, OnSelchangeComboPlayback)
	ON_CBN_SELCHANGE(IDC_COMBO_PLAYBACK_DEVICE, OnModified)
	ON_EN_CHANGE(IDC_EDIT_PLAYBACK, OnModified)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CPlaybackPage::Attach( CArray<CMailbox*> *pMailboxArray )
{
	ASSERT( pMailboxArray );
	m_pMailboxArray = pMailboxArray;
}

BOOL CPlaybackPage::OnInitDialog() 
{
	if( !CPropertyPage::OnInitDialog() ) return FALSE;
	DlgTranslate(this);
	
	if( m_pMailboxArray )
	{
		m_intPlayback = m_pMailboxArray->GetAt( 0 )->m_intPlayback;
		m_intPlaybackDevice = m_pMailboxArray->GetAt( 0 )->m_intPlaybackDevice;
		m_strPlayback = m_pMailboxArray->GetAt( 0 )->m_strPlayback;

		for( int i = m_pMailboxArray->GetUpperBound(); i; --i )
		{
			CMailbox &mbox = *( m_pMailboxArray->GetAt( i ) );
			if( m_intPlayback != mbox.m_intPlayback ) m_intPlayback = ACTION_NULL;
			if( m_intPlaybackDevice != mbox.m_intPlaybackDevice ) m_intPlaybackDevice = PLAYBACK_DEVICE_NULL;
			if( !m_strPlayback.IsEmpty() && ( m_strPlayback != m_pMailboxArray->GetAt( i )->m_strPlayback ) ) m_strPlayback.Empty();
		}
	}
	else
	{
		GetDlgItem( IDC_COMBO_PLAYBACK )->SendMessage( CB_DELETESTRING, (WPARAM) ACTION_SPECIFIC );
		m_intPlayback = theApp.intPlayback;
		m_intPlaybackDevice = theApp.intPlaybackDevice;
		m_strPlayback = theApp.strPlayback;
	}

	UpdateData( FALSE );
	OnSelchangeComboPlayback();
	GetDlgItem( IDC_TEST )->EnableWindow( ACTION_NONE != m_intPlayback );
	SetModified( FALSE );
	return TRUE;
}

BOOL CPlaybackPage::OnApply() 
{
	if( m_pMailboxArray )
	{
		CMailboxView &view = *( (CMailboxView*) ( ( (CMagicFrame*) AfxGetMainWnd() )->GetActiveView() ) );
		CMagicDoc &doc = *( view.GetDocument() );
		view.EnableSort( FALSE );

		for( int i = m_pMailboxArray->GetSize(); i; --i )
		{
			CMailbox &mbox = *( m_pMailboxArray->GetAt( i-1 ) );
			if( ACTION_NULL != m_intPlayback ) mbox.m_intPlayback = m_intPlayback;
			if( ACTION_SPECIFIC == mbox.m_intPlayback )
			{
				if( PLAYBACK_DEVICE_NULL != m_intPlaybackDevice ) mbox.m_intPlaybackDevice = m_intPlaybackDevice;
				if( !m_strPlayback.IsEmpty() ) mbox.m_strPlayback = m_strPlayback;
			}

			doc.SetModifiedFlag();
		}

		view.EnableSort();
	}
	else
	{
		theApp.intPlayback = m_intPlayback;
		theApp.intPlaybackDevice = m_intPlaybackDevice;
		theApp.strPlayback = m_strPlayback;
	}

	GetDlgItem( IDC_TEST )->EnableWindow( ACTION_NONE != m_intPlayback );
	return CPropertyPage::OnApply();
}

/////////////////////////////////////////////////////////////////////////////
// CPlaybackPage message handlers


void CPlaybackPage::OnBrowse() 
{
	MAKE_STRING(strFilter, IDP_PLAYBACK_FILES );
	CFileDialog dlg( TRUE, NULL, NULL, 0, strFilter, this );
	if( IDOK == dlg.DoModal() ) SetDlgItemText( IDC_EDIT_PLAYBACK, dlg.GetPathName() );
}

void CPlaybackPage::OnTest() 
{
	theApp.m_pMainWnd->SendMessage( VM_START_PLAYBACK, 0, 
		LPARAM( m_pMailboxArray ? m_pMailboxArray->GetAt( 0 ) : 0 ) );
}

void CPlaybackPage::OnSelchangeComboPlayback() 
{
	int intEnable = GetDlgItem( IDC_COMBO_PLAYBACK )->SendMessage( CB_GETCURSEL );
	intEnable = ( ACTION_SPECIFIC == intEnable ) || ( ACTION_NULL == intEnable );
	GetDlgItem( IDC_STATIC_DEVICE )->EnableWindow( intEnable );
	GetDlgItem( IDC_COMBO_PLAYBACK_DEVICE)->EnableWindow( intEnable );
	GetDlgItem( IDC_STATIC_PATH )->EnableWindow( intEnable );
	GetDlgItem( IDC_EDIT_PLAYBACK )->EnableWindow( intEnable );
	GetDlgItem( IDC_BROWSE )->EnableWindow( intEnable );
	GetDlgItem( IDC_TEST )->EnableWindow( FALSE );
	SetModified();
}

void CPlaybackPage::OnModified() 
{
	GetDlgItem( IDC_TEST )->EnableWindow( FALSE );
	SetModified();
}
