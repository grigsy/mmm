// DNewImport.cpp : implementation file
//

#include "stdafx.h"
#include "magic.h"
#include "DNewImport.h"
#include "registry.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DNewImport dialog


DNewImport::DNewImport(CWnd* pParent /*=NULL*/)
	: CDialog(DNewImport::IDD, pParent)
{
	//{{AFX_DATA_INIT(DNewImport)
	m_nCur = -1;
	m_nSel = 0;
	//}}AFX_DATA_INIT
}


void DNewImport::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DNewImport)
	DDX_Control(pDX, IDC_LIST, m_lbAccounts);
	DDX_LBIndex(pDX, IDC_LIST, m_nCur);
	DDX_Radio(pDX, IDC_EMPTY, m_nSel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DNewImport, CDialog)
	//{{AFX_MSG_MAP(DNewImport)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_BN_CLICKED(IDC_IMPORT, OnImport)
	ON_BN_CLICKED(IDC_EMPTY, OnEmpty)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DNewImport message handlers

BOOL DNewImport::OnInitDialog() 
{
	CDialog::OnInitDialog();

	DlgTranslate(this);
	
	return TRUE;  
}

void DNewImport::OnOk() 
{
	UpdateData();
	if (m_nSel == 1)	// need import
		GetAccountData(m_nCur);
	CDialog::OnOK();
}

static LPCTSTR asKey = _T("Software\\Microsoft\\Internet Account Manager\\Accounts\\%08x");
void DNewImport::GetAccountList()
{
	m_asAccs.RemoveAll();
	m_anAccs.RemoveAll();
	CRegistry reg;
	for (int i=0; i<64; i++)
	{
		CString sKey;
		sKey.Format(asKey, i);
		CString sServer = reg.GetValueString(HKEY_CURRENT_USER, sKey, "POP3 Server");
		if (sServer.IsEmpty())
			continue;
		CString sName = reg.GetValueString(HKEY_CURRENT_USER, sKey, "Account Name");
		if (sName.IsEmpty())
			continue;
		sKey.Format("%s (%s)", sName, sServer);
		m_asAccs.Add(sKey);
		m_anAccs.Add(i);
	}
}

void DNewImport::GetAccountData(int nKey)
{
	if (nKey<0 || nKey>=m_anAccs.GetSize())
		return;
	CString sKey;
	CRegistry reg;
	sKey.Format(asKey, m_anAccs[nKey]);
	m_sServer = reg.GetValueString(HKEY_CURRENT_USER, sKey, "POP3 Server");
	m_sAlias = reg.GetValueString(HKEY_CURRENT_USER, sKey, "Account Name");
	m_sUser = reg.GetValueString(HKEY_CURRENT_USER, sKey, "POP3 User Name");
	m_nPort = reg.GetValueInt(HKEY_CURRENT_USER, sKey, "POP3 Port");
	if (m_nPort == 0)
		m_nPort = 110;
}

void DNewImport::OnImport() 
{
	GetDlgItem(IDC_LIST)->EnableWindow(TRUE);
	// fill account list
	if (m_lbAccounts.GetCount())
		return;	// already filled
	GetAccountList();
	for (int i=0; i<m_asAccs.GetSize(); i++)
	{
		m_lbAccounts.AddString(m_asAccs[i]);
	}
	m_lbAccounts.SetCurSel(0);
}

void DNewImport::OnEmpty() 
{
	GetDlgItem(IDC_LIST)->EnableWindow(FALSE);
}
