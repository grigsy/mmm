// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// MessagePage.cpp : implementation file
//

#include "stdafx.h"
#include "magic.h"
#include "Mailbox.h"
#include "MagicDoc.h"
#include "MagicFrame.h"
#include "MailboxView.h"
#include "MessagePage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMessagePage property page

IMPLEMENT_DYNCREATE(CMessagePage, CPropertyPage)

CMessagePage::CMessagePage() : CPropertyPage(CMessagePage::IDD)
{
	m_pMailboxArray = 0;

	//{{AFX_DATA_INIT(CMessagePage)
	m_intMessage = 0;
	//}}AFX_DATA_INIT
}

CMessagePage::~CMessagePage()
{
}

void CMessagePage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMessagePage)
	DDX_CBIndex(pDX, IDC_COMBO_MESSAGE, m_intMessage);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMessagePage, CPropertyPage)
	//{{AFX_MSG_MAP(CMessagePage)
	ON_BN_CLICKED(IDC_TEST, OnTest)
	ON_CBN_SELCHANGE(IDC_COMBO_MESSAGE, OnSelchangeComboMessage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CMessagePage::Attach( CArray<CMailbox*> *pMailboxArray )
{
	ASSERT( pMailboxArray );
	m_pMailboxArray = pMailboxArray;
}

BOOL CMessagePage::OnInitDialog() 
{
	if( !CPropertyPage::OnInitDialog() ) return FALSE;

	ASSERT( m_pMailboxArray );
	m_intMessage = m_pMailboxArray->GetAt( 0 )->m_intMessage;

	for( int i = m_pMailboxArray->GetUpperBound(); i; --i )
	{
		CMailbox &mbox = *( m_pMailboxArray->GetAt( i ) );
		if( m_intMessage != mbox.m_intMessage ) m_intMessage = ACTION_NULL;
	}

	UpdateData( FALSE );
	OnSelchangeComboMessage();
	SetModified( FALSE );
	DlgTranslate(this);
	return TRUE;
}

BOOL CMessagePage::OnApply() 
{
	CMailboxView &view = *( (CMailboxView*) ( ( (CMagicFrame*) AfxGetMainWnd() )->GetActiveView() ) );
	CMagicDoc &doc = *( view.GetDocument() );

	for( int i = m_pMailboxArray->GetSize(); i; --i )
	{
		CMailbox &mbox = *( m_pMailboxArray->GetAt( i-1 ) );
		if( ACTION_NULL != m_intMessage ) mbox.m_intMessage = m_intMessage;
		doc.SetModifiedFlag();
	}

	GetDlgItem( IDC_TEST )->EnableWindow( ACTION_SPECIFIC == m_intMessage );
	return CPropertyPage::OnApply();
}

/////////////////////////////////////////////////////////////////////////////
// CMessagePage message handlers


void CMessagePage::OnTest() 
{
	theApp.m_pMainWnd->SendMessage( VM_TEST_MESSAGE, 0, 
		LPARAM( m_pMailboxArray ? m_pMailboxArray->GetAt( 0 ) : 0 ) );
}

void CMessagePage::OnSelchangeComboMessage() 
{
	UpdateData();
	GetDlgItem( IDC_TEST )->EnableWindow( ACTION_SPECIFIC == m_intMessage );
	SetModified();
}
