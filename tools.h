// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef TOOLS_INCLUDED
#define TOOLS_INCLUDED

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

BOOL FindLocalFile(LPCTSTR sFile, CString& sPath, BOOL bCreate = FALSE);
BOOL LoadTextFile(LPCTSTR sFile, CStringArray& asValues, TCHAR cRem = 0, BOOL bCaseSens = FALSE);
BOOL IsStringMatch(LPCTSTR sValue, LPCTSTR sMask);	// with basic support of '*'
BOOL GetTextHandler(CString& sRet, LPCTSTR sFile);
BOOL GetAddressesFrom(CString& sFull, CStringArray& as);
BOOL GetAddressFrom(CString& sFull, CString& sAddr);	// extracts address from Name<addr>
BOOL FindIgnoreCase(LPCTSTR s, LPCTSTR SubS);

DWORD GetDataHash(LPBYTE pData, DWORD dwLen);
BOOL IsEncrypted(LPBYTE pData, DWORD dwSize);
BOOL DecryptData(LPBYTE &pData, DWORD &dwSize, LPCTSTR sPass);
BOOL EncryptData(LPBYTE &pData, DWORD &dwSize, LPCTSTR sPass);

BOOL DoChangeFont(CFont& font, LOGFONT* pLF, BOOL bFixed=FALSE);
void GetFontName(const LOGFONT& lf, CString& s);
BOOL LF2String(LOGFONT& lf, CString& s, BOOL bToString);

class CLog : public CStdioFile
{
public:
	CLog(LPCTSTR filename, int nMaxSize = 0);
	~CLog();
	void AFX_CDECL Log(LPCTSTR text, ...);
protected:
	BOOL	m_bOpen;
	TCHAR m_szText[500];
private:
	CLog(){};	// prevent using such constructor
};


/////////////////////////////////////////////////////////////////////////////
// CColorSample window

class CColorSample : public CStatic
{
// Construction
public:
	CColorSample();
	void SetColor(BOOL bStd, COLORREF rgb=0);

	//{{AFX_VIRTUAL(CColorSample)
	//}}AFX_VIRTUAL

public:
	virtual ~CColorSample();
protected:
	CBrush		m_brSample;
	COLORREF	m_clrText;
	BOOL		m_bStd;
	//{{AFX_MSG(CColorSample)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
BOOL DoAskText(LPCTSTR sTitle, LPCTSTR sTip, CString& sRet);
BOOL DoAskList(LPCTSTR sTitle, LPCTSTR sTip, int& sRet, const CStringArray& asList);
/////////////////////////////////////////////////////////////////////////////

#endif
