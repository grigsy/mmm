// GetWinVer.cpp
//
// Copyright (C) 2001 Hans Dietrich
//
// This software is released into the public domain.  
// You are free to use it in any way you like.
//
// This software is provided "as is" with no expressed 
// or implied warranty.  I accept no liability for any 
// damage or loss of business that this software may cause. 
//
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GetWinVer.h"
#include <shlwapi.h>

// from winbase.h
#ifndef VER_PLATFORM_WIN32s
#define VER_PLATFORM_WIN32s             0
#endif
#ifndef VER_PLATFORM_WIN32_WINDOWS
#define VER_PLATFORM_WIN32_WINDOWS      1
#endif
#ifndef VER_PLATFORM_WIN32_NT
#define VER_PLATFORM_WIN32_NT           2
#endif
#ifndef VER_PLATFORM_WIN32_CE
#define VER_PLATFORM_WIN32_CE           3
#endif


/*
    This table has been assembled from Usenet postings, personal
    observations, and reading other people's code.  Please feel
    free to add to it or correct it.


         dwPlatFormID  dwMajorVersion  dwMinorVersion  dwBuildNumber
95             1              4               0             950
95 SP1         1              4               0        >950 && <=1080
95 OSR2        1              4             <10           >1080
98             1              4              10            1998
98 SP1         1              4              10       >1998 && <2183
98 SE          1              4              10          >=2183
ME             1              4              90            3000

NT 3.51        2              3              51
NT 4           2              4               0            1381
2000           2              5               0            2195
XP             2              5               1

CE             3

*/

///////////////////////////////////////////////////////////////////////////////
// GetWinVer
BOOL GetWinVer(CString& strVersion, int *nVersion)
{
	strVersion = WUNKNOWNSTR;
	*nVersion = WUNKNOWN;

	OSVERSIONINFO osinfo;
	osinfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	if (!GetVersionEx(&osinfo))
		return FALSE;

	DWORD dwPlatformId   = osinfo.dwPlatformId;
	DWORD dwMinorVersion = osinfo.dwMinorVersion;
	DWORD dwMajorVersion = osinfo.dwMajorVersion;
	DWORD dwBuildNumber  = osinfo.dwBuildNumber & 0xFFFF;	// Win 95 needs this
	
	if ((dwPlatformId == VER_PLATFORM_WIN32_WINDOWS) && (dwMajorVersion == 4))
	{
		if ((dwMinorVersion < 10) && (dwBuildNumber == 950))
		{
			strVersion = W95STR;
			*nVersion = W95;
		}
		else if ((dwMinorVersion < 10) && 
				((dwBuildNumber > 950) && (dwBuildNumber <= 1080)))
		{
			strVersion = W95SP1STR;
			*nVersion = W95SP1;
		}
		else if ((dwMinorVersion < 10) && (dwBuildNumber > 1080))
		{
			strVersion = W95OSR2STR;
			*nVersion = W95OSR2;
		}
		else if ((dwMinorVersion == 10) && (dwBuildNumber == 1998))
		{
			strVersion = W98STR;
			*nVersion = W98;
		}
		else if ((dwMinorVersion == 10) && 
				((dwBuildNumber > 1998) && (dwBuildNumber < 2183)))
		{
			strVersion = W98SP1STR;
			*nVersion = W98SP1;
		}
		else if ((dwMinorVersion == 10) && (dwBuildNumber >= 2183))
		{
			strVersion = W98SESTR;
			*nVersion = W98SE;
		}
		else if (dwMinorVersion == 90)
		{
			strVersion = WMESTR;
			*nVersion = WME;
		}
	}
	else if (dwPlatformId == VER_PLATFORM_WIN32_NT)
	{
		if ((dwMajorVersion == 3) && (dwMinorVersion == 51))
		{
			strVersion = WNT351STR;
			*nVersion = WNT351;
		}
		else if ((dwMajorVersion == 4) && (dwMinorVersion == 0))
		{
			strVersion = WNT4STR;
			*nVersion = WNT4;
		}
		else if ((dwMajorVersion == 5) && (dwMinorVersion == 0))
		{
			strVersion = W2KSTR;
			*nVersion = W2K;
		}
		else if ((dwMajorVersion == 5) && (dwMinorVersion == 1))
		{
			strVersion = WXPSTR;
			*nVersion = WXP;
		}
		else if ((dwMajorVersion == 5) && (dwMinorVersion == 2))
		{
			strVersion = W2003STR;
			*nVersion = W2003;
		}
		else if ((dwMajorVersion == 6) && (dwMinorVersion == 0))
		{
			strVersion = WVISTASTR;
			*nVersion = WVISTA;
		}
		else if ((dwMajorVersion == 6))
		{
			strVersion = W7STR;
			*nVersion = W7;
		}
	}
	else if (dwPlatformId == VER_PLATFORM_WIN32_CE)
	{
		strVersion = WCESTR;
		*nVersion = WCE;
	}
	return TRUE;
}

BOOL IsComCtl6()
{

	HMODULE hComCtlDll = LoadLibrary("comctl32.dll");
	if (!hComCtlDll)
		return FALSE;

	typedef HRESULT (CALLBACK *PFNDLLGETVERSION)(DLLVERSIONINFO*);
	PFNDLLGETVERSION pfnDllGetVersion = (PFNDLLGETVERSION)GetProcAddress(hComCtlDll, "DllGetVersion");
	
	BOOL bIsComCtl6 = FALSE;
	if (pfnDllGetVersion)
	{
		DLLVERSIONINFO dvi;
		ZeroMemory(&dvi, sizeof(dvi));
		dvi.cbSize = sizeof(dvi);

		HRESULT hRes = (*pfnDllGetVersion)(&dvi);

		if (SUCCEEDED(hRes) && dvi.dwMajorVersion >= 6)
			bIsComCtl6 = TRUE;
	}

	FreeLibrary(hComCtlDll);
	return bIsComCtl6;
}