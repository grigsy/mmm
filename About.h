#ifndef ABOUT_INCLUDED
#define ABOUT_INCLUDED

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	virtual BOOL OnInitDialog();
	void GetVersionData();
	CString m_strShortVersion;
	CString m_strProductVersion;
	CString m_strComments;
	CString m_strFileDescription;
	CString m_strLegalCopyright;
	CString m_strProductName;
	CString m_strSpecialBuild;

	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


#endif /* ABOUT_INCLUDED */