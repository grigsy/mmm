//
//  Copyright (c) Cail Lomecb (Igor Ruskih) 1999-2000 <ruiv@uic.nnov.ru>
//  You can use, modify, distribute this code or any other part
//  of colorer library in sources or in binaries only according
//  to Colorer License (see /doc/license.txt for more information).
//
#include "stdafx.h"
#include "cregexp.h"

//Up: /[A-Z \x80-\x9f \xf0 ]/x
//Lo: /[a-z \xa0-\xaf \xe0-\xef \xf1 ]/x
//Wd: /[\d _ A-Z a-z \xa0-\xaf \xe0-\xf1 \x80-\x9f]/x
/*   // koi8
SCharData UCData  = {0x0, 0x0, 0x7fffffe, 0x0, 0x0, 0x80000, 0x0, 0xffffffff},
          LCData  = {0x0, 0x0, 0x0, 0x7fffffe, 0x0, 0x8, 0xffffffff, 0x0},
          WdData  = {0x0, 0x3ff0000, 0x87fffffe, 0x7fffffe, 0x0, 0x80008, 0xffffffff, 0xffffffff},
          DigData = {0x0, 0x3ff0000, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0};
//*/
//*/ dos866
SCharData UCData  = {0x0, 0x0, 0x7fffffe, 0x0, 0xffffffff, 0x0, 0x0, 0x10000},
          LCData  = {0x0, 0x0, 0x0, 0x7fffffe, 0x0, 0xffff, 0x0, 0x2ffff},
          WdData  = {0x0, 0x3ff0000, 0x87fffffe, 0x7fffffe, 0xffffffff, 0xffff, 0x0, 0x3ffff},
          DigData = {0x0, 0x3ff0000, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0};
/*/   // cp1251
SCharData UCData  = {0x0, 0x0, 0x7fffffe, 0x0, 0x0, 0x100, 0xffffffff, 0x0},
          LCData  = {0x0, 0x0, 0x0, 0x7fffffe, 0x0, 0x1000000, 0x0, 0xffffffff},
          WdData  = {0x0, 0x3ff0000, 0x87fffffe, 0x7fffffe, 0x0, 0x1000100, 0xffffffff, 0xffffffff},
          DigData = {0x0, 0x3ff0000, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0};
//*/


/////////////////////////////////////////////////////////////////////////////
/*
void *operator new(size_t sz)
{
  return malloc(sz);
};

void operator delete(void *v)
{
  free(v);
};

void *operator new[](size_t sz)
{
  return malloc(sz);
};
void operator delete[](void *v)
{
  free(v);
};
//*/

bool inline IsDigit(char c)
{
  return DigData.GetBit(c);
};
bool inline IsWord(char c)
{
  return WdData.GetBit(c);
};
bool inline IsUpperCase(char c)
{
  return UCData.GetBit(c);
};
bool inline IsLowerCase(char c)
{
  return LCData.GetBit(c);
};
char inline LowCase(char c)
{
  if (UCData.GetBit(c))
    return c + 0x20;
  return c;
};

int GetNumber(int *str, int s, int e)
{
int r = 1, num = 0;
  if (e < s) return -1;
  for(int i = e-1; i >= s; i--){
    if (str[i] > '9' || str[i] < '0') return -1;
    num += (str[i] - 0x30)*r;
    r *= 10;
  };
  return num;
};
int GetHex(char c)
{
  c = LowCase(c);
  c -= 0x30;
  if (c >= 0x31 && c <= 0x36) c -= 0x27;
  else if (c < 0 || c > 9) return -1;
  return c;
};

int IsAccent(char c, int nType)
{
	char chI[] = {'i', 'I', '1', 'l', '|'};
	if (nType == ReMultiI)
	{
		for (int i=0; i<sizeof(chI); i++)
		{
			if (c == chI[i])
				return 1;
		}
		if (c>=0xCC && c<=0xCF)
			return 1;
		if (c>=0xEC && c<=0xEF)
			return 1;
		return -1;
	}
	int chA[] = {0xC0, 0xC5, 0xE0, 0xE5};
	int chE[] = {0xC8, 0xCB, 0xE8, 0xEB};
	int chU[] = {0xD9, 0xDC, 0xF9, 0xFC};
	int chO[] = {0xD2, 0xD6, 0xF2, 0xF6};
	int* chTest = NULL;
	switch (nType)
	{
		case ReMultiA:
			if (c == 'a' || c=='A')
				return 1;
			chTest = chA;
			break;
		case ReMultiE:
			if (c == 'e' || c=='E')
				return 1;
			chTest = chE;
			break;
		case ReMultiU:
			if (c == 'u' || c=='U')
				return 1;
			chTest = chU;
			break;
		case ReMultiO:
			if (c == 'o' || c=='O')
				return 1;
			chTest = chO;
			break;
		default:
			return -1;
	}
	if (c>=chTest[0] && c<=chTest[1])
		return 1;
	if (c>=chTest[2] && c<=chTest[3])
		return 1;
	return -1;
}

int GetAccent(char c)
{
	switch (c)
	{
	case 'a':
		return ReMultiA;
	case 'o':
		return ReMultiO;
	case 'u':
		return ReMultiU;
	case 'e':
		return ReMultiE;
	case 'i':
		return ReMultiI;
	}
	return -1;
}
/////////////////////////////////////////////////////////////////////////////
//

SRegInfo::SRegInfo()
{
  Next = Parent = 0;
  un.Param = 0;
  Op = ReEmpty;
};
SRegInfo::~SRegInfo()
{
  if (Next) delete Next;
  if (un.Param)
    switch(Op){
      case ReEnum:
      case ReNEnum:
        delete un.ChrClass;
        break;
      default:
        if (Op > ReBlockOps && Op < ReSymbolOps || Op == ReBrackets)
          delete un.Param;
        break;
    };
};

////////////////////////////////////////////////////////////////////////////
// bits
void SCharData::SetBit(unsigned char Bit)
{
  int p = Bit/8;
  CArr[p] |= (1 << Bit%8);
};
void SCharData::ClearBit(unsigned char Bit)
{
  int p = Bit/8;
  CArr[p] &= ~(1 << Bit%8);
};
bool SCharData::GetBit(unsigned char Bit)
{
  int p = (unsigned char)Bit/8;
  return (CArr[p] & (1 << Bit%8))!=0;
};

////////////////////////////////////////////////////////////////////////////
// regexp class
CRegExp::CRegExp()
{
  Info = 0;
  Exprn = 0;
  NoMoves = false;
  Error = EERROR;
  FirstChar = 0;
  CurMatch = 0;
  CodePage = 0;
};
CRegExp::CRegExp(char *Text)
{
  Info = 0;
  Exprn = 0;
  NoMoves = false;
  Error = EERROR;
  FirstChar = 0;
  CurMatch = 0;
  CodePage = 0;
  if (Text) SetExpr(Text);
};
CRegExp::~CRegExp()
{
  if (Info) delete Info;
};

bool CRegExp::SetExpr(LPCSTR Expr)
{
  if (!this) return false;
  Error = EERROR;
  CurMatch = 0;
  Error = SetExprLow(Expr);
  return Error == EOK;
};
bool CRegExp::isok()
{
  return Error == EOK;
};
EError CRegExp::geterror()
{
  return Error;
};

EError CRegExp::SetExprLow(const char *Expr)
{
int  EnterBr = 0, EnterGr = 0, EnterFg = 0;
int  pos, tmp, i, j, s = 0;
bool Ok = false;
int  Len = 0;

  while (Expr[Len]) Len++;
  if (!Len) return EERROR;
  if (Info) delete Info;
  Info = new SRegInfo;
  Exprn = new int[Len];

  NoCase = false;
  Extend = false;
  if (Expr && Expr[0] == '/') s++;
  else return ESYNTAX;

  for (i = Len; i > 0 && !Ok;i--)
    if (Expr[i] == '/'){
      Len = i-s;
      Ok = true;
      for (int j = i+1; Expr[j]; j++){
        if (Expr[j] == 'i') NoCase = true;
        if (Expr[j] == 'x') Extend = true;
      };
    };
  if (!Ok) return ESYNTAX;

  //
  for (j = 0,pos = 0; j < Len; j++,pos++)
  {
    if (Extend && (Expr[j+s] == ' ' || Expr[j+s] == '\n'|| Expr[j+s] == '\r'))
	{
      pos--;
      continue;
    };
    Exprn[pos] = (int)(unsigned char)Expr[j+s];
    if (Expr[j+s] == BACKSLASH){
      switch (Expr[j+s+1]){
        case 'A':
			tmp = GetAccent(Expr[j+s+2]);
			if (tmp==-1)
				return ESYNTAX;
			Exprn[pos] = tmp;
			j++;
          break;
        case 'd':
          Exprn[pos] = ReDigit;
          break;
        case 'D':
          Exprn[pos] = ReNDigit;
          break;
        case 'w':
          Exprn[pos] = ReWordSymb;
          break;
        case 'W':
          Exprn[pos] = ReNWordSymb;
          break;
        case 's':
          Exprn[pos] = ReWSpace;
          break;
        case 'S':
          Exprn[pos] = ReNWSpace;
          break;
        case 'u':
          Exprn[pos] = ReUCase;
          break;
        case 'l':
          Exprn[pos] = ReNUCase;
          break;
        case 't':
          Exprn[pos] = '\t';
          break;
        case 'n':
          Exprn[pos] = '\n';
          break;
        case 'r':
          Exprn[pos] = '\r';
          break;
        case 'b':
          Exprn[pos] = ReWBound;
          break;
        case 'B':
          Exprn[pos] = ReNWBound;
          break;
        case 'c':
          Exprn[pos] = RePreNW;
          break;
        case 'm':
          Exprn[pos] = ReStart;
          break;
        case 'M':
          Exprn[pos] = ReEnd;
          break;
        case 'x':
          tmp = GetHex(Expr[j+s+2]);
          if (tmp == -1 || GetHex(Expr[j+s+3]) == -1) return ESYNTAX;
          tmp = (tmp<<4) + GetHex(Expr[j+s+3]);
          Exprn[pos] = tmp;
          j += 2;
          break;
        case 'y':
          tmp = GetHex(Expr[j+s+2]);
          if (tmp == -1) return ESYNTAX;
          Exprn[pos] = ReBkTrace + tmp;
          j++;
          break;
        default:
          tmp = GetHex(Expr[j+s+1]);
          if (tmp != -1){
            Exprn[pos] = ReBkBrack + tmp;
            break;
          }else
            Exprn[pos] = Expr[j+s+1];
          break;
      };
      j++;
      continue;
    };
    if (Expr[j+s] == ']'){
      Exprn[pos] = ReEnumE;
      if (EnterFg || !EnterGr) return EBRACKETS;
      EnterGr--;
    };
    if (Expr[j+s] == '-' && EnterGr) Exprn[pos] = ReFrToEnum;

    if (EnterGr) continue;

    if (Expr[j+s] == '[' && Expr[j+s+1] == '^'){
      Exprn[pos] = ReNEnumS;
      if (EnterFg) return EBRACKETS;
      EnterGr++;
      j++;
      continue;
    };
    if (Expr[j+s] == '*' && Expr[j+s+1] == '?'){
      Exprn[pos] = ReNGMul;
      j++;
      continue;
    };
    if (Expr[j+s] == '+' && Expr[j+s+1] == '?'){
      Exprn[pos] = ReNGPlus;
      j++;
      continue;
    };
    if (Expr[j+s] == '?' && Expr[j+s+1] == '?'){
      Exprn[pos] = ReNGQuest;
      j++;
      continue;
    };
    if (Expr[j+s] == '?' && Expr[j+s+1] == '#' &&
        Expr[j+s+2]>='0' && Expr[j+s+2]<='9'){
      Exprn[pos] = ReBehind+Expr[j+s+2]-0x30;
      j+=2;
      continue;
    };
    if (Expr[j+s] == '?' && Expr[j+s+1] == '~' &&
        Expr[j+s+2]>='0' && Expr[j+s+2]<='9'){
      Exprn[pos] = ReNBehind+Expr[j+s+2]-0x30;
      j+=2;
      continue;
    };
    if (Expr[j+s] == '?' && Expr[j+s+1] == '='){
      Exprn[pos] = ReAhead;
      j++;
      continue;
    };
    if (Expr[j+s] == '?' && Expr[j+s+1] == '!'){
      Exprn[pos] = ReNAhead;
      j++;
      continue;
    };

    if (Expr[j+s] == '('){
      Exprn[pos] = ReLBrack;
      if (EnterFg) return EBRACKETS;
      EnterBr++;
    };
    if (Expr[j+s] == ')'){
      Exprn[pos] = ReRBrack;
      if (!EnterBr || EnterFg) return EBRACKETS;
      EnterBr--;
    };
    if (Expr[j+s] == '['){
      Exprn[pos] = ReEnumS;
      if (EnterFg) return EBRACKETS;
      EnterGr++;
    };
    if (Expr[j+s] == '{'){
      Exprn[pos] = ReRangeS;
      if (EnterFg) return EBRACKETS;
      EnterFg++;
    };
    if (Expr[j+s] == '}' && Expr[j+s+1] == '?'){
      Exprn[pos] = ReNGRangeE;
      if (!EnterFg) return EBRACKETS;
      EnterFg--;
      j++;
      continue;
    };
    if (Expr[j+s] == '}'){
      Exprn[pos] = ReRangeE;
      if (!EnterFg) return EBRACKETS;
      EnterFg--;
    };

    if (Expr[j+s] == '^') Exprn[pos] = ReSoL;
    if (Expr[j+s] == '$') Exprn[pos] = ReEoL;
    if (Expr[j+s] == '.') Exprn[pos] = ReAnyChr;
    if (Expr[j+s] == '*') Exprn[pos] = ReMul;
    if (Expr[j+s] == '+') Exprn[pos] = RePlus;
    if (Expr[j+s] == '?') Exprn[pos] = ReNGQuest;//ReQuest;
    if (Expr[j+s] == '|') Exprn[pos] = ReOr;
  };
  if (EnterGr || EnterBr || EnterFg) return EBRACKETS;

  Info->Op = ReBrackets;
  Info->un.Param = new SRegInfo;
  Info->s = CurMatch++;

  EError err = SetStructs(Info->un.Param,0,pos);
  delete Exprn;
  if (err) return err;
  Optimize();
  return EOK;
};

void CRegExp::Optimize()
{
PRegInfo Next = Info;
  FirstChar = 0;
  while(Next){
    if (Next->Op == ReBrackets || Next->Op == RePlus  || Next->Op == ReNGPlus){
      Next = Next->un.Param;
      continue;
    };
    if (Next->Op == ReSymb){
      if (Next->un.Symb & 0xFF00 &&  Next->un.Symb != ReSoL && Next->un.Symb != ReWBound)
        break;
      FirstChar = Next->un.Symb;
      break;
    };
    break;
  };
};

EError CRegExp::SetStructs(PRegInfo &re,int start,int end)
{
PRegInfo Next,Prev,Prev2;
int comma,st,en,ng,i, j,k;
int EnterBr;
bool Add;

  if (end - start < 0) return EERROR;
  Next = re;
  for (i = start; i < end; i++){
    Add = false;
    // Ops
    if (Exprn[i] > ReBlockOps && Exprn[i] < ReSymbolOps){
      Next->un.Param = 0;
      Next->Op = (EOps)Exprn[i];
      Add = true;
    };
    // {n,m}
    if (Exprn[i] == ReRangeS){
      st = i;
      en = -1;
      comma = -1;
      ng = 0;
      for (j = i;j < end;j++){
        if (Exprn[j] == ReNGRangeE){
          en = j;
          ng = 1;
          break;
        };
        if (Exprn[j] == ReRangeE){
          en = j;
          break;
        };
        if ((char)Exprn[j] == ',')
          comma = j;
      };
      if (en == -1) return EBRACKETS;
      if (comma == -1) comma = en;
      Next->s = GetNumber(Exprn,st+1,comma);
      if (comma != en)
        Next->e = GetNumber(Exprn,comma+1,en);
      else
        Next->e = Next->s;
      Next->un.Param = 0;
      Next->Op = ng?ReNGRangeNM:ReRangeNM;
      if (en-comma == 1){
        Next->e = -1;
        Next->Op = ng?ReNGRangeN:ReRangeN;
      };
      i=j;
      Add = true;
    };
    // [] [^]
    if (Exprn[i] == ReEnumS || Exprn[i] == ReNEnumS){
      Next->Op = (Exprn[i] == ReEnumS)?ReEnum:ReNEnum;
      for (j = i+1;j < end;j++){
        if (Exprn[j] == ReEnumE)
          break;
      };
      if (j == end) return EBRACKETS;
      Next->un.ChrClass = new SCharData;

      for(k = 0; k < 8; k++)
        Next->un.ChrClass->IArr[k] = 0x0;

      for (j = i+1;Exprn[j] != ReEnumE;j++){
        if (Exprn[j+1] == ReFrToEnum){
          for (i = (Exprn[j]&0xFF); i < (Exprn[j+2]&0xFF);i++)
            Next->un.ChrClass->SetBit(i&0xFF);
          j++;
          continue;
        };
        switch(Exprn[j])
		{
		case ReMultiA: case ReMultiE:
		case ReMultiO: case ReMultiU:
		case ReMultiI:
            for (k = 0x41;k < 256;k++)
              if (IsAccent((char)k, Exprn[j]))
                Next->un.ChrClass->SetBit(k);
			break;

          case ReDigit:
            for (k = 0x30;k < 0x40;k++)
              if (IsDigit((char)k))
                Next->un.ChrClass->SetBit(k);
            break;
          case ReNDigit:
            for (k = 0x30;k < 0x40;k++)
              if (!IsDigit((char)k))
                Next->un.ChrClass->SetBit(k);
            break;
          case ReWordSymb:
            for (k = 0;k < 256;k++)
              if (IsWord((char)k))
                Next->un.ChrClass->SetBit(k);
            break;
          case ReNWordSymb:
            for (k = 0;k < 256;k++)
              if (!IsWord((char)k))
                Next->un.ChrClass->SetBit(k);
            break;
          case ReWSpace:
            Next->un.ChrClass->SetBit(0x20);
            // tab also!
            Next->un.ChrClass->SetBit(0x9);
            break;
          case ReNWSpace:
            for(k = 0; k < 8; k++)
              Next->un.ChrClass->IArr[k] = 0xFFFFFFFF;
            Next->un.ChrClass->ClearBit(0x20);
            Next->un.ChrClass->ClearBit(0x9);
            break;
          default:
            if (!(Exprn[j]&0xFF00))
              Next->un.ChrClass->SetBit(Exprn[j]&0xFF);
            break;
        };
      };
      Add = true;
      i=j;
    };
    // ( ... )
    if (Exprn[i] == ReLBrack){
      EnterBr = 1;
      for (j = i+1;j < end;j++){
        if (Exprn[j] == ReLBrack) EnterBr++;
        if (Exprn[j] == ReRBrack) EnterBr--;
        if (!EnterBr) break;
      };
      if (EnterBr) return EBRACKETS;
      Next->Op = ReBrackets;
      Next->un.Param = new SRegInfo;
      Next->un.Param->Parent = Next;
      Next->s = CurMatch++;
      if (CurMatch > MATCHESNUM) CurMatch = MATCHESNUM;
      EError er = SetStructs(Next->un.Param,i+1,j);
      if (er) return er;
      Add = true;
      i = j;
    };
    if ((Exprn[i]&0xFF00) == ReBkTrace){
      Next->Op = ReBkTrace;
      Next->un.Symb = Exprn[i]&0xFF;
      Add = true;
    };
    if ((Exprn[i]&0xFF00) == ReBkBrack){
      Next->Op = ReBkBrack;
      Next->un.Symb = Exprn[i]&0xFF;
      Add = true;
    };
    if ((Exprn[i]&0xFF00) == ReBehind){
      Next->Op = ReBehind;
      Next->s = Exprn[i]&0xFF;
      Add = true;
    };
    if ((Exprn[i]&0xFF00) == ReNBehind){
      Next->Op = ReNBehind;
      Next->s = Exprn[i]&0xFF;
      Add = true;
    };
    // Chars
    if (Exprn[i] >= ReAnyChr && Exprn[i] < ReTemp || Exprn[i] < 0x100){
      Next->Op = ReSymb;
      Next->un.Symb = Exprn[i];
      Add = true;
    };
    // Next
    if (Add && i != end-1){
      Next->Next = new SRegInfo;
      Next->Next->Parent = Next->Parent;
      Next = Next->Next;
    };
  };
  Next = re;
  Prev = Prev2 = 0;
  while(Next){
    if (Next->Op > ReBlockOps && Next->Op < ReSymbolOps){
      if (!Prev) return EOP;
      if (!Prev2) re = Next;
        else Prev2->Next = Next;
      //if (Prev->Op > ReBlockOps && Prev->Op < ReSymbolOps) return false;
      Prev->Parent = Next;
      Prev->Next = 0;
      Next->un.Param = Prev;
      Prev = Prev2;
    };
    Prev2 = Prev;
    Prev = Next;
    Next = Next->Next;
  };

  return EOK;
};

////////////////////////////////////////////////////////////////////////////
// parsing

bool CRegExp::CheckSymb(int Symb, bool Inc)
{
bool Res;
  switch(Symb){
	case ReMultiA: case ReMultiE:
	case ReMultiO: case ReMultiU:
	case ReMultiI:
      Res = IsAccent(cptrans(*toParse), Symb);
      if (Res && Inc) toParse++;
      return Res;
    case ReAnyChr:
      if (toParse >= End) return false;
      if (Inc) toParse++;
      return true;
    case ReSoL:
      return (Start == toParse);
    case ReEoL:
      return (End == toParse);
    case ReDigit:
      if (toParse >= End) return false;
      Res = IsDigit(cptrans(*toParse));
      if (Res && Inc) toParse++;
      return Res;
    case ReNDigit:
      if (toParse >= End) return false;
      Res = !IsDigit(cptrans(*toParse));
      if (Res && Inc) toParse++;
      return Res;
    case ReWordSymb:
      if (toParse >= End) return false;
      Res = IsWord(cptrans(*toParse));
      if (Res && Inc) toParse++;
      return Res;
    case ReNWordSymb:
      if (toParse >= End) return false;
      Res = !IsWord(cptrans(*toParse));
      if (Res && Inc) toParse++;
      return Res;
    case ReWSpace:
      if (toParse >= End) return false;
      Res = (*toParse == 0x20 || *toParse == '\t');
      if (Res && Inc) toParse++;
      return Res;
    case ReNWSpace:
      if (toParse >= End) return false;
      Res = !(*toParse == 0x20 || *toParse == '\t');
      if (Res && Inc) toParse++;
      return Res;
    case ReUCase:
      if (toParse >= End) return false;
      Res = IsUpperCase(cptrans(*toParse));
      if (Res && Inc) toParse++;
      return Res;
    case ReNUCase:
      if (toParse >= End) return false;
      Res = IsLowerCase(cptrans(*toParse));
      if (Res && Inc) toParse++;
      return Res;
    case ReWBound:
      if (toParse >= End) return true;
      return IsWord(cptrans(*toParse)) && (toParse == Start || !IsWord(cptrans(*(toParse - 1))));
    case ReNWBound:
      if (toParse >= End) return true;
      return !IsWord(cptrans(*toParse)) && IsWord(cptrans(*(toParse-1)));
    case RePreNW:
      if (toParse >= End) return true;
      return (toParse == Start || !IsWord(cptrans(*(toParse-1))));
    case ReStart:
      Matches->s[0] = (toParse - Start);
      return true;
    case ReEnd:
      Matches->e[0] = (toParse - Start);
      return true;
    default:
      if ((Symb & 0xFF00) || toParse >= End) return false;
      if (NoCase){
        if (LowCase(cptrans(*toParse)) != LowCase(cptrans((char)Symb&0xFF))) return false;
      }else
        if (*toParse != (char)(Symb&0xFF)) return false;
      if (Inc) toParse++;
      return true;
  };
}

bool CRegExp::LowParseRe(PRegInfo &Next)
{
PRegInfo OrNext;
int i,match,sv;
char *tStr;

  switch(Next->Op){
    case ReSymb:
      if (!CheckSymb(Next->un.Symb,true)) return false;
      break;
    case ReEmpty:
      break;
    case ReBkTrace:
      if (!bkstr | !bktrace) return false;
      sv = Next->un.Symb;
      tStr = toParse;
      for (i = bktrace->s[sv]; i < bktrace->e[sv]; i++){
        if (*tStr != bkstr[i] || End == tStr) return false;
        tStr++;
      };
      toParse = tStr;
      break;
    case ReBkBrack:
      sv = Next->un.Symb;
      tStr = toParse;
      if (Matches->s[sv] == -1 || Matches->e[sv] == -1) return false;
      for (i = Matches->s[sv]; i < Matches->e[sv]; i++){
        if (*tStr != Start[i] || End == tStr) return false;
        tStr++;
      };
      toParse = tStr;
      break;
    case ReBehind:
      sv = Next->s;
      tStr = toParse;
      toParse -= sv;
      if (!LowParse(Next->un.Param)) return false;
      toParse = tStr;
      break;
    case ReNBehind:
      sv = Next->s;
      tStr = toParse;
      toParse -= sv;
      if (LowParse(Next->un.Param)) return false;
      toParse = tStr;
      break;
    case ReAhead:
      tStr = toParse;
      if (!LowParse(Next->un.Param)) return false;
      toParse = tStr;
      break;
    case ReNAhead:
      tStr = toParse;
      if (LowParse(Next->un.Param)) return false;
      toParse = tStr;
      break;
    case ReEnum:
      if (toParse >= End) return false;
      if (!Next->un.ChrClass->GetBit(*toParse)) return false;
      toParse++;
      break;
    case ReNEnum:
      if (toParse >= End) return false;
      if (Next->un.ChrClass->GetBit(*toParse)) return false;
      toParse++;
      break;
    case ReBrackets:
      match = Next->s;
      sv = toParse-Start;
      tStr = toParse;
      if (LowParse(Next->un.Param)){
        if (match < MATCHESNUM && (match || (Matches->s[match] == -1)))
          Matches->s[match] = sv;
        if (match < MATCHESNUM && (match || (Matches->e[match] == -1)))
          Matches->e[match] = toParse-Start;
        return true;
      };
      toParse = tStr;
      return false;
    case ReMul:
      tStr = toParse;
      while (LowParse(Next->un.Param));
      while(!LowCheckNext(Next) && tStr < toParse) toParse--;
      break;
    case ReNGMul:
      do{
        if (LowCheckNext(Next)) break;
      }while (LowParse(Next->un.Param));
      break;
    case RePlus:
      tStr = toParse;
      match = false;
      while (LowParse(Next->un.Param))
        match = true;
      if (!match) return false;
      while(!LowCheckNext(Next) && tStr < toParse) toParse--;
      break;
    case ReNGPlus:
      if (!LowParse(Next->un.Param)) return false;
      do{
        if (LowCheckNext(Next)) break;
      }while (LowParse(Next->un.Param));
      break;
    case ReQuest:
      LowParse(Next->un.Param);
      break;
    case ReNGQuest:
      if (LowCheckNext(Next)) break;
      if (!LowParse(Next->un.Param)) return false;
      break;
    case ReOr:
      OrNext = Next;
      // tStr = toParse;
      if (LowParse(Next->un.Param)){
        while (OrNext && OrNext->Op == ReOr)
          OrNext = OrNext->Next;
        /*if (!LowCheckNext(OrNext)){
          toParse = tStr;
          OrNext = Next;
        };*/
      };
      Next = OrNext;
      break;
    case ReRangeN:
      tStr = toParse;
      i = 0;
      while (LowParse(Next->un.Param)) i++;
      do{
        if (i < Next->s){
          toParse = tStr;
          return false;
        };
        i--;
      }while(!LowCheckNext(Next) && tStr < toParse--);
      break;
    case ReNGRangeN:
      tStr = toParse;
      i = 0;
      while (LowParse(Next->un.Param)){
        i++;
        if (i >= Next->s && LowCheckNext(Next))
          break;
      };
      if (i < Next->s){
        toParse = tStr;
        return false;
      };
      break;
    case ReRangeNM:
      tStr = toParse;
      i = 0;
      while (i < Next->s && LowParse(Next->un.Param))
        i++;
      if (i < Next->s){
        toParse = tStr;
        return false;
      };
      while (i < Next->e && LowParse(Next->un.Param))
        i++;

      while(!LowCheckNext(Next)){
        i--;
        toParse--;
        if (i < Next->s){
          toParse = tStr;
          return false;
        };
      };
      break;
    case ReNGRangeNM:
      tStr = toParse;
      i = 0;
      while (i < Next->s && LowParse(Next->un.Param))
        i++;
      if (i < Next->s){
        toParse = tStr;
        return false;
      };
      while(!LowCheckNext(Next)){
        i++;
        if (!LowParse(Next->un.Param) || i > Next->e){
          toParse = tStr;
          return false;
        };
      };
      break;
  };
  return true;
};

bool CRegExp::LowCheckNext(PRegInfo Re)
{
PRegInfo Next;
char *tmp = toParse;
Next = Re;
  do{
    if (Next && Next->Op == ReOr)
      while (Next && Next->Op == ReOr)
        Next = Next->Next;
    if (Next->Next && !LowParse(Next->Next)){
      toParse = tmp;
//      Ok = false;
      return false;
    };
    Next = Next->Parent;
  }while(Next);
  toParse = tmp;
//  if (Ok != false) Ok = true;
  return true;
};

bool CRegExp::LowParse(PRegInfo Re)
{
  while(Re && toParse <= End){
    if (!LowParseRe(Re)) return false;
    if (Re) Re = Re->Next;
  };
  return true;
};

bool CRegExp::QuickCheck()
{
  if (!NoMoves || !FirstChar)
    return true;
  switch(FirstChar){
    case ReSoL:
      if (toParse != Start) return false;
      return true;
    case ReWBound:
      return IsWord(cptrans(*toParse)) && (toParse == Start || !IsWord(cptrans(*(toParse-1))));
    default:
      if (NoCase && LowCase(cptrans(*toParse)) != LowCase(cptrans(FirstChar))) return false;
      if (!NoCase && *toParse != (char)FirstChar) return false;
      return true;
  };
};

bool CRegExp::ParseRe(char *Str)
{
  if (Error) return false;

  toParse = Str;
  if (!QuickCheck()) return false;

  for (int i = 0; i < MATCHESNUM; i++)
    Matches->s[i] = Matches->e[i] = -1;
  Matches->CurMatch = CurMatch;

//  Ok = -1;
  do{
    if (!LowParse(Info)){
      if (NoMoves) return false;
    }else
      return true;
    toParse = ++Str;
  }while(toParse != End+1);
  return false;
};

bool CRegExp::Parse(char *Str,char *Sol, char *Eol, PMatches Mtch, int Moves)
{
  if (!this) return false;

  bool s = NoMoves;
  if (Moves != -1) NoMoves = Moves!=0;
  Start = Sol;
  End   = Eol;
  Matches = Mtch;
  bool r = ParseRe(Str);
  NoMoves = s;
  return r;
};

bool CRegExp::Parse(char *Str, PMatches Mtch)
{
  if (!this) return false;
  End = Start = Str;
  while(*End) End++;
  Matches = Mtch;
  return ParseRe(Str);
};

bool CRegExp::SetNoMoves(bool Moves)
{
  NoMoves = Moves;
  return true;
};

bool CRegExp::SetBkTrace(char *str, PMatches trace)
{
  bktrace = trace;
  bkstr = str;
  return true;
};
bool CRegExp::GetBkTrace(char **str, PMatches *trace)
{
  *str = bkstr;
  *trace = bktrace;
  return true;
};

char inline CRegExp::cptrans(char c)
{
  return CodePage?CodePage[(unsigned char)c]:c;
};

void CRegExp::SetCodePage(char* Table)
{
  CodePage = Table;
};
