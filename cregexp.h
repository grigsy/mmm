//
//  Copyright (c) Cail Lomecb (Igor Ruskih) 1999-2000 <ruiv@uic.nnov.ru>
//  You can use, modify, distribute this code or any other part
//  of colorer library in sources or in binaries only according
//  to Colorer License (see /doc/license.txt for more information).
//
#ifndef __CREGEXP__
#define __CREGEXP__

#define MATCHESNUM 0x10

enum EOps
{
  ReBlockOps = 0x1000,
  ReMul,              // *
  RePlus,             // +
  ReQuest,            // ?
  ReNGMul,            // *?
  ReNGPlus,           // +?
  ReNGQuest,          // ??
  ReRangeN,           // {n,}
  ReRangeNM,          // {n,m}
  ReNGRangeN,         // {n,}?
  ReNGRangeNM,        // {n,m}?
  ReOr,               // |
  ReBehind  = 0x1100, // ?#n
  ReNBehind = 0x1200, // ?~n
  ReAhead   = 0x1300, // ?=
  ReNAhead  = 0x1400, // ?!

  ReSymbolOps = 0x2000,
  ReEmpty,
  ReSymb,             // a b \W \s ...
  ReEnum,             // []
  ReNEnum,            // [^]
  ReBrackets,         // (...)
  ReBkTrace = 0x2100, // \yN
  ReBkBrack = 0x2200 // \N
};

enum ESymbols
{
  ReAnyChr = 0x4000,  // .
  ReSoL,              // ^
  ReEoL,              // $
  ReDigit,            // \d
  ReNDigit,           // \D
  ReWordSymb,         // \w
  ReNWordSymb,        // \W
  ReWSpace,           // \s
  ReNWSpace,          // \S
  ReUCase,            // \u
  ReNUCase ,          // \l
  ReWBound,           // \b
  ReNWBound,          // \B
  RePreNW,            // \c
  ReStart,            // \m
  ReEnd,              // \M
  ReMultiA,           // \Aa variations of A
  ReMultiE,           // \Ae
  ReMultiU,           // \Au
  ReMultiO,           // \Ao
  ReMultiI,           // \Ai spam variations of i

  ReChr    = 0x0      // Char in Lower Byte
};
enum ETempSymb
{
  ReTemp = 0x7000,
  ReLBrack, ReRBrack,
  ReEnumS, ReEnumE, ReNEnumS,
  ReRangeS, ReRangeE, ReNGRangeE, ReFrToEnum
};
enum EError
{
  EOK = 0, EERROR, ESYNTAX, EBRACKETS, EOP
};

#define BACKSLASH '\\'

typedef union SCharData
{
  long IArr[8];  // must be 32 bytes!!!
  char CArr[32];
  void SetBit(unsigned char Bit);
  void ClearBit(unsigned char Bit);
  bool GetBit(unsigned char Bit);
} *PCharData;

typedef struct SRegInfo
{
  SRegInfo();
  ~SRegInfo();

  EOps Op;
  union{
    SRegInfo *Param;
    int Symb;
    PCharData ChrClass;
  }un;
  int s,e;
  SRegInfo *Parent;
  SRegInfo *Next;
} *PRegInfo;

typedef struct SMatches
{
  int s[MATCHESNUM];
  int e[MATCHESNUM];
  int CurMatch;
} *PMatches;

typedef class CRegExp
{
  bool NoCase, Extend, NoMoves;
  char *toParse, *StartPos;
  char *End, *Start;
  PRegInfo Info;
  EError Error;
  int  FirstChar;
  char *CodePage;
  int  *Exprn;
  PMatches bktrace;
  char *bkstr;

  EError SetExprLow(const char *Expr);
  EError SetStructs(PRegInfo &Info, int st, int end);
  void Optimize();
  bool CheckSymb(int Symb, bool Inc);
  bool LowParse(PRegInfo Re);
  bool LowParseRe(PRegInfo &Next);
  bool LowCheckNext(PRegInfo Re);
  bool ParseRe(char *Str);
  bool QuickCheck();
  char inline cptrans(char c);
  PMatches Matches;
  int CurMatch;
public:

  CRegExp();
  CRegExp(char *Text);
  ~CRegExp();

  bool isok();
  EError geterror();
  bool SetNoMoves(bool Moves);
  bool SetBkTrace(char *str, PMatches trace);
  bool GetBkTrace(char **str, PMatches *trace);
  bool SetExpr(LPCSTR Expr);
  void SetCodePage(char *Table);
  bool Parse(char *Str, PMatches Mtch);
  bool Parse(char *Str, char *Sol, char *Eol, PMatches Mtch, int Moves = -1);
  //bool static Evaluate(char *Expr,char *Str);
} *PRegExp;

#endif