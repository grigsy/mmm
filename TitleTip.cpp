////////////////////////////////////////////////////////////////////////////
// TitleTip.cpp : implementation file
//
// Based on code by Zafir Anjum
//
// Adapted by Chris Maunder <cmaunder@mail.com>
// Copyright (c) 1998-2000. All Rights Reserved.
//
// This code may be used in compiled form in any way you desire. This
// file may be redistributed unmodified by any means PROVIDING it is 
// not sold for profit without the authors written consent, and 
// providing that this notice and the authors name and all copyright 
// notices remains intact. 
//
// An email letting me know how you are using it would be nice as well. 
//
// This file is provided "as is" with no expressed or implied warranty.
// The author accepts no liability for any damage/loss of business that
// this product may cause.
//
// For use with CGridCtrl v2.10
//
// History
//         10 Apr 1999  Now accepts a LOGFONT pointer and 
//                      a tracking rect in Show(...)  (Chris Maunder)
//         18 Apr 1999  Resource leak in Show fixed by Daniel Gehriger
//
/////////////////////////////////////////////////////////////////////////////
 
#include "stdafx.h"

#ifdef _USE_TITLE_TIPS_

#include "TitleTip.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTitleTip

CTitleTip::CTitleTip()
{
	// Register the window class if it has not already been registered.
	WNDCLASS wndcls;
	HINSTANCE hInst = AfxGetInstanceHandle();
	if(!(::GetClassInfo(hInst, TITLETIP_CLASSNAME, &wndcls)))
	{
		// otherwise we need to register a new class
		wndcls.style			= CS_SAVEBITS;
		wndcls.lpfnWndProc		= ::DefWindowProc;
		wndcls.cbClsExtra		= wndcls.cbWndExtra = 0;
		wndcls.hInstance		= hInst;
		wndcls.hIcon			= NULL;
		wndcls.hCursor			= LoadCursor( hInst, IDC_ARROW );
		wndcls.hbrBackground	= (HBRUSH)(COLOR_INFOBK +1);
		wndcls.lpszMenuName		= NULL;
		wndcls.lpszClassName	= TITLETIP_CLASSNAME;

		if (!AfxRegisterClass(&wndcls))
			AfxThrowResourceException();
	}

	m_ptLast.x = -1;
	m_nTimer = 0;
	m_nTimeCounter = 0;
	m_nMaxLen = -1;
}

CTitleTip::~CTitleTip()
{
	Deactivate();
}

void CTitleTip::Deactivate()
{
	if (m_nTimer)
		KillTimer(m_nTimer);
	m_nTimer = 0;
}

BEGIN_MESSAGE_MAP(CTitleTip, CWnd)
	//{{AFX_MSG_MAP(CTitleTip)
	//}}AFX_MSG_MAP
	ON_WM_NCHITTEST()
	ON_WM_TIMER()
END_MESSAGE_MAP()

void CTitleTip::OnTimer(UINT nIDEvent) 
{
	if( m_nTimer != nIDEvent )
		CWnd::OnTimer( nIDEvent );
	else
	{
		if (!IsWindowVisible())
		{
			m_nTimeCounter = 0;
			if (m_pParentWnd && !m_pParentWnd->IsWindowVisible())
				Deactivate();	// parent was hidden, stop timer
			return;
		}
		CPoint ptMouse;
		::GetCursorPos(&ptMouse);
		int hittest = (int)m_pParentWnd->SendMessage(WM_NCHITTEST,0,MAKELONG(ptMouse.x,ptMouse.y));
		BOOL bParent = (hittest == HTCLIENT);

		if (!m_rectHover.PtInRect(ptMouse)) 
		{
			if (bParent)	// parent should handle this by hiding or changing pos
			{
				if (m_nTimeCounter>2)
					Hide();
				else
					m_nTimeCounter++;
				return;
			}
			Hide();
		}
		m_nTimeCounter = 0;
	}
}

LRESULT CTitleTip::OnNcHitTest(CPoint ) 
{
	return (UINT)HTTRANSPARENT;
}


/////////////////////////////////////////////////////////////////////////////
// CTitleTip message handlers

BOOL CTitleTip::Create(CWnd * pParentWnd)
{
	ASSERT_VALID(pParentWnd);

	DWORD dwStyle = WS_BORDER | WS_POPUP; 
	DWORD dwExStyle = WS_EX_TOOLWINDOW | WS_EX_TOPMOST;
	m_pParentWnd = pParentWnd;

	return CreateEx(dwExStyle, TITLETIP_CLASSNAME, NULL, dwStyle, 
                    CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, 
		            NULL, NULL, NULL );
}


// Show 		 - Show the titletip if needed
// rectTitle	 - The rectangle within which the original 
//				    title is constrained - in client coordinates
// lpszTitleText - The text to be displayed
// xoffset		 - Number of pixel that the text is offset from
//				   left border of the cell
void CTitleTip::Show(CRect rectTitle, LPCTSTR lpszTitleText, int xoffset /*=0*/,
                     LPRECT lpHoverRect /*=NULL*/,
                     const LOGFONT* lpLogFont /*=NULL*/)
{
	ASSERT( ::IsWindow( GetSafeHwnd() ) );

    if (rectTitle.IsRectEmpty())
        return;

	if( IsWindowVisible() ) 
	{
		// rect was changed?
		if (m_rectDisplay != rectTitle)
			Hide();
		else
			return;
	}
	m_rectDisplay = rectTitle;

	if (!m_nTimer)
	{
		m_nTimer = SetTimer(12, 100, NULL);
	}

	CPoint ptMouse;
	::GetCursorPos(&ptMouse);
	if (ptMouse == m_ptLast)
		return;

    m_rectHover = (lpHoverRect != NULL)? lpHoverRect : rectTitle;
    m_rectHover.right++; m_rectHover.bottom++;

	m_pParentWnd->ClientToScreen( m_rectHover );

	// Do not display the titletip is app does not have focus
	if( GetFocus() == NULL )
		return;

	// Determine the width of the text
	m_pParentWnd->ClientToScreen( rectTitle );

	CClientDC dc(this);
	CString strTitle = _T("");
    strTitle += _T(" ");
    strTitle += lpszTitleText; 
    strTitle += _T(" ");

	CFont font, *pOldFont = NULL;
    if (lpLogFont)
    {
        font.CreateFontIndirect(lpLogFont);
	    pOldFont = dc.SelectObject( &font );
    }
    else
    {
        // use same font as ctrl
	    pOldFont = dc.SelectObject( m_pParentWnd->GetFont() );
    }

	CSize size = dc.GetTextExtent( strTitle );
	BOOL bWrap = FALSE;
	if (m_nMaxLen>0 && m_nMaxLen<size.cx)
	{
		CRect rcCalc(rectTitle);
		rcCalc.right = rcCalc.left + m_nMaxLen;
		dc.DrawText( strTitle, rcCalc, DT_WORDBREAK|DT_VCENTER|DT_LEFT|DT_NOPREFIX|DT_CALCRECT);
		size.cx = m_nMaxLen;//rcCalc.Width();
		size.cy = rcCalc.Height();
		bWrap = TRUE;
		strTitle = lpszTitleText;	// we use additional margin instead
	}
	
    TEXTMETRIC tm;
    dc.GetTextMetrics(&tm);
    size.cx += tm.tmOverhang;

	CRect rectDisplay = rectTitle;
	rectDisplay.left += xoffset;
	rectDisplay.right = rectDisplay.left + size.cx + xoffset;
	if (bWrap)
	{
		rectDisplay.bottom = rectDisplay.top + size.cy + 3;
	}
    
    // Do not display if the text fits within available space
    if ( rectDisplay.right <= rectTitle.right-xoffset && !bWrap)
		m_ptLast.x = -1;
	else
    {
		// Show the titletip
        SetWindowPos( &wndTop, rectDisplay.left, rectDisplay.top, 
            rectDisplay.Width(), rectDisplay.Height(), 
            SWP_SHOWWINDOW|SWP_NOACTIVATE );
        
		CRect rect;
		dc.GetClipBox(&rect);     // Erase the area needed 
		dc.FillSolidRect(rect, GetSysColor(COLOR_INFOBK));

		dc.SetTextColor(GetSysColor(COLOR_INFOTEXT));

        dc.SetBkMode( TRANSPARENT );
		int nFlags = DT_VCENTER|DT_LEFT|DT_NOPREFIX;
		if (bWrap)
		{
			nFlags |= DT_WORDBREAK;
			rect.left += 3;
		}
		dc.DrawText( strTitle, rect, nFlags);

		m_ptLast = ptMouse;
    }
    
    dc.SelectObject( pOldFont );
}

void CTitleTip::Hide()
{
  	if (!::IsWindow(GetSafeHwnd()))
        return;

	::GetCursorPos(&m_ptLast);

	ShowWindow( SW_HIDE );
}


#endif // _USE_TITLE_TIPS_