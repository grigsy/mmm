// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef GENERAL_PAGE_INCLUDED
#define GENERAL_PAGE_INCLUDED

class CMailbox;
template < class BASE_CLASS, class TYPE > class CTypedPtrArray;

/////////////////////////////////////////////////////////////////////////////
// CGeneralPage dialog

class CGeneralPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CGeneralPage)

	CArray<CMailbox*> *m_pMailboxArray;

public:
	CGeneralPage();
	~CGeneralPage();
	void Attach( CArray<CMailbox*> * );

// Dialog Data
	//{{AFX_DATA(CGeneralPage)
	enum { IDD = IDD_GENERAL_PAGE };
	CSpinButtonCtrl	m_spinPort;
	CSpinButtonCtrl	m_spinPoll;
	CString	m_strAlias;
	CString	m_strHost;
	CString	m_strPass;
	CString	m_strUser;
	int		m_intPoll;
	int		m_intPort;
	int		m_intExtraLines;
	int		m_nSpecTop;
	int		m_nNoRETR;
	int		m_nSSL;
	//}}AFX_DATA
	int		m_intAPOP;
	int		m_intDisableBox;
	int		m_intAskPass;


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CGeneralPage)
	public:
	virtual BOOL OnApply();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	void UpdatePassState();

	//{{AFX_MSG(CGeneralPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnModified();
	afx_msg void OnAskPwd();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif
