// RawHead.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "magic.h"
#include "Excerpt.h"
#include "Mailbox.h"
#include "RawHead.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Eigenschaftenseite CRawHead 

IMPLEMENT_DYNCREATE(CRawHead, CPropertyPage)

CRawHead::CRawHead() : CPropertyPage(CRawHead::IDD)
{
	//{{AFX_DATA_INIT(CRawHead)
	m_sHeader = _T("");
	m_bJumpEnd = FALSE;
	//}}AFX_DATA_INIT
	m_pExcerptArray = NULL;
}

CRawHead::~CRawHead()
{
}

void CRawHead::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRawHead)
	DDX_Text(pDX, IDC_RAW_HEADER, m_sHeader);
	DDX_Check(pDX, IDC_JUMPEND, m_bJumpEnd);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRawHead, CPropertyPage)
	//{{AFX_MSG_MAP(CRawHead)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_JUMPEND, OnJumpend)
	ON_COMMAND(IDC_JUMPEND, OnJumpend)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CRawHead 

void CRawHead::Attach( CTypedPtrArray < CPtrArray, CExcerpt* > *pExcerptArray )
{
	ASSERT( pExcerptArray );
	m_pExcerptArray = pExcerptArray;
}

BOOL CRawHead::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	DlgTranslate(this);

	m_bJumpEnd = (theApp.m_dwFlags & MMF_JUMPEND)!=0;
	
	if (m_pExcerptArray)
	{
		CStringArray& all= (*m_pExcerptArray)[0]->m_asRawHeader;
		for (int i=0; i<all.GetSize(); i++)
		{
			m_sHeader += all[i];
			m_sHeader += "\r\n";
		}
		UpdateData(FALSE);
	}
	if (theApp.m_dwFlags & MMF_JUMPEND)
		PostMessage(WM_COMMAND, IDC_JUMPEND);
	SheetTranslate(GetParent());	// translate parent from 1st page
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CRawHead::OnClose() 
{
	GetParent()->PostMessage(WM_COMMAND, IDCANCEL);
}

void CRawHead::OnJumpend() 
{
	UpdateData();
	if (m_bJumpEnd)
	{
		JumpEnd();
		theApp.m_dwFlags |= MMF_JUMPEND;
	}
	else
		theApp.m_dwFlags &= ~MMF_JUMPEND;

}

void CRawHead::JumpEnd()
{
	DWORD dwLen = SendDlgItemMessage(IDC_RAW_HEADER, WM_GETTEXTLENGTH, 0, 0);
	SendDlgItemMessage(IDC_RAW_HEADER, EM_SETSEL, dwLen, dwLen);
	SendDlgItemMessage(IDC_RAW_HEADER, EM_SCROLLCARET, 0, 0);
}
