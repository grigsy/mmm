#if !defined(AFX_ADVOPTIONSPAGE_H__04BCDDCF_2300_4E44_A254_78E6B09053BB__INCLUDED_)
#define AFX_ADVOPTIONSPAGE_H__04BCDDCF_2300_4E44_A254_78E6B09053BB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AdvOptionsPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAdvOptionsPage dialog

class CAdvOptionsPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CAdvOptionsPage)

// Construction
public:
	CAdvOptionsPage();
	~CAdvOptionsPage();

// Dialog Data
	//{{AFX_DATA(CAdvOptionsPage)
	enum { IDD = IDD_ADVOPTIONS_PAGE };
	CColorSample	m_Sample;
	BOOL	m_bLog;
	int		m_nLogSize;
	BOOL	m_bColor;
	BOOL	m_bSingle;
	BOOL	m_bAllDel;
	BOOL	m_bDbl;
	BOOL	m_bSmartSort;
	BOOL	m_bProtectFriend;
	BOOL	m_bMassiveMode;
	int		m_nMassiveBoxes;
	int		m_nMassivePeriod;
	//}}AFX_DATA
	COLORREF	m_clrFriend;
	
	//{{AFX_VIRTUAL(CAdvOptionsPage)
	public:
	virtual BOOL OnApply();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	afx_msg void UpdateButtons();
	//{{AFX_MSG(CAdvOptionsPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnClrBrs();
	afx_msg void OnFriendColor();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

class CLocalOptionsPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CLocalOptionsPage)

public:
	CLocalOptionsPage();
	~CLocalOptionsPage();

	// Dialog Data
	//{{AFX_DATA(CLocalOptionsPage)
	enum { IDD = IDD_LOCALS_PAGE };
	BOOL	m_bFont;
	//}}AFX_DATA

	LOGFONT	m_lfMain;
	CFont	m_fntMain;
	CString	m_strTemp;

	CComboBox m_cbEnc;

	//{{AFX_VIRTUAL(CLocalOptionsPage)
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	afx_msg void UpdateButtons();
	//{{AFX_MSG(CLocalOptionsPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnFontBrs();
	afx_msg void OnBrsFolder();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADVOPTIONSPAGE_H__04BCDDCF_2300_4E44_A254_78E6B09053BB__INCLUDED_)
