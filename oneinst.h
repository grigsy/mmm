/////////
///// COneInstance implementation
////////

#ifndef _ONEINSTANCE_H_
#define _ONEINSTANCE_H_

/* common usage:
if (!AmIFirst())
{
SendData();
}
  // ... initialization
Init(hMainFrame);
*/

class COneInstance
{
public:
	COneInstance();
	~COneInstance();

	BOOL	AmIFirst(LPCTSTR sUniq);
	BOOL   Init(HWND hw);

	BOOL	SendData(DWORD dwData, DWORD dwSize=0, LPVOID pData=NULL);
	// receiver will have WM_COPYDATA(dwData)
protected:
	HANDLE	m_hFile;
	BOOL	m_bFirst;
	LPVOID	m_pData;
};

#endif //_ONEINSTANCE_H_
