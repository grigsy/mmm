#ifndef _CCLIPBOARD_H_
#define _CCLIPBOARD_H_

class CClipBoard
{
 public:
	CClipBoard(LPCTSTR szFormatName);

	BOOL CopyToClipboard();
	BOOL IsPossiblePaste();
	BOOL PasteFromClipboard();

 protected:
	virtual BOOL Serialize(CArchive& ar)=0;

 private:
	int     m_iFormatNumber;
};

#endif