// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// WatchDog.cpp: implementation of the CWatchDog class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "WatchDog.h"

unsigned CWatchDog::s_uInstanceCount = 0;

HWND	CWatchDog::s_hWnd			= NULL;
WNDPROC	CWatchDog::OldWndProc		= NULL;

LRESULT CALLBACK CWatchDog::WndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	if( WM_TIMER == uMsg )
	{
		if( wParam )
		{
			CWatchDog* pWatchDog = reinterpret_cast< CWatchDog* >( wParam );
			pWatchDog->OnTimer();
		}

		return 0;
	}

	// out of respect to the BUTTON :)
	return CallWindowProc( OldWndProc, hWnd, uMsg, wParam, lParam );
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWatchDog::CWatchDog() :
	m_bEnabled( false ),
	m_uTimerID( 0 )
{
	// if this is the first instance
	if( !s_uInstanceCount++ )
	{
		// create the service window
		s_hWnd = CreateWindow( "BUTTON", NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL ); 
		ASSERT( s_hWnd );

		// subclass the window
		OldWndProc = (WNDPROC) SetWindowLong( s_hWnd, GWL_WNDPROC, (DWORD) WndProc );
		ASSERT( OldWndProc );
	}
}

CWatchDog::~CWatchDog()
{
	DestroyWatchDog();

	// if this is the last instance
	if( !--s_uInstanceCount )
	{
		VERIFY( DestroyWindow( s_hWnd ) );
		s_hWnd = 0;
	}
}


//////////////////////////////////////////////////////////////////////
// METHODS
//////////////////////////////////////////////////////////////////////

void CWatchDog::DestroyWatchDog()
{
	m_bEnabled = false;
	KillTimer( s_hWnd, reinterpret_cast< UINT >( this ) );
}

void CWatchDog::CreateWatchDog( UINT uintTimeout )
{
	// in case the timer existed
	DestroyWatchDog();
	m_bEnabled = true;

	// install timer
	UINT uTimerID = SetTimer( s_hWnd, reinterpret_cast< UINT >( this ), uintTimeout, NULL );

	ASSERT( uTimerID == reinterpret_cast< UINT >( this ) );
}

void CWatchDog::OnTimer()
{
	if( m_bEnabled )
	{
		DestroyWatchDog();

		WatchDogTimedOut();
	}
}

bool CWatchDog::IsWatchdogOn()
{
	return m_bEnabled;
}