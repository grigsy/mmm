#if !defined(AFX_DPASSWORD_H__31AF62C7_32CA_406D_874D_59A9B8BD3AF3__INCLUDED_)
#define AFX_DPASSWORD_H__31AF62C7_32CA_406D_874D_59A9B8BD3AF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DPassword.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DPassword dialog

class DPassword : public CDialog
{
public:
	DPassword(BOOL bCnf, LPCTSTR sAlias=NULL);

	//{{AFX_DATA(DPassword)
	enum { IDD = IDD_PASSWORD };
	CString	m_sPassword;
	CString	m_sOpen;
	//}}AFX_DATA


	//{{AFX_VIRTUAL(DPassword)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	BOOL		m_bCnf;
	UINT		m_nTimer;	// for CAPS notification
	CString		m_sAlias;

	//{{AFX_MSG(DPassword)
	virtual void OnOK();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnChangePassword();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DPASSWORD_H__31AF62C7_32CA_406D_874D_59A9B8BD3AF3__INCLUDED_)
