// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// CommandPage.cpp : implementation file
//														   

#include "stdafx.h"
#include "magic.h"
#include "Mailbox.h"
#include "MagicDoc.h"
#include "MagicFrame.h"
#include "MailboxView.h"
#include "CommandPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCommandPage property page

IMPLEMENT_DYNCREATE(CCommandPage, CPropertyPage)

CCommandPage::CCommandPage() : CPropertyPage(CCommandPage::IDD)
{
	m_pMailboxArray = 0;

	//{{AFX_DATA_INIT(CCommandPage)
	m_intCommandRun = -1;
	m_intCommand = -1;
	//}}AFX_DATA_INIT
}

CCommandPage::~CCommandPage()
{
}

void CCommandPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCommandPage)
	DDX_Text(pDX, IDC_EDIT_COMMAND, m_strCommand);
	DDX_CBIndex(pDX, IDC_COMBO_COMMAND_RUN, m_intCommandRun);
	DDX_CBIndex(pDX, IDC_COMBO_COMMAND, m_intCommand);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCommandPage, CPropertyPage)
	//{{AFX_MSG_MAP(CCommandPage)
	ON_BN_CLICKED(IDC_BROWSE, OnBrowse)
	ON_BN_CLICKED(IDC_TEST, OnTest)
	ON_CBN_SELCHANGE(IDC_COMBO_COMMAND, OnSelchangeComboCommand)
	ON_EN_CHANGE(IDC_EDIT_COMMAND, OnModified)
	ON_CBN_SELCHANGE(IDC_COMBO_COMMAND_RUN, OnModified)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CCommandPage::Attach( CArray<CMailbox*> *pMailboxArray )
{
	ASSERT( pMailboxArray );
	m_pMailboxArray = pMailboxArray;
}

BOOL CCommandPage::OnInitDialog() 
{
	if( !CPropertyPage::OnInitDialog() ) return FALSE;

	if( m_pMailboxArray )
	{
		m_intCommand = m_pMailboxArray->GetAt( 0 )->m_intCommand;
		m_intCommandRun = m_pMailboxArray->GetAt( 0 )->m_intCommandRun;
		m_strCommand = m_pMailboxArray->GetAt( 0 )->m_strCommand;

		for( int i = m_pMailboxArray->GetUpperBound(); i; --i )
		{
			CMailbox &mbox = *( m_pMailboxArray->GetAt( i ) );
			if( m_intCommand != mbox.m_intCommand ) m_intCommand = ACTION_NULL;
			if( m_intCommandRun != mbox.m_intCommandRun ) m_intCommandRun = COMMAND_RUN_NULL;
			if( !m_strCommand.IsEmpty() && ( m_strCommand != mbox.m_strCommand ) ) m_strCommand.Empty();
		}
	}
	else
	{
		GetDlgItem( IDC_COMBO_COMMAND )->SendMessage( CB_DELETESTRING, (WPARAM) ACTION_SPECIFIC );
		m_intCommand =  theApp.intCommand;
		m_intCommandRun = theApp.intCommandRun;
		m_strCommand = theApp.strCommand;
	}

	DlgTranslate(this);
	UpdateData( FALSE );
	OnSelchangeComboCommand();
	GetDlgItem( IDC_TEST )->EnableWindow( ACTION_NONE != m_intCommand );
	SetModified( FALSE );
	return TRUE;
}

BOOL CCommandPage::OnApply() 
{
	if( m_pMailboxArray )
	{
		CMailboxView &view = *( (CMailboxView*) ( ( (CMagicFrame*) AfxGetMainWnd() )->GetActiveView() ) );
		CMagicDoc &doc = *( view.GetDocument() );
		view.EnableSort( FALSE );

		for( int i = m_pMailboxArray->GetSize(); i; --i )
		{
			CMailbox &mbox = *( m_pMailboxArray->GetAt( i-1 ) );
			if( ACTION_NULL != m_intCommand ) mbox.m_intCommand = m_intCommand;
			if( ACTION_SPECIFIC == mbox.m_intCommand )
			{
				if( COMMAND_RUN_NULL != m_intCommandRun ) mbox.m_intCommandRun = m_intCommandRun;
				if( !m_strCommand.IsEmpty() ) mbox.m_strCommand = m_strCommand;
			}

//			doc.UpdateItem( &mbox );
			doc.SetModifiedFlag();
		}

		view.EnableSort();
	}
	else
	{
		theApp.intCommand = m_intCommand;
		theApp.intCommandRun = m_intCommandRun;
		theApp.strCommand = m_strCommand;
	}

	GetDlgItem( IDC_TEST )->EnableWindow( ACTION_NONE != m_intCommand );
	return CPropertyPage::OnApply();
}

/////////////////////////////////////////////////////////////////////////////
// CCommandPage message handlers

void CCommandPage::OnBrowse() 
{
	MAKE_STRING(strFilter, IDP_COMMAND_FILES);

	CFileDialog dlg( TRUE, NULL, NULL, 0, strFilter, this );

	if( IDOK == dlg.DoModal() )
	{
		CString strPath = dlg.GetPathName();
		if( -1 != strPath.Find( _T(' ')	) ) strPath = _T('\"') + dlg.GetPathName() + _T('\"');
		SetDlgItemText( IDC_EDIT_COMMAND, strPath );
	}
}

void CCommandPage::OnTest() 
{
	theApp.m_pMainWnd->SendMessage( VM_START_COMMAND, 0, 
		LPARAM( m_pMailboxArray ? m_pMailboxArray->GetAt( 0 ) : 0 ) );
}

void CCommandPage::OnSelchangeComboCommand() 
{
	int intEnable = GetDlgItem( IDC_COMBO_COMMAND )->SendMessage( CB_GETCURSEL );
	intEnable = ( ACTION_SPECIFIC == intEnable ) || ( ACTION_NULL == intEnable );
	GetDlgItem( IDC_STATIC_RUN )->EnableWindow( intEnable );
	GetDlgItem( IDC_COMBO_COMMAND_RUN )->EnableWindow( intEnable );
	GetDlgItem( IDC_STATIC_COMMAND )->EnableWindow( intEnable );
	GetDlgItem( IDC_EDIT_COMMAND )->EnableWindow( intEnable );
	GetDlgItem( IDC_BROWSE )->EnableWindow( intEnable );
	GetDlgItem( IDC_TEST )->EnableWindow( FALSE );
	SetModified();
}

void CCommandPage::OnModified() 
{
	GetDlgItem( IDC_TEST )->EnableWindow( FALSE );
	SetModified();
}
