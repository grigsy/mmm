     Overview of Magic Mail Monitor's 'QuickTools' menu features
------------------------------------------------------------------------------------
     Note that most features discussed below are also available by
     highlighting a given header list item and then pressing the right
     mouse button, which opens the item's so-called 'context menu' - 
     in order to keep things clear, we just call it its 'Right-mouse-
     button-menu'.

----------

'QUICK VIEW' (= 'Quick View' in the Right-mouse-button menu)
     downloads the corresponding message file completely and in its 
     actual internet transportation format (MIME), i.e. with binary 
     attachments as embedded UUE or Base64 code etc., into your Win-
     dows temporary folder, e.g. 'D:\Documents and settings\Username\
     Local settings\Temp', and displays it afterwards according to 
     the file name's extension (default=TXT) with the corresponding 
     file viewer.

     Downloaded message files are stored with the following name for-
     mat: (Message-ID)@(POP-Server).(Extension), e.g. MC3-1-F68C-
     F001@pop.site1.csi.com.TXT, where '.TXT' stands for the above 
     mentioned file name extension, which can be changed at any time 
     via 'File~User preferences~Options~File extension for messages'.

     If chosen file name extension is TXT (default), most likely your
     Windows text editor will be "responsible" for displaying these 
     files later, otherwise, e.g. if you chose EML as extension, Out-
     look Express might be your corresponding viewer.

     Note that in this case also unsafe file attachments etc. may be
     opened without additional warning, unless you disable opening
     and/or storage of such risky attachments via OE's safety option!

     Note also that all downloaded mail messages remain at their ear-
     lier mentioned storage place, i.e. in your main temporary folder, 
     even if you leave MMM later. Knowing this folder's place, you 
     can conveniently access all downloaded mail messages at any 
     later time with any particular program of your choice.

     In common, 'Quick View' does actually the same as 'Load' + 'View
     as text' (see below).

'QUICK REPLY' (= 'Quick Reply' in the Right-mouse-button menu)
     lets you create an immediate reply to the marked message with 
     your preferred email programm, e.g. Outlook Express, and return 
     to MMM later.

'QUICK SEND' (= 'Quick Send' in the Right-mouse-button menu)
     lets you create an email message to any addressee with your pre-
     ferred email programm, e.g. Outlook Express, and return to MMM
     later.

----------

'QUICK DELETE' (= 'Quick Delete' in the Right-mouse-button menu,
     or 'Del' in 'Edit' menu)
     deletes marked mail message from your local mail header list and -
     if you are online - also deletes the corresponding message file
     itself from your remote mail server; if you are offline however, 
     this action will take place not earlier than when you are online 
     for the next time.

     [Note that other mail maintenance programs and/or features, e.g.
     MS-Outlook's 'remote mail' feature, may only delete the marked
     mail header from your local list, but leave the corresponding mes-
     sage file itself on your server untouched, i.e. still present!]

----------

'CHECK NOW!' (= 'Check Now!' in the Right-mouse-button menu, or
     'Refresh' in 'View' menu)
     checks all mailboxes (or if only some of them have been marked,
     only these) for new messages and loads their message headers to
     MMM.

----------

'LOAD' (= 'More~Load' in the Right-mouse-button menu)
     downloads corresponding message file completely and in its actual
     internet transportation format (MIME), i.e. with binary attachments 
     as embedded UUE or Base64 code etc., into your Windows temporary 
     folder, e.g. 'D:\Documents and settings\Username\Local settings\
     Temp', but afterwards not automatically displays its contents with 
     corresponding file viewer.

     Downloaded message files are stored with the following name for-
     mat: (Message-ID)@(POP-Server).(Extension), e.g. MC3-1-F68C-F001
     @pop.site1.csi.com.TXT, where '.TXT' stands for the above mentio-
     ned file name extension, and can be changed at any time via
     'File~User preferences~Options~File extension for messages'.

     Note also that all downloaded mail messages remain at their ear-
     lier mentioned storage place, i.e. in your main temporary folder, 
     even if you leave MMM later. Knowing this folder's place, you 
     can conveniently access all downloaded mail messages at any 
     later time with any particular program of your choice.

'VIEW AS TEXT' (= 'More~View as text' in the Right-mouse-button menu)
     displays the corresponding mail message file in your main tempo-
     rary folder (see above) with the "responsible" viewer/editor.

     If the message _file_ itself hasn't been downloaded already to
     your computer, it will be _completely_ downloaded first to your 
     main temporary folder, and then displayed as described.

     Note that the menu's title 'View as text' however is somewhat 
     misleading: If the name extension for downloaded messages remains
     TXT (default), normally your Windows text editor becomes the "re-
     sponsible" viewer, otherwise other viewers/editors may be called. 

     E.g. if you chose EML as file name extension, on most Windows sy-
     stems Outlook Express will become the default viewer for such files,
     which also opens unsafe binary attachments etc., unless you dis-
     able this possibility with OE's safety option!

'PREVIEW' (= 'More~Preview as text' in the Right-mouse-button menu)
     also displays the corresponding mail message file in your main
     temporary folder (see above), but in this case, regardless of which
     file name extension you chose, the message will be _always displayed
     as plain text_, e.g. with your Windows' text editor - no potentially 
     harmful binary attachments etc. will be ever loaded!

     If the message _file_ itself hasn't been downloaded already to
     your computer, it will be _partially_ downloaded first to your 
     main temporary folder first, and then displayed as decsribed, where
     "partially" means that MMM doesn't load the entire message, but
     only as many bytes as you've specified by 'File~User preferences~
     Options~Preview size', at least 1 kB).

----------

'FILTERS' (= 'Friends' in the Right-mouse-button menu)
     'ADD TO FRIENDS' includes the senders email address to your per-
     sonal friends list which is stored as FRIENDLST.TXT in your MMM 
     directory.
     'DOMAIN TO FRIENDS' includes the entire domain of somebody's 
     email address, e.g. *.gmx.net, into your above mentioned personal 
     friends list.

     Note that there is also a 'Filters' submenu of MMM's main 'File' 
     menu, which has nothing in common with the currently discussed!

----------

'SUSPEND'
     suspends checking your mailbox contents in regular time intervals,
     as if you had disabled it generally.


     Additional features accessible through Right-mouse-button only:
---------------------------------------------------------------------------------------

'MORE~MARK AS ...'
     'MARK AS READ' toggles the icon at left margin to 'open envelope'.
     'MARK AS UNREAD' toggles the icon at left margin to 'closed enve-
     lope'.
     'MARK AS SPAM' removes the envelope icon at left margin and pre-
     ceedes the corresponding mail's subject with a "[SPAM]" prefix.
     'MARK AS NOT SPAM' restores the envelope icon at left margin and
     removes the "[SPAM]" prefix from the beginning of the correspon-
     ding mail's subject.

'ITEM PROPERTIES' (= 'Properties' in 'Edit' menu)
     'HEADER' displays main characteristics of the marked mail mes-
     sage: mailbox (mail account), sender, subject, date, and size of
     the message.
     'RAW HEADER' displays the entire internet header of the marked mail
     message as received from mail server.

'ADD TO LISTS'
     <unclear function>

     Compiled by Piotr Niemiec, Berlin, January 2008