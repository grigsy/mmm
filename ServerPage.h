// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef SERVER_PAGE_INCLUDED
#define SERVER_PAGE_INCLUDED

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ServerPage.h : header file
//

#include "Mailbox.h"

/////////////////////////////////////////////////////////////////////////////
// CServerPage dialog

class CServerPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CServerPage)

	CArray<CMailbox*> *m_pMailboxArray;

// Construction
public:
	CServerPage();
	~CServerPage();

	void Attach( CArray<CMailbox*> * );

// Dialog Data
	//{{AFX_DATA(CServerPage)
	enum { IDD = IDD_SERVER_PAGE };
	CString	m_strAPOPSupported;
	CString	m_strUIDLSupported;
	CString	m_strIPAddress;
	CString	m_strGreeting;
	CString	m_strBurstWritesSupported;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CServerPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CServerPage)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	BOOL OnInitDialog();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // SERVER_PAGE_INCLUDED
