// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
/////////////////////////////////////////////////////////////////////
//
//   File : Excerpt.h
//   Description : message entity
//
//   Modification history ( date, name, description ) : 
//		1.	15.12.2002	Igor Green, mmm3@grigsoft.com
//			ViewText, ReplyTo, raw header
//
//HISTORY_ENTRY_END:1!15.12.2002
/////////////////////////////////////////////////////////////////////

#ifndef EXCERPT_INCLUDED
#define EXCERPT_INCLUDED

#include "main.h"

class CMailbox;

class CExcerpt : public CObject
{
	DECLARE_DYNAMIC( CExcerpt );

public:
	CMailbox *m_pMailbox;

	int m_intChanged;
	int IsChanged( int intColumn ) { return m_intChanged & BIT( intColumn ); }
	void Change( int intColumn ) { m_intChanged |= BIT( intColumn ); }

	void ParseTopLine( const char *str );
	void NoMoreTopLines();

	void QuickView();
	void QuickLoad();
	void QuickPreview();
	void QuickPreviewText();
	void QuickText();

	void MarkAsRead(BOOL bRead);
	void MarkAsSpam(BOOL bSpam);
	
	void BeginDataDownload();
	void EndDataDownload();
	BOOL WriteBufferToDataFile( const char *buf, unsigned size );

	CString m_strID;
	CString m_strFrom;
	CString m_strFromName;
	CString m_strToName;
	CString m_strSubject;
	CString m_strContent;
	CTime m_tmDate;
	int m_intSize;

	CTime m_tmCheckTime;

	COLORREF	m_Color;	// for filtered highlight
	DWORD		m_dwFiltered;
	CString		m_sFilters;	// filters applied

	CStringArray	m_asRawHeader;
	CString			m_sReplyTo;
	int				m_nPriority;
	
	int		m_nPreviewLines;
	int		m_nPreviewSize;
	int		m_nLoadedSize;
	
	int m_intOrder;
	unsigned m_bitWasReported : 1;
	unsigned m_bitFilled : 1;
	unsigned m_bitDownloadData : 1;
	unsigned m_bitDownloaded : 1;
	unsigned m_bitQuickView : 1;
	unsigned m_bitRemoveFromServer : 1;
	unsigned m_bitCreated : 1;
	unsigned m_bitDeleted : 1;
	unsigned m_bitViewText : 1;
	unsigned m_bitPreviewData : 1;
	unsigned m_bitPreviewLoaded : 1;
	unsigned m_bitWasRead : 1;
	unsigned m_bitProtected : 1;

	CExcerpt( CMailbox* );
	~CExcerpt();

	BOOL	WasRead();
	BOOL	IsSpam();
	//{{AFX_VIRTUAL(CExcerpt)
	//}}AFX_VIRTUAL
	void CheckByFilters();
	void RecheckByFilters();
	void LogDeleted(const CMailFilter* mf);

protected:
	void DoUTF();
	void DoKOI8();
	BOOL IsFromFriend();	// check by friends list
	void PostProcessMessage();
	void ApplyFilterAction(const CMailFilter& mf, DWORD dwLine);
	BOOL CheckByFilter(CMailFilter&, DWORD& dwLine);
	BOOL CheckByFilterCnd(CFilterCnd& fc, DWORD& dwLine);
	void ExtractHeaderData(LPCTSTR sField, CStringArray& ar);
	//{{AFX_MSG(CExcerpt)
	//}}AFX_MSG
//	DECLARE_MESSAGE_MAP()

private:

	HANDLE m_hViewer;
	HANDLE m_hDataFile;
	CString m_strDataFileName;
	int	 m_nIsParsingColumn;

	static CTimeSpan tsLocalTimeAdjustment;
	static int intLocalTimeAdjustmentSign;
};

#endif