#if !defined(AFX_DNEWIMPORT_H__DFB61E8F_A255_4322_B0AB_4FFDF9AB398B__INCLUDED_)
#define AFX_DNEWIMPORT_H__DFB61E8F_A255_4322_B0AB_4FFDF9AB398B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DNewImport.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DNewImport dialog

class DNewImport : public CDialog
{
// Construction
public:
	DNewImport(CWnd* pParent = NULL);   // standard constructor

	//{{AFX_DATA(DNewImport)
	enum { IDD = IDD_NEW_IMPORT };
	CListBox	m_lbAccounts;
	int		m_nCur;
	int		m_nSel;
	//}}AFX_DATA

	// extracted data
	CString		m_sAlias;
	CString		m_sUser;
	CString		m_sServer;
	int			m_nPort;


	//{{AFX_VIRTUAL(DNewImport)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	CStringArray	m_asAccs;
	CUIntArray		m_anAccs;

	void GetAccountList();
	void GetAccountData(int nKey);

	//{{AFX_MSG(DNewImport)
	virtual BOOL OnInitDialog();
	afx_msg void OnOk();
	afx_msg void OnImport();
	afx_msg void OnEmpty();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DNEWIMPORT_H__DFB61E8F_A255_4322_B0AB_4FFDF9AB398B__INCLUDED_)
