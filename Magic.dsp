# Microsoft Developer Studio Project File - Name="Magic" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Magic - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Magic.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Magic.mak" CFG="Magic - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Magic - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "Magic - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Magic - Win32 ReleaseMinDep" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "Perforce Project"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Magic - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ".\Debug"
# PROP BASE Intermediate_Dir ".\Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"StdAfx.h" /c
# ADD CPP /nologo /Zp1 /MDd /W4 /Gm /GX /ZI /Od /I "htmlhelp" /I "ssl" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_AFXEXT" /D "USE_HTML_HELP" /FR /Yu"StdAfx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x419 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386
# ADD LINK32 htmlhelp/htmlhelp.lib winmm.lib /nologo /subsystem:windows /debug /machine:I386

!ELSEIF  "$(CFG)" == "Magic - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ".\Release"
# PROP BASE Intermediate_Dir ".\Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"StdAfx.h" /c
# ADD CPP /nologo /Zp1 /MD /W4 /Gi /GX /O1 /I "htmlhelp" /I "ssl" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_AFXEXT" /D "_AFXDLL" /D "USE_HTML_HELP" /Yu"StdAfx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x419 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 htmlhelp/htmlhelp.lib winmm.lib /nologo /subsystem:windows /incremental:yes /machine:I386
# SUBTRACT LINK32 /verbose /force
# Begin Special Build Tool
TargetPath=.\Release\Magic.exe
TargetName=Magic
SOURCE="$(InputPath)"
PostBuild_Desc=Copying Distrib files..
PostBuild_Cmds=copy $(TargetPath) Distrib	BIN\upx Distrib\$(TargetName).exe	copy CHANGES.txt Distrib	copy COPYING.txt Distrib	copy CREDITS.txt Distrib	copy Distrib\$(TargetName).exe x:\upload\*.*	Makehelp
# End Special Build Tool

!ELSEIF  "$(CFG)" == "Magic - Win32 ReleaseMinDep"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Magic___Win32_ReleaseMinDep"
# PROP BASE Intermediate_Dir "Magic___Win32_ReleaseMinDep"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "ReleaseMinDep"
# PROP Intermediate_Dir "ReleaseMinDep"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W4 /Gi /GX /O1 /I "htmlhelp" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_AFXEXT" /D "_AFXDLL" /D "USE_HTML_HELP" /Yu"StdAfx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W4 /Gi /GX /O2 /I "htmlhelp" /I "ssl" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "USE_HTML_HELP" /D "_AFXEXT" /D "_AFXDLL" /Yu"StdAfx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 htmlhelp/htmlhelp.lib winmm.lib /nologo /subsystem:windows /incremental:yes /machine:I386
# SUBTRACT BASE LINK32 /verbose /force
# ADD LINK32 htmlhelp/htmlhelp.lib winmm.lib /nologo /subsystem:windows /incremental:yes /machine:I386
# SUBTRACT LINK32 /verbose /debug /force
# Begin Special Build Tool
TargetPath=.\ReleaseMinDep\Magic.exe
TargetName=Magic
SOURCE="$(InputPath)"
PostBuild_Desc=Copying Distrib files..
PostBuild_Cmds=copy $(TargetPath) Distrib	BIN\upx Distrib\$(TargetName).exe	copy CHANGES.txt Distrib	copy COPYING.txt Distrib	copy CREDITS.txt Distrib	Makehelp
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "Magic - Win32 Debug"
# Name "Magic - Win32 Release"
# Name "Magic - Win32 ReleaseMinDep"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;hpj;bat;for;f90"
# Begin Source File

SOURCE=.\ActPPage.cpp
# End Source File
# Begin Source File

SOURCE=.\AdvOptionsPage.cpp
# End Source File
# Begin Source File

SOURCE=.\base64.cpp
# End Source File
# Begin Source File

SOURCE=.\blowfish.cpp
# End Source File
# Begin Source File

SOURCE=.\clipbrd.cpp
# End Source File
# Begin Source File

SOURCE=.\Color.cpp
# End Source File
# Begin Source File

SOURCE=.\CommandPage.cpp
# End Source File
# Begin Source File

SOURCE=.\cregexp.cpp
# End Source File
# Begin Source File

SOURCE=.\DFilters.cpp
# End Source File
# Begin Source File

SOURCE=.\diction.cpp
# End Source File
# Begin Source File

SOURCE=.\DNewImport.cpp
# End Source File
# Begin Source File

SOURCE=.\DPassword.cpp
# End Source File
# Begin Source File

SOURCE=.\EnBitmap.cpp
# End Source File
# Begin Source File

SOURCE=.\Excerpt.cpp
# End Source File
# Begin Source File

SOURCE=.\ExcerptView.cpp
# End Source File
# Begin Source File

SOURCE=.\FolderDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\GeneralPage.cpp
# End Source File
# Begin Source File

SOURCE=.\GetWinVer.cpp
# End Source File
# Begin Source File

SOURCE=.\HeaderPage.cpp
# End Source File
# Begin Source File

SOURCE=.\ListViewEx.cpp
# End Source File
# Begin Source File

SOURCE=.\LogPage.cpp
# End Source File
# Begin Source File

SOURCE=.\Magic.cpp
# End Source File
# Begin Source File

SOURCE=.\HLP\magic.hhp

!IF  "$(CFG)" == "Magic - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Magic - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Magic - Win32 ReleaseMinDep"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\MagicDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\MagicFrame.cpp
# End Source File
# Begin Source File

SOURCE=.\Mailbox.cpp
# End Source File
# Begin Source File

SOURCE=.\MailboxView.cpp
# End Source File
# Begin Source File

SOURCE=.\MD5.CPP
# End Source File
# Begin Source File

SOURCE=.\MessagePage.cpp
# End Source File
# Begin Source File

SOURCE=.\MIME.cpp
# End Source File
# Begin Source File

SOURCE=.\oneinst.cpp
# End Source File
# Begin Source File

SOURCE=.\OptionsPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PlaybackPage.cpp
# End Source File
# Begin Source File

SOURCE=.\RawHead.cpp
# End Source File
# Begin Source File

SOURCE=.\REGISTRY.CPP
# End Source File
# Begin Source File

SOURCE=.\SecureString.cpp
# End Source File
# Begin Source File

SOURCE=.\ServerPage.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"StdAfx.h"
# End Source File
# Begin Source File

SOURCE=.\TitleTip.cpp
# End Source File
# Begin Source File

SOURCE=.\tools.cpp
# End Source File
# Begin Source File

SOURCE=.\WatchDog.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Source File

SOURCE=.\ActPPage.h
# End Source File
# Begin Source File

SOURCE=.\AdvOptionsPage.h
# End Source File
# Begin Source File

SOURCE=.\CommandPage.h
# End Source File
# Begin Source File

SOURCE=.\DFilters.h
# End Source File
# Begin Source File

SOURCE=.\DListEditor.h
# End Source File
# Begin Source File

SOURCE=.\DNewImport.h
# End Source File
# Begin Source File

SOURCE=.\DPassword.h
# End Source File
# Begin Source File

SOURCE=.\Excerpt.h
# End Source File
# Begin Source File

SOURCE=.\ExcerptView.h
# End Source File
# Begin Source File

SOURCE=.\GeneralPage.h
# End Source File
# Begin Source File

SOURCE=.\HeaderPage.h
# End Source File
# Begin Source File

SOURCE=.\ListViewEx.h
# End Source File
# Begin Source File

SOURCE=.\LogPage.h
# End Source File
# Begin Source File

SOURCE=.\Magic.h
# End Source File
# Begin Source File

SOURCE=.\MagicDoc.h
# End Source File
# Begin Source File

SOURCE=.\MagicFrame.h
# End Source File
# Begin Source File

SOURCE=.\Mailbox.h
# End Source File
# Begin Source File

SOURCE=.\MailboxView.h
# End Source File
# Begin Source File

SOURCE=.\Main.h
# End Source File
# Begin Source File

SOURCE=.\MD5.h
# End Source File
# Begin Source File

SOURCE=.\MessagePage.h
# End Source File
# Begin Source File

SOURCE=.\MIME.h
# End Source File
# Begin Source File

SOURCE=.\OptionsPage.h
# End Source File
# Begin Source File

SOURCE=.\PlaybackPage.h
# End Source File
# Begin Source File

SOURCE=.\RawHead.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\SecureString.h
# End Source File
# Begin Source File

SOURCE=.\ServerPage.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\WatchDog.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\RES\0mmicons.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\0Toolbar.fullcolor.bmp
# End Source File
# Begin Source File

SOURCE=.\CHANGES.txt
# End Source File
# Begin Source File

SOURCE=.\COPYING.txt
# End Source File
# Begin Source File

SOURCE=.\CREDITS.txt
# End Source File
# Begin Source File

SOURCE=.\RES\down.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\down.ico
# End Source File
# Begin Source File

SOURCE=.\RES\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\RES\ico00002.ico
# End Source File
# Begin Source File

SOURCE=.\RES\ico00003.ico
# End Source File
# Begin Source File

SOURCE=.\RES\ico00004.ico
# End Source File
# Begin Source File

SOURCE=.\RES\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\Magic.rc
# End Source File
# Begin Source File

SOURCE=.\HLP\Magic.RTF
# End Source File
# Begin Source File

SOURCE=.\RES\MagicMail.ico
# End Source File
# Begin Source File

SOURCE=.\RES\mail_att.ico
# End Source File
# Begin Source File

SOURCE=.\RES\mail_del.ico
# End Source File
# Begin Source File

SOURCE=.\RES\mail_loa.ico
# End Source File
# Begin Source File

SOURCE=.\RES\mail_pre.ico
# End Source File
# Begin Source File

SOURCE=.\RES\mail_tol.ico
# End Source File
# Begin Source File

SOURCE=.\RES\MailboxEmpty.ico
# End Source File
# Begin Source File

SOURCE=.\RES\MailboxEmpty16.ico
# End Source File
# Begin Source File

SOURCE=.\RES\MailboxFull.ico
# End Source File
# Begin Source File

SOURCE=.\RES\MailboxFull16.ico
# End Source File
# Begin Source File

SOURCE=.\RES\MailboxFull_32x32.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\mm_icons.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\mmicons.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\OverlayDisabled.ico
# End Source File
# Begin Source File

SOURCE=.\RES\OverlayError.ico
# End Source File
# Begin Source File

SOURCE=.\RES\OverlaySecure.ico
# End Source File
# Begin Source File

SOURCE=.\RES\OverlaySuspended.ico
# End Source File
# Begin Source File

SOURCE=.\RES\tbfulldis.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\ToolBar.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\Toolbar.fullcolor.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\up.bmp
# End Source File
# Begin Source File

SOURCE=.\RES\up.ico
# End Source File
# End Group
# Begin Group "SSL"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\ssl\CONNECTS.CPP
# End Source File
# Begin Source File

SOURCE=.\ssl\OpenSSLConnector.cpp
# End Source File
# Begin Source File

SOURCE=.\ssl\OpenSSLConnectorBase.cpp
# End Source File
# Begin Source File

SOURCE=.\ssl\UsesOpenSSL.cpp
# End Source File
# End Group
# Begin Source File

SOURCE=.\RES\manifest.xml
# End Source File
# End Target
# End Project
