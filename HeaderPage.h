// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef HEADER_PAGE_INCLUDED
#define HEADER_PAGE_INCLUDED

class CExcerpt;
template < class BASE_CLASS, class TYPE > class CTypedPtrArray;

/////////////////////////////////////////////////////////////////////////////
// CHeaderPage dialog

class CHeaderPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CHeaderPage)

	CTypedPtrArray < CPtrArray, CExcerpt* > *m_pExcerptArray;

public:
	CHeaderPage();
	~CHeaderPage();
	void Attach( CTypedPtrArray < CPtrArray, CExcerpt* > * );

// Dialog Data
	//{{AFX_DATA(CHeaderPage)
	enum { IDD = IDD_HEADER_PAGE };
	CString	m_strDate;
	CString	m_strFrom;
	CString	m_strSize;
	CString	m_strSubject;
	CString	m_strMailbox;
	CString	m_sFilters;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CHeaderPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CHeaderPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

#endif
