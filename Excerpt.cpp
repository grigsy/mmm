// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// Excerpt.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////
//
//   File : Excerpt.cpp
//   Description : message entity
//
//   Modification history ( date, name, description ) : 
//		1.	15.12.2002	Igor Green, mmm3@grigsoft.com
//		  1.1	Extracting ReplyTo field, KOI8 and UTF8 subjects
//		  1.2	QuickLoad, fixed bug with QuickView if file was deleted
//		2.	17.12.2002	Igor Green, mmm3@grigsoft.com
//			Added partial load support
//
//HISTORY_ENTRY_END:2!17.12.2002
/////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Magic.h"
#include "Excerpt.h"
#include "Mailbox.h"
#include "MagicDoc.h"
#include "MagicFrame.h"
#include "MIME.h"
#include "tools.h"
#include "base64.h"
#include "cregexp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const char FROM[] = "from:";
static const char SUBJECT[] = "subject:";
static const char szDATE[] = "date:";
static const char REPLYTO[] = "reply-to:";
static const char PRIORITY[] = "x-priority:";
static const char S_TO[] = "to:";
static const char S_CONTENT[] = "content-type:";

static
const char months[][4] = {	"Jan", "Feb", "Mar", "Apr", 
							"May", "Jun", "Jul", "Aug", 
							"Sep", "Oct", "Nov", "Dec" };


CTimeSpan CExcerpt::tsLocalTimeAdjustment;
int CExcerpt::intLocalTimeAdjustmentSign;

/////////////////////////////////////////////////////////////////////////////
// CExcerpt

IMPLEMENT_DYNAMIC( CExcerpt, CObject );

CExcerpt::CExcerpt( CMailbox *pMailbox )
:	m_hDataFile( INVALID_HANDLE_VALUE ),
	m_hViewer(0),
	m_intSize(0),
	m_intOrder(0),
	m_bitWasReported(0),
	m_bitFilled(0),
	m_bitDownloadData(0),
	m_bitDownloaded(0),
	m_bitQuickView(0),
	m_bitViewText(0),
	m_bitRemoveFromServer(0),
	m_bitCreated(1),
	m_bitDeleted(0),
	m_bitPreviewData(0),
	m_bitPreviewLoaded(0),
	m_pMailbox( pMailbox ),
	m_intChanged(-1),
	m_tmDate( 1971, 1, 1, 1, 1, 1 ),
	m_nIsParsingColumn( 0 )
{
	m_strSubject = m_strFrom = m_strFromName = _T("...");
	m_asRawHeader.SetSize(0, 20);
	m_nPreviewLines = 0;
	m_nPreviewSize = 0;
	m_nLoadedSize = 0;
	m_nPriority = 0;
	
	if (theApp.intMarkRead == READ_ALWAYS)
		m_bitWasRead = 1;
	else
		m_bitWasRead = 0;
	m_bitProtected = 0;


	static bool bStaticInitialize = true;
	if( bStaticInitialize )
	{
		bStaticInitialize = false;
		// figure out the local time adjustment
		TIME_ZONE_INFORMATION tz = {0};

		DWORD dwResult = GetTimeZoneInformation( &tz );

		if( dwResult == TIME_ZONE_ID_STANDARD )
		{
			tz.Bias += tz.StandardBias;
		}
		else if( dwResult == TIME_ZONE_ID_DAYLIGHT )
		{
			tz.Bias += tz.DaylightBias;
		}

		if( 0 != tz.Bias )
		{
			intLocalTimeAdjustmentSign = -tz.Bias;

			if( tz.Bias < 0 ) 
			{
				tz.Bias = -tz.Bias;
			}

			tsLocalTimeAdjustment = CTimeSpan( 0, tz.Bias / 60, tz.Bias % 60, 0 );
		}
	}
	m_dwFiltered = 0;
	m_Color = 0;
}

CExcerpt::~CExcerpt()
{
	m_bitDeleted = 1;
	Change( COLUMN_MBOX );

	CWnd *wnd = (CWnd*) AfxGetMainWnd();
	if( wnd ) wnd->SendMessage( VM_UPDATEITEM, (WPARAM) this );

	if( INVALID_HANDLE_VALUE != m_hDataFile ) CloseHandle( m_hDataFile );
	if( !m_strDataFileName.IsEmpty() ) DeleteFile( m_strDataFileName );
}

//BEGIN_MESSAGE_MAP(CExcerpt, CObject)
	//{{AFX_MSG_MAP(CExcerpt)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
//END_MESSAGE_MAP()

void CExcerpt::ParseTopLine( const char *str )
{
	// strip last CRs from line
	CString sHandle(str);
	int nLen = sHandle.GetLength();
	LPTSTR pBuf = sHandle.GetBuffer(nLen);
	while (pBuf[nLen-1] == '\n' || pBuf[nLen-1] == '\r' || pBuf[nLen-1] == '\t')
		nLen--;
	pBuf[nLen] = 0;
	sHandle.ReleaseBuffer();
	str = (LPCTSTR)sHandle;

	m_asRawHeader.Add(str);

	if( !_strnicmp( str, FROM, sizeof( FROM )-1 ) && 
		(m_strFrom.IsEmpty() || m_strFrom==_T("...")))
	{
		m_nIsParsingColumn = 0;

		str += sizeof( FROM )-1;

		m_strFrom = str;
		m_strFrom.TrimLeft();
		m_strFrom.TrimRight();

		char *szTmp = _tcsdup( str );

		TCHAR *left = 0, *right = 0;
		while( 
			( left = _tcschr( szTmp, _T('<') ) )!=0 && 
			( right = _tcschr( left, _T('>') ) )!=0 
			)
		{
			++right;
			while (( *(left++) = *(right++) )!=0);
		}

		while (( left = _tcschr( szTmp, _T('\"') ) )!=0)
		{
			*left = _T(' ');
		}

		m_strFromName = szTmp;
		free( szTmp );

		m_strFromName = MIMEStringDecode( m_strFromName, theApp.GetEncoding());

		if( m_strFromName.IsEmpty() ) m_strFromName = m_strFrom;
		Change( COLUMN_FROM );
	}
	else if( !_strnicmp( str, SUBJECT, sizeof( SUBJECT )-1 ) &&
		(m_strSubject.IsEmpty() || m_strSubject==_T("...")) )
	{
		str += sizeof( SUBJECT )-1;
		m_strSubject = str;
		
		m_nIsParsingColumn = COLUMN_SUBJ;
	}
	else if( !_strnicmp( str, S_CONTENT, sizeof( S_CONTENT )-1 ) && m_strContent.IsEmpty())
	{
		str += sizeof( S_CONTENT )-1;
		m_strContent = str;

		m_nIsParsingColumn = 0;
	}
	else if( !_strnicmp( str, S_TO, sizeof( S_TO )-1 ) &&
		m_strToName.IsEmpty())
	{
		m_nIsParsingColumn = COLUMN_TO;

		str += sizeof( S_TO )-1;
		m_strToName = str;
		m_strToName.TrimLeft();
		m_strToName.TrimRight();
		Change( COLUMN_TO );
	}
	else if( !_strnicmp( str, PRIORITY, sizeof( PRIORITY)-1 ) )
	{
		str += sizeof( PRIORITY )-1;
		m_nPriority = _ttoi(str);
	}
	else if( !_strnicmp( str, REPLYTO, sizeof( REPLYTO )-1 ) )
	{
		str += sizeof( REPLYTO )-1;
		m_sReplyTo = str;
		m_sReplyTo.TrimLeft();
		m_sReplyTo.TrimRight();
	}
	else if( ( *str == '\t' || *str == ' ') && m_nIsParsingColumn == COLUMN_SUBJ)
	{
		if (*str == '\t')
			str++;
		m_strSubject += str;
	}
	else if( ( *str == '\t' || *str == ' ') && m_nIsParsingColumn == COLUMN_TO)
	{
		if (*str == '\t')
			str++;
		m_strToName += str;
	}
	else if( !_strnicmp( str, szDATE, sizeof( szDATE )-1 ) )
	{
		m_nIsParsingColumn = 0;

		str += sizeof( szDATE )-1;
		while( ' ' == *str || '\t' == *str ) ++str;
		if( 5 < strlen( str ) && ( str[3] == ',' ) ) str += 5;

		char szMonth[4] = "";
		char szTimeZone[6] = "";
		int intYear = 1970, intDay = 1, intHour = 0, intMin = 0, intSec = 0;
		if( 6 > sscanf_s( str, "%d %3s %d %d:%d:%d %5s", &intDay, szMonth, _countof(szMonth), 
			&intYear, &intHour, &intMin, &intSec, szTimeZone, _countof(szTimeZone) ) ) 
		{
			 // 30-Aug-2005 kh: added little support for alternate date format
			intSec = 0;
			// "Date: 17 Aug 2005 19:41 GMT"
			if( 5 > sscanf_s( str, "%d %3s %d %d:%d %5s", &intDay, szMonth, _countof(szMonth),
				&intYear, &intHour, &intMin, szTimeZone, _countof(szTimeZone) ) )
				return;
			if( 0 == _tcsicmp( szTimeZone, "GMT" ) )
				_tcscpy_s (szTimeZone, _countof(szTimeZone), "+0000");
		}

		// figure out the pivot point
		if( intYear < 50 )
		{
			intYear += 2000;
		}
		else if( intYear < 100 )
		{
			intYear += 1900;
		}
		else if( intYear == 100 )
		{
			// fix the Y2K glitch
			intYear = 2000;
		}
		
		// time correction. Could be ["AM"|"PM"] or ["+"|"-","XXXX"]
		if( 0 == _tcsicmp( szTimeZone, "PM" ) )
		{
			intHour += 12;
		}

		CTimeSpan oTimeCorrection;
		if( szTimeZone[0] == '+' || szTimeZone[0] == '-' )
		{	
			char szHour[3] = { szTimeZone[1], szTimeZone[2] };
			char szMin[3] = { szTimeZone[3], szTimeZone[4] };
			
			oTimeCorrection = CTimeSpan( 0, atoi(szHour), atoi(szMin), 0 );
		}

//		ASSERT( szMonth[0] );
		int intMonth = sizeof( months ) / sizeof( months[0] ); 
		while( intMonth )
		{
			if( !_tcsnicmp( szMonth, months[ --intMonth ], 3 ) ) break;
		}
		++intMonth;

		m_tmDate = CTime( intYear, intMonth, intDay, intHour, intMin, intSec );
		if( szTimeZone[0] == '+' )
		{
			m_tmDate -= oTimeCorrection;
		}
		else if( szTimeZone[0] == '-' )
		{
			m_tmDate += oTimeCorrection;
		}

		//
		// adjust from UTC/GMT to local time
		//
		if(	0 != intLocalTimeAdjustmentSign )
		{
			if( 0 < intLocalTimeAdjustmentSign )
			{
				m_tmDate += tsLocalTimeAdjustment;
			}
			else
			{
				m_tmDate -= tsLocalTimeAdjustment;
			}
		}

		Change( COLUMN_DATE );
	}
	else
	{
		m_nIsParsingColumn = 0;
	}

	Change( COLUMN_SIZE );
}

void CExcerpt::NoMoreTopLines()
{
	m_bitFilled = 1;

	m_strSubject = MIMEStringDecode( m_strSubject, theApp.GetEncoding());
	m_strToName = MIMEStringDecode( m_strToName, theApp.GetEncoding());
	
	// process encoding as separate header field, without ?=koi8
	PostProcessMessage();

#ifndef _DEBUG	// do not apply filters in debug mode
	if (theApp.intEnableFilters)
		CheckByFilters();
#endif

	Change( COLUMN_MBOX );
	Change( COLUMN_SUBJ );
	Change( COLUMN_TO);
}
BOOL SearchForText(CStringArray& ar, LPCTSTR sText)
{
	for (int i=0; i<ar.GetSize(); i++)
	{
		if (FindIgnoreCase(ar[i], sText))
			return TRUE;
	}
	return FALSE;
}

// NOTE: converting only text without tags, since text with tags is already converted
void CExcerpt::DoKOI8()
{
	CStringArray asValues;
	// check if fields have encoding within text
	ExtractHeaderData("subject:", asValues);
	if (!SearchForText(asValues, "=?koi8-r?"))
		KOI8ToWin(m_strSubject);
	
	ExtractHeaderData("from:", asValues);
	if (!SearchForText(asValues, "=?koi8-r?"))
		KOI8ToWin(m_strFromName);

	ExtractHeaderData("to:", asValues);
	if (!SearchForText(asValues, "=?koi8-r?"))
		KOI8ToWin(m_strToName);
	
	// change all non-header lines
	int i=0;
	while (i<m_asRawHeader.GetSize() && !m_asRawHeader[i].IsEmpty())
		i++;
	while (i<m_asRawHeader.GetSize())
	{
		KOI8ToWin(m_asRawHeader.ElementAt(i));
		i++;
	}
}
void CExcerpt::DoUTF()
{
	CStringArray asValues;

	// change all non-header lines
	int i=0;
	while (i<m_asRawHeader.GetSize() && !m_asRawHeader[i].IsEmpty())
		i++;
	CString s;
	int nStart = i;

	while (i<m_asRawHeader.GetSize())
		s += m_asRawHeader[i++];
	CString sDecoded = Base64Decode(s);
	UTF8ToWin(sDecoded, theApp.GetEncoding());
	if ( nStart>=m_asRawHeader.GetSize() ) // added by Schobi
		nStart = m_asRawHeader.GetSize()-1;
	if (nStart>=0)
		m_asRawHeader[nStart] = sDecoded;

	/*
	while (i<m_asRawHeader.GetSize())
	{		   
		CString sDecoded = Base64Decode(m_asRawHeader.ElementAt(i));
		UTF8ToWin(sDecoded, theApp.GetEncoding());
		m_asRawHeader[i] = sDecoded;
		i++;
	}
	*/
}
void CExcerpt::PostProcessMessage()
{
	CStringArray asValues;
	ExtractHeaderData("content-type:", asValues);
	if (SearchForText(asValues, "koi8-r"))
	{
		DoKOI8();
		return;
	}
	if (SearchForText(asValues, "utf-8"))
	{
		DoUTF();
		return;
	}

}

void CExcerpt::RecheckByFilters()
{
	m_sFilters.Empty();
	m_dwFiltered = 0;
	m_bitRemoveFromServer = 0;
	m_bitProtected = 0;	// depends on protected mailbox!

	CheckByFilters();

	Change( COLUMN_MBOX);
	Change( COLUMN_SUBJ);
}

void CExcerpt::ApplyFilterAction(const CMailFilter& mf, DWORD dwLine)
{
	if (mf.m_dwAction & MFA_READ)
	{
		m_bitWasRead = 1;
		m_bitWasReported = 1;	// avoid reporting such mail
	}
	if (mf.m_dwAction & MFA_COLOR)
	{
		m_dwFiltered |= MFA_COLOR;
		m_Color = mf.m_Color;
	}
	if (mf.m_dwAction & MFA_SPAM)
	{
		m_dwFiltered |= MFA_SPAM;
	}
	if (mf.m_dwAction & MFA_DELETE && !m_bitProtected)
	{
		m_bitRemoveFromServer = 1;
		LogDeleted(&mf);
	}
	if (mf.m_dwAction & MFA_PROTECT)
	{
		m_bitRemoveFromServer = 0;
		m_bitProtected = 1;
	}
	if (mf.m_dwAction & MFA_FRIEND)
	{
		m_dwFiltered = MFA_FRIEND;	// overcome all others
	}
	CString sFlt;
	WORD wLine = LOWORD(dwLine);
	CString sNum;
	if (HIWORD(dwLine))
		sNum = _T(" #2");
	if (wLine)
		sFlt.Format(_T("%s %s:%d"), mf.m_sName, sNum, wLine);
	else
		sFlt = mf.m_sName;
	m_sFilters += sFlt;
	m_sFilters += _T("; ");
}

void CExcerpt::LogDeleted(const CMailFilter* mf)
{
	if ((theApp.m_dwFlags & MMF_LOG_DELETED) == 0)
		return;
	if (!mf && (theApp.m_dwFlags & MMF_LOG_FILTER) != 0) 
		return;
	CLog log(DELLOG_FILE, theApp.m_nMaxLogSize*1024);
	log.Log("\n---\nFrom:%s\nTo:%s\nSubject:%s\n", 
		m_strFrom.Left(100), m_strToName.Left(100), m_strSubject.Left(100));
	CTime time = CTime::GetCurrentTime();
	log.Log("Date:%s\nDeleted on: %s from <%s>\nFilter:%s\n---\n", 
		m_tmDate.Format("%B %d, %Y, %H:%M:%S"), time.Format("%B %d, %Y, %H:%M:%S"),
		m_pMailbox ? m_pMailbox->m_strAlias:"?", mf ? mf->m_sName : "---");
}

BOOL CExcerpt::IsFromFriend()
{
	CStringArray asData;
	if (!LoadTextFile(FRIENDS_FILE, asData, ';'))
		return FALSE;
	// extract address from 'From'
	CString s;
	GetAddressFrom(m_strFrom, s);
	s.MakeLower();
	for (int i=0; i<asData.GetSize(); i++)
	{
		if (IsStringMatch(s, asData[i]))
			return TRUE;
	}
	return FALSE;
}
void CExcerpt::CheckByFilters()
{
	if (IsFromFriend())
	{
		m_sFilters = _T("From friend!");
		m_dwFiltered = MFA_FRIEND;
		if (theApp.m_dwFlags & MMF_PROTECT)
		{
			m_bitRemoveFromServer = 0;
			m_bitProtected = 1;
		}
		return;
	}

	int nFilters = theApp.m_Filters.GetSize();
	DWORD dwLine = 0;
	for (int i=0; i<nFilters; i++)
	{
		CMailFilter& mf = theApp.m_Filters.ElementAt(i);
		if ((mf.m_dwAction & MFA_ENABLED)==0)
			continue;
		if ( !CheckByFilter(mf, dwLine) )
			continue;
		ApplyFilterAction(mf, dwLine);
		if ((mf.m_dwAction & MFA_OTHER)==0 || (mf.m_dwAction & MFA_FRIEND))
			break;
	}
}
void CExcerpt::ExtractHeaderData(LPCTSTR sField, CStringArray& ar)
{
	ar.RemoveAll();
	CString sSearch(sField);
	sSearch.MakeLower();
	for (int i=0; i<m_asRawHeader.GetSize(); i++)
	{
		CString s = m_asRawHeader[i];
		s.MakeLower();
		if (s.Find(sSearch)!=0)
			continue;
		ar.Add((LPCTSTR)s + sSearch.GetLength());
		for (int j=i+1; j<m_asRawHeader.GetSize(); j++)
		{
			s = m_asRawHeader[j];
			s.MakeLower();
			if (s.IsEmpty() || s[0]!='\t')
				break;
			ar.Add(s);
		}
		break;
	}
}

void PrepareValues(CStringArray& asValues, BOOL bCase)
{
	for (int i=0; i<asValues.GetSize(); i++)
	{
		if (!bCase)
			asValues[i].MakeLower();
		asValues[i].TrimLeft();
		asValues[i].TrimRight();
	}
}

BOOL CExcerpt::CheckByFilterCnd(CFilterCnd& fc, DWORD& dwLine)
{
	CStringArray asValues;
	switch (fc.m_nField)
	{
		case MFF_SUBJECT:
			asValues.Add(m_strSubject);
			break;
		case MFF_FROM:
			asValues.Add(m_strFrom);
			asValues.Add(m_strFromName);	// allow Equal match for name
			break;
		case MFF_TO:
			ExtractHeaderData("To:", asValues);
			break;
		case MFF_CC:
			ExtractHeaderData("CC:", asValues);
			break;
		case MFF_HEADER:
			asValues.Copy(m_asRawHeader);
			break;
		case MFF_SIZE:
		{
			if (fc.m_nOperation != MFO_GREATER)
				return FALSE;
			int nSize = atoi(fc.m_sText);
			if (nSize<0)
				return FALSE;
			return (m_intSize > nSize);
		}
		case MFF_DATE:
		{
			if (fc.m_nOperation != MFO_GREATER)
				return FALSE;
			BOOL bDays = fc.m_sText.Find('.')<0;
			if (bDays)
			{
				int nDays = atoi(fc.m_sText);
				CTimeSpan ts = CTime::GetCurrentTime()- m_tmDate;
				if (ts.GetTotalHours()>nDays*24)
					return TRUE;
				return FALSE;
			}
			// handle full format: dd.mm.yyyy
			LPSTR pEnd;
			LPCTSTR pCur = fc.m_sText;
			int nDay = strtol(pCur, &pEnd, 10);
			if (!pEnd)
				return FALSE;
			int nMonth = strtol(pEnd+1, &pEnd, 10);
			if (!pEnd)
				return FALSE;
			int nYear = strtol(pEnd+1, &pEnd, 10);
			if (nDay<=0 || nMonth<=0 || nYear<=1900)
				return FALSE;
			CTime time(nYear, nMonth, nDay, 23, 59, 59);
			if (m_tmDate<time)
				return TRUE;
			return FALSE;
		}
	}
	// if value starts with $, this can be file name
	BOOL bTestCase = (fc.m_nOperation == MFO_INC_RE);
	PrepareValues(asValues, bTestCase);
	fc.Prepare();

	// now scan values and try to match
	switch (fc.m_nOperation)
	{
		case MFO_INC_RE:
		{
			for (int i=0; i<fc.m_asData.GetSize(); i++)
			{
				if (fc.m_asData[i].IsEmpty())
					continue;
				CRegExp re;
				SMatches m;
				CString sTrueRE;
				if (fc.m_asData[i].GetAt(0)!='/')
					sTrueRE.Format("/%s/i", fc.m_asData[i]);	// i = ignore case
				else
					sTrueRE = fc.m_asData[i];
				if (!re.SetExpr(sTrueRE))
					continue;
				for (int j=0; j<asValues.GetSize(); j++)
				{
					if (asValues[j].IsEmpty())
						continue;
					if (re.Parse((LPTSTR)(LPCTSTR)asValues[j], &m))
					{
						TRACE("RegExp %s found in %s at %d(%d)\n", 
							sTrueRE, asValues[j], m.s[0], m.e[0]-m.s[0]);
						if (fc.m_anLines.GetSize()>i)
							dwLine = fc.m_anLines[i];
						return TRUE;
					}
				}
			}
			return FALSE;
		}
		case MFO_EQUAL:
		{
			for (int i=0; i<fc.m_asData.GetSize(); i++)
			{
				for (int j=0; j<asValues.GetSize(); j++)
				{
					if (asValues[j].IsEmpty())
						continue;
					if (IsStringMatch(asValues[j], fc.m_asData[i]))
					{
						if (fc.m_anLines.GetSize()>i)
							dwLine = fc.m_anLines[i];
						return TRUE;
					}
				}
			}
			return FALSE;
		}
		case MFO_INCLUDE:
		case MFO_NOT_INCL:
		{
			BOOL bIncl = (fc.m_nOperation == MFO_INCLUDE);
			for (int i=0; i<fc.m_asData.GetSize(); i++)
			{
				for (int j=0; j<asValues.GetSize(); j++)
				{
					if (asValues[j].Find(fc.m_asData[i])>=0)
					{
						if (fc.m_anLines.GetSize()>i)
							dwLine = fc.m_anLines[i];
						return bIncl;
					}
				}
			}
			return (bIncl==0);
		}
	}
	return FALSE;
}
BOOL CExcerpt::CheckByFilter(CMailFilter& mf, DWORD& dwLine)
{
	// mailbox match?
	if (mf.m_sMailBox != "*" &&
		mf.m_sMailBox != m_pMailbox->m_strAlias)
		return FALSE;
	BOOL bCnd1 = CheckByFilterCnd(mf.m_aCnd[0], dwLine);
	if (mf.m_nCombination == MFC_OR)
	{
		if (bCnd1)
			return TRUE;
		bCnd1 = CheckByFilterCnd(mf.m_aCnd[1], dwLine);
		if (bCnd1 && dwLine)
			dwLine = MAKELONG(dwLine, 1);
		return bCnd1;
	}
	if (mf.m_nCombination == MFC_AND)
	{
		if (!bCnd1)
			return FALSE;
		bCnd1 = CheckByFilterCnd(mf.m_aCnd[1], dwLine);
		if (bCnd1 && dwLine)
			dwLine = MAKELONG(dwLine, 1);
		return bCnd1;
	}
	// MFC_NONE is implied
	return bCnd1;
}

void CExcerpt::BeginDataDownload()
{
	if (m_bitPreviewData)
	{
		m_nPreviewLines = 0;
		m_nPreviewSize = 0;
	}
	m_nLoadedSize = 0;
	CString sTempPath;
	if (!theApp.m_strTemp.IsEmpty())
	{
		if (theApp.m_strTemp.Find('%')>=0)
		{
			DWORD dwLen = ExpandEnvironmentStrings(theApp.m_strTemp, sTempPath.GetBuffer(MAX_PATH*4+1), 
				MAX_PATH*4);
			sTempPath.ReleaseBuffer();
			if (dwLen>MAX_PATH*4)
				sTempPath.Empty();
		}
		else
			sTempPath = theApp.m_strTemp;
		if (sTempPath.Right(1) != "\\")
			sTempPath += "\\";
	}
	if (sTempPath.GetLength()<3 || sTempPath[1]!=':')	// only expect local path
	{
		GetTempPath( MAX_PATH*2, sTempPath.GetBuffer(MAX_PATH*2+1));
		sTempPath.ReleaseBuffer();
	}
	
	// check for free space on tmp drive
	{
		TCHAR szRootPathName[] = { sTempPath[0], sTempPath[1], sTempPath[2], _T('\x0') };
		DWORD dwSectorsPerCluster = 0;		// sectors per cluster 
		DWORD dwBytesPerSector = 0;			// bytes per sector 
		DWORD dwNumberOfFreeClusters = 0;	// number of free clusters  
		DWORD dwTotalNumberOfClusters = 0; 	// total number of clusters  

		BOOL bRes = GetDiskFreeSpace
		(
			szRootPathName,	
			&dwSectorsPerCluster,
			&dwBytesPerSector,
			&dwNumberOfFreeClusters,
			&dwTotalNumberOfClusters
		);

		ULONG ulongFreeBytes = ULONG( dwNumberOfFreeClusters ) 
			* ULONG( dwSectorsPerCluster )
			* ULONG( dwBytesPerSector );

		if( !bRes || ulongFreeBytes < ULONG( m_intSize ) )
		{
			CString strMessage;
			AfxFormatString1( strMessage, IDP_INSUFFICIENT_TMP_FREE_SPACE_1, CString( szRootPathName ) );
			AfxMessageBox( strMessage, MB_ICONSTOP );
			EndDataDownload();
			return;
		}
	}

	CString strTitle = m_strID;
	int i;
	while( -1 != ( i = strTitle.FindOneOf( _T("/\\:*?\"<>|") ) ) ) strTitle.SetAt( i, _T(' ') );
	strTitle.TrimLeft();
	strTitle.TrimRight();

	m_strDataFileName.Format( _T("%s%s@%s.%s"), sTempPath, strTitle, m_pMailbox->m_strHost, theApp.strFileExtensionForMessages );

	if( INVALID_HANDLE_VALUE != m_hDataFile )
	{
		CloseHandle( m_hDataFile );
		m_hDataFile = INVALID_HANDLE_VALUE;
	}

	m_hDataFile = CreateFile
	(
		m_strDataFileName,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL
	);
	
	if( INVALID_HANDLE_VALUE == m_hDataFile )
	{
		TRACE2( "File Error #%d; [%s] cannot create\n", GetLastError(), m_strDataFileName );
		EndDataDownload();
	}
}

BOOL CExcerpt::WriteBufferToDataFile( const char *buf, unsigned size )
{
	if (m_bitPreviewData)
	{
		m_nPreviewLines++;
		if (m_nPreviewLines>m_asRawHeader.GetSize())
			m_nPreviewSize += size;
	}
	m_nLoadedSize += size;
	Change( COLUMN_SIZE );
	ASSERT( size > 0 );
	if( INVALID_HANDLE_VALUE == m_hDataFile ) return FALSE;

	DWORD dwTmp = 0;
	if( FALSE == WriteFile( m_hDataFile, buf, size, &dwTmp, NULL ) )
	{
		ASSERT( false );
		CloseHandle( m_hDataFile );
		m_hDataFile = INVALID_HANDLE_VALUE;
	};

	ASSERT( dwTmp == size );
	if (m_bitPreviewData && m_nPreviewSize>(theApp.intPreviewSize*1024))
	{
		m_bitPreviewData = 0;
		m_bitPreviewLoaded = 1;
		return FALSE;
	}

	return TRUE;
}

void CExcerpt::EndDataDownload()
{
	m_bitDownloadData = 0;

	if ( INVALID_HANDLE_VALUE != m_hDataFile ) 
	{
		if (m_bitPreviewData)	// but not m_bitPreviewLoaded -> short message
		{
			m_bitPreviewLoaded = 0;
			m_bitPreviewData = 0;
		}
		if (!m_bitPreviewLoaded)
			m_bitDownloaded = 1;
	}
	else 
	{
		m_bitPreviewData = 0;
		m_bitDownloaded = 0; 
		m_bitPreviewLoaded = 0;
		return; 
	}
	m_bitPreviewData = 0;

	Change( COLUMN_MBOX );
	Change( COLUMN_SUBJ );

	CloseHandle( m_hDataFile );
	m_hDataFile = INVALID_HANDLE_VALUE;

	if( m_bitQuickView )
	{
		QuickView();
	}
	if( m_bitViewText )
	{
		QuickText();
	}
/*	
	if( m_bitRedirect )
	{

	}
*/
}

void CExcerpt::QuickPreviewText()
{
	BOOL bLoaded = FALSE;
	
	if ((GetFileAttributes(m_strDataFileName) & FILE_ATTRIBUTE_DIRECTORY)==0)
	{
		if (m_bitDownloaded || m_bitPreviewLoaded)
			bLoaded = TRUE;
		else if (m_nLoadedSize>=theApp.intPreviewSize*1024)	// maybe some data was loaded?
			bLoaded = TRUE;
	}
	if (bLoaded)
	{
		// get txt handler, if any
		CString sTxtApp;
		GetTextHandler(sTxtApp, m_strDataFileName);

		WinExec(sTxtApp, SW_SHOW);

		m_bitViewText = 0;
		m_bitQuickView = 0;
		m_bitPreviewData = 0;
		if (theApp.intMarkRead == READ_VIEW || 
			theApp.intMarkRead == READ_BOTH)
			MarkAsRead(TRUE);
		return;
	}

	m_bitPreviewData = 1;
	m_bitViewText = 1;
	m_bitDownloaded = 0;
	m_bitPreviewLoaded = 0;
	Change( COLUMN_MBOX );
	Change( COLUMN_SUBJ );

	m_pMailbox->Check();
}

void CExcerpt::QuickPreview()
{
	if ( (m_bitDownloaded || m_bitPreviewLoaded) && 
		(GetFileAttributes(m_strDataFileName) & FILE_ATTRIBUTE_DIRECTORY)==0
		)
	{
		return;
	}

	m_bitPreviewData = 1;
	m_bitQuickView = 1;
	m_bitDownloaded = 0;
	m_bitPreviewLoaded = 0;
	Change( COLUMN_SUBJ );

	m_pMailbox->Check();
}

void CExcerpt::QuickLoad()
{
	if ( m_bitDownloaded && 
		(GetFileAttributes(m_strDataFileName) & FILE_ATTRIBUTE_DIRECTORY)==0
		)
		return;	// already done

	m_bitDownloadData = 1;
	m_bitDownloaded = 0;
	m_bitPreviewData = 0;
	m_bitPreviewLoaded = 0;
	Change( COLUMN_SUBJ );

	m_pMailbox->Check();
}

void CExcerpt::QuickView()
{
	DWORD dwExitCode;
	if( !GetExitCodeProcess( m_hViewer, &dwExitCode ) || 
		STILL_ACTIVE != dwExitCode )
	{
		m_hViewer = 0;
	}

	if( m_hViewer )
	{
		
		
		// make process foreground
	}
	else if ( m_bitDownloaded && 
		(GetFileAttributes(m_strDataFileName) & FILE_ATTRIBUTE_DIRECTORY)==0
		)
	{
		CString sApp;
		if (theApp.strApp.IsEmpty() || !FindLocalFile(theApp.strApp, sApp, FALSE))
		{
			SHELLEXECUTEINFO sei = 
			{
				sizeof( SHELLEXECUTEINFO ),
				SEE_MASK_DOENVSUBST | SEE_MASK_NOCLOSEPROCESS,
				NULL,
				_T("open"),
				m_strDataFileName,
				NULL,
				NULL,
				SW_NORMAL,
				0, 0, 0, 0, 0, 0, 0
			};		
			
			ShellExecuteEx( &sei );
			m_hViewer = sei.hProcess;
		}
		else	// app is specified
		{
			ShellExecute(NULL, NULL, theApp.strApp, m_strDataFileName, NULL, SW_SHOW);
		}

			m_bitQuickView = 0;
			if (theApp.intMarkRead == READ_VIEW || 
				theApp.intMarkRead == READ_BOTH)
				MarkAsRead(TRUE);
	}
	else
	{
		m_bitDownloadData = m_bitQuickView = 1;
		m_bitDownloaded = 0;
		m_bitPreviewData = 0;
		m_bitPreviewLoaded = 0;
		Change( COLUMN_MBOX );

		m_pMailbox->Check();
	}
}

void CExcerpt::QuickText()
{
	if ( ( m_bitDownloaded || m_bitPreviewLoaded) &&
		(GetFileAttributes(m_strDataFileName) & FILE_ATTRIBUTE_DIRECTORY)==0
		)
	{
		CString sTxtApp;
		GetTextHandler(sTxtApp, m_strDataFileName);

		WinExec(sTxtApp, SW_SHOW);

		m_bitViewText = 0;
		m_bitQuickView = 0;
		if (theApp.intMarkRead == READ_VIEW || 
			theApp.intMarkRead == READ_BOTH)
			MarkAsRead(TRUE);
	}
	else
	{
		m_bitDownloadData = 1;
		m_bitQuickView = 0;
		m_bitViewText = 1;
		m_bitDownloaded = 0;
		m_bitPreviewData = 0;
		m_bitPreviewLoaded = 0;
		Change( COLUMN_MBOX );

		m_pMailbox->Check();
	}
}

void CExcerpt::MarkAsRead(BOOL bRead)
{
	m_bitWasRead = bRead;
	Change( COLUMN_MBOX );
	m_pMailbox->Change( COLUMN_MAIL );	// need redraw
}

void CExcerpt::MarkAsSpam(BOOL bSpam)
{
	if (bSpam)
		m_dwFiltered |= MFA_SPAM_MAN;
	else
		m_dwFiltered &= ~MFA_SPAM_MAN;
	Change( COLUMN_MBOX );
	Change( COLUMN_SUBJ );
	m_pMailbox->Change( COLUMN_MAIL );	// need redraw

}
BOOL CExcerpt::WasRead()
{
	return (m_bitWasRead==1 || (m_dwFiltered & MFA_SPAM_MAN) == MFA_SPAM_MAN);
}

BOOL CExcerpt::IsSpam()
{
	return ((m_dwFiltered & MFA_SPAM) != 0);
}
