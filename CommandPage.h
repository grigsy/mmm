// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef COMMAND_PAGE_INCLUDED
#define COMMAND_PAGE_INCLUDED

class CMalibox;
template < class BASE_CLASS, class TYPE > class CTypedPtrArray;

/////////////////////////////////////////////////////////////////////////////
// CCommandPage dialog

class CCommandPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CCommandPage)

	CArray<CMailbox*> *m_pMailboxArray;

public:
	CCommandPage();
	~CCommandPage();
	void Attach( CArray<CMailbox*> * );

// Dialog Data
	//{{AFX_DATA(CCommandPage)
	enum { IDD = IDD_COMMAND_PAGE };
	CString	m_strCommand;
	int		m_intCommandRun;
	int		m_intCommand;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CCommandPage)
	public:
	virtual BOOL OnApply();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCommandPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnBrowse();
	afx_msg void OnTest();
	afx_msg void OnSelchangeComboCommand();
	afx_msg void OnModified();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif
