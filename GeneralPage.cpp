// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// GeneralPage.cpp : implementation file
//

#include "stdafx.h"
#include "magic.h"
#include "Mailbox.h"
#include "diction.h"
#include "GeneralPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGeneralPage property page

IMPLEMENT_DYNCREATE(CGeneralPage, CPropertyPage)

CGeneralPage::CGeneralPage() : CPropertyPage(CGeneralPage::IDD)
{
	m_pMailboxArray = 0;

	//{{AFX_DATA_INIT(CGeneralPage)
	m_intPoll = 0;
	m_intPort = 0;
	m_intExtraLines = 0;
	m_nSpecTop = FALSE;
	m_nNoRETR = FALSE;
	m_nSSL = FALSE;
	//}}AFX_DATA_INIT
	m_intAPOP = 0;
	m_intDisableBox = 0;
	m_intAskPass = 0;
}

CGeneralPage::~CGeneralPage()
{
}

void CGeneralPage::Attach( CArray<CMailbox*> *pMailboxArray )
{
	ASSERT( pMailboxArray );
	m_pMailboxArray = pMailboxArray;
}

void CGeneralPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CGeneralPage)
	DDX_Control(pDX, IDC_SPIN_PORT, m_spinPort);
	DDX_Control(pDX, IDC_SPIN_POLL, m_spinPoll);
	DDX_Text(pDX, IDC_EDIT_ALIAS, m_strAlias);
	DDX_Text(pDX, IDC_EDIT_HOST, m_strHost);
	DDX_Text(pDX, IDC_EDIT_PASS, m_strPass);
	DDV_MaxChars(pDX, m_strPass, 80);
	DDX_Text(pDX, IDC_EDIT_USER, m_strUser);
	DDV_MaxChars(pDX, m_strUser, 80);
	DDX_Check(pDX, IDC_SPEC_TOP, m_nSpecTop);
	DDX_Check(pDX, IDC_NO_RETR, m_nNoRETR);
	DDX_Check(pDX, IDC_SSL, m_nSSL);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_NO_APOP, m_intAPOP);
	DDX_Check(pDX, IDC_DISABLE_MBOX, m_intDisableBox);
	DDX_Check(pDX, IDC_ASK_PWD, m_intAskPass);
	

	if( pDX->m_bSaveAndValidate ) 
	{
		CString strTmp;

		GetDlgItemText( IDC_EDIT_PORT, strTmp );
		if( strTmp.IsEmpty() ) m_intPort = -1;
		else
		{
			DDX_Text( pDX, IDC_EDIT_PORT, m_intPort );
			DDV_MinMaxInt(pDX, m_intPort, 1, 65536);
		}

		GetDlgItemText( IDC_EDIT_POLL, strTmp );
		if( strTmp.IsEmpty() ) m_intPoll = -1;
		else
		{
			DDX_Text( pDX, IDC_EDIT_POLL, m_intPoll );
			DDV_MinMaxInt( pDX, m_intPoll, 0, 32767 );
		}

		GetDlgItemText( IDC_EDIT_EXTRA_LINES, strTmp );
		if( strTmp.IsEmpty() ) m_intExtraLines = -1;
		else
		{
			DDX_Text( pDX, IDC_EDIT_EXTRA_LINES, m_intExtraLines);
		}
	}
	else
	{
		if( -1 == m_intPort ) SetDlgItemText( IDC_EDIT_PORT, _T("") );
		else DDX_Text( pDX, IDC_EDIT_PORT, m_intPort );

		if( -1 == m_intPoll ) SetDlgItemText( IDC_EDIT_POLL, _T("") );
		else DDX_Text( pDX, IDC_EDIT_POLL, m_intPoll );

		if( -1 == m_intExtraLines ) SetDlgItemText( IDC_EDIT_EXTRA_LINES, _T("") );
		else DDX_Text( pDX, IDC_EDIT_EXTRA_LINES, m_intExtraLines );
	}
	
}


BEGIN_MESSAGE_MAP(CGeneralPage, CPropertyPage)
	//{{AFX_MSG_MAP(CGeneralPage)
	ON_EN_CHANGE(IDC_EDIT_ALIAS, OnModified)
	ON_EN_CHANGE(IDC_EDIT_HOST, OnModified)
	ON_EN_CHANGE(IDC_EDIT_EXTRA_LINES, OnModified)
	ON_EN_CHANGE(IDC_EDIT_PASS, OnModified)
	ON_EN_CHANGE(IDC_EDIT_POLL, OnModified)
	ON_EN_CHANGE(IDC_EDIT_PORT, OnModified)
	ON_EN_CHANGE(IDC_EDIT_USER, OnModified)
	ON_BN_CLICKED(IDC_ASK_PWD, OnAskPwd)
	ON_BN_CLICKED(IDC_NO_APOP, OnModified)
	ON_BN_CLICKED(IDC_DISABLE_MBOX, OnModified)
	ON_BN_CLICKED(IDC_DISABLE_MBOX, OnModified)
	ON_BN_CLICKED(IDC_SPEC_TOP, OnModified)
	ON_BN_CLICKED(IDC_NO_RETR, OnModified)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CGeneralPage::OnInitDialog() 	   
{
	if( !CPropertyPage::OnInitDialog() ) return FALSE;
	
	m_spinPort.SetRange( 1, 32767 );
	m_spinPort.SetBase( 10 );

	m_spinPoll.SetRange( 0, 600 );
	m_spinPoll.SetBase( 10 );

	SendDlgItemMessage(IDC_SPIN_POLL2, UDM_SETRANGE32, 0, 1000);

	CMailbox &mbox = *( m_pMailboxArray->GetAt( 0 ) );
	m_strAlias = mbox.m_strAlias;
	m_strUser = mbox.m_strUser;
	m_strHost = mbox.m_strHost;
	m_strPass = mbox.m_strPass;
	m_intPort = mbox.m_intPort;
	m_intExtraLines = mbox.GetExtraLines();
	m_intPoll = mbox.m_intPoll;
	m_intAPOP = (mbox.m_dwFlags & MBF_NO_APOP)!=0;
	m_intDisableBox = (mbox.m_dwFlags & MBF_PASSIVE)!=0;
	m_intAskPass = (mbox.m_dwFlags & MBF_ASK_PASS)!=0;
	m_nSpecTop = (mbox.m_dwFlags & MBF_SPEC_TOP)!=0;
	m_nNoRETR = (mbox.m_dwFlags & MBF_NO_RETR)!=0;
	m_nSSL = (mbox.m_dwFlags & MBF_SSL)!=0;

	if (m_intAskPass)
		m_strPass.Empty();


	for( int i = m_pMailboxArray->GetUpperBound(); i; --i )
	{
		CMailbox &mbox = *( m_pMailboxArray->GetAt( i ) );
		if( ( cstrMultipleSelection != m_strAlias ) && 
			( m_strAlias != mbox.m_strAlias ) ) m_strAlias = cstrMultipleSelection;

		if( ( cstrMultipleSelection != m_strUser ) && 
			( m_strUser != mbox.m_strUser ) ) m_strUser = cstrMultipleSelection;

		if( ( cstrMultipleSelection != m_strHost ) && 
			( m_strHost != mbox.m_strHost ) ) m_strHost = cstrMultipleSelection;

		if( !m_strPass.IsEmpty() && ( m_strPass != mbox.m_strPass ) ) m_strPass.Empty();

		if( m_intPort != mbox.m_intPort ) m_intPort = -1;
		if( m_intExtraLines != mbox.GetExtraLines()) m_intExtraLines= -1;
		if( m_intPoll != mbox.m_intPoll ) m_intPoll = -1;
		if( m_intAPOP != ((mbox.m_dwFlags & MBF_NO_APOP)!=0) )
			m_intAPOP = 2;
		if( m_intDisableBox!= ((mbox.m_dwFlags & MBF_PASSIVE)!=0) )
			m_intDisableBox = 2;
		if( m_intAskPass!= ((mbox.m_dwFlags & MBF_ASK_PASS)!=0) )
			m_intAskPass = 2;
		if( m_nSpecTop != ((mbox.m_dwFlags & MBF_SPEC_TOP)!=0) )
			m_nSpecTop = 2;
		if( m_nNoRETR != ((mbox.m_dwFlags & MBF_NO_RETR)!=0) )
			m_nNoRETR = 2;
		if( m_nSSL != ((mbox.m_dwFlags & MBF_SSL)!=0) )
			m_nSSL = 2;
	}

	UpdateData( FALSE );
	DlgTranslate(this);
	SheetTranslate(GetParent());	// translate parent from 1st page

	return TRUE;
}

BOOL CGeneralPage::OnApply() 
{
	CFrameWnd *wnd = (CFrameWnd*) AfxGetMainWnd();
	CDocument *doc = wnd ? wnd->GetActiveDocument() : 0;

	for( int i = m_pMailboxArray->GetSize(); i; --i )
	{
		CMailbox &mbox = *( m_pMailboxArray->GetAt( i-1 ) );

		if( cstrMultipleSelection != m_strAlias ) mbox.SetAlias( m_strAlias ); 
		if( cstrMultipleSelection != m_strUser ) mbox.SetUser( m_strUser );
		if( cstrMultipleSelection != m_strHost ) mbox.SetHost( m_strHost ); 
 		if( !m_strPass.IsEmpty() || m_intAskPass==1)
			mbox.SetPass( m_strPass, m_intAskPass );
		if( -1 != m_intPort ) mbox.SetPort( m_intPort );
		if( -1 != m_intPoll ) mbox.SetPoll( m_intPoll );
		if( -1 != m_intExtraLines ) mbox.m_nExtraLines = (BYTE)m_intExtraLines;
		if( 2 != m_intAPOP ) mbox.SetFlag( MBF_NO_APOP, m_intAPOP );
		if( 2 != m_intDisableBox) mbox.SetFlag( MBF_PASSIVE, m_intDisableBox );
		if( 2 != m_nSpecTop) mbox.SetFlag( MBF_SPEC_TOP, m_nSpecTop );
		if( 2 != m_nNoRETR) mbox.SetFlag( MBF_NO_RETR, m_nNoRETR);
		if( 2 != m_nSSL) mbox.SetFlag( MBF_SSL, m_nSSL);

		mbox.m_strAlias.TrimLeft();
		mbox.m_strAlias.TrimRight();
		if( mbox.m_strAlias.IsEmpty() )
		{
			CString strAlias;
			strAlias.Format( _T("<%s@%s>"), mbox.m_strUser, mbox.m_strHost );
			mbox.SetAlias( strAlias );
		}

		if( doc ) doc->SetModifiedFlag();
	}

	m_strAlias.TrimLeft();
	m_strAlias.TrimRight();

	return CPropertyPage::OnApply();
}


/////////////////////////////////////////////////////////////////////////////
// CGeneralPage message handlers

void CGeneralPage::OnModified() 
{
	SetModified();
	UpdatePassState();
}

void CGeneralPage::OnAskPwd() 
{
	OnModified();
}

void CGeneralPage::UpdatePassState()
{
	CString sPass;
	GetDlgItemText(IDC_EDIT_PASS, sPass);
	if (!GetDlgItem(IDC_ASK_PWD))
		return;
	GetDlgItem(IDC_ASK_PWD)->EnableWindow(sPass.IsEmpty());
	if (!sPass.IsEmpty())
		SendDlgItemMessage(IDC_ASK_PWD, BM_SETCHECK);
}

BOOL CGeneralPage::PreTranslateMessage(MSG* pMsg) 
{
#ifdef _DEBUG	
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_F1)
	{
		AfxMessageBox(m_strPass);
	}
#endif
	return CPropertyPage::PreTranslateMessage(pMsg);
}
