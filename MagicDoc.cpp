// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// MagicDoc.cpp : implementation of the CMagicDoc class
//
#include "stdafx.h"
#include "Magic.h"
#include "Mailbox.h"
#include "Excerpt.h"
#include "MagicFrame.h"
#include "GeneralPage.h"
#include "PlaybackPage.h"
#include "CommandPage.h"
#include "MessagePage.h"
#include "ServerPage.h"
#include "LogPage.h"

#include "MagicDoc.h"
#include "dpassword.h"
#include "tools.h"
#include "dnewimport.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMagicDoc

IMPLEMENT_DYNCREATE(CMagicDoc, CDocument)

BEGIN_MESSAGE_MAP(CMagicDoc, CDocument)
	//{{AFX_MSG_MAP(CMagicDoc)
	ON_COMMAND(ID_TIMER_CHECK, OnTimerCheck)
	ON_COMMAND(ID_STOP_COMMAND_ALL, OnStopCommandAll)
	ON_COMMAND(ID_STOP_PLAYBACK_ALL, OnStopPlaybackAll)
	ON_UPDATE_COMMAND_UI(ID_STOP_PLAYBACK_ALL, OnUpdateStopPlaybackAll)
	ON_UPDATE_COMMAND_UI(ID_STOP_COMMAND_ALL, OnUpdateStopCommandAll)
	ON_COMMAND(ID_PASSWORD, OnPassword)
	ON_UPDATE_COMMAND_UI(ID_PASSWORD, OnUpdatePassword)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_IMPORT_CSV, &CMagicDoc::OnImport)
	ON_COMMAND(ID_NEW_FOLDER, OnNewFolder)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMagicDoc construction/destruction

CMagicDoc::CMagicDoc()
{
	m_nLastMassiveBox = -1;
	m_nMassivePoll = 0;
}

CMagicDoc::~CMagicDoc()
{
}

/////////////////////////////////////////////////////////////////////////////
// CMagicDoc serialization
static BYTE nVersion = 2;
void CMagicDoc::Serialize(CArchive& ar)
{
	// keep old serialization
	CTypedPtrList < CObList, CMailbox* > list;
	if (ar.IsStoring())
	{
		for (int i=0; i<m_aMailboxes.GetSize(); i++)
			list.AddTail(m_aMailboxes[i]);
		list.Serialize( ar );

		ar << nVersion;
		theApp.m_Filters.Serialize( ar );
	}
	else
	{
		list.Serialize( ar );
		for( POSITION pos = list.GetHeadPosition(); pos; list.GetNext( pos ) )
			m_aMailboxes.Add(list.GetAt( pos ));

		// old version does not have version number stored, so
		//  have to use exception handler
		try
		{
			BYTE bVer = 0;
			ar >> bVer;
			if (bVer <= nVersion)
			{
				theApp.m_Filters.Serialize( ar );
			}
		}
		catch(CArchiveException* e)
		{
			e->Delete();
			theApp.GenDefaultFilters();
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMagicDoc diagnostics

#ifdef _DEBUG
void CMagicDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMagicDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMagicDoc commands

void CMagicDoc::DeleteContents() 
{
	POSITION pos = GetFirstViewPosition();
	while( pos ) VERIFY( ( (CListView*) GetNextView( pos ) )->GetListCtrl().DeleteAllItems() );	
	for (int i=0; i<m_aMailboxes.GetSize(); i++)
		delete m_aMailboxes[i];
	m_aMailboxes.RemoveAll();
	
	CDocument::DeleteContents();
}

void CMagicDoc::CopyMailBox(CMailbox* pCopy)
{
	CMailbox *pMailbox = new CMailbox();
	*pMailbox = *pCopy;
	m_aMailboxes.Add(pMailbox );
	SetModifiedFlag();
}
void CMagicDoc::OnNewFolder()
{
	CString sFolder(_T("Folder"));
	if (!DoAskText(_T("MMM"), _T("${Folder mame}:"), sFolder))
		return;
	CMailboxFolder* pMailbox = new CMailboxFolder();
	pMailbox->m_strAlias = sFolder;
	pMailbox->CloseFolder();
	m_aMailboxes.Add(pMailbox );
	SetModifiedFlag();
}
void CMagicDoc::NewMailbox()
{
	DNewImport dlg;
	if (dlg.DoModal() == IDCANCEL)
		return;

	CMailbox *pMailbox = new CMailbox();
	if (dlg.m_nSel == 1)
	{
		pMailbox->m_strAlias = dlg.m_sAlias;
		pMailbox->m_strUser = dlg.m_sUser;
		pMailbox->m_strHost = dlg.m_sServer;
		pMailbox->m_intPort = dlg.m_nPort;
	}
	CArray< CMailbox* > arrayMailbox;
	arrayMailbox.Add( pMailbox );
	if( MailboxProperties(arrayMailbox) )
	{
		m_aMailboxes.Add( pMailbox );
		SetModifiedFlag();
	}
	else
	{
		delete pMailbox;
	}
}

int CMagicDoc::GetFoldersList(CStringArray& as)
{
	for (int i=0; i<m_aMailboxes.GetSize(); i++)
	{
		if (m_aMailboxes[i]->IsFolder())
			as.Add(m_aMailboxes[i]->m_strAlias);
	}
	return as.GetSize(); 
}
int CMagicDoc::FindFolder(LPCTSTR sName, BOOL bCreate )
{
	for (int i=0; i<m_aMailboxes.GetSize(); i++)
	{
		if (!m_aMailboxes[i]->IsFolder())
			continue;
		if (m_aMailboxes[i]->m_strAlias.CompareNoCase(sName)==0)
			return i;
	}
	if (bCreate)
	{
		CMailboxFolder* pMailbox = new CMailboxFolder();
		pMailbox->m_strAlias = sName;
		return m_aMailboxes.Add(pMailbox );
	}
	return -1;
}
int CMagicDoc::FindFolder(int nIdx)
{
	int nCur = 0;
	for (int i=0; i<m_aMailboxes.GetSize(); i++)
	{
		if (!m_aMailboxes[i]->IsFolder())
			continue;
		if (nCur==nIdx)
			return i;
		nCur++;
	}
	return -1;
}

int CMagicDoc::FindRoot(CMailbox* pMailbox)
{
	if (!pMailbox->InFolder())
		return -1;
	int nBox = FindBox(pMailbox);
	if (nBox<0)
		return -1;
	while (nBox>=0 && !m_aMailboxes[nBox]->IsFolder())
		nBox--;
	return nBox;
}
int CMagicDoc::FindBox( CMailbox* pMailbox ) 
{
	for (int i=0; i<m_aMailboxes.GetSize(); i++)
	{
		if (m_aMailboxes[i]==pMailbox)
			return i;
	}
	return -1;
}
void CMagicDoc::DeleteMailbox( CMailbox* pMailbox ) 
{
	int nPos= FindBox(pMailbox);
	if (nPos<0)
		return;
	if (pMailbox->IsFolder())
	{
		// scan all boxes inside
		for (int t=nPos+1; t<m_aMailboxes.GetSize() && !m_aMailboxes[t]->IsFolder(); t++)
		{
			if (!m_aMailboxes[t]->InFolder())
				continue;
			m_aMailboxes[t]->MakeInFolder(FALSE);
		}
	}
	m_aMailboxes.RemoveAt( nPos );
	delete pMailbox;

	SetModifiedFlag();
}

void CMagicDoc::OnTimerCheck() 
{
	if (theApp.bIsSuspended )
		return;
	BOOL bMassive = (theApp.m_dwFlags & MMF_MASSIVE)!=0;
	if (bMassive)
		m_nMassivePoll++;
	int nCheckStart = (m_nLastMassiveBox+1) % m_aMailboxes.GetSize();
	int nMassiveChecked = 0;

	for (int i=0; i<m_aMailboxes.GetSize(); i++)
	{
		CMailbox &mbox = *m_aMailboxes.ElementAt( i );
		mbox.SetElapsed(/*bMassive*/);
		if (mbox.IsDisabled() || mbox.IsFolder())
			continue;
		if (bMassive)
		{
			if (m_nMassivePoll<theApp.m_nMassivePeriod)	// general timeout
				continue;
			if (i<nCheckStart)	// we have checked it earlier
				continue;
			if (mbox.m_intElapsed>=0 && mbox.m_intElapsed < mbox.m_intPoll)	// this box is too fresh
				continue;
			if (nMassiveChecked>=theApp.m_nMassiveBoxes)	// got enough for this time
				continue;
			if (STATE_FINAL( mbox.m_intState ))
			{
				mbox.Check();
				nMassiveChecked++;
				m_nLastMassiveBox = i;
			}
			
			continue;
		}
		if( mbox.m_intPoll )
		{
			if( ( mbox.m_intElapsed && !( mbox.m_intElapsed % mbox.m_intPoll ) ) ||
				( mbox.m_intElapsed < 0 )
				)
			{
				if (STATE_FINAL( mbox.m_intState ))
					mbox.Check();
			}
		}
	}
	if (bMassive)
	{
		if (nMassiveChecked)
			m_nMassivePoll = 0;
		else if (m_nMassivePoll>=theApp.m_nMassivePeriod)	// nothing is checked this time
			m_nLastMassiveBox = -1;	// restart cycle
	}
}

void CMagicDoc::EndResolveHostByName( WPARAM wParam, LPARAM lParam )
{
	for (int i=0; i<m_aMailboxes.GetSize(); i++)
	{
		m_aMailboxes[i]->EndResolveHostByName( wParam, lParam );
	}
}

BOOL CMagicDoc::MailboxProperties( CArray<CMailbox* > &arrayMailbox ) 
{
	CGeneralPage pageG; 
	CCommandPage pageC; 
	CPlaybackPage pageP; 
	CMessagePage pageM;
	CServerPage pageS;
	CLogPage pageL;

	pageG.Attach( &arrayMailbox );
	pageC.Attach( &arrayMailbox );
	pageP.Attach( &arrayMailbox );
	pageM.Attach( &arrayMailbox );
	pageS.Attach( &arrayMailbox );
	pageL.Attach( &arrayMailbox );

	CPropertySheet sheet( IDP_MAILBOX_PROPERTIES_TITLE, AfxGetMainWnd() );
	sheet.AddPage( &pageG );
	sheet.AddPage( &pageC );
	sheet.AddPage( &pageP );
	sheet.AddPage( &pageM );
	sheet.AddPage( &pageS );
	sheet.AddPage( &pageL );

	return (sheet.DoModal()==IDOK);
}

void CMagicDoc::OnStopCommandAll() 
{
	if( theApp.m_hCmdID )
	{
		TerminateProcess( theApp.m_hCmdID, 0 );
		theApp.m_hCmdID = 0;
	}

	for (int i=0; i<m_aMailboxes.GetSize(); i++)
	{
		CMailbox &mbox = *m_aMailboxes.ElementAt( i );
		if( mbox.m_hCmdID )
		{
			TerminateProcess( mbox.m_hCmdID, 0 );
			mbox.m_hCmdID = 0;
		}
	}
}

void CMagicDoc::OnUpdateStopCommandAll(CCmdUI* pCmdUI) 
{
	BOOL bEnable = FALSE;
	DWORD dwExitCode = 0;

	if( theApp.m_hCmdID )
	{
		if( !GetExitCodeProcess( theApp.m_hCmdID, &dwExitCode ) || STILL_ACTIVE != dwExitCode )
		{
			theApp.m_hCmdID = 0;
		}
		else bEnable = TRUE;
	}


	for (int i=0; i<m_aMailboxes.GetSize(); i++)
	{
		CMailbox &mbox = *m_aMailboxes.ElementAt( i );
		if( mbox.m_hCmdID )
		{
			if( !GetExitCodeProcess( mbox.m_hCmdID, &dwExitCode ) || STILL_ACTIVE != dwExitCode )
			{
				mbox.m_hCmdID = 0;
			}
			else bEnable = TRUE;
		}
	}

	if( pCmdUI ) pCmdUI->Enable( bEnable );		  
}

void CMagicDoc::OnStopPlaybackAll() 
{
	if( theApp.m_uMciID )
	{
		mciSendCommand( theApp.m_uMciID, MCI_STOP, MCI_WAIT, NULL );
		mciSendCommand( theApp.m_uMciID, MCI_CLOSE, 0, NULL);
		theApp.m_uMciID = 0;
	}

	for (int i=0; i<m_aMailboxes.GetSize(); i++)
	{
		CMailbox &mbox = *m_aMailboxes.ElementAt( i );
		if( mbox.m_uMciID ) 
		{
			mciSendCommand( mbox.m_uMciID, MCI_STOP, MCI_WAIT, NULL );
			mciSendCommand( mbox.m_uMciID, MCI_CLOSE, 0, NULL);
			mbox.m_uMciID = 0;
		}
	}

}

void CMagicDoc::OnUpdateStopPlaybackAll(CCmdUI* pCmdUI) 
{
	BOOL bEnable = FALSE;

	if( theApp.m_uMciID ) bEnable = TRUE;
	else
	{
		for (int i=0; i<m_aMailboxes.GetSize(); i++)
		{
			CMailbox &mbox = *m_aMailboxes.GetAt( i );
			if( mbox.m_uMciID )
			{
				bEnable = TRUE;
				break;
			}
		}
	}

	if( pCmdUI ) pCmdUI->Enable( bEnable );
}

void CMagicDoc::MciNotify( UINT uMciID )
{
	mciSendCommand( uMciID, MCI_STOP, MCI_WAIT, NULL );
	mciSendCommand( uMciID, MCI_CLOSE, 0, NULL);

	if( theApp.m_uMciID == uMciID ) theApp.m_uMciID = 0;
	else
	{
		for (int i=0; i<m_aMailboxes.GetSize(); i++)
		{
			CMailbox &mbox = *m_aMailboxes.ElementAt( i );
			if( mbox.m_uMciID == uMciID ) 
			{
				mbox.m_uMciID = 0;
				break;
			}
		}
	}
}

void CMagicDoc::Check(BOOL bForce)
{
	CTime tmCheck = CTime::GetCurrentTime();
	for (int i=0; i<m_aMailboxes.GetSize(); i++)
	{
		CMailbox &mbox = *m_aMailboxes.ElementAt( i );
		if( !mbox.m_bitSelected)
			continue;
		if (bForce || !mbox.IsDisabled())
		{
			mbox.m_tmCheckTime = tmCheck;
			mbox.Check();
		}
	}
}

void CMagicDoc::StopChecking()
{
	for (int i=0; i<m_aMailboxes.GetSize(); i++)
	{
		CMailbox &mbox = *m_aMailboxes.ElementAt( i );
		if( mbox.m_bitSelected ) mbox.StopChecking();
	}
}

void CMagicDoc::UpdateItem( CObject *pObject )
{
	ASSERT( pObject );

	if( pObject->IsKindOf( RUNTIME_CLASS( CMailbox ) ) )
	{
		CMailbox &mbox = *(CMailbox*) pObject;
		int intChanged = mbox.m_intChanged;
		if( !intChanged ) return;

		UpdateAllViews( 0, HINT_UPDATE_ITEM, &mbox );
	
		if( mbox.IsChanged( COLUMN_MAIL ) && STATE_FINAL( mbox.m_intState ) )
		{
			int intNewMailCount = 0;

			for( int i = 0; i < mbox.m_arrayExcerpt.GetSize(); ++i )
			{
				if( !mbox.m_arrayExcerpt[i]->m_bitWasReported )
				{
					++intNewMailCount;
					mbox.m_arrayExcerpt[i]->m_bitWasReported = 1;
				}
			}

			int intTotalMailCount = 0;

			for (int i=0; i<m_aMailboxes.GetSize(); i++)
				intTotalMailCount += m_aMailboxes[i]->m_arrayExcerpt.GetSize();

			CMagicFrame *wnd = (CMagicFrame*) AfxGetMainWnd();
			if( wnd )
			{
				wnd->UpdateMailCount( intTotalMailCount, intNewMailCount );
				if( intNewMailCount )
				{
					wnd->PostMessage( VM_START_COMMAND, intNewMailCount, (LPARAM) &mbox ); 
					wnd->PostMessage( VM_START_PLAYBACK, intNewMailCount, (LPARAM) &mbox ); 
					wnd->PostMessage( VM_START_MESSAGE, intNewMailCount, (LPARAM) &mbox ); 
				}
			}
		}

		mbox.m_bitCreated = 0;
		mbox.m_intChanged ^= intChanged;
	}
	else if( pObject->IsKindOf( RUNTIME_CLASS( CExcerpt ) )	)
	{
		CExcerpt &xrpt = *(CExcerpt*) pObject;
		int intChanged = xrpt.m_intChanged;
		if( !intChanged ) return;
		UpdateAllViews( 0, HINT_UPDATE_ITEM, &xrpt );
		xrpt.m_bitCreated = 0;
		xrpt.m_intChanged ^= intChanged;
	}
}

BOOL CMagicDoc::UpdateRequired()
{
	for (int i=0; i<m_aMailboxes.GetSize(); i++)
	{	
		CMailbox &mbox = *m_aMailboxes.ElementAt( i );
		if ( mbox.m_intChanged )
			return TRUE;
		int nC = mbox.m_arrayExcerpt.GetSize(); 
		for (int i=0; i<nC; i++)
		{
			if (mbox.m_arrayExcerpt[i]->m_intChanged)
				return TRUE;
		}
	}
	return FALSE;
}

void CMagicDoc::UpdateIdle()
{
	if (!UpdateRequired())
		return;

	UpdateAllViews( 0, HINT_DISABLE_SORT, 0 );

	for (int i=0; i<m_aMailboxes.GetSize(); i++)
	{	
		CMailbox &mbox = *m_aMailboxes.ElementAt( i );
		UpdateItem( &mbox );
		int m = mbox.m_arrayExcerpt.GetSize(); 
		while( m ) UpdateItem( mbox.m_arrayExcerpt[--m] );
	}

	UpdateAllViews( 0, HINT_ENABLE_SORT, 0 );
}


BOOL CMagicDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	LPBYTE pData = NULL;
	BOOL bOK = TRUE;
	CWaitCursor wait;
	try
	{
		CFile file;
		CFileException fe;
		if (!file.Open(lpszPathName, CFile::modeRead, &fe))
		{
			ReportSaveLoadException(lpszPathName, &fe, 
				FALSE, AFX_IDP_FAILED_TO_OPEN_DOC);
			return FALSE;
		}
		DeleteContents();
		SetModifiedFlag();  // dirty during de-serialize

		DWORD dwLen = (DWORD)file.GetLength();
		pData = new BYTE[dwLen+5];
		DWORD dwRead = (DWORD)file.Read(pData, dwLen);

		// check signiture - encrypted?
		if (IsEncrypted(pData, dwRead))		// decrypt data
		{
			// prompt for password
			do 
			{
				DPassword dlg(FALSE);
				if (dlg.DoModal() != IDOK)
				{
					delete pData;
					SetModifiedFlag(FALSE);
					// can not return FALSE, due to bugs in reinit
					if (AfxGetMainWnd())
						AfxGetMainWnd()->PostMessage(WM_COMMAND, ID_FILE_NEW);
					else
						((CMagicApp*)AfxGetApp())->m_bReset = TRUE;
					return TRUE;
				}
				m_sPassword = dlg.m_sPassword;
			}
			while (!DecryptData(pData, dwRead, m_sPassword));
		}
		else
			m_sPassword.Empty();

		CMemFile mf;
		mf.Attach(pData, dwRead);
		CArchive ar(&mf, CArchive::load);
		ar.m_pDocument = this;
		Serialize(ar);     // load me
		ar.Close();
		mf.Detach();
	}
	catch (CException* e)
	{
		ReportSaveLoadException(lpszPathName, e, 
				FALSE, AFX_IDP_FAILED_TO_OPEN_DOC);
		e->Delete();
		bOK = FALSE;
	}
	catch(...)
	{
		ReportSaveLoadException(lpszPathName, NULL, 
				FALSE, AFX_IDP_FAILED_TO_OPEN_DOC);
		bOK = FALSE;
	}
	delete pData;
	if (!bOK)
	{
		DeleteContents();
		return FALSE;
	}
	
	SetModifiedFlag(FALSE);
	UpdateFolderStats();
	
	CMagicFrame *wnd = (CMagicFrame*) AfxGetMainWnd();
	if( wnd )
	{
		wnd->UpdateMailCount( 0, 0 );
	}

	return TRUE;
}

BOOL CMagicDoc::OnNewDocument() 
{
	CMagicFrame *wnd = (CMagicFrame*) AfxGetMainWnd();
	if( wnd )
	{
		wnd->UpdateMailCount( 0, 0 );
	}
	
	return CDocument::OnNewDocument();
}

BOOL CMagicDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	LPBYTE pData = NULL;
	BOOL bOK = TRUE;
	try
	{
		CMemFile mf;
		CArchive ar(&mf, CArchive::store | CArchive::bNoFlushOnDelete);
		ar.m_pDocument = this;
		CWaitCursor wait;
		Serialize(ar);     // save me
		ar.Close();
		DWORD dwLen = (DWORD)mf.GetLength();
		pData = mf.Detach();
		if (!m_sPassword.IsEmpty())
		{
			EncryptData(pData, dwLen, m_sPassword);
		}
		CFile file;
		CFileException fe;
		if (file.Open(lpszPathName, CFile::modeWrite|CFile::modeCreate, &fe))
		{
			file.Write(pData, dwLen);
			file.Close();
		}
		else
		{
			ReportSaveLoadException(lpszPathName, &fe, 
				TRUE, AFX_IDP_FAILED_TO_SAVE_DOC);
			bOK = FALSE;
		}
	}
	catch (CException* e)
	{
		ReportSaveLoadException(lpszPathName, e, 
			TRUE, AFX_IDP_FAILED_TO_SAVE_DOC);
		e->Delete();
		bOK = FALSE;
	}
	catch(...)
	{
		ReportSaveLoadException(lpszPathName, NULL, 
			TRUE, AFX_IDP_FAILED_TO_SAVE_DOC);
		bOK = FALSE;
	}
	
	if (bOK)
		SetModifiedFlag(FALSE);
	delete pData;

	return bOK;
}

void CMagicDoc::OnPassword() 
{
	DPassword dlg(TRUE);
	dlg.m_sPassword = m_sPassword;
	if (dlg.DoModal()!=IDOK)
		return;
	m_sPassword = dlg.m_sPassword;
	SetModifiedFlag(TRUE);
}

void CMagicDoc::OnUpdatePassword(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(!m_sPassword.IsEmpty());
}

void SplitStringToArray(const CString& s, CStringArray& as, char chSep)
{
	int i=0;
	CString sWord;
	while (AfxExtractSubString(sWord, s, i, chSep))
	{
		sWord.TrimLeft();
		sWord.TrimRight();
		as.SetAtGrow(i++, sWord);
	}
	as.SetSize(i);
}
enum CSVCols 
{
	CSV_SERVER=1, CSV_NAME, CSV_USER, 
	CSV_PASS, CSV_PORT, CSV_SSL, 
	CSV_RETR, CSV_POLL, CSV_FOLDER,
};
LPCTSTR asCols[]=
{
	_T("server|host"), _T("name|alias"), _T("user"),
	_T("password|pass"), _T("port"), _T("ssl"),
	_T("retr"), _T("poll"), _T("folder"),
};
void CMagicDoc::OnImport()
{
	CFileDialog dlg(TRUE, "csv", NULL, OFN_FILEMUSTEXIST, "*.csv|*.csv|*.*|*.*||");
	if (dlg.DoModal()!=IDOK)
		return;
	CString sFile = dlg.GetPathName();
	try
	{
		CStdioFile file;
		if (!file.Open(sFile, CFile::typeText|CFile::modeRead, NULL))
			return;
		CString sHead;
		if (!file.ReadString(sHead))
			return;
		char chSep=',';
		if (sHead.Find(',')>=0)
			chSep=',';
		else if (sHead.Find(';')>=0)
			chSep=';';
		CStringArray asHeads;
		SplitStringToArray(sHead, asHeads, chSep);
		CByteArray anCols;
		if (asHeads.GetSize()<3)
		{
			AfxMessageBox("At least 3 columns names required in first line!");
			return;
		}
		for (int i=0; i<asHeads.GetSize(); i++)
		{
			CString sName(asHeads[i]);
			sName.MakeLower();
			for (int t=0; t<_countof(asCols); t++)
			{
				CString sMatch(asCols[t]);
				if (sMatch.Find(sName)>=0)
					anCols.SetAtGrow(i, t+1);
			}
		}
		CString s;
		while (file.ReadString(s))
		{
			CStringArray asCols;
			SplitStringToArray(s, asCols, chSep);
			if (!asCols.GetSize())
				continue;
			CMailbox* pBox = new CMailbox();
			int nCreateAt = -1;
			for (int c=0; c<asCols.GetSize(); c++)
			{
				if (c>=anCols.GetSize() || anCols[c]==0 || asCols[c].IsEmpty())
					continue;
				switch (anCols[c])
				{
				case CSV_SERVER:
					pBox->m_strHost = asCols[c];
					break;
				case CSV_NAME:
					pBox->m_strAlias= asCols[c];
					break;
				case CSV_USER:
					pBox->m_strUser= asCols[c];
					break;
				case CSV_PASS:
					pBox->m_strPass= asCols[c];
					break;
				case CSV_PORT:
					pBox->m_intPort = atoi(asCols[c]);
					break;
				case CSV_SSL:
					pBox->SetFlag(MBF_SSL, atoi(asCols[c]));
					break;
				case CSV_RETR:
					pBox->SetFlag(MBF_NO_RETR, atoi(asCols[c]));
					break;
				case CSV_POLL:
					pBox->m_intPoll = atoi(asCols[c]);
					break;
				case CSV_FOLDER:
					{
						nCreateAt = FindFolder(asCols[c], TRUE);
						if (nCreateAt>=0)
						{
							pBox->MakeInFolder(TRUE);
						}
					}
					break;
				}
			}
			if (nCreateAt<0)
				m_aMailboxes.Add(pBox);
			else
				m_aMailboxes.InsertAt(nCreateAt+1, pBox);
		}
	}

	catch (CException* e)
	{
		e->Delete();
	}
	catch (...)
	{
	}
	UpdateFolderStats();

	SetModifiedFlag();
}

CMailbox* CMagicDoc::GetBox(int idx)
{
	if (idx<0 || idx>=m_aMailboxes.GetSize())
		return NULL;
	return m_aMailboxes[idx];
}

void CMagicDoc::UpdateFolder(CMailbox* pBox)
{
	if (!pBox->IsFolder())
	{
		int nPos = FindRoot(pBox);
		if (nPos<0)
			return;
		pBox = m_aMailboxes[nPos];
	}
	pBox->Change(COLUMN_MAIL);
	if (pBox->UpdateUnreadStatus())
		pBox->Change(COLUMN_ALIAS);
	//UpdateItem(pBox);
}

void CMagicDoc::UpdateFolderStats()
{
	CMailboxFolder* pFolder = NULL;
	for (int i=0; i<m_aMailboxes.GetSize(); i++)
	{
		if (m_aMailboxes[i]->IsFolder())
		{
			if (pFolder)
			{
				pFolder->Change(COLUMN_USER);
				UpdateItem(pFolder);
			}
			pFolder = (CMailboxFolder*)m_aMailboxes[i];
			pFolder->ResetBoxes();
			continue;
		}
		if (m_aMailboxes[i]->InFolder())
			pFolder->AddBox(m_aMailboxes[i]);
		else
			m_aMailboxes[i]->CloseFolder(FALSE);

	}
	if (pFolder)
	{
		pFolder->Change(COLUMN_USER);
		pFolder->Change(COLUMN_MAIL);
		UpdateItem(pFolder);
	}
}

void CMagicDoc::ToggleFolder(CMailbox* pFolder)
{
	int nFolder = FindBox(pFolder);
	if (nFolder<0)
		return;
	pFolder->CloseFolder(!pFolder->IsClosed());
	for (int i=nFolder+1; i<m_aMailboxes.GetSize()&& !m_aMailboxes[i]->IsFolder(); i++)
	{
		if (!m_aMailboxes[i]->InFolder())
			continue;
		m_aMailboxes[i]->CloseFolder(pFolder->IsClosed());
		if (pFolder->IsClosed())
			UpdateItem(m_aMailboxes[i]);
		else
		{
			m_aMailboxes[i]->m_bitCreated = 1;
			int nStore = m_aMailboxes[i]->m_intChanged;
			m_aMailboxes[i]->m_intChanged = -1;
			UpdateItem(m_aMailboxes[i]);
			m_aMailboxes[i]->m_intChanged = nStore;
		}

		UpdateItem(m_aMailboxes[i]);
	}

}
