/////////
///// COneInstance implementation
////////

#include "stdafx.h"
#include "oneinst.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[ ] = __FILE__;
#endif

COneInstance::COneInstance()
{
	m_hFile=NULL;
	m_bFirst=TRUE;
	m_pData=NULL;
}
COneInstance::~COneInstance()
{
	if (m_pData)
		UnmapViewOfFile(m_pData);
	if (m_hFile)
		CloseHandle(m_hFile);
}

BOOL	COneInstance::AmIFirst(LPCTSTR sUnique)
{
	// add current user name - this will allow app to run under different users at the same time
	TCHAR szUser[50];
	DWORD dwSize = 49;
	GetUserName(szUser, &dwSize);
	CString sName(sUnique);
	sName += szUser;
	m_hFile=CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 
		0, sizeof(HWND)*2, sName);
	m_bFirst = (ERROR_ALREADY_EXISTS != ::GetLastError());
	if (m_hFile == INVALID_HANDLE_VALUE)
		return TRUE;
	DWORD dwAccs = m_bFirst ? FILE_MAP_WRITE : FILE_MAP_READ;
	m_pData = MapViewOfFile(m_hFile, dwAccs, 0, 0, 0);
	if (!m_pData)
		return TRUE;
	if (m_bFirst)
		ZeroMemory(m_pData, sizeof(HWND));
	return m_bFirst;
}

BOOL	COneInstance::Init(HWND hw)
{
	// save our hw to file
	if (!m_pData || !m_bFirst)
		return  FALSE;
	memcpy(m_pData, &hw, sizeof(hw));
	return TRUE;
}

BOOL	COneInstance::SendData(DWORD dwData, DWORD dwSize, LPVOID pData)
{
	if (!m_pData)
		return FALSE;
	HWND hwFirst = 0;
	memcpy(&hwFirst, m_pData, sizeof(HWND));
	if (!hwFirst)
		return FALSE;
	COPYDATASTRUCT cds;
	cds.dwData = dwData;
	cds.cbData = dwSize;
	cds.lpData = pData;
	SendMessage(hwFirst, WM_COPYDATA, NULL, (LPARAM)&cds);
	return TRUE;
}
