// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// LogPage.cpp : implementation file
//

#include "stdafx.h"
#include "magic.h"
#include "LogPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLogPage property page

IMPLEMENT_DYNCREATE(CLogPage, CPropertyPage)

CLogPage::CLogPage() : CPropertyPage(CLogPage::IDD)
{
	//{{AFX_DATA_INIT(CLogPage)
	//}}AFX_DATA_INIT
}

CLogPage::~CLogPage()
{
}

void CLogPage::Attach( CArray<CMailbox*> *pMailboxArray )
{
	ASSERT( pMailboxArray );
	m_pMailboxArray = pMailboxArray;
}

void CLogPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLogPage)
	DDX_Control(pDX, IDC_LOG, m_edtLog);
	DDX_Control(pDX, IDC_ENABLE_LOGGING, m_chkLog);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLogPage, CPropertyPage)
	//{{AFX_MSG_MAP(CLogPage)
	ON_BN_CLICKED(IDC_ENABLE_LOGGING, OnEnableLogging)
	ON_BN_CLICKED(IDC_LOG_REFRESH, OnLogRefresh)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CLogPage::OnInitDialog()
{
	if( !CPropertyPage::OnInitDialog() ) return FALSE;
	
	CMailbox &mbox = *( m_pMailboxArray->GetAt( 0 ) );
	m_chkLog.SetCheck( mbox.IsLoggingEnabled() );

	for( int i = m_pMailboxArray->GetUpperBound(); i; --i )
	{
		CMailbox &mbox = *( m_pMailboxArray->GetAt( i ) );

		if( ( m_chkLog.GetCheck() != 2 ) && 
			( ( m_chkLog.GetCheck() != 0 ) != mbox.IsLoggingEnabled() ) ) 
		{
			m_chkLog.SetCheck( 2 );
		}
	}

	OnLogRefresh();

	UpdateData( FALSE );
	DlgTranslate(this);

	return TRUE;
}

BOOL CLogPage::OnApply() 
{
	for( int i = m_pMailboxArray->GetSize(); i; --i )
	{
		CMailbox &mbox = *( m_pMailboxArray->GetAt( i-1 ) );
		
		if( m_chkLog.GetCheck() != 2 ) mbox.EnableLogging( 0 != m_chkLog.GetCheck() );
	}

	return CPropertyPage::OnApply();
}

/////////////////////////////////////////////////////////////////////////////
// CLogPage message handlers

void CLogPage::OnEnableLogging() 
{
	SetModified();
}

void CLogPage::OnLogRefresh() 
{
	CMailbox &mbox = *( m_pMailboxArray->GetAt( 0 ) );
	m_edtLog.SetWindowText( mbox.GetLogString() );

	for( int i = m_pMailboxArray->GetUpperBound(); i; --i )
	{
		CMailbox &mbox = *( m_pMailboxArray->GetAt( i ) );
		
		CString strTemp; 
		m_edtLog.GetWindowText( strTemp );

		if( ( strTemp != cstrMultipleSelection ) && 
			( strTemp != mbox.GetLogString() ) )
		{
			m_edtLog.SetWindowText( cstrMultipleSelection );
		}
	}
}
