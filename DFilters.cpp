// DFilters.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "magic.h"
#include "magicdoc.h"
#include "mailbox.h"
#include "DFilters.h"
#include "tools.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DFilterEditor dialog

class DFilterEditor : public CDialog
{
public:
	DFilterEditor(CFilterCnd* pCnd);

	//{{AFX_DATA(DFilterEditor)
	enum { IDD = IDD_EDIT_FILTER };
	CComboBox	m_cbField;
	CComboBox	m_cbCondition;
	CString		m_sFilterText;
	int		m_nField;
	int		m_nCnd;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(DFilterEditor)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
protected:
	void UpdateSampleString();
	CFilterCnd* m_pCnd;


	//{{AFX_MSG(DFilterEditor)
	afx_msg void OnChangeFltText();
	afx_msg void OnEditFile();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnUpdateString();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DFilters 


DFilters::DFilters(CMagicDoc* pDoc)
	: CDialog(DFilters::IDD, NULL)
{
	//{{AFX_DATA_INIT(DFilters)
	m_sName = _T("");
	m_bOthers = FALSE;
	m_bColor = FALSE;
	m_bDelete = FALSE;
	m_bMark = FALSE;
	m_bRead = FALSE;
	m_bAllow = FALSE;
	m_sFilter1 = _T("");
	m_sFilter2 = _T("");
	m_nCombine = 0;
	m_bFriend = FALSE;
	m_bProtect = FALSE;
	//}}AFX_DATA_INIT
	m_pDoc = pDoc;
	m_nCurSel = -1;
}


void DFilters::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	if (!pDX->m_bSaveAndValidate && 
		m_nCurSel >=0 &&
		m_nCurSel < m_Filters.GetSize() )	// get data from cur mailbox
	{
		CMailFilter& mf = m_Filters.ElementAt(m_nCurSel);
		m_sName = mf.m_sName;
		m_nCombine = mf.m_nCombination;
		m_bOthers = (mf.m_dwAction & MFA_OTHER)!=0;
		m_bColor = (mf.m_dwAction & MFA_COLOR)!=0;
		m_bMark = (mf.m_dwAction & MFA_SPAM)!=0;
		m_bDelete = (mf.m_dwAction & MFA_DELETE)!=0;
		m_bRead = (mf.m_dwAction & MFA_READ)!=0;
		m_bProtect = (mf.m_dwAction & MFA_PROTECT)!=0;
		m_bFriend = (mf.m_dwAction & MFA_FRIEND)!=0;
		mf.m_aCnd[0].ToText(m_sFilter1);
		mf.m_aCnd[1].ToText(m_sFilter2);
		
		int idx = m_cbMailbox.FindStringExact(0, mf.m_sMailBox);
		m_cbMailbox.SetCurSel(idx);
	}
	//{{AFX_DATA_MAP(DFilters)
	DDX_Control(pDX, IDC_LIST, m_lbList);
	DDX_Control(pDX, IDC_FLT_MBOX, m_cbMailbox);
	DDX_Text(pDX, IDC_FILTER_NAME, m_sName);
	DDX_Check(pDX, IDC_FLT_OTHERS, m_bOthers);
	DDX_Check(pDX, IDC_FLT_COLOR, m_bColor);
	DDX_Check(pDX, IDC_FLT_DELETE, m_bDelete);
	DDX_Check(pDX, IDC_FLT_MARK1, m_bMark);
	DDX_Check(pDX, IDC_FLT_READ, m_bRead);
	DDX_Check(pDX, IDC_ALLOW_FILTERS, m_bAllow);
	DDX_Text(pDX, IDC_FLT1, m_sFilter1);
	DDX_Text(pDX, IDC_FLT2, m_sFilter2);
	DDX_CBIndex(pDX, IDC_COMBINE, m_nCombine);
	DDX_Control(pDX, IDC_FLT_CLR_SAMPLE, m_Sample);
	DDX_Check(pDX, IDC_FLT_FRIEND, m_bFriend);
	DDX_Check(pDX, IDC_FLT_PROTECT, m_bProtect);
	//}}AFX_DATA_MAP
	if (pDX->m_bSaveAndValidate)
	{
		if ( m_nCurSel >=0 &&
			m_nCurSel < m_Filters.GetSize() )	// get data from cur mailbox
		{
			CMailFilter& mf = m_Filters.ElementAt(m_nCurSel);
			mf.m_sName = m_sName;
			mf.m_nCombination = (BYTE)m_nCombine ;
			mf.m_dwAction = 0;
			if (m_bOthers)
				mf.m_dwAction |= MFA_OTHER;
			if (m_bColor)
				mf.m_dwAction |= MFA_COLOR;
			if (m_bMark)
				mf.m_dwAction |= MFA_SPAM;
			if (m_bDelete)
				mf.m_dwAction |= MFA_DELETE;
			if (m_bProtect)
				mf.m_dwAction |= MFA_PROTECT;
			if (m_bFriend)
				mf.m_dwAction |= MFA_FRIEND;
			if (m_bRead)
				mf.m_dwAction |= MFA_READ;
			if (m_lbList.GetCheck(m_nCurSel))
				mf.m_dwAction |= MFA_ENABLED;
			int nCur = m_cbMailbox.GetCurSel();
			if (nCur == 0 || nCur == CB_ERR)
				mf.m_sMailBox = "*";
			else
				mf.m_sMailBox = (LPCTSTR)m_cbMailbox.GetItemData(nCur);
		}
	}
}


BEGIN_MESSAGE_MAP(DFilters, CDialog)
	//{{AFX_MSG_MAP(DFilters)
	ON_LBN_SELCHANGE(IDC_LIST, OnSelchangeList)
	ON_BN_CLICKED(IDC_FLT_ADD, OnFltAdd)
	ON_BN_CLICKED(IDC_FLT_DEL, OnFltDel)
	ON_BN_CLICKED(IDC_FLT_DOWN, OnFltDown)
	ON_BN_CLICKED(IDC_FLT_UP, OnFltUp)
	ON_EN_CHANGE(IDC_FILTER_NAME, OnChangeFilterName)
	ON_BN_CLICKED(IDC_FLT_COLOR_SEL, OnFltColorSel)
	ON_BN_CLICKED(IDC_FLT_COLOR, OnFltColor)
	ON_BN_CLICKED(IDC_FLT1_BRS, OnFlt1Brs)
	ON_BN_CLICKED(IDC_FLT2_BRS, OnFlt2Brs)
	ON_CBN_SELENDOK(IDC_COMBINE, OnSelendokCombine)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten DFilters 

LPCTSTR asFields[]=
{
	"${Subject}", "${From}", "${To}",
	"${CC}",		"${Header}", "${Size}",
	"${Date}",
};

LPCTSTR asOps[]=
{
	"${Equals}", "${Includes}", ">",
		"${NOT Includes}", "${Includes RegExp}",
};

BOOL DFilters::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	Prepare();

	DlgTranslate(this);

	GetData();

	return TRUE;
}

CMailFilter::CMailFilter()
{
	for (int i=0; i<nConditions; i++)
	{
		m_aCnd[i].m_nField = MFF_FROM;
		m_aCnd[i].m_nOperation = MFO_EQUAL;
	}
	m_nCombination = MFC_NONE;
	m_dwAction = MFA_NONE;
	m_Color = 0;	// black
	m_sMailBox = "*";
}

CMailFilter::CMailFilter(const CMailFilter& a)
{
	*this = a;
}


CMailFilter& CMailFilter::operator=(const CMailFilter& src)
{
	m_sMailBox = src.m_sMailBox;
	m_Color = src.m_Color;
	m_dwAction = src.m_dwAction;
	m_sName = src.m_sName;
	
	m_nCombination = src.m_nCombination;
	for (int i=0; i<nConditions; i++)
	{
		m_aCnd[i].m_nField = src.m_aCnd[i].m_nField;
		m_aCnd[i].m_nOperation = src.m_aCnd[i].m_nOperation;
		m_aCnd[i].m_sText = src.m_aCnd[i].m_sText;
	}

	return *this;
}
/* version history
  2. added array of conditions
  1. start
*/
static BYTE nLastVer = 2;
void CMailFilter::Serialize(CArchive& ar)
{
	if (ar.IsLoading())
	{
		BYTE bVersion = 0;
		ar >> bVersion;
		if (bVersion > nLastVer)
			AfxThrowArchiveException( CArchiveException::badIndex, _T("") );
		if (bVersion<2)
		{
			ar >> m_aCnd[0].m_nField;
			ar >> m_aCnd[0].m_nOperation;
			ar >> m_aCnd[0].m_sText;
			m_nCombination = MFC_NONE;
			m_aCnd[1].Reset();
			m_aCnd[0].ClearData();
		}
		else
		{
			ar >> m_nCombination;
			BYTE bCnds = 0;
			ar >> bCnds;
			for (int i=0; i<bCnds; i++)
			{
				int nIdx = min(i, nConditions-1);
				ar >> m_aCnd[nIdx].m_nField;
				ar >> m_aCnd[nIdx].m_nOperation;
				ar >> m_aCnd[nIdx].m_sText;
				m_aCnd[nIdx].ClearData();
			}
		}
		ar >> m_sMailBox;
		ar >> m_Color;
		ar >> m_dwAction;
		ar >> m_sName;
	}
	else	// writing
	{
		ar << nLastVer;
		ar << m_nCombination;
		ar << (BYTE)nConditions;
		for (int i=0; i<nConditions; i++)
		{
			ar << m_aCnd[i].m_nField;
			ar << m_aCnd[i].m_nOperation;
			ar << m_aCnd[i].m_sText;
		}
		ar << m_sMailBox;
		ar << m_Color;
		ar << m_dwAction;
		ar << m_sName;
	}
}

void DFilters::OnOK() 
{
	UpdateData();
	theApp.m_Filters.Copy(m_Filters);
	
	theApp.intEnableFilters = m_bAllow;

	if (m_pDoc)
		m_pDoc->SetModifiedFlag();

	CDialog::OnOK();
}

void DFilters::GetData()
{
	m_bAllow = theApp.intEnableFilters;

	m_Filters.Copy(theApp.m_Filters);
	m_lbList.ResetContent();
	for (int i=0; i<m_Filters.GetSize(); i++)
	{
		CMailFilter& mf = m_Filters.ElementAt(i);
		int idx = m_lbList.AddString(mf.m_sName);
		m_lbList.SetCheck(idx, (mf.m_dwAction & MFA_ENABLED)!=0 );
	}
	m_lbList.SetCurSel(0);
	UpdateData(FALSE);
	OnSelchangeList();
}

void DFilters::OnSelchangeList() 
{
	UpdateData();
	m_nCurSel = m_lbList.GetCurSel();
	UpdateData(FALSE);
	UpdateColor();
	OnSelendokCombine();
}

void DFilters::OnFltAdd() 
{
	CMailFilter mf;
	m_Filters.Add(mf);
	int i = m_lbList.AddString("?");
	m_lbList.SetCurSel(i);
	OnSelchangeList();
	if (GetDlgItem(IDC_FILTER_NAME))
		GetDlgItem(IDC_FILTER_NAME)->SetFocus();
}

void DFilters::OnFltDel() 
{
	if (m_nCurSel<0 || m_nCurSel>=m_Filters.GetSize())
		return;
	m_Filters.RemoveAt(m_nCurSel);
	m_lbList.DeleteString(m_nCurSel);
	m_lbList.SetCurSel(max(0, m_nCurSel-1));
	m_nCurSel = -1;
	OnSelchangeList();
}

void DFilters::OnFltDown() 
{
	if (m_nCurSel<0 || m_nCurSel>=m_Filters.GetSize()-1)
		return;
	CMailFilter mf = m_Filters[m_nCurSel];
	m_Filters[m_nCurSel] = m_Filters[m_nCurSel+1];
	m_Filters[m_nCurSel+1] = mf;
	m_lbList.DeleteString(m_nCurSel);
	m_lbList.InsertString(m_nCurSel+1, mf.m_sName);
	m_lbList.SetCurSel(m_nCurSel+1);
	m_lbList.SetCheck(m_nCurSel+1, (mf.m_dwAction & MFA_ENABLED)!=0 );
	m_nCurSel = m_nCurSel+1;
	OnSelchangeList();
}

void DFilters::OnFltUp() 
{
	if (m_nCurSel<=0 || m_nCurSel>=m_Filters.GetSize())
		return;
	CMailFilter mf = m_Filters[m_nCurSel];
	m_Filters[m_nCurSel] = m_Filters[m_nCurSel-1];
	m_Filters[m_nCurSel-1] = mf;
	m_lbList.DeleteString(m_nCurSel);
	m_lbList.InsertString(m_nCurSel-1, mf.m_sName);
	m_lbList.SetCurSel(m_nCurSel-1);
	m_lbList.SetCheck(m_nCurSel-1, (mf.m_dwAction & MFA_ENABLED)!=0 );
	m_nCurSel = m_nCurSel-1;
	OnSelchangeList();
}

void DFilters::OnChangeFilterName() 
{
	if (m_nCurSel<0 || m_nCurSel>=m_Filters.GetSize())
		return;
	UpdateData();
	m_lbList.DeleteString(m_nCurSel);
	m_lbList.InsertString(m_nCurSel, m_Filters[m_nCurSel].m_sName);
	m_lbList.SetCurSel(m_nCurSel);
}

void AFXAPI SerializeElements(CArchive& ar, CMailFilter* pElements, INT_PTR nCount)
{
	ASSERT(nCount == 0 ||
		AfxIsValidAddress(pElements, nCount * sizeof(CMailFilter)));

	for (int i=0; i<nCount; i++)
		pElements[i].Serialize(ar);
}

void DFilters::Prepare()
{
	// mailbox list
	m_cbMailbox.ResetContent();
	m_cbMailbox.AddString("*");	// for all boxes
	if (m_pDoc)
	{
		for (int i=0;i<m_pDoc->m_aMailboxes.GetSize(); i++)
		{
			CMailbox &mbox = *m_pDoc->m_aMailboxes.GetAt( i );
			int idx = m_cbMailbox.AddString(mbox.m_strAlias);
			// we can store string pointer, since no one can change strings,
			//  while dlg is active
			m_cbMailbox.SetItemData(idx, (DWORD)(LPCTSTR)mbox.m_strAlias);
		}
	}
	SendDlgItemMessage(IDC_FLT_UP, BM_SETIMAGE, IMAGE_ICON, 
		(LPARAM)AfxGetApp()->LoadIcon(IDI_UP));
	SendDlgItemMessage(IDC_FLT_DOWN, BM_SETIMAGE, IMAGE_ICON, 
		(LPARAM)AfxGetApp()->LoadIcon(IDI_DOWN));
}

void DFilters::OnFltColorSel() 
{
	if (m_nCurSel<0 || m_nCurSel>=m_Filters.GetSize())
		return;
	CColorDialog dlg(m_Filters[m_nCurSel].m_Color);
	if (dlg.DoModal()!=IDOK)
		return;
	m_Filters[m_nCurSel].m_Color = dlg.GetColor();
	UpdateColor();
}

void DFilters::OnFltColor() 
{
	UpdateData();
	UpdateColor();
}

void DFilters::UpdateColor()
{
	BOOL bStd = (!m_bColor || m_nCurSel<0 || m_nCurSel >= m_Filters.GetSize());
	if (bStd)
		m_Sample.SetColor(TRUE);
	else
		m_Sample.SetColor(FALSE, m_Filters[m_nCurSel].m_Color);
}


void DFilters::OnFlt1Brs() 
{
	if (m_nCurSel<0 || m_nCurSel>=m_Filters.GetSize())
		return;
	DFilterEditor dlg(&m_Filters[m_nCurSel].m_aCnd[0]);
	dlg.DoModal();
	CString s;
	m_Filters[m_nCurSel].m_aCnd[0].ToText(s);
	SetDlgItemText(IDC_FLT1, s);
}

void DFilters::OnFlt2Brs() 
{
	if (m_nCurSel<0 || m_nCurSel>=m_Filters.GetSize())
		return;
	DFilterEditor dlg(&m_Filters[m_nCurSel].m_aCnd[1]);
	dlg.DoModal();
	CString s;
	m_Filters[m_nCurSel].m_aCnd[1].ToText(s);
	SetDlgItemText(IDC_FLT2, s);
}

BOOL FieldToText(int nF, CString& s)
{
	int nMax = sizeof(asFields)/sizeof(asFields[0]);
	if (nF>=0 && nF<nMax)
	{
		s = asFields[nF];
		StrTranslate(s);
		return TRUE;
	}
	return FALSE;
}
BOOL OpToText(int nOp, CString& s)
{
	int nMax = sizeof(asOps)/sizeof(asOps[0]);
	if (nOp>=0 && nOp<nMax)
	{
		s = asOps[nOp];
		StrTranslate(s);
		return TRUE;
	}
	return FALSE;
}

void CFilterCnd::ToText(CString& s)
{
	s.Empty();
	if (m_sText.IsEmpty())
		return;
	CString sField;
	if (!FieldToText(m_nField, sField))
		return;
	CString sOp;
	if (!OpToText(m_nOperation, sOp))
		return;
	s.Format("<%s> %s \'%s\'", sField, sOp, m_sText);
}
/////////////////////////////////////////////////////////////////////////////
// DFilterEditor dialog


DFilterEditor::DFilterEditor(CFilterCnd* pCnd)
	: CDialog(DFilterEditor::IDD, NULL)
{
	//{{AFX_DATA_INIT(DFilterEditor)
	m_sFilterText = _T("");
	m_nField = -1;
	m_nCnd = -1;
	//}}AFX_DATA_INIT
	m_pCnd = pCnd;
}


void DFilterEditor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DFilterEditor)
	DDX_Control(pDX, IDC_FLT_FIELD, m_cbField);
	DDX_Control(pDX, IDC_FLT_CND, m_cbCondition);
	DDX_Text(pDX, IDC_FLT_TEXT, m_sFilterText);
	DDX_CBIndex(pDX, IDC_FLT_FIELD, m_nField);
	DDX_CBIndex(pDX, IDC_FLT_CND, m_nCnd);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DFilterEditor, CDialog)
	//{{AFX_MSG_MAP(DFilterEditor)
	ON_EN_CHANGE(IDC_FLT_TEXT, OnChangeFltText)
	ON_BN_CLICKED(IDC_EDIT_FILE, OnEditFile)
	ON_CBN_SELENDOK(IDC_FLT_CND, OnUpdateString)
	ON_CBN_SELENDOK(IDC_FLT_FIELD, OnUpdateString)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DFilterEditor message handlers

BOOL DFilterEditor::OnInitDialog() 
{
	CDialog::OnInitDialog();
	DlgTranslate(this);
	// possible fields
	m_cbField.ResetContent();
	int nMax = sizeof(asFields)/sizeof(asFields[0]);
	int i=0;
	for (i=0; i<nMax; i++)
	{
		CString s(asFields[i]);
		StrTranslate(s);
		m_cbField.AddString(s);
	}

	// possible operations
	m_cbCondition.ResetContent();
	nMax = sizeof(asOps)/sizeof(asOps[0]);
	for (i=0; i<nMax; i++)
	{
		CString s(asOps[i]);
		StrTranslate(s);
		m_cbCondition.AddString(s);
	}
	m_sFilterText = m_pCnd->m_sText;
	m_nField = m_pCnd->m_nField;
	m_nCnd = m_pCnd->m_nOperation;
	UpdateData(FALSE);

	OnChangeFltText();

	return TRUE;
}

void DFilterEditor::OnOK() 
{
	UpdateData();

	m_pCnd->m_sText = m_sFilterText;
	m_pCnd->m_nField = (BYTE)m_nField;
	m_pCnd->m_nOperation = (BYTE)m_nCnd;
	
	CDialog::OnOK();
}
void DFilterEditor::OnChangeFltText() 
{
	UpdateData();
	GetDlgItem(IDC_EDIT_FILE)->ShowWindow(m_sFilterText.Find("$")==0);
	UpdateSampleString();
}

void DFilterEditor::UpdateSampleString()
{
	UpdateData();
	CFilterCnd cnd;
	cnd.m_sText = m_sFilterText;
	cnd.m_nField = (BYTE)m_nField;
	cnd.m_nOperation = (BYTE)m_nCnd;
	CString s;
	cnd.ToText(s);
	SetDlgItemText(IDC_SAMPLE, s);
} 

void DFilterEditor::OnEditFile() 
{
	UpdateData();
	CString s;
	if (!FindLocalFile( ((LPCTSTR)m_sFilterText)+1 , s, TRUE) )
	{
		return;
	}
	ShellExecute(NULL, "Open", s, NULL, NULL, SW_SHOW);
}

void DFilterEditor::OnUpdateString() 
{
	UpdateSampleString();
}

void DFilters::OnSelendokCombine() 
{
	UpdateData();
	GetDlgItem(IDC_FLT2)->EnableWindow(m_nCombine>0);
	GetDlgItem(IDC_FLT2_BRS)->EnableWindow(m_nCombine>0);
}

BOOL CFilterCnd::LoadFile(LPCTSTR sPath, TCHAR cRem, BOOL bCaseSens)
{
	CStdioFile file;
	try
	{
		if (!file.Open(sPath, CFile::modeRead|CFile::typeText))
			return FALSE;
		m_asData.SetSize(1, 10);
		int nCur = 0;
		int nLine = 0;
		while (file.ReadString(m_asData[nCur]))
		{
			if (!m_asData[nCur].IsEmpty() && m_asData[nCur].GetAt(0)!=cRem)
			{
				if (!bCaseSens)
					m_asData.ElementAt(nCur).MakeLower();
				m_asData[nCur].TrimLeft();
				m_asData[nCur].TrimRight();
				m_anLines.SetAtGrow(nCur, nLine);
				nCur++;
				m_asData.Add("");
			}
			nLine++;
		}
		m_asData.RemoveAt(m_asData.GetSize()-1);
	}
	catch(CException* e)
	{
		e->Delete();
	}
	catch(...)
	{
	}
	file.Close();
	return TRUE;
}

void CFilterCnd::Prepare()
{
	BOOL bTestCase = (m_nOperation == MFO_INC_RE);
	CString sPath;
	if (m_sText.Find("$")==0 && FindLocalFile(m_sText.Mid(1), sPath))
	{
		// may be we have latest version?
		WIN32_FILE_ATTRIBUTE_DATA data;
		if (!GetFileAttributesEx(sPath,  GetFileExInfoStandard, &data))
			return;
		if (data.nFileSizeLow == m_dwFileSize && CompareFileTime(&data.ftLastWriteTime, &m_ft)==0)
			return;
	
		LoadFile(sPath, ';', bTestCase);
		m_dwFileSize = data.nFileSizeLow;
		memcpy(&m_ft, &data.ftLastWriteTime, sizeof(FILETIME));
		return;
	}
	m_asData.SetSize(1);
	m_asData.SetAt(0, m_sText);
	if (!bTestCase)
		m_asData.ElementAt(0).MakeLower();
	m_anLines.SetSize(0);
}
void CFilterCnd::ClearData()
{
	m_asData.SetSize(0);
	m_anLines.SetSize(0);
	m_dwFileSize = 0;
}
