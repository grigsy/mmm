// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Magic.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_SOCKETS_INIT_FAILED         101
#define IDP_INSUFFICIENT_TMP_FREE_SPACE_1 102
#define IDP_MAILBOX_PROPERTIES_TITLE    103
#define IDP_EXCERPT_PROPERTIES_TITLE    104
#define IDP_PREFERENCES_TITLE           105
#define IDP_TIMER_CREATE_FAILED         106
#define IDP_MBOX_RESOLVING              107
#define IDP_MBOX_CONNECTING             108
#define IDP_MBOX_WF_USER_RESPONSE       109
#define IDP_MBOX_WF_PASS_RESPONSE       110
#define IDP_MBOX_WF_UIDL_RESPONSE       111
#define IDP_MBOX_WF_TOPEX_RESPONSE      112
#define IDP_MBOX_WF_LIST_RESPONSE       113
#define IDP_MBOX_WF_TOP_RESPONSE        114
#define IDP_MBOX_WF_RETR_RESPONSE       115
#define IDP_MBOX_CANNOT_SEND            116
#define IDP_MBOX_WF_DELE_RESPONSE       117
#define IDP_MBOX_NOT_CHECKED            118
#define IDP_MBOX_CHECKED                119
#define IDP_MBOX_CANNOT_CONNECT         120
#define IDP_MBOX_INVALID_USER           121
#define IDP_MBOX_INVALID_HOST           122
#define IDP_MBOX_INVALID_PASS           123
#define IDP_MBOX_ERROR                  124
#define IDP_MBOX_CANNOT_CHECK           125
#define IDP_MBOX_NETWORK_FAILED         126
#define IDP_MBOX_SOCKET_ERROR           127
#define IDP_MBOX_SERVER_FAILED          128
#define IDP_MBOX_CONNECTION_WAS_LOST    129
#define IDP_COLUMN_ALIAS                130
#define IDP_COLUMN_USER                 131
#define IDP_COLUMN_HOST                 132
#define IDP_COLUMN_MAIL                 133
#define IDP_COLUMN_STAT                 134
#define IDP_COLUMN_PORT                 135
#define IDP_COLUMN_POLL                 136
#define IDP_COLUMN_FROM                 137
#define IDP_COLUMN_SUBJ                 138
#define IDP_COLUMN_DATE                 139
#define IDP_COLUMN_SIZE                 140
#define IDP_COLUMN_ELAPSED              141
#define IDP_COLUMN_MBOX                 142
#define IDR_MAINFRAME                   143
#define IDR_MAGICTYPE                   144
#define IDP_CANNOT_FIND_1               144
#define IDP_CANNOT_FOLLOW_THE_LINK_1    145
#define IDR_MAILBOX_POPUP_MENU          146
#define IDP_MBOX_INTERRUPTED_BY_USER    146
#define IDR_OVERLAYERROR                147
#define IDP_MBOX_WF_APOP_RESPONSE       147
#define IDP_YOU_HAVE_NEW_2              148
#define IDR_EXCERPT_POPUP_MENU          149
#define IDP_MBOX_WF_NOOP_RESPONSE       149
#define IDR_OVERLAYDISABLED             150
#define IDP_MESSAGE                     150
#define IDR_OVERLAYSECURE               151
#define IDP_MBOX_DOWNLOADING_DATA       151
#define IDR_NOTIFY_MENU                 152
#define IDP_MESSAGES                    153
#define IDP_PLAYBACK_FILES              154
#define IDP_COMMAND_FILES               155
#define IDD_GENERAL_PAGE                156
#define IDP_DISABLED                    157
#define IDD_COMMAND_PAGE                158
#define IDP_MAILBOX                     159
#define IDD_MESSAGE_PAGE                160
#define IDP_MAILBOXES                   161
#define IDD_HEADER_PAGE                 162
#define IDP_SELECTED                    163
#define IDD_OPTIONS_PAGE                164
#define IDP_BYTES                       165
#define IDD_PLAYBACK_PAGE               166
#define IDP_MEGABYTES                   167
#define IDP_KILOBYTES                   168
#define IDR_TOOLBAR                     169
#define IDD_SERVER_PAGE                 170
#define IDR_OVERLAYSUSPENDED            171
#define IDD_LOG_PAGE					172
#define IDI_MAILBOX_EMPTY_16            184
#define IDI_MAILBOX_FULL_16             185
#define IDC_EDIT_USER                   1004
#define IDC_EDIT_PASS                   1005
#define IDC_EDIT_HOST                   1006
#define IDC_EDIT_PORT                   1007
#define IDC_EDIT_POLL                   1008
#define IDC_EDIT_ALIAS                  1009
#define IDC_SPIN_PORT                   1010
#define IDC_SPIN_POLL                   1011
#define IDC_COMBO_PLAYBACK              1012
#define IDC_COMBO_COMMAND_RUN           1013
#define IDC_EDIT_PLAYBACK               1014
#define IDC_EDIT_COMMAND                1015
#define IDC_TEST                        1016
#define IDC_BROWSE                      1017
#define IDC_COMBO_COMMAND               1018
#define IDC_STATIC_RUN                  1019
#define IDC_STATIC_COMMAND              1020
#define IDC_STATIC_DEVICE               1021
#define IDC_COMBO_PLAYBACK_DEVICE       1022
#define IDC_STATIC_PATH                 1023
#define IDC_COMBO_MESSAGE               1024
#define IDC_EDIT_FROM                   1025
#define IDC_EDIT_MAILBOX                1026
#define IDC_START_HIDDEN                1027
#define IDC_EDIT_SUBJECT                1028
#define IDC_CHECK_IMMEDIATELY           1029
#define IDC_EDIT_DATE                   1030
#define IDC_EV_CONFIRM_DELETE           1031
#define IDC_EDIT_SIZE                   1032
#define IDC_POP_UP_MAIN_WINDOW          1033
#define IDC_EDIT_FILE_EXTENSIONS_FOR_MESSAGES 1034
#define IDC_EDIT_GREETING               1035
#define IDC_EDIT_IP_ADDRESS             1036
#define IDC_EDIT_APOP_SUPPORTED         1037
#define IDC_EDIT_UIDL_SUPPORTED         1038
#define IDC_EDIT_BURST_WRITES_SUPPORTED 1039
#define IDC_ENABLE_LOGGING              1040
#define IDC_LOG                         1041
#define IDC_LOG_REFRESH                 1044

#define ID_VIEW_ICON                    32773
#define ID_VIEW_REPORT                  32776
#define ID_TIMER_CHECK                  32779
#define ID_RESTORE                      32781
#define ID_EDIT_PROPERTIES              32783
#define ID_PREFERENCES                  32786
#define ID_STOP_COMMAND_ALL             32789
#define ID_EDIT_DELETE                  32790
#define ID_EDIT_NEW                     32791
#define ID_VIEW_REFRESH                 32792
#define ID_STOP_PLAYBACK_ALL            32793
#define ID_COMMAND                      32794
#define ID_VIEW_QUICK_VIEW              32795
#define ID_VIEW_QUICK_REPLY             32796
#define ID_QUICK_SEND                   32797
#define ID_QUICK_REPLY                  32798
#define ID_QUICK_VIEW                   32799
#define ID_QUICK_DELETE                 32800
#define ID_STOP_CHECKING                32821
#define ID_SUSPEND                      32822
#define IDS_URL_MY_HOMEPAGE             57346
#define IDS_EMAIL                       57347
#define IDS_URL_MAGIC_HOMEPAGE          57348
#define AFX_IDS_HELPMODEMESSAGE2        57349
#define ID_INDICATOR_OBJECTS            61204
#define ID_INDICATOR_CONTENTS           61205
#define IDS_UNKNOWN                     61206
#define IDS_YES                         61207
#define IDS_NO                          61208
