// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// OptionsPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COptionsPage dialog

class COptionsPage : public CPropertyPage
{
	DECLARE_DYNCREATE(COptionsPage)

// Construction
public:
	COptionsPage();
	~COptionsPage();

// Dialog Data
	//{{AFX_DATA(COptionsPage)
	enum { IDD = IDD_OPTIONS_PAGE };
	CComboBox	m_cbMarkRead;
	BOOL	m_bCheckImmediately;
	BOOL	m_bStartAlwaysHidden;
	BOOL	m_bEVConfirmDelete;
	BOOL	m_bPopUpMainWindow;
	int		m_nPreviewSize;
	CString	m_strFileExtensionForMessages;
	int		m_nMarkRead;
	CString	m_sApp;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(COptionsPage)
	public:
	virtual BOOL OnApply();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(COptionsPage)
	afx_msg void OnModified();
	virtual BOOL OnInitDialog();
	afx_msg void OnBrs();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};
