// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// Mailbox.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////
//
//   File : Mailbox.cpp
//   Description : basic connection code
//
//   Modification history ( date, name, description ) : 
//		1.	17.12.2002	Igor Green, mmm3@grigsoft.com
//			Partial preview support
//		2.	22.01.2003	brendanryan59 (BRN)
//			Fixed problem with -- in header data
//
//HISTORY_ENTRY_END:2!22.01.2003
/////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MD5.h"
#include "Magic.h"
#include "Excerpt.h"
#include "MagicDoc.h"
#include "Mailbox.h"
#include "MagicFrame.h"
#include "MailboxView.h"
#include "SecureString.h"
#include "dpassword.h"

#include <afxpriv.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
void CMailbox::RecoverString( CString &str )
{
	str.MakeReverse();
	srand( m_intClue ^ 0x55555555 );
	for( int i = str.GetLength(); i; --i ) 
	{
		TCHAR ch = TCHAR(str.GetAt( i-1 ) ^ (TCHAR) rand());
		str.SetAt( i-1,  ch);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMailbox
/* version history:
252: flag for new Doc version
251: added APOP disable flag
 */


IMPLEMENT_SERIAL( CMailbox, CAsyncSocket, 252 | VERSIONABLE_SCHEMA );

CMailbox::CMailbox()
:	m_bitSelected(1),
	m_bitCreated(1),
	m_bitDeleted(0),
	m_intOrder(0),
	m_tServerSupportsAPOP( true ),
	m_tServerSupportsUIDL( true ),
	m_tServerSupportsBurstWrites( true ),
	m_bLog( false )
{
	InitializeServerTraits();
	m_pFolder = NULL;

	m_nStatTime = 0;
	m_nStatCur = 0;
	m_nStatMax = 0;

	m_nUnread = 0;

	m_hResolveJob = 0;
	m_pResolveBuffer = NULL;
	m_arrayExcerpt.SetSize( 0, 8 );

	m_uMciID = 0;
	m_hCmdID = 0;
	m_intChanged = -1;

	m_intState = MBOX_NOT_CHECKED;
	m_intClue = 0xBadBabe ^ (int) this;
	m_intPort = 110;
	m_nExtraLines=0;
	m_intPoll = 10;
	m_dwFlags = MBF_DEFAULT;
	m_intElapsed = theApp.intCheckImmediately || (theApp.m_dwFlags & MMF_MASSIVE)!=0 ? -1 : 0;

	m_intCommand = ACTION_DEFAULT;
	m_intCommandRun = COMMAND_RUN_NORMAL;
	m_intPlayback = ACTION_DEFAULT;
	m_intPlaybackDevice = PLAYBACK_DEVICE_FILE;
	m_intMessage = FALSE;
	m_tmLastCheck = (time_t)0;
#ifdef USE_SSL
	m_pSSL = NULL;
	m_SSL.SetMailbox(this);
#endif
}

const CMailbox& CMailbox::operator=( const CMailbox& a)
{
		m_intClue = a.m_intClue;
		m_intPort = a.m_intPort;
		m_intPoll = a.m_intPoll;
		m_ulongIP = a.m_ulongIP;
		m_intCommand = a.m_intCommand;
		m_intCommandRun = a.m_intCommandRun;
		m_intPlayback = a.m_intPlayback;
		m_intPlaybackDevice = a.m_intPlaybackDevice;
		m_intMessage = a.m_intMessage;
		m_dwFlags = a.m_dwFlags;
		
		m_nExtraLines = a.m_nExtraLines;
		m_strPass = a.m_strPass;
		m_strUser = a.m_strUser;
		m_strHost = a.m_strHost;
		m_strAlias = a.m_strAlias;
		m_strCommand = a.m_strCommand;
		m_strPlayback = a.m_strPlayback;
		return *this;
}

CMailbox::~CMailbox()
{
	CancelPendingJobs();

	m_bitDeleted = 1;
	Change( COLUMN_STATE );

	CWnd *wnd = (CWnd*) AfxGetMainWnd();
	if( wnd ) wnd->SendMessage( VM_UPDATEITEM, (WPARAM) this );

	int i = m_arrayExcerpt.GetSize();
	while( i ) delete m_arrayExcerpt[--i];
	m_arrayExcerpt.RemoveAll();
}

/////////////////////////////////////////////////////////////////////////////
// CMailbox message handlers

void CMailbox::Serialize( CArchive& ar ) 
{
	if( ar.IsLoading() ) // READ
	{
		int intVersion = ar.GetObjectSchema();

		if( intVersion < 250 )
		{
			DWORD wordTmp;
			ar >> wordTmp; m_intClue = (int) wordTmp;
			ar >> wordTmp; m_intPort = (int) wordTmp;
			ar >> wordTmp; m_intPoll = (int) wordTmp;
			ar >> wordTmp; m_ulongIP = (int) wordTmp;
			ar >> m_strPass;
			ar >> m_strUser;
			ar >> m_strHost;
			
			if( 200 <= intVersion )
			{
				ar >> m_strAlias;
				ar >> wordTmp; m_intCommand = (int) wordTmp;
				ar >> wordTmp; m_intCommandRun = (int) wordTmp;
				ar >> m_strCommand;
				ar >> wordTmp; m_intPlayback = (int) wordTmp;
				ar >> wordTmp; m_intPlaybackDevice = (int) wordTmp;
				ar >> m_strPlayback;
				ar >> wordTmp; m_intMessage = (int) wordTmp;
			}

			m_intClue ^= 0xAAAAAAAA;
			m_intPort ^= m_intClue;
			m_intPoll ^= m_intClue;
			m_ulongIP ^= m_intClue;
			m_intCommand ^= m_intClue;
			m_intCommandRun ^= m_intClue;
			m_intPlayback ^= m_intClue;
			m_intPlaybackDevice ^= m_intClue;
			m_intMessage ^= m_intClue;
			RecoverString( m_strAlias );
			RecoverString( m_strUser );
			RecoverString( m_strHost );
			RecoverString( m_strPass );
			RecoverString( m_strCommand );
			RecoverString( m_strPlayback );
		}
		else // if( 250 <= intVersion )
		{
			DWORD dwTmp;
			ar >> dwTmp; m_intClue = (int) dwTmp;
			ar >> dwTmp; m_intPort = (int) dwTmp;
			ar >> dwTmp; m_intPoll = (int) dwTmp;
			ar >> dwTmp; m_ulongIP = (int) dwTmp;
			ar >> dwTmp; m_intCommand = (int) dwTmp;
			ar >> dwTmp; m_intCommandRun = (int) dwTmp;
			ar >> dwTmp; m_intPlayback = (int) dwTmp;
			ar >> dwTmp; m_intPlaybackDevice = (int) dwTmp;
			ar >> dwTmp; m_intMessage = (int) dwTmp;
			if (intVersion >= 251)
			{
				ar >> m_dwFlags;
			}
			else
				m_dwFlags = MBF_DEFAULT;
			if (intVersion >= 252)
				ar >> m_nExtraLines;
			else
				m_nExtraLines = 255;


			// assure checksum
			DWORD dwCheckSum = 0;
			ar >> dwCheckSum;
			dwCheckSum ^= m_intClue ^ m_intPort ^ m_intPoll ^ m_ulongIP ^ 
				m_intCommand ^ m_intCommandRun ^ 
				m_intPlayback ^ m_intPlaybackDevice ^ 
				m_intMessage;
			if( dwCheckSum ) AfxThrowArchiveException( CArchiveException::badIndex, _T("") );

			CSecureString sstrTmp; 
			sstrTmp.Serialize( ar ); sstrTmp.DecodeString( m_intClue, m_strPass ); 
			sstrTmp.Serialize( ar ); sstrTmp.DecodeString( m_intClue, m_strUser ); 
			sstrTmp.Serialize( ar ); sstrTmp.DecodeString( m_intClue, m_strHost ); 
			sstrTmp.Serialize( ar ); sstrTmp.DecodeString( m_intClue, m_strAlias ); 
			sstrTmp.Serialize( ar ); sstrTmp.DecodeString( m_intClue, m_strCommand ); 
			sstrTmp.Serialize( ar ); sstrTmp.DecodeString( m_intClue, m_strPlayback ); 
		}

		m_ulongIP = 0;
	}
	else // WRITE
	{
		ar << (DWORD) m_intClue;
		ar << (DWORD) m_intPort;
		ar << (DWORD) m_intPoll;
		ar << (DWORD) m_ulongIP;
		ar << (DWORD) m_intCommand;
		ar << (DWORD) m_intCommandRun;
		ar << (DWORD) m_intPlayback;
		ar << (DWORD) m_intPlaybackDevice;
		ar << (DWORD) m_intMessage;
		ar << (DWORD) m_dwFlags;
		if (m_nExtraLines==255)
			m_nExtraLines = 0;
		ar << (BYTE)  m_nExtraLines;

		// calculate checksum
		DWORD dwCheckSum = 0;
		dwCheckSum ^= m_intClue ^ m_intPort ^ m_intPoll ^ m_ulongIP ^ 
			m_intCommand ^ m_intCommandRun ^ m_intPlayback ^ 
			m_intPlaybackDevice ^ m_intMessage;
		ar << dwCheckSum;

		CSecureString sstrTmp; 
		CString sSavePass;
		if ((m_dwFlags & MBF_ASK_PASS)==0) 
			sSavePass = m_strPass;

		sstrTmp.EncodeString( m_intClue, sSavePass); sstrTmp.Serialize( ar );
		sstrTmp.EncodeString( m_intClue, m_strUser ); sstrTmp.Serialize( ar );
		sstrTmp.EncodeString( m_intClue, m_strHost ); sstrTmp.Serialize( ar );
		sstrTmp.EncodeString( m_intClue, m_strAlias ); sstrTmp.Serialize( ar );
		sstrTmp.EncodeString( m_intClue, m_strCommand ); sstrTmp.Serialize( ar );
		sstrTmp.EncodeString( m_intClue, m_strPlayback ); sstrTmp.Serialize( ar );
	}
}

void CMailbox::SetAlias( CString &strAlias )
{
	if( m_strAlias == strAlias ) return;
	m_strAlias = strAlias;
	Change( COLUMN_ALIAS );
	SetClue();
}
void CMailbox::SetElapsed( BOOL  ) 
{
	if (m_tmLastCheck.GetTime()==0)	// not checked yet
		return;
	CTime tmCur = CTime::GetCurrentTime();
	CTimeSpan ts = tmCur-m_tmLastCheck;
	int nNew = (int)(ts.GetTotalSeconds()+20)/60;

	if( nNew != m_intElapsed) 
	{
		m_intElapsed = nNew;
		Change( COLUMN_ELAPSED );
	} 
}

void CMailbox::SetUser( CString &strUser )
{
	if( m_strUser == strUser ) return;
	m_strUser = strUser;
	Change( COLUMN_USER );
	SetState( MBOX_NOT_CHECKED );
	SetClue();
}

void CMailbox::SetHost( CString &strHost )
{
	if( m_strHost == strHost ) return;
	m_strHost = strHost;
	Change( COLUMN_HOST );
	InitializeServerTraits();
	SetState( MBOX_NOT_CHECKED );
	SetClue();
}

void CMailbox::SetPass( CString &strPass, BOOL bAsk )
{
	if (bAsk)
		m_strPass.Empty();
	else
		m_strPass = strPass;
	SetBoolFlag(m_dwFlags, MBF_ASK_PASS, bAsk);
	SetClue();
}

void CMailbox::SetState( int intState )
{
	if( STATE_FINAL( m_intState ) && !STATE_FINAL( intState ) )
	{
		m_strLastError.Empty();

#ifdef USE_SSL
		if (m_dwFlags & MBF_SSL)
		{
			m_pSSLctx = SSL_CTX_new(SSLv23_client_method());
			//m_pSSLctx = SSL_CTX_new(SSLv3_client_method());
			m_pSSL = SSL_new(m_pSSLctx);
			m_SSL.CreateConnector(m_pSSLctx);
			m_SSL.Create();
		}
		else
#endif
		Create();

	}

	if( !STATE_FINAL( m_intState ) && STATE_FINAL( intState ) ) 
	{ 
		Close();
		CancelPendingJobs();
		Change( COLUMN_MAIL );
#ifdef USE_SSL
		if (m_pSSL)
		{
			m_SSL.Close();
			SSL_shutdown(m_pSSL);
			SSL_free(m_pSSL);
			SSL_CTX_free(m_pSSLctx);
			m_dwFlags &= ~0x1000;
		}
#endif
	}


	m_intState = intState;
	Change( COLUMN_STATE );

	if( MBOX_CHECKED == intState )
	{
		m_intElapsed = 0;
		m_tmLastCheck = CTime::GetCurrentTime();
		Change( COLUMN_ELAPSED );
	}
	else if( MBOX_NOT_CHECKED == intState )
	{
		Change( COLUMN_MAIL );
		Change( COLUMN_ELAPSED );
		m_intElapsed = -1;
		int i = m_arrayExcerpt.GetSize();
		while( i ) delete m_arrayExcerpt[ --i ];
		m_arrayExcerpt.RemoveAll();
	}
	else if( MBOX_INTERRUPTED_BY_USER )
	{
	}
}

void CMailbox::SetPort( int intPort )
{
	if( m_intPort == intPort ) return;
	m_intPort = intPort;
	Change( COLUMN_PORT );
	SetClue();
}


void CMailbox::SetFlag( int nFlag, int nSet)
{
	BOOL bNow = GetBoolFlag(m_dwFlags, nFlag);
	if( bNow == nSet ) return;
	SetBoolFlag(m_dwFlags, nFlag, nSet);
	Change( COLUMN_POLL );
	SetClue();
}

void CMailbox::SetPoll( int intPoll )
{
	if( m_intPoll == intPoll ) return;
	m_intPoll = intPoll;
	Change( COLUMN_POLL );
	SetClue();
}

void CMailbox::BeginResolveHostByName()
{
	if( m_strHost.IsEmpty() ) { SetState( MBOX_INVALID_HOST ); return; }
	SetState( MBOX_RESOLVING );

	ASSERT( !m_pResolveBuffer && !m_hResolveJob );

	char szHost[256] = ""; CharToOem( m_strHost, szHost );

	m_ulongIP = inet_addr( szHost );
	if( INADDR_NONE == m_ulongIP )
	{
		m_ulongIP = 0;
		m_pResolveBuffer = new char[ MAXGETHOSTSTRUCT ];
		m_hResolveJob = WSAAsyncGetHostByName(
			AfxGetMainWnd()->m_hWnd,
			VM_END_RESOLVE_HOST_BY_NAME,
			szHost,
			m_pResolveBuffer,
			MAXGETHOSTSTRUCT );
	}
	else
	{
		Check();
	}
}

void CMailbox::EndResolveHostByName( WPARAM wParam, LPARAM lParam )
{
	if( m_hResolveJob != (HANDLE) wParam ) return;

	if( HIWORD( lParam ) ) SetState( MBOX_INVALID_HOST );
	else
	{
		m_ulongIP = *(ULONG*) ( (struct hostent*) m_pResolveBuffer )->h_addr;
		Check();
	}

	m_hResolveJob = 0;
	delete m_pResolveBuffer;
	m_pResolveBuffer = NULL;
}

void CMailbox::Check()
{
	if( MBOX_CONNECTING == m_intState )
	{
		// MFC can't handle the case when Close is called before OnConnect had fired.
		// as a result, an assertion occurs in sockcore.cpp on line 513.
		// this measure will avoid the problem in the first place.
		return;
	}

	if( !STATE_FINAL( m_intState ) )
	{
		SetState( MBOX_CANNOT_CHECK );
	}

	if( !m_ulongIP )
	{
		BeginResolveHostByName();
		return;
	}

	SetState( MBOX_CONNECTING ); 
	SOCKADDR_IN a;
	a.sin_family = AF_INET;
	a.sin_port = htons( (u_short)m_intPort );
	a.sin_addr.S_un.S_addr = m_ulongIP;
	
	
#ifdef USE_SSL
    if (m_pSSL && !m_SSL.Connect( (SOCKADDR*) &a, sizeof(a) ) &&
		( WSAEWOULDBLOCK != GetLastError() ) )
	{
		TRACE2( "SSL Error #%d; [%s] cannot connect\n", GetLastError(), m_strAlias );
		SetState( MBOX_SOCKET_ERROR );
	}
	else if (!m_pSSL)
#endif
	if( !Connect( (SOCKADDR*) &a, sizeof(a) ) &&
		( WSAEWOULDBLOCK != GetLastError() ) )
	{
		TRACE2( "Socket Error #%d; [%s] cannot connect\n", GetLastError(), m_strAlias );
		SetState( MBOX_SOCKET_ERROR );
	}
}

void CMailbox::StopChecking()
{
	if( !STATE_FINAL( m_intState ) )
	{
		SetState( MBOX_INTERRUPTED_BY_USER );
	}

	// clear out all pending tasks
	for( int i = m_arrayExcerpt.GetSize(); i; --i )
	{
		m_arrayExcerpt[i-1]->m_bitRemoveFromServer = false;
		m_arrayExcerpt[i-1]->m_bitDownloadData = false;
		m_arrayExcerpt[i-1]->m_bitPreviewData = false;
		m_arrayExcerpt[i-1]->Change( COLUMN_MBOX );
	}
}

void CMailbox::OnConnect( int nErrorCode )
{
	if( nErrorCode ) 
	{
		TRACE2( "Socket Error #%d; [%s] OnConnect: cannot connect\n", nErrorCode, m_strAlias );
		SetState( MBOX_CANNOT_CONNECT );
		InitializeServerTraits();
	}
	else
	{
		TRACE1( "Socket connected #%d\n", m_hSocket );
	}

	//m_strLog.Empty();

	// no initial answer over ssl?
	if (m_pSSL && m_intState == MBOX_CONNECTING)
	{
		if ((m_dwFlags & 0x1000) == 0)
		{
			m_dwFlags |= 0x1000;
			CString strPacket;
			char szPacket[ 1024 ];
			strPacket.Format( _T("USER %s\r\n"), m_strUser );
			CharToOem( strPacket, szPacket );
			SetState( Send( szPacket, strlen( szPacket ) ) ?
				MBOX_WF_USER_RESPONSE_SSL  : MBOX_CANNOT_SEND );

			m_tServerSupportsAPOP = false;
		}
	}
}

void CMailbox::OnReceive( int nErrorCode )
{
	char szPacket[ 1024 ];
	
	if( nErrorCode ) { SetState( MBOX_NETWORK_FAILED ); return; }
	
	while( TRUE )
	{
#ifdef USE_SSL
		if (m_pSSL)
		{
			nErrorCode = m_SSL.GetData( szPacket, sizeof( szPacket )-1 );
			if (nErrorCode==0)
				return;
		}
		else
#endif
		nErrorCode = Receive( szPacket, sizeof( szPacket )-1 );
		switch( nErrorCode )
		{
			case 0:
			{
				if( m_intState == MBOX_WF_USER_RESPONSE )
				{
					m_tServerSupportsAPOP = false;
					Check();
				}
				else
				{
					SetState( IDP_MBOX_CONNECTION_WAS_LOST ); 
				}

				return; 
			}
			
			case SOCKET_ERROR:
			{ 
				DWORD dwLastError = GetLastError();

				if( WSAEWOULDBLOCK != dwLastError )
				{
					if( WSAECONNABORTED == dwLastError ||
						WSAECONNRESET == dwLastError )
					{
						if( m_intState == MBOX_WF_APOP_RESPONSE )
						{
							m_tServerSupportsAPOP = false;
							Check();
						}
						else
						{
							SetState( MBOX_CONNECTION_WAS_LOST );
						}
					}
					else
					{
						TRACE2( "Socket Error #%d; [%s] cannot receive\n", GetLastError(), m_strAlias );
						SetState( MBOX_SOCKET_ERROR ); 
					}
				}
				return; 
			}

			default:
			{
//				TRACE2( "[+%d bytes]->[%d bytes]\n", nErrorCode, m_arrayPacket.GetSize()+nErrorCode );
				ASSERT( nErrorCode <= sizeof( szPacket ) );
				m_arrayPacket.SetSize( m_arrayPacket.GetSize() + nErrorCode );
				memcpy( &m_arrayPacket[ m_arrayPacket.GetSize() - nErrorCode ], szPacket, nErrorCode );

				if( nErrorCode < sizeof( szPacket ) )
				{
					while( m_arrayPacket.GetSize() ) 
					{
						int i = 1;
						while( i < m_arrayPacket.GetSize() &&
							'\x0D' != m_arrayPacket[i-1] && 
							'\x0A' != m_arrayPacket[i] ) ++i;
						
						int intStringLength = i - 1;
						
						if( i == m_arrayPacket.GetSize() ) break;

						int intBufferLength = i + 1;

						AppendToLog( "Rcvd: " );
						CString sLog((char*) &m_arrayPacket[0], intBufferLength );
						AppendToLog( sLog );
						static int nTraceLog = 0;
						if (nTraceLog)
							TRACE(sLog);

						// skip any extra returns at the beginning
						int nSkip = 0;
						while (nSkip<intStringLength &&
							(m_arrayPacket[nSkip] == 0x0A || m_arrayPacket[nSkip] == 0x0D))
							nSkip++;
						if (nSkip == intStringLength)
							nSkip = 0;
						ParseString( (char*) &m_arrayPacket[0] + nSkip, intStringLength-nSkip, intBufferLength-nSkip );

						if( m_arrayPacket.GetSize() ) m_arrayPacket.RemoveAt( 0, intBufferLength );
					}   // while
				}   // if

				if( INVALID_SOCKET == m_hSocket ) return;
			}   // default
		}   //  switch  receive
	}   //  while
}

void CMailbox::ParseString( char *str, int len, int size )
{
	char szPacket[ 1024 ];
	CString strPacket;

	switch( m_intState )
	{
		case MBOX_CONNECTING:
		{
			// null-terminate
			str[ len ] = 0;

			char *stamp_begin = (char*) memchr( str, '<', size );
			char *stamp_end = (char*) memchr( str, '>', size );

			m_sServerGreeting = CString( str + 3, len - 3 );
			m_sServerGreeting.TrimLeft();
			m_sServerGreeting.TrimRight();

#if 1
			if  ( ((m_dwFlags & MBF_NO_APOP) == 0) && m_tServerSupportsAPOP &&
				( stamp_begin ) && 
				( stamp_end ) && 
				( stamp_begin < stamp_end ) )
			{
				/* APOP command is supported - use secure method */
				*(++stamp_end) = 0;
				CString sPass = GetPassword();
				char *pass = new char[ sPass.GetLength() + 1 ];
				strcpy_s(pass, sPass.GetLength() + 1, sPass);
				int nBufLen = strlen(stamp_begin) + strlen(pass) + 1;
				char *string = new char[ nBufLen ];
				sprintf_s( string, nBufLen, "%s%s", stamp_begin, pass );

				unsigned char digest[16];

				MD5String( string, digest );
				
				strPacket.Format( _T("APOP %s "), m_strUser );
				strcpy_s( szPacket, _countof(szPacket), strPacket);

				for( int i = 0; i < sizeof( digest ); ++i )
				{
					char hex[3];
					sprintf_s( hex, _countof(hex),  "%02x", digest[i] );
					strcat_s( szPacket, _countof(szPacket), hex );
				}
				strcat_s( szPacket, _countof(szPacket), "\r\n" );
				
				delete pass;
				delete string;

				SetState( Send( szPacket, strlen( szPacket ) ) ?
					MBOX_WF_APOP_RESPONSE : MBOX_CANNOT_SEND );
			}
			else 
#endif
			{
				/* APOP command is NOT supported - use plain text method */
				strPacket.Format( _T("USER %s\r\n"), m_strUser );
				CharToOem( strPacket, szPacket );
				SetState( Send( szPacket, strlen( szPacket ) ) ?
					MBOX_WF_USER_RESPONSE : MBOX_CANNOT_SEND );

				m_tServerSupportsAPOP = false;
			}
		}
		break;

		case MBOX_WF_APOP_RESPONSE:
		{
			m_strLastError.Empty();

			if( '-' == *str )
			{
				// note, this piece is duplicated:
				{
					/* APOP command is NOT supported - use plain text method */
					strPacket.Format( _T("USER %s\r\n"), m_strUser );
					CharToOem( strPacket, szPacket );
					SetState( Send( szPacket, strlen( szPacket ) ) ?
						MBOX_WF_USER_RESPONSE : MBOX_CANNOT_SEND );
					
					m_tServerSupportsAPOP = false;
				}
			}
			else
			{
				m_tServerSupportsAPOP = true;
				SendBurstWriteSenseRequest();
			}
		}
		break;

		case MBOX_WF_USER_RESPONSE_SSL:
		case MBOX_WF_USER_RESPONSE:
		{
			if( '-' == *str )
			{
				if( len > 5 )
				{
					str[ len ] = 0;
					m_strLastError = "error: ";
					m_strLastError += str + 5;
					m_strLastError.MakeLower();
					m_strLastError.Replace( '.', ' ' );
				}

				SendQuitRequest( MBOX_INVALID_USER ); 
			}
			else
			{
				if (m_intState == MBOX_WF_USER_RESPONSE_SSL)
				{
					m_intState = MBOX_WF_USER_RESPONSE;
					break;
				}
				CString sPass = GetPassword();
				strPacket.Format( _T("PASS %s\r\n"), sPass );
				strcpy_s(szPacket, _countof(szPacket), strPacket);
				SetState( Send( szPacket, strlen( szPacket ) ) ?
					MBOX_WF_PASS_RESPONSE : MBOX_CANNOT_SEND );
			}
		}
		break;

		case MBOX_WF_PASS_RESPONSE:
		{
			m_strLastError.Empty();

			if( '-' == *str )
			{
				if( len > 5 )
				{
					str[ len ] = 0;
					m_strLastError = "error: ";
					m_strLastError += str + 5;
					m_strLastError.MakeLower();
					m_strLastError.Replace( '.', ' ' );
				}

				SendQuitRequest( MBOX_INVALID_PASS );
			}
			else
			{
				m_tServerSupportsAPOP = false;
				SendBurstWriteSenseRequest();
			}
		}
		break;


		case MBOX_WF_FIRST_NOOP:
		{
			CreateWatchDog( 4000 );
			SetState( MBOX_WF_SECOND_NOOP );
		}
		break;

		case MBOX_WF_SECOND_NOOP:
		{
			DestroyWatchDog();
			m_tServerSupportsBurstWrites = true;
			SendUidlRequest();
		}
		break;

		case MBOX_WF_UIDL_RESPONSE:
		{
			m_strLastError.Empty();

			switch( *str )
			{ 
				case '-':
				{
					m_tServerSupportsUIDL = false;
					m_intOrder = 0;
					SendTopExRequest();
				}
				break;

				case '.': 
				{
					// brieuc
					if (m_dwFlags & MBF_SPEC_TOP)
						SendTopFRequest();   //  for my FLASH POP server
					else
						FinishReceivingID();
				}
				break;

				case '+':
				{
					m_tServerSupportsUIDL = true;
					break;
				}

				default:
				{
					str[ len ] = 0;

					int order = 0;
					if( 1 != sscanf_s( str, "%d", &order ) )
					{
						SetState( MBOX_SERVER_FAILED );
						break;
					};
					
					char *id_space = strchr( str, ' ' );

					if( id_space == NULL )
					{
						SetState( MBOX_SERVER_FAILED );
						break;
					}
					
					OrderAssignedToID( order, id_space + 1 );
				}
			}
		}
		break;

		case MBOX_WF_TOPEX_RESPONSE:
		{
			switch( *str )
			{ 
				case '-': FinishReceivingID(); break;
				case '.': SendTopExRequest(); break;
				default:
				{
					const char msgid[] = "message-id: ";
					if( _strnicmp( str, msgid, sizeof( msgid )-1 ) ) break;
					str[ len ] = 0;
					str += sizeof( msgid )-1;
					OrderAssignedToID( m_intOrder, str );
				}
			}
		}
		break;

		
        case MBOX_WF_TOPF_RESPONSE:
        {
        	switch( *str )
        	{
        		case '-':
        		case '.': 	FinishReceivingID();   break;

        		default: {  }
        	}
        }
        break;


		case MBOX_WF_LIST_RESPONSE:
		{
			switch( *str )
			{
				case '.': 
				case '-': SendInitialTopRequest();
				case '+': break;

				default: 
				{
					str[ len ] = 0;

					int order = 0, size = 0;
					if( ( 2 != sscanf_s( str, "%d %d", &order, &size  ) ) || !order || !size )
					{
						SendQuitRequest( MBOX_SERVER_FAILED );
						break;
					}
					int i=0;

					for( i = m_arrayExcerpt.GetSize(); i; --i )
					{
//						char a[111];
//						TRACE( strcat( itoa( m_arrayExcerpt[i-1]->m_intOrder, a, 10 ), "\n" ) );
						if( order == m_arrayExcerpt[i-1]->m_intOrder ) break;
					}

					if( !i )
					{
						SendQuitRequest( MBOX_SERVER_FAILED );
						break;
					}

					m_arrayExcerpt[--i]->m_intSize = size;
					m_arrayExcerpt[i]->Change( COLUMN_SIZE );
				}
			}
		}
		break;

		case MBOX_WF_TOP_RESPONSE:
		{
			ASSERT( m_aIndices.GetSize() > 0 );
			// BRN
			str[ len ] = 0;
			if (_tcscmp(str, _T("."))==0)
			{
				m_arrayExcerpt[ m_aIndices[0] ]->NoMoreTopLines();
				m_arrayExcerpt[ m_aIndices[0] ]->m_tmCheckTime = m_tmCheckTime;
				m_aIndices.RemoveAt(0);
				SendTopRequest();
			}
			else if (_tcsncmp(str, _T("-ERR"),4)==0)
			{
				m_aIndices.RemoveAt(0);
				SendTopRequest();
			}
			else if (_tcsncmp(str, _T("+OK"),4)==0)
			{

			}
			else
			{
				m_arrayExcerpt[ m_aIndices[0] ]->ParseTopLine( str );
			}

			/* BRN
			switch( *str )
			{
				case '.': m_arrayExcerpt[ m_aIndices[0] ]->NoMoreTopLines();
				
				case '-': m_aIndices.RemoveAt(0);
						  SendTopRequest();

				case '+': break;

				default: 
				{
					str[ len ] = 0;
					m_arrayExcerpt[ m_aIndices[0] ]->ParseTopLine( str );
				}
			}
			*/
		}
		break;

		case MBOX_WF_RETR_RESPONSE:
		{
			switch( *str )
			{
				case '-':
				{
					SendRetrRequest();
				}
				break;

				case '+': 
				{
					m_arrayExcerpt[ m_aIndices[0] ]->BeginDataDownload();
					SetState( MBOX_DOWNLOADING_DATA );
				}
				break;

				default: SetState( MBOX_SERVER_FAILED );
			}
		}
		break;

		case MBOX_DOWNLOADING_DATA:
		{
			BOOL bStopDownload = FALSE;
			if( ( *str == '.' ) && ( len == 1 ) )
			{
				// terminal dot
				bStopDownload = TRUE;
			}
			else
			{
				if (!m_arrayExcerpt[ m_aIndices[0] ]->WriteBufferToDataFile( str, size ))
					bStopDownload = TRUE;
			}
			if (bStopDownload)
			{
				m_arrayExcerpt[ m_aIndices[0] ]->EndDataDownload();
				m_aIndices.RemoveAt(0);
				SetState( MBOX_WF_RETR_RESPONSE );
				SendRetrRequest();
			}
		}
		break;

		case MBOX_WF_DELE_RESPONSE:
		{
			switch( *str )
			{
				case '+':	// we know that indices are in descending order,
							// so we don't worry about fixing remaining indices in
							// array m_aIndices
							delete m_arrayExcerpt[ m_aIndices[0] ];
							m_arrayExcerpt.RemoveAt( m_aIndices[0] );

				case '-':	m_aIndices.RemoveAt(0);
							Change( COLUMN_MAIL );
							SendDeleRequest();
							break;

				default:	SetState( MBOX_SERVER_FAILED );
			}
		}
	}
}

void CMailbox::OrderAssignedToID( int order, const char *str )
{
	TCHAR *szID = new TCHAR [ strlen( str ) + 1 ];
	OemToChar( str, szID );

	// search for this id
	int i=0;
	for( i = m_arrayExcerpt.GetSize(); i; --i )
	{
		if( !m_arrayExcerpt[i-1]->m_strID.Compare( szID ) ||
		    ( m_arrayExcerpt[i-1]->m_intOrder == order ) ) break;
	}

	if( i && m_arrayExcerpt[i-1]->m_intOrder == 0 ) // excerpt with this id exists
	{
		m_arrayExcerpt[i-1]->m_intOrder = order;
	}
	else // create a new one
	{
		Change( COLUMN_MAIL );
		CExcerpt *xrpt = new CExcerpt( this );
		xrpt->m_intOrder = order;
		xrpt->m_strID = szID;
		m_arrayExcerpt.Add( xrpt );
	}

	delete szID;
}

void CMailbox::FinishReceivingID()
{
	int i = m_arrayExcerpt.GetSize();
	while( i-- )
	{
		if( !m_arrayExcerpt[i]->m_intOrder )
		{
			Change( COLUMN_MAIL );
			delete m_arrayExcerpt[i];
			m_arrayExcerpt.RemoveAt(i);
		}
	}

	SendListRequest();
}


void CMailbox::SendListRequest()
{
	int i = m_arrayExcerpt.GetSize();
	while( i-- )
	{
		if( m_arrayExcerpt[i]->m_intSize == 0 )
		{
			SetState( Send( "LIST\r\n", 6 ) ?
				MBOX_WF_LIST_RESPONSE : MBOX_CANNOT_SEND );

			return;
		}
	}

	SendInitialTopRequest();
}

void CMailbox::SendBurstWriteSenseRequest()
{
	if( m_tServerSupportsBurstWrites.IsSet() )
	{
		SendUidlRequest();
	}
	else
	{
		SetState( Send( "NOOP\r\nNOOP\r\n", 12 ) ?
			MBOX_WF_FIRST_NOOP : MBOX_CANNOT_SEND );
	}
}

void CMailbox::SendUidlRequest()
{
	for( int i = 0; i < m_arrayExcerpt.GetSize(); ++i )
	{
		m_arrayExcerpt[i]->m_intOrder = 0;
	}

	if( m_tServerSupportsUIDL )
	{
		SetState( Send( "UIDL\r\n", 6 ) ?
			MBOX_WF_UIDL_RESPONSE : MBOX_CANNOT_SEND );
	}
	else
	{
		m_intOrder = 0;
		SendTopExRequest();
	}
}

void CMailbox::SendTopExRequest()
{
	char szPacket[80];
	sprintf_s( szPacket, _countof(szPacket), "TOP %d %d\r\n", ++m_intOrder, GetExtraLines() );
//	TRACE1( "\nsend: %s", szPacket );
	SetState( Send( szPacket, strlen( szPacket ) ) ?
		MBOX_WF_TOPEX_RESPONSE : MBOX_CANNOT_SEND );
}


void CMailbox::SendTopFRequest()
{
	char szPacket[80];
	strcpy_s( szPacket, _countof(szPacket), "TOP 1 0\r\n" );
	SetState( Send( szPacket, strlen( szPacket ) ) ?
		MBOX_WF_TOPF_RESPONSE : MBOX_CANNOT_SEND );
}
bool CMailbox::SendTopRequest( unsigned uIndex )
{
	char szPacket[ 16 ];

	sprintf_s( szPacket, _countof(szPacket), "TOP %d %d\r\n", 
		m_arrayExcerpt[ m_aIndices[ uIndex ] ]->m_intOrder, GetExtraLines() );

	return Send( szPacket, strlen( szPacket ) );
}

void CMailbox::SendTopRequest()
{
	if( m_aIndices.GetSize() > 0 )
	{
		if( m_tServerSupportsBurstWrites )
		{
			return;
		}
		else
		{
			SendTopRequest( 0 );
		}
	}
	else
	{
		SendInitialTopRequest();
	}
}

void CMailbox::SendInitialTopRequest()
{
	m_aIndices.RemoveAll();

	int i = m_arrayExcerpt.GetSize();
	while( i-- )
	{
		if( !m_arrayExcerpt[i]->m_bitFilled )
		{
			m_aIndices.Add( i );
		}
	}

	if( m_aIndices.GetSize() == 0 )
	{
		SendInitialRetrRequest();
	}
	else
	{
		if( m_tServerSupportsBurstWrites )
		{
			for( int i = 0; i < m_aIndices.GetSize(); ++i )
			{
				if( !SendTopRequest( i ) )
				{
					if( i == 0 )
					{
						SetState( MBOX_CANNOT_SEND );
					}
					else
					{
						// outbound buffer is full
						// stop sending for now -- truncate the array
						m_aIndices.RemoveAt( i, m_aIndices.GetSize() - i );
					}
				}
			}
		}
		else
		{
			SendTopRequest( 0 );
		}

		SetState( MBOX_WF_TOP_RESPONSE );
	}
}

void CMailbox::SendRetrRequest( unsigned uIndex )
{
	char szPacket[ 16 ];

	if (m_dwFlags & MBF_NO_RETR)
		sprintf_s( szPacket, _countof(szPacket), "TOP %d 100\r\n", m_arrayExcerpt[ m_aIndices[ uIndex ] ]->m_intOrder );
	else
		sprintf_s( szPacket, _countof(szPacket), "RETR %d\r\n", m_arrayExcerpt[ m_aIndices[ uIndex ] ]->m_intOrder );

	SetState( Send( szPacket, strlen( szPacket ) ) ?
		MBOX_WF_RETR_RESPONSE : MBOX_CANNOT_SEND );
}

void CMailbox::SendRetrRequest()
{
	if( m_aIndices.GetSize() > 0 )
	{
		if( m_tServerSupportsBurstWrites )
		{
			return;
		}
		else
		{
			SendRetrRequest( 0 );
		}
	}
	else
	{
		SendInitialDeleRequest();
	}
}

void CMailbox::SendInitialRetrRequest()
{
	m_aIndices.RemoveAll();

	int i = m_arrayExcerpt.GetSize();
	while( i-- )
	{
		if( m_arrayExcerpt[i]->m_bitDownloadData || 
			m_arrayExcerpt[i]->m_bitPreviewData)
		{
			m_aIndices.Add( i );
		}
	}

	if( m_aIndices.GetSize() == 0 )
	{
		SendInitialDeleRequest();
	}
	else
	{
		if( m_tServerSupportsBurstWrites )
		{
			for( int i = 0; i < m_aIndices.GetSize(); ++i )
			{
				SendRetrRequest( i );
			}
		}
		else
		{
			SendRetrRequest( 0 );
		}
	}
}

//
// DELE
//
void CMailbox::SendInitialDeleRequest()
{
	m_aIndices.RemoveAll();

	// we traverse in the reverse order, so that when we remove
	// Excerpts from an array, other indices remain the same
	int i = m_arrayExcerpt.GetSize();
	while( i-- )
	{
		if( !m_arrayExcerpt[i]->m_bitProtected && m_arrayExcerpt[i]->m_bitRemoveFromServer )
		{
			m_aIndices.Add( i );
		}
	}

	if( m_aIndices.GetSize() == 0 )
	{
		SendQuitRequest( MBOX_CHECKED );
	}
	else
	{
		if( m_tServerSupportsBurstWrites )
		{
			for( int i = 0; i < m_aIndices.GetSize(); ++i )
			{
				SendDeleRequest( i );
			}
		}
		else
		{
			SendDeleRequest( 0 );
		}
	}
}

void CMailbox::SendDeleRequest( unsigned uIndex )
{
	char szPacket[16];

	sprintf_s( szPacket, _countof(szPacket), "DELE %d\r\n", m_arrayExcerpt[ m_aIndices[ uIndex ] ]->m_intOrder );

	SetState( Send( szPacket, strlen( szPacket ) ) ?
		MBOX_WF_DELE_RESPONSE : MBOX_CANNOT_SEND );
}

void CMailbox::SendDeleRequest()
{
	if( m_aIndices.GetSize() > 0 )
	{
		if( m_tServerSupportsBurstWrites )
		{
			return;
		}
		else
		{
			SendDeleRequest( 0 );
		}
	}
	else
	{
		SendQuitRequest( MBOX_CHECKED );
	}
}

//
// QUIT
//
void CMailbox::SendQuitRequest( int intState )
{
	SetState( Send( "QUIT\r\n", 6 ) ?
		intState : MBOX_CANNOT_SEND );
}

void CMailbox::CancelPendingJobs()
{
	if( m_hResolveJob )
	{
		WSACancelAsyncRequest( m_hResolveJob );
		m_hResolveJob = 0;
	}

	if( m_pResolveBuffer )
	{
		delete m_pResolveBuffer;
		m_pResolveBuffer = NULL;
	}

	m_arrayPacket.RemoveAll();
	m_aIndices.RemoveAll();
}


bool CMailbox::Send( const void* lpBuf, int nBufLen )
{
	int iSentTotal = 0; 
	
	/// Do not send too many requests to avoid choke, which sometimes happens
	time_t t;
	time(&t);
	if (t-m_nStatTime <2)
		m_nStatCur++;
	else
	{
		m_nStatTime = (int)t;
		if (m_nStatCur>m_nStatMax)
			m_nStatMax = m_nStatCur;
		m_nStatCur = 0;
	}

	while( true )
	{
		int iSent = 0;
#ifdef USE_SSL
		if (m_pSSL)
			iSent = m_SSL.WriteString((const char*) lpBuf + iSentTotal, nBufLen - iSentTotal);
		else
#endif
		iSent = CAsyncSocket::Send
		( 
			(const char*) lpBuf + iSentTotal, 
			nBufLen - iSentTotal 
		);
		if ((m_pSSL && m_nStatCur > 40) || (m_nStatCur >=150) )
		{
			Sleep(250);
			m_nStatCur = 0;
			m_nStatTime = 0;
		}

		if( iSent == SOCKET_ERROR )
		{
			if( iSentTotal > 0 )
			{
				AppendToLog( "Sent: " );
				AppendToLog( CString( (char*) lpBuf, iSentTotal ) );
			}

			return false;
		}
		else
		{
			iSentTotal += iSent;

			if( iSentTotal == nBufLen )
			{
				AppendToLog( "Sent: " );
				if( ( nBufLen > 4 )
					&& ((char*) lpBuf)[0] == 'P'
					&& ((char*) lpBuf)[1] == 'A'
					&& ((char*) lpBuf)[2] == 'S'
					&& ((char*) lpBuf)[3] == 'S'
				)
				{
					AppendToLog( "PASS ********\r\n" );
				}
				else
				{
					AppendToLog( CString( (char*) lpBuf, iSentTotal ) );
				}
				return true;
			}

			Sleep( 1 );
		}
	}
}

void CMailbox::OnClose( int /*nErrorCode*/ )
{
	if( !STATE_FINAL( m_intState ) )
	{
		if( m_intState == MBOX_WF_USER_RESPONSE )
		{
			// not sure about this, but this can be a reason for "1st check" bug
			//if (!m_tServerSupportsAPOP.IsSet() || m_tServerSupportsAPOP)
			{
				m_tServerSupportsAPOP = false;
				Check();
			}
		}
		else
		{
			if( m_intState == MBOX_WF_FIRST_NOOP )
			{
				// pop3.freenet.de cannot handle "NOOP<CRLF>NOOP<CRLF>" !!!
				// it drops the connection, maybe it crashes or something..
				m_tServerSupportsBurstWrites = false;
				Check();	
			}
			else if( m_intState == MBOX_WF_APOP_RESPONSE && !m_tServerSupportsAPOP.IsSet() )
			{
				m_tServerSupportsAPOP = false;
				Check();
			}
			else
			{
				SetState( MBOX_CONNECTION_WAS_LOST );
			}
		}
	}
}

const CString& CMailbox::GetGreeting() const
{
	return m_sServerGreeting;
}

unsigned CMailbox::GetIPAddress() const
{
	return m_ulongIP;
}

const Tristate& CMailbox::GetAPOPSupported() const
{
	return m_tServerSupportsAPOP;
}

const Tristate& CMailbox::GetUIDLSupported() const
{
	return m_tServerSupportsUIDL;
}

const Tristate& CMailbox::GetBurstWritesSupported() const
{
	return m_tServerSupportsBurstWrites;
}

void CMailbox::WatchDogTimedOut()
{
	if( m_intState == MBOX_WF_SECOND_NOOP )
	{
		m_tServerSupportsBurstWrites = false;

		// at this point, we may get into a deadlock situation
		// because the command sequence may have been violated by
		// a delay
		// SendUidlRequest();
		Check();
	}
}

bool CMailbox::IsLoggingEnabled() const
{
	return m_bLog;
}

void CMailbox::EnableLogging( bool bValue )
{
	if( !bValue )
	{
		m_strLog.Empty();
	}

	m_bLog = bValue;
}

const CString& CMailbox::GetLogString() const
{
	static CString s;
	s.Format(_T("%d\n\n"), m_nStatMax);
	s += m_strLog;
	return s;
	//return m_strLog;
}

void CMailbox::AppendToLog( const CString& strLog )
{
	if( m_bLog )
	{
		m_strLog += strLog;
		TRACE(_T("\n==========log======"));
		TRACE(strLog);
		TRACE(_T("\n"));
	}
}
BOOL CMailbox::IsDisabled() const
{
	return (m_dwFlags & MBF_PASSIVE)!=0;
}

CString CMailbox::GetPassword()
{
	if (!m_strPass.IsEmpty())	// entered or saved
		return m_strPass;
		
	if ( m_dwFlags & MBF_ASK_PASS)
	{
		DPassword dlg(FALSE, m_strAlias);
		if (dlg.DoModal() == IDOK)
		{
			m_strPass = dlg.m_sPassword;
			return m_strPass;
		}
	}

	return "";	// ask password? what if minimized?
}

int  CMailbox::GetExtraLines()
{
	if (m_nExtraLines == 255)
		return 0;
	return m_nExtraLines;
}

BOOL CMailboxFolder::UpdateUnreadStatus(int* pAll)
{
	int nAll = 0;
	int nNew = 0;
	for (int i=0; i<m_aBoxes.GetSize(); i++)
	{
		for (int m=0; m<m_aBoxes[i]->m_arrayExcerpt.GetSize(); m++)
		{
			if (m_aBoxes[i]->m_arrayExcerpt[m] && 
				!m_aBoxes[i]->m_arrayExcerpt[m]->WasRead())
				nNew++;
		}
		nAll += m_aBoxes[i]->m_arrayExcerpt.GetSize();
	}
	if (pAll)
		*pAll = nAll;
	if (m_nUnread!=nNew)
	{
		m_nUnread = nNew;
		return TRUE;
	}
	return FALSE;

}

BOOL CMailbox::UpdateUnreadStatus(int* )
{
	int nNew = 0;
	for (int m=0; m<m_arrayExcerpt.GetSize(); m++)
	{
		if (m_arrayExcerpt[m] && 
			!m_arrayExcerpt[m]->WasRead())
			nNew++;
	}
	if (m_nUnread == nNew)
		return FALSE;
	m_nUnread = nNew;
	
	return (TRUE);//(bWasUnread != bUnread);
}

CMailBoxClip::CMailBoxClip() : CClipBoard(_T("MAGIC_MAILBOX"))
{
}
CMailBoxClip::~CMailBoxClip()
{
	Cleanup();
}
void CMailBoxClip::Cleanup()
{
	for (int i=0; i<m_aData.GetSize(); i++)
		delete m_aData[i];
	m_aData.RemoveAll();
}
BOOL CMailBoxClip::SaveData(CMailbox** pData, int nCount)
{					
	Cleanup();
	for (int i=0; i<nCount; i++, pData++)
		m_aData.Add(*pData);
	int nRet = CopyToClipboard();
	m_aData.RemoveAll();	// clean up - do not delete objects!
	return nRet;
}

CMailbox**	CMailBoxClip::GetData(int& nCount)
{
	Cleanup();
	m_aData.RemoveAll();
	if (!PasteFromClipboard())
		return NULL;
	nCount = m_aData.GetSize();
	return (CMailbox**)m_aData.GetData();
}

BOOL CMailBoxClip::Serialize(CArchive& ar)
{
	BYTE bVer = 1;
	if (ar.IsStoring())
	{
		ar << bVer;
		BYTE bC = (BYTE)m_aData.GetSize();
		ar << bC;
		m_aData.Serialize(ar);
	}
	else
	{
		BYTE b = 0;
		ar >> b;
		if (b>bVer || b==0)
			return FALSE;
		ar >> b;
		m_aData.Serialize(ar);
		if (m_aData.GetSize()!=b)
			return FALSE;
	}
	return TRUE;
}
void CMailbox::OnSSLEvent(int nEv)
{
	switch (nEv)
	{
	case 1:
		OnConnect(0);
		break;
	case 2:
		if (m_SSL.GetData(NULL, 0))
			OnReceive(0);
		break;
	}
}
int CMailbox::SuggestIcon() {return ( m_arrayExcerpt.GetSize() ? II_FULL : II_EMPTY );};
int CMailboxFolder::SuggestIcon() {return II_FOLDER;};
CMailboxFolder::CMailboxFolder()
{
	m_intChanged = 0;
	Change(COLUMN_ALIAS);
	Change(COLUMN_MAIL);
	SetFlag(MBF_FOLDER, TRUE);
}
IMPLEMENT_SERIAL( CMailboxFolder, CMailbox, 101 | VERSIONABLE_SCHEMA );
void CMailboxFolder::Serialize(CArchive& ar)
{
	if( ar.IsLoading() ) // READ
	{
		int nVer = ar.GetObjectSchema();
		ASSERT(nVer>=100);
		ar >> m_strAlias;
		if (nVer>100)
			ar >> m_dwFlags;
	}
	else
	{
		ar << m_strAlias;
		ar << m_dwFlags;
	}
}
static CString FormatHoursMinutes( int minutes )
{
	int hours = minutes / 60;
	minutes = minutes % 60;

	CString strTmp;

	if( hours )
	{
		if( minutes )
		{
			strTmp.Format( _T("%d hrs %d min"), hours, minutes );
		}
		else
		{
			strTmp.Format( _T("%d hrs"), hours );
		}
	}
	else
	{
		strTmp.Format( _T("%d min"), minutes ); 
	}

	return strTmp;
}

void CMailbox::GetText(int nCol, CString& s)
{
	s.Empty();	
	switch(nCol)
	{
	case COLUMN_USER:
		s = m_strUser;
		return;
	case COLUMN_MAIL:
		{
			UpdateUnreadStatus();
			if (m_nUnread>0)
				s.Format( _T("%d/%d"), m_nUnread, m_arrayExcerpt.GetSize());
			else
				s.Format( _T("%d"), m_arrayExcerpt.GetSize() );
		}
		return;

	case COLUMN_ELAPSED:
		if( -1 == m_intElapsed ) 
			s = _T("...");
		else 
			s = FormatHoursMinutes( m_intElapsed );
		return;
	case COLUMN_POLL:
		if( m_intPoll )
		{
			s = FormatHoursMinutes( m_intPoll );
		}
		else 
		{
			s.LoadString( IDP_DISABLED );
			StrTranslate(s);
		}
		return;
	}
}
void CMailboxFolder::GetText(int nCol, CString& s)
{
	s.Empty();	
	switch(nCol)
	{
	case COLUMN_MAIL:
	{
		int nAll=0;
		UpdateUnreadStatus(&nAll);
		if (m_nUnread>0)
			s.Format( _T("%d/%d"), m_nUnread , nAll);
		else
			s.Format( _T("%d"), nAll);
		return;
	}
	case COLUMN_USER:
		s.Format(_T("%d"), m_aBoxes.GetSize());
		return;
	}
}
void CMailbox::MarkRead(BOOL b)
{
	for (int m=0; m<m_arrayExcerpt.GetSize(); m++)
	{
		m_arrayExcerpt[m]->MarkAsRead(b);
	}
}

void CMailbox::SetSelection(int nState)
{
	m_bitSelected = nState;
	Change( COLUMN_SELECTED );
}
void CMailboxFolder::AddBox(CMailbox* p)
{
	m_aBoxes.Add(p); 
	p->m_pFolder = this;
	p->CloseFolder(IsClosed());
}

void CMailboxFolder::SetSelection(int nState)
{
	CMailbox::SetSelection(nState);
	for (int i=0; i<m_aBoxes.GetSize(); i++)
	{
		m_aBoxes[i]->SetSelection(nState);
	}

}
void CMailboxFolder::Check()
{
	for (int i=0; i<m_aBoxes.GetSize(); i++)
	{
		if (!m_aBoxes[i]->IsFolder())
			m_aBoxes[i]->Check();
	}
}
void CMailbox::Change( int intColumn ) 
{
	m_intChanged |= BIT( intColumn ); 
	if (m_pFolder && intColumn == COLUMN_MAIL)
		m_pFolder->Change(COLUMN_MAIL);
}
