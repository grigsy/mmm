
#include "stdafx.h"
#include "magic.h"
#include "About.h"
#include "statlink.h"
#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About


CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CAboutDlg::OnInitDialog()
{
	HCURSOR cHand = ::LoadCursor(NULL, IDC_HAND);
	CreateStaticLink(this, IDC_MAIL, _T("mailto:mmm@grigsoft.com?subject=MMM support"), cHand);
	CreateStaticLink(this, IDC_WWW, _T("http://www.grigsoft.com/?MMM"), cHand);
	CreateStaticLink(this, IDC_WWW1, _T("http://mmm3.sourceforge.net/"), cHand);
	CreateStaticLink(this, IDC_WWW2, _T("http://sourceforge.net/projects/mmm3/"), cHand);
	CreateStaticLink(this, IDC_DEV1, NULL, cHand);
	CreateStaticLink(this, IDC_DEV2, NULL, cHand);

	GetVersionData();

	CString strTmp1, strTmp2;
	strTmp1 = m_strProductName;
	strTmp1 += _T(' ');
	strTmp1 += m_strShortVersion;

	if( m_strSpecialBuild.GetLength() )
	{
		strTmp1 += _T(" (");
		strTmp1 += m_strSpecialBuild;
		strTmp1 += _T(')');
	}

	strTmp1 += _T("\n\n");
	strTmp1 += m_strFileDescription;
	strTmp1 += _T("\n\n");
	strTmp1 += m_strLegalCopyright;
	strTmp1 += _T("\n\n");
	strTmp1 += m_strComments;
	SetDlgItemText( IDC_VER, strTmp1 );

	return CDialog::OnInitDialog();
}

void CAboutDlg::GetVersionData()
{
	CString sFile;
	GetModuleFileName(NULL, sFile.GetBuffer(500), 500);
	sFile.ReleaseBuffer();
	DWORD dwZero = 0;
	DWORD dwBufferSize = GetFileVersionInfoSize( sFile, &dwZero );
	if (!dwBufferSize)
		return;

	CByteArray aData;
	aData.SetSize(dwBufferSize);
	char *pBuffer = (char*)aData.GetData();
	VERIFY( GetFileVersionInfo( sFile, 0, dwBufferSize, pBuffer ) );

	char *pVerInfo = 0;
	UINT uintVerInfoSize = 0;
/*
	if( VerQueryValue( pBuffer, _T("\\"), (void**) &pVerInfo, &uintVerInfoSize ) )
	{
		VS_FIXEDFILEINFO &FixedFileInfo = *(VS_FIXEDFILEINFO*) pVerInfo;
	}
*/	

	if( VerQueryValue( pBuffer, _T("\\StringFileInfo\\040904b0\\Comments"), (void**) &pVerInfo, &uintVerInfoSize ) )
	{
		TCHAR *tszTmp = new TCHAR[ uintVerInfoSize ];
		OemToChar( pVerInfo, tszTmp );
		m_strComments = tszTmp;
		delete tszTmp;
	}
	
	if( VerQueryValue( pBuffer, _T("\\StringFileInfo\\040904b0\\FileDescription"), (void**) &pVerInfo, &uintVerInfoSize ) )
	{
		TCHAR *tszTmp = new TCHAR[ uintVerInfoSize ];
		OemToChar( pVerInfo, tszTmp );
		m_strFileDescription = tszTmp;
		delete tszTmp;
	}
	
	if( VerQueryValue( pBuffer, _T("\\StringFileInfo\\040904b0\\LegalCopyright"), (void**) &pVerInfo, &uintVerInfoSize ) )
	{
		TCHAR *tszTmp = new TCHAR[ uintVerInfoSize ];
		OemToChar( pVerInfo, tszTmp );
		m_strLegalCopyright = tszTmp;
		delete tszTmp;
	}
	
	if( VerQueryValue( pBuffer, _T("\\StringFileInfo\\040904b0\\ProductName"), (void**) &pVerInfo, &uintVerInfoSize ) )
	{
		TCHAR *tszTmp = new TCHAR[ uintVerInfoSize ];
		OemToChar( pVerInfo, tszTmp );
		m_strProductName = tszTmp;
		delete tszTmp;
	}
	
	if( VerQueryValue( pBuffer, _T("\\StringFileInfo\\040904b0\\ProductVersion"), (void**) &pVerInfo, &uintVerInfoSize ) )
	{
		TCHAR *tszTmp = new TCHAR[ uintVerInfoSize ];
		OemToChar( pVerInfo, tszTmp );
		m_strProductVersion = tszTmp;
		delete tszTmp;
	}

	if( VerQueryValue( pBuffer, _T("\\StringFileInfo\\040904b0\\SpecialBuild"), (void**) &pVerInfo, &uintVerInfoSize ) )
	{
		TCHAR *tszTmp = new TCHAR[ uintVerInfoSize ];
		OemToChar( pVerInfo, tszTmp );
		m_strSpecialBuild = tszTmp;
		delete tszTmp;
	}
	
	m_strShortVersion = _T("v");
	m_strShortVersion += m_strProductVersion[0];
	m_strShortVersion += _T(".");
	m_strShortVersion += m_strProductVersion[2];
	if( _T('0') != m_strProductVersion[4])
	{
		m_strShortVersion += m_strProductVersion[4];
	}
	if( _T('0') != m_strProductVersion[6] ) 
	{
		m_strShortVersion += _T("b");
		m_strShortVersion += m_strProductVersion.Mid(6);
	}

}