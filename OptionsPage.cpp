// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// OptionsPage.cpp : implementation file
//

#include "stdafx.h"
#include "magic.h"
#include "OptionsPage.h"
#include "resrc1.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COptionsPage property page

IMPLEMENT_DYNCREATE(COptionsPage, CPropertyPage)

COptionsPage::COptionsPage() : CPropertyPage(COptionsPage::IDD)
{
	//{{AFX_DATA_INIT(COptionsPage)
	m_bCheckImmediately = theApp.intCheckImmediately;
	m_bStartAlwaysHidden = theApp.intStartAlwaysHidden;
	m_bEVConfirmDelete = theApp.intEVConfirmDelete;
	m_bPopUpMainWindow = theApp.intPopUpMainWindow;
	m_nPreviewSize = theApp.intPreviewSize;
	m_strFileExtensionForMessages = theApp.strFileExtensionForMessages;
	m_nMarkRead = theApp.intMarkRead;
	m_sApp = theApp.strApp;
	//}}AFX_DATA_INIT
}

COptionsPage::~COptionsPage()
{
}

void COptionsPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptionsPage)
	DDX_Control(pDX, IDC_READ_OPT, m_cbMarkRead);
	DDX_Check(pDX, IDC_CHECK_IMMEDIATELY, m_bCheckImmediately);
	DDX_Check(pDX, IDC_START_HIDDEN, m_bStartAlwaysHidden);
	DDX_Check(pDX, IDC_EV_CONFIRM_DELETE, m_bEVConfirmDelete);
	DDX_Check(pDX, IDC_POP_UP_MAIN_WINDOW, m_bPopUpMainWindow);
	DDX_Text(pDX, IDC_PREVIEW_SIZE, m_nPreviewSize);
	DDX_Text(pDX, IDC_EDIT_FILE_EXTENSIONS_FOR_MESSAGES, m_strFileExtensionForMessages);
	DDX_CBIndex(pDX, IDC_READ_OPT, m_nMarkRead);
	DDX_Text(pDX, IDC_APP, m_sApp);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COptionsPage, CPropertyPage)
	//{{AFX_MSG_MAP(COptionsPage)
	ON_BN_CLICKED(IDC_CHECK_IMMEDIATELY, OnModified)
	ON_BN_CLICKED(IDC_START_HIDDEN, OnModified)
	ON_BN_CLICKED(IDC_EV_CONFIRM_DELETE, OnModified)
	ON_BN_CLICKED(IDC_POP_UP_MAIN_WINDOW, OnModified)
	ON_EN_CHANGE(IDC_EDIT_FILE_EXTENSIONS_FOR_MESSAGES, OnModified)
	ON_CBN_SELCHANGE(IDC_READ_OPT, OnModified)
	ON_BN_CLICKED(IDC_BRS, OnBrs)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptionsPage message handlers

void COptionsPage::OnModified() 
{
	SetModified();
}

LPCTSTR asTexts[]=
{
	"${Always}",
	"${After preview}",
	"${When minimized only}",
	"${After preview or minimize}",
};

BOOL COptionsPage::OnInitDialog() 
{
	
	if( !CPropertyPage::OnInitDialog() ) return FALSE;
	DlgTranslate(this);
	SheetTranslate(GetParent());	// translate parent from 1st page
	
	int nMax = sizeof(asTexts)/sizeof(asTexts[0]);
	for (int i=0; i<nMax; i++)
	{
		CString s = asTexts[i];
		StrTranslate(s);
		m_cbMarkRead.AddString(s);
	}

	SendDlgItemMessage(IDC_SPIN, UDM_SETRANGE32, 1, 100);
	UpdateData(FALSE);
	
	return TRUE;
}

BOOL COptionsPage::OnApply() 
{
	theApp.intCheckImmediately = m_bCheckImmediately;
	theApp.intStartAlwaysHidden = m_bStartAlwaysHidden;
	theApp.intEVConfirmDelete = m_bEVConfirmDelete;
	theApp.intPopUpMainWindow = m_bPopUpMainWindow;
	theApp.intPreviewSize = m_nPreviewSize;
	theApp.intMarkRead = m_nMarkRead;
	theApp.strFileExtensionForMessages = m_strFileExtensionForMessages;
	theApp.strApp = m_sApp;

	return CPropertyPage::OnApply();
}


static TCHAR BASED_CODE szFilter[] = _T( "Executable files (*.exe)|*.exe|All Files (*.*)|*.*||");

void COptionsPage::OnBrs() 
{
	UpdateData();
	CFileDialog dlg(TRUE, _T(".exe"), m_sApp, 
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_FILEMUSTEXIST,
		szFilter);
	if (dlg.DoModal()!=IDOK)
		return;
	m_sApp = dlg.GetPathName();
	SetDlgItemText(IDC_APP, m_sApp);
}
