// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// SecureString.cpp : implementation file
//

#include "stdafx.h"
#include "magic.h"
#include "SecureString.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
static DWORD dwSeed = 1;
inline void SetSeed( int intSeed ) { dwSeed = (DWORD) intSeed; }
inline DWORD Random() { return dwSeed = 33 * dwSeed + 153; }

/////////////////////////////////////////////////////////////////////////////
// CSecureString

IMPLEMENT_SERIAL( CSecureString, CObject, 250 )

CSecureString::CSecureString()
:	m_dwCharSize( sizeof(TCHAR) ),
	m_dwCheckSum( 0 )
{
	m_arrayData.SetSize( 0, 256 );
}

CSecureString::~CSecureString()
{
}

void CSecureString::Serialize( CArchive &ar )
{
	CObject::Serialize( ar );
	
	if( ar.IsLoading() ) // READ
	{
		ar >> m_dwCharSize;
		if( sizeof( TCHAR ) < m_dwCharSize ) AfxThrowArchiveException( CArchiveException::badIndex, _T("") );
		m_arrayData.Serialize( ar );
		ar >> m_dwCheckSum;
	}
	else // WRITE
	{
		ar << m_dwCharSize;
		m_arrayData.Serialize( ar );
		ar << m_dwCheckSum;
	}
}

void CSecureString::EncodeString( int intClue, CString &str )
{
	m_dwCharSize = sizeof( TCHAR );
	int intBufferSize = m_dwCharSize * str.GetLength();

	// write to byte array
	m_arrayData.SetSize( intBufferSize );
	if( intBufferSize ) memcpy( m_arrayData.GetData(), str.GetBuffer(0), intBufferSize );
	str.ReleaseBuffer();

	// encrypt
	SetSeed( intClue );
	int i=0;
	for( i = 0; i < intBufferSize; ++i ) m_arrayData[i] ^= Random();

	// calculate checksum
	m_dwCheckSum = intClue;
	for( i = 0; i < intBufferSize; ++i ) m_dwCheckSum ^= ( DWORD( m_arrayData[i] ) >> i%4 );
}

void CSecureString::DecodeString( int intClue, CString &str )
{
	int intBufferSize = m_arrayData.GetSize();
	int intStringLength = intBufferSize / sizeof( TCHAR );

	// assure checksum
	m_dwCheckSum ^= intClue;
	int i=0;
	for( i = 0; i < intBufferSize; ++i ) m_dwCheckSum ^= ( DWORD( m_arrayData[i] ) >> i%4 );
	if( m_dwCheckSum ) AfxThrowArchiveException( CArchiveException::badIndex, _T("") );

	// decrypt
	SetSeed( intClue );
	for( i = 0; i < intBufferSize; ++i ) m_arrayData[i] ^= Random();

	// write to string
	if( intBufferSize )
	{
		if( sizeof( TCHAR ) == m_dwCharSize )
		{
			memcpy( str.GetBuffer( intStringLength + 1 ), m_arrayData.GetData(), intBufferSize );
		}
		else
		{
			OemToCharBuff( (char*) m_arrayData.GetData(), str.GetBuffer( intStringLength + 1 ), intStringLength )	;
		}
		str.ReleaseBuffer( intStringLength );
	}
	else str.Empty();
}

//BEGIN_MESSAGE_MAP(CSecureString, CObject)
	//{{AFX_MSG_MAP(CSecureString)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
//END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CSecureString message handlers
