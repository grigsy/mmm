// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// HeaderPage.cpp : implementation file
//

#include "stdafx.h"
#include "magic.h"
#include "Excerpt.h"
#include "Mailbox.h"
#include "HeaderPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHeaderPage property page

IMPLEMENT_DYNCREATE(CHeaderPage, CPropertyPage)

CHeaderPage::CHeaderPage() : CPropertyPage(CHeaderPage::IDD)
{
	//{{AFX_DATA_INIT(CHeaderPage)
	m_strDate = _T("");
	m_strFrom = _T("");
	m_strSize = _T("");
	m_strSubject = _T("");
	m_strMailbox = _T("");
	m_sFilters = _T("");
	//}}AFX_DATA_INIT
}

CHeaderPage::~CHeaderPage()
{
}

void CHeaderPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHeaderPage)
	DDX_Text(pDX, IDC_EDIT_DATE, m_strDate);
	DDX_Text(pDX, IDC_EDIT_FROM, m_strFrom);
	DDX_Text(pDX, IDC_EDIT_SIZE, m_strSize);
	DDX_Text(pDX, IDC_EDIT_SUBJECT, m_strSubject);
	DDX_Text(pDX, IDC_EDIT_MAILBOX, m_strMailbox);
	DDX_Text(pDX, IDC_EDIT_FILTERS, m_sFilters);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHeaderPage, CPropertyPage)
	//{{AFX_MSG_MAP(CHeaderPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHeaderPage message handlers

void CHeaderPage::Attach( CTypedPtrArray < CPtrArray, CExcerpt* > *pExcerptArray )
{
	ASSERT( pExcerptArray );
	m_pExcerptArray = pExcerptArray;
}

BOOL CHeaderPage::OnInitDialog() 
{
	if( !CPropertyPage::OnInitDialog() ) return FALSE;

	ASSERT( m_pExcerptArray );

	m_strMailbox	= (*m_pExcerptArray)[0]->m_pMailbox->m_strAlias;
	m_strFrom		= (*m_pExcerptArray)[0]->m_strFrom;
	m_strSubject	= (*m_pExcerptArray)[0]->m_strSubject;
	CTime tmDate	= (*m_pExcerptArray)[0]->m_tmDate;
	m_strDate		= ( 1971 < tmDate.GetYear() ) ? tmDate.Format( _T("%#c") ) : _T("...");
	int intSize		= (*m_pExcerptArray)[0]->m_intSize;
	m_sFilters		= (*m_pExcerptArray)[0]->m_sFilters;

	for( int i = m_pExcerptArray->GetUpperBound(); i; --i )
	{
		CExcerpt &expt = *( (*m_pExcerptArray)[i] );
		if( !m_strMailbox.IsEmpty() && ( m_strMailbox != expt.m_pMailbox->m_strAlias ) ) m_strMailbox.Empty();
		if( !m_strFrom.IsEmpty() && ( m_strFrom != expt.m_strFrom ) ) m_strFrom.Empty();
		if( !m_strSubject.IsEmpty() && ( m_strSubject != expt.m_strSubject ) ) m_strSubject.Empty();
		if( !m_strDate.IsEmpty() && ( tmDate != expt.m_tmDate ) ) m_strDate.Empty();
		intSize += expt.m_intSize;
	}

	if( m_strMailbox.IsEmpty() )	m_strMailbox = cstrMultipleSelection;
	if( m_strFrom.IsEmpty() )		m_strFrom = cstrMultipleSelection;
	if( m_strSubject.IsEmpty() )	m_strSubject = cstrMultipleSelection;
	if( m_strDate.IsEmpty() )		m_strDate = cstrMultipleSelection;
	BytesToString( intSize, m_strSize );

	UpdateData( FALSE );
	SetModified( FALSE );
	DlgTranslate(this);
	SheetTranslate(GetParent());	// translate parent from 1st page
	
	return TRUE;
}
