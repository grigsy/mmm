// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef MAGIC_FRAME_INCLUDED
#define MAGIC_FRAME_INCLUDED

#include "WatchDog.h"

class CMailbox;

class CMagicFrame : public CFrameWnd, public CWatchDog
{
protected: // create from serialization only
	CMagicFrame();
	DECLARE_DYNCREATE(CMagicFrame)

public:
	void SwitchPane();

	void UpdateMailCount( int intTotalMailCount, int intNewMailCount );
	UINT m_uintTimer;

	Dictions m_aLanguages;
	CString	m_sDicRoot;

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMagicFrame)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual void GetMessageString( UINT nID, CString& rMessage ) const;
	//}}AFX_VIRTUAL

public:
	virtual ~CMagicFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	CStringArray	m_asFilterLists;
protected:

	BOOL			m_bListReady;
	void			UpdateFilterLists();

	afx_msg BOOL OnMyBarCheck(UINT nID);
	//{{AFX_MSG(CMagicFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnRestore();
	afx_msg void OnUpdateRestore(CCmdUI* pCmdUI);
	afx_msg void OnPreferences();
	afx_msg void OnEnterIdle(UINT nWhy, CWnd* pWho);
	afx_msg void OnQuickSend();
	afx_msg void OnSuspend();
	afx_msg void OnUpdateSuspend(CCmdUI* pCmdUI);
	afx_msg void OnEndSession(BOOL bEnding);
	afx_msg void OnUpdateFirstLanguage(CCmdUI* pCmdUI);
	afx_msg void OnFileApplyfilters();
	afx_msg void OnFriends();
	afx_msg void OnClose();
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg void OnViewDelLog();
	afx_msg void OnKillSpam();
	afx_msg void OnTestRe();
	afx_msg void OnFilters();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg BOOL OnQueryEndSession();
	//}}AFX_MSG
	afx_msg void OnLanguageSelect(UINT nID);
	afx_msg void OnSysCommand( UINT, LPARAM );
	afx_msg void OnEnterMenuLoop( BOOL );
	afx_msg void OnExitMenuLoop( BOOL );
	afx_msg void OnActivate( UINT nState, CWnd* pWndOther, BOOL bMinimized );
	afx_msg BOOL OnToolTipText(UINT nID, NMHDR* pNMHDR, LRESULT* pResult);

	LRESULT OnTaskbarNotification( WPARAM, LPARAM );
	LRESULT OnEndResolveHostByName( WPARAM, LPARAM );
	LRESULT OnStartCommand( WPARAM, LPARAM );
	LRESULT OnStartPlayback( WPARAM, LPARAM );
	LRESULT OnStartMessage( WPARAM, LPARAM );
	LRESULT OnTestMessage( WPARAM, LPARAM );
	LRESULT ShowMessage( WPARAM, LPARAM, BOOL );
	LRESULT OnUpdateItem( WPARAM, LPARAM );
	LRESULT OnMciNotify( WPARAM, LPARAM );
	
	LRESULT OnTaskBarCreated( WPARAM, LPARAM );

	DECLARE_MESSAGE_MAP()

private:
	CSplitterWnd m_wndSplitter;
	NOTIFYICONDATA m_nidA;
	NOTIFYICONDATA m_nidB;
	CStatusBar  m_wndStatusBar;
	CToolBar	m_wndToolBar;

	HICON		m_hMailboxFull;
	HICON		m_hMailboxEmpty;
	HICON		m_hMailboxFull16;
	HICON		m_hMailboxEmpty16;
	HICON		m_hMailboxSuspended;

	void SetIndicators( BOOL );

	BOOL			m_bActive;

	bool m_bIsAlternativeIcon;
	bool m_bFlashNewMail;
	virtual void WatchDogTimedOut();
	
	void SetMainWindowIcon( HICON hTaskbarIcon );
	void SaveSettings();
};

#endif
/////////////////////////////////////////////////////////////////////////////
// DReTester dialog

class DReTester : public CDialog
{
public:
	DReTester(CWnd* pParent = NULL);   // standard constructor

	//{{AFX_DATA(DReTester)
	enum { IDD = IDD_RE_TEST };
	CString	m_sRE;
	CString	m_sText;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(DReTester)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:

	//{{AFX_MSG(DReTester)
	afx_msg void OnOk();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
