// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// MailboxView.cpp : implementation of the CMailboxView class
//

#include "stdafx.h"
#include "Magic.h"
#include "Mailbox.h"
#include "Excerpt.h"
#include "MagicDoc.h"
#include "MailboxView.h"
#include "MagicFrame.h"
#include "dfilters.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMailboxView

IMPLEMENT_DYNCREATE(CMailboxView, CListViewEx)

BEGIN_MESSAGE_MAP(CMailboxView, CListViewEx)
	//{{AFX_MSG_MAP(CMailboxView)
	ON_COMMAND(ID_VIEW_LIST, OnViewList)
	ON_COMMAND(ID_VIEW_REPORT, OnViewReport)
	ON_COMMAND(ID_VIEW_SMALLICON, OnViewSmallicon)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ICON, OnUpdateViewIcon)
	ON_UPDATE_COMMAND_UI(ID_VIEW_LIST, OnUpdateViewList)
	ON_UPDATE_COMMAND_UI(ID_VIEW_REPORT, OnUpdateViewReport)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SMALLICON, OnUpdateViewSmallicon)
	ON_NOTIFY_REFLECT(LVN_COLUMNCLICK, OnColumnclick)
	ON_NOTIFY_REFLECT(NM_RCLICK, OnRclick)
	ON_COMMAND(ID_EDIT_SELECT_ALL, OnEditSelectAll)
	ON_UPDATE_COMMAND_UI(ID_EDIT_SELECT_ALL, OnUpdateEditSelectAll)
	ON_NOTIFY_REFLECT(LVN_ITEMCHANGED, OnItemChanged)
	ON_COMMAND(ID_EDIT_DELETE, OnEditDelete)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PROPERTIES, OnUpdateIfSelectedCount)
	ON_COMMAND(ID_EDIT_PROPERTIES, OnEditProperties)
	ON_COMMAND(IDC_RENAME, OnRename)
	ON_COMMAND(IDC_MAIL_READ, OnMarkRead)
	ON_COMMAND(IDC_MAIL_UNREAD, OnMarkUnread)
	ON_UPDATE_COMMAND_UI(IDC_RENAME, OnUpdateIfSelectedCount)
	ON_COMMAND(ID_EDIT_NEW, OnEditNew)
	ON_COMMAND(ID_VIEW_REFRESH, OnViewRefresh)
	ON_UPDATE_COMMAND_UI(ID_STOP_CHECKING, OnUpdateIfItemCount)
	ON_UPDATE_COMMAND_UI(ID_COMMAND, OnUpdateIfFocusedCount)
	ON_NOTIFY_REFLECT(NM_DBLCLK, OnDblclk)
	ON_COMMAND(ID_COMMAND, OnCommand)
	ON_WM_DESTROY()
	ON_COMMAND(ID_STOP_CHECKING, OnStopChecking)
	ON_NOTIFY_REFLECT(LVN_KEYDOWN, OnKeydown)
	ON_UPDATE_COMMAND_UI(ID_VIEW_REFRESH, OnUpdateIfItemCount)
	ON_UPDATE_COMMAND_UI(ID_EDIT_DELETE, OnUpdateIfSelectedCount)
	ON_COMMAND(ID_MAILBOX_COPY, OnMailboxCopy)
	ON_UPDATE_COMMAND_UI(ID_MAILBOX_COPY, OnUpdateMailboxCopy)
	ON_COMMAND(ID_MAILBOX_PASTE, OnMailboxPaste)
	ON_UPDATE_COMMAND_UI(ID_MAILBOX_PASTE, OnUpdateMailboxPaste)
	//}}AFX_MSG_MAP
	ON_UPDATE_COMMAND_UI( ID_INDICATOR_OBJECTS, OnUpdateIndicatorObjects )
	ON_UPDATE_COMMAND_UI( ID_INDICATOR_CONTENTS, OnUpdateIndicatorContents )
	ON_MESSAGE(VM_LANGUAGE, OnLanguage)
	ON_NOTIFY(HDN_ENDDRAG, 0, OnHeaderEndDrag)
	ON_WM_INITMENUPOPUP()
	ON_COMMAND_RANGE(ID_FOLDER_LIST, ID_FOLDER_LIST2, OnSelectFolder)
	ON_COMMAND(ID_FOLDER_REMOVE, OnRemoveFromFolder)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMailboxView construction/destruction

CMailboxView::CMailboxView()
:	m_bitSortEnabled(1),
	m_bitSortRequired(0)
{
}

CMailboxView::~CMailboxView()
{
}	 

/////////////////////////////////////////////////////////////////////////////
// CMailboxView diagnostics

#ifdef _DEBUG
void CMailboxView::AssertValid() const
{
	CListViewEx::AssertValid();
}

void CMailboxView::Dump(CDumpContext& dc) const
{
    	CListViewEx::Dump(dc);
}

CMagicDoc* CMailboxView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMagicDoc)));
	return (CMagicDoc*)m_pDocument;
}
#endif //_DEBUG

void CMailboxView::EnableSort( BOOL bEnable )
{
	m_bitSortEnabled = bEnable;
	if( bEnable && m_bitSortRequired ) 
		SortItems();
}

void CMailboxView::SortItems()
{
	if( !m_bitSortEnabled ) { m_bitSortRequired = 1; return; }

	theApp.DoWaitCursor(1);
	m_bitSortRequired = 0;
	
	GetListCtrl().SortItems( &CompareSubItems, (DWORD_PTR)GetDocument() );
	theApp.DoWaitCursor(-1);
}
						 
/////////////////////////////////////////////////////////////////////////////
// CMailboxView message handlers

void CMailboxView::OnInitialUpdate() 
{
	CListViewEx::OnInitialUpdate();
	
	ASSERT( GetDocument() );
	CMagicDoc &doc = *(CMagicDoc*) GetDocument();
	CListCtrl &list = GetListCtrl();

	list.SetItemCountEx( doc.m_aMailboxes.GetSize(), 0 );

	if( theApp.intCheckImmediately ) doc.OnTimerCheck();
	SetFullRowSel(TRUE);

	theApp.SetDefFont(GetFont());
	if (theApp.GetFont())
		SetFont(theApp.GetFont());
}


void CMailboxView::OnUpdate(CView* , LPARAM lHint, CObject* pHint)
{
	switch (lHint)
	{
		case HINT_OPTIONS:
			if (theApp.GetFont())
			{
				SetFont(theApp.GetFont());
				m_fntBold.DeleteObject();
				Invalidate();
			}
			return;
		case HINT_DISABLE_SORT:
			EnableSort( FALSE );
			return;
		case HINT_ENABLE_SORT:
			EnableSort();
			return;
		case HINT_UPDATE_ITEM:	// handled below
			break;
		default:
			return;
	}

	if( !pHint ) return;

	if( !pHint->IsKindOf( RUNTIME_CLASS( CMailbox ) ) ) return;
	CMailbox *mbox = (CMailbox*) pHint;
	CListCtrl &list = GetListCtrl();

	if( mbox->m_bitCreated )
	{
		VERIFY( -1 != list.InsertItem( 0, _T("...") ) );
		VERIFY( LB_ERR != list.SetItemData( 0, (DWORD) mbox ) );
	}

	LV_FINDINFO findInfo = { LVFI_PARAM, 0, (LPARAM) mbox };
	int intIndex = list.FindItem( &findInfo );
	if( -1 == intIndex ) return;

	if( mbox->m_bitDeleted ||  (mbox->IsClosed() && !mbox->IsFolder()))
	{
		VERIFY( list.DeleteItem( intIndex ) );
		return;
	}

	CString strTmp;

	if( mbox->IsChanged( COLUMN_MAIL ) )
	{
		if (mbox->UpdateUnreadStatus())
			mbox->Change( COLUMN_ALIAS );	// need redraw
		mbox->GetText(COLUMN_MAIL, strTmp);
		if (!mbox->IsFolder() && mbox->InFolder())
			GetDocument()->UpdateFolder(mbox);

		VERIFY( list.SetItemText( intIndex, COLUMN_MAIL, strTmp ) );
		VERIFY( list.SetItem( intIndex, 0, LVIF_IMAGE, 0, mbox->SuggestIcon(), 0, 0, 0 ) );
	}

	if( mbox->IsChanged( COLUMN_ALIAS ) )
	{
		VERIFY( list.SetItemText( intIndex, COLUMN_ALIAS, mbox->m_strAlias ) );
	}
	
	if( mbox->IsChanged( COLUMN_USER ) )
	{
		mbox->GetText(COLUMN_USER , strTmp);
		VERIFY( list.SetItemText( intIndex, COLUMN_USER, strTmp) );
	}
	
	if( mbox->IsChanged( COLUMN_HOST ) )
	{
		VERIFY( list.SetItemText( intIndex, COLUMN_HOST, mbox->m_strHost ) );
	}

	if( mbox->IsChanged( COLUMN_STATE ) )
	{
		int intState = IDP_MBOX_ERROR;

		if( mbox->GetLastErrorDescription().GetLength() > 0 )
		{
			VERIFY( list.SetItemText( intIndex, COLUMN_STATE, mbox->GetLastErrorDescription() ) );
		}
		else
		{
			switch( mbox->m_intState )
			{
				case MBOX_NOT_CHECKED:			intState = IDP_MBOX_NOT_CHECKED; break;
				case MBOX_RESOLVING:			intState = IDP_MBOX_RESOLVING; break;
				case MBOX_CONNECTING:			intState = IDP_MBOX_CONNECTING; break;
				case MBOX_WF_USER_RESPONSE:		
				case MBOX_WF_USER_RESPONSE_SSL: intState = IDP_MBOX_WF_USER_RESPONSE; break;
				case MBOX_WF_PASS_RESPONSE:		intState = IDP_MBOX_WF_PASS_RESPONSE; break;
				case MBOX_WF_APOP_RESPONSE:		intState = IDP_MBOX_WF_APOP_RESPONSE; break;
				case MBOX_WF_FIRST_NOOP:		intState = IDP_MBOX_WF_NOOP_RESPONSE; break;
				case MBOX_WF_SECOND_NOOP:		intState = IDP_MBOX_WF_NOOP_RESPONSE; break;
				case MBOX_WF_UIDL_RESPONSE:		intState = IDP_MBOX_WF_UIDL_RESPONSE; break;
				case MBOX_WF_LIST_RESPONSE:		intState = IDP_MBOX_WF_LIST_RESPONSE; break;
				case MBOX_WF_TOPEX_RESPONSE:	intState = IDP_MBOX_WF_TOPEX_RESPONSE; break;
				case MBOX_WF_TOPF_RESPONSE:		intState = IDP_MBOX_WF_TOPEX_RESPONSE; break;
				case MBOX_WF_TOP_RESPONSE:		intState = IDP_MBOX_WF_TOP_RESPONSE; break;
				case MBOX_DOWNLOADING_DATA:		intState = IDP_MBOX_DOWNLOADING_DATA; break;
				case MBOX_WF_RETR_RESPONSE:		intState = IDP_MBOX_WF_RETR_RESPONSE; break;
				case MBOX_WF_DELE_RESPONSE:		intState = IDP_MBOX_WF_DELE_RESPONSE; break;
				case MBOX_CHECKED:				intState = IDP_MBOX_CHECKED; break;
				case MBOX_INTERRUPTED_BY_USER:	intState = IDP_MBOX_INTERRUPTED_BY_USER; break;
				case MBOX_CANNOT_CONNECT:		intState = IDP_MBOX_CANNOT_CONNECT; break;
				case MBOX_CANNOT_CHECK:			intState = IDP_MBOX_CANNOT_CHECK; break;
				case MBOX_CANNOT_SEND:			intState = IDP_MBOX_CANNOT_SEND; break;
				case MBOX_NETWORK_FAILED:		intState = IDP_MBOX_NETWORK_FAILED; break;
				case MBOX_SERVER_FAILED:		intState = IDP_MBOX_SERVER_FAILED; break;
				case MBOX_CONNECTION_WAS_LOST:	intState = IDP_MBOX_CONNECTION_WAS_LOST; break;
				case MBOX_SOCKET_ERROR:			intState = IDP_MBOX_SOCKET_ERROR; break;
				case MBOX_INVALID_USER:			intState = IDP_MBOX_INVALID_USER; break;
				case MBOX_INVALID_HOST:			intState = IDP_MBOX_INVALID_HOST; break;
				case MBOX_INVALID_PASS:			intState = IDP_MBOX_INVALID_PASS; break;
			}

			VERIFY( strTmp.LoadString( intState ) );
			StrTranslate(strTmp);
			VERIFY( list.SetItemText( intIndex, COLUMN_STATE, strTmp ) );
		}

		int item_state = list.GetItemState( intIndex, LVIS_OVERLAYMASK );
		item_state &= ~LVIS_OVERLAYMASK;

		int intOverlay = 0;

		if( STATE_ERROR( mbox->m_intState ) )
			intOverlay = 1;
		else if ( mbox->m_intPoll == 0 || mbox->IsDisabled())
			intOverlay = 2;
		else if( mbox->IsSecure() )
			intOverlay = 3;

		item_state |= INDEXTOOVERLAYMASK( intOverlay );
		VERIFY( list.SetItemState( intIndex, item_state, LVIS_OVERLAYMASK ) );
	}

	if( mbox->IsChanged( COLUMN_ELAPSED ) )
	{
		mbox->GetText(COLUMN_ELAPSED, strTmp);
		VERIFY( list.SetItemText( intIndex, COLUMN_ELAPSED, strTmp ) );
	}

	if( mbox->IsChanged( COLUMN_PORT ) )
	{
		strTmp.Format( _T("%d"), mbox->m_intPort );
		VERIFY( list.SetItemText( intIndex, COLUMN_PORT, strTmp ) );
	}

	if( mbox->IsChanged( COLUMN_POLL ) )
	{
		mbox->GetText(COLUMN_POLL, strTmp);
		
		VERIFY( list.SetItemText( intIndex, COLUMN_POLL, strTmp ) );

		int item_state = list.GetItemState( intIndex, LVIS_OVERLAYMASK );
		item_state &= ~LVIS_OVERLAYMASK;
		
		int intOverlay = 0;
		if( STATE_ERROR( mbox->m_intState ) )
			intOverlay = 1;
		if( mbox->m_intPoll == 0 || mbox->IsDisabled())
			intOverlay = 2;
		else if( mbox->IsSecure() )
			intOverlay = 3;

		item_state |= INDEXTOOVERLAYMASK( intOverlay );
		VERIFY( list.SetItemState( intIndex, item_state, LVIS_OVERLAYMASK ) );
	}

	if( mbox->IsChanged( theApp.intMVSortColumn ) ) SortItems(); 
}

UINT anMBCols[]=
{
	IDP_COLUMN_ALIAS, IDP_COLUMN_USER, IDP_COLUMN_HOST,
	IDP_COLUMN_MAIL, IDP_COLUMN_STAT , IDP_COLUMN_ELAPSED,
	IDP_COLUMN_PORT, IDP_COLUMN_POLL,
};
BOOL CMailboxView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	if( !CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext) ) return FALSE;

	VERIFY( ModifyStyle( 
		LVS_TYPEMASK, 
		theApp.intMVMode | LVS_ALIGNLEFT | LVS_ALIGNTOP | LVS_AUTOARRANGE | LVS_SHOWSELALWAYS ) );

	ASSERT( &GetListCtrl() );
	CListCtrl &list = GetListCtrl();

	list.SetExtendedStyle(LVS_EX_HEADERDRAGDROP);

	CString strColumn;
	LOAD_STRING(strColumn, IDP_COLUMN_ALIAS ); list.InsertColumn( 0, strColumn, LVCFMT_LEFT, theApp.intMCAliasWidth, 0 );
	LOAD_STRING(strColumn, IDP_COLUMN_USER ); list.InsertColumn( 1, strColumn, LVCFMT_RIGHT, theApp.intMCUserWidth, 1 );
	LOAD_STRING(strColumn, IDP_COLUMN_HOST ); list.InsertColumn( 2, strColumn, LVCFMT_LEFT, theApp.intMCHostWidth, 2 );
	LOAD_STRING(strColumn, IDP_COLUMN_MAIL ); list.InsertColumn( 3, strColumn, LVCFMT_LEFT, theApp.intMCMailWidth, 3 );
	LOAD_STRING(strColumn, IDP_COLUMN_STAT ); list.InsertColumn( 4, strColumn, LVCFMT_LEFT, theApp.intMCStatWidth, 4 );
	LOAD_STRING(strColumn, IDP_COLUMN_ELAPSED ); list.InsertColumn( 5, strColumn, LVCFMT_RIGHT, theApp.intMCElapsedWidth, 5 );
	LOAD_STRING(strColumn, IDP_COLUMN_PORT ); list.InsertColumn( 6, strColumn, LVCFMT_RIGHT, theApp.intMCPortWidth, 6 );
	LOAD_STRING(strColumn, IDP_COLUMN_POLL ); list.InsertColumn( 7, strColumn, LVCFMT_RIGHT, theApp.intMCPollWidth, 7 );

	OnOptionsChange(FALSE);

	list.SetImageList( &theApp.m_imgMailbox, LVSIL_SMALL );

	m_ColSortGUI.Init(IDB_UP, IDB_DOWN);
	m_ColSortGUI.SetSortMark(list, theApp.intMVSortColumn, theApp.intMVSortAscend);
	
	return TRUE;
}
#define ASSIGN(a, i) if (a>=0 && a<nCount) anColumns[a] = i; else bFail = TRUE;

void CMailboxView::OnOptionsChange(BOOL bWidth) 
{
	CListCtrl &list = GetListCtrl();

	int nCount = list.GetHeaderCtrl()->GetItemCount();
	int *anColumns = new int[nCount];
	ZeroMemory(anColumns, sizeof(int)*nCount);
	BOOL bFail = FALSE;
	ASSIGN(theApp.intMCAliasPos, 0)
	ASSIGN(theApp.intMCUserPos, 1)
	ASSIGN(theApp.intMCHostPos, 2)
	ASSIGN(theApp.intMCMailPos, 3)
	ASSIGN(theApp.intMCStatPos, 4)
	ASSIGN(theApp.intMCElapsedPos, 5)
	ASSIGN(theApp.intMCPortPos, 6)
	ASSIGN(theApp.intMCPollPos, 7)
	if (bFail)	// default settings
	{
		for (int i=0; i<nCount; i++)
		{
			anColumns[i] = i;
		}
	}
	list.GetHeaderCtrl()->SetOrderArray(nCount, anColumns);
	delete[] anColumns;

	if (bWidth)
	{
		list.SetColumnWidth(0, theApp.intMCAliasWidth);
		list.SetColumnWidth(1, theApp.intMCUserWidth);
		list.SetColumnWidth(2, theApp.intMCHostWidth);
		list.SetColumnWidth(3, theApp.intMCMailWidth);
		list.SetColumnWidth(4, theApp.intMCStatWidth);
		list.SetColumnWidth(5, theApp.intMCElapsedWidth);
		list.SetColumnWidth(6, theApp.intMCPortWidth);
		list.SetColumnWidth(7, theApp.intMCPollWidth);
	}
}


void CMailboxView::OnDestroy() 
{
	if( LVS_REPORT == theApp.intMVMode )
	{
		GetListSettings();
	}
	CListViewEx::OnDestroy();
}

void CMailboxView::OnViewList() 
{
	if (theApp.intMVMode == LVS_REPORT)
	{
		GetListSettings();
	}
	ModifyStyle( LVS_TYPEMASK, LVS_LIST );
	theApp.intMVMode = LVS_LIST;
}

void CMailboxView::OnViewReport() 
{
	ModifyStyle( LVS_TYPEMASK, LVS_REPORT );
	theApp.intMVMode = LVS_REPORT;
}

void CMailboxView::OnViewSmallicon() 
{
	if (theApp.intMVMode == LVS_REPORT)
	{
		GetListSettings();
	}
	ModifyStyle( LVS_TYPEMASK, LVS_SMALLICON );
	theApp.intMVMode = LVS_SMALLICON;
}

void CMailboxView::OnUpdateViewIcon(CCmdUI* pCmdUI) 
{
	pCmdUI->SetRadio( LVS_ICON == theApp.intMVMode );
}

void CMailboxView::OnUpdateViewList(CCmdUI* pCmdUI) 
{
	pCmdUI->SetRadio( LVS_LIST == theApp.intMVMode );
}

void CMailboxView::OnUpdateViewReport(CCmdUI* pCmdUI) 
{
	pCmdUI->SetRadio( LVS_REPORT == theApp.intMVMode );
}

void CMailboxView::OnUpdateViewSmallicon(CCmdUI* pCmdUI) 
{
	pCmdUI->SetRadio( LVS_SMALLICON == theApp.intMVMode );
}

void CMailboxView::OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	if( theApp.intMVSortColumn == pNMListView->iSubItem )
	{
		theApp.intMVSortAscend = !theApp.intMVSortAscend;
	}
	theApp.intMVSortColumn = pNMListView->iSubItem; 
	SortItems();
	
	m_ColSortGUI.SetSortMark(GetListCtrl(), theApp.intMVSortColumn, theApp.intMVSortAscend);

	*pResult = 0;
}

int CALLBACK CMailboxView::CompareSubItems(LPARAM lp0, LPARAM lp1, LPARAM doc)
{
	ASSERT( lp0 && lp1 );
	CMagicDoc* pDoc = (CMagicDoc*)doc;
	if (!pDoc)
		return 0;
	CMailbox &mbox0 = *(CMailbox*) lp0;
	CMailbox &mbox1 = *(CMailbox*) lp1;
	if (mbox0.InFolder() != mbox1.InFolder())
		return mbox0.InFolder() ? -1 : 1;
	// if both in folders, find roots
	if (mbox0.InFolder())
	{
		int nRoot1 = pDoc->FindRoot((CMailbox*) lp0);
		int nRoot2 = pDoc->FindRoot((CMailbox*) lp1);
		// in different folders - by folder
		if (nRoot1!=nRoot2)
		{
			CMailbox* pFolder1 = pDoc->GetBox(nRoot1);
			CMailbox* pFolder2 = pDoc->GetBox(nRoot2);
			if (!pFolder2 || !pFolder1)
				return 0;
			return pFolder1->m_strAlias.CompareNoCase(pFolder2->m_strAlias);
		}
		// same folder
		if (mbox0.IsFolder())
			return -1;
		if (mbox1.IsFolder())
			return 1;
		// just boxes in same folder
	}

	int intRes = -1;
	switch( theApp.intMVSortColumn )
	{
		case COLUMN_ALIAS:	
			intRes = mbox0.m_strAlias.CompareNoCase( mbox1.m_strAlias );
			break;

		case COLUMN_USER:	
			intRes = mbox0.m_strUser.CompareNoCase( mbox1.m_strUser );
			break;

		case COLUMN_HOST:	
			intRes = mbox0.m_strHost.CompareNoCase( mbox1.m_strHost );
			break;

		case COLUMN_MAIL:	
			if( mbox0.m_arrayExcerpt.GetSize() == mbox1.m_arrayExcerpt.GetSize() ) intRes = 0;
			else if( mbox0.m_arrayExcerpt.GetSize() < mbox1.m_arrayExcerpt.GetSize() ) intRes = -1;
			else intRes = 1;
			break;

		case COLUMN_STATE:	
			if( mbox0.m_intState == mbox1.m_intState ) intRes = 0;
			else if( mbox0.m_intState < mbox1.m_intState ) intRes = -1;
			else intRes = 1;
			break;

		case COLUMN_ELAPSED:	
			if( mbox0.m_intElapsed == mbox1.m_intElapsed ) intRes = 0;
			else if( mbox0.m_intElapsed < mbox1.m_intElapsed ) intRes = -1;			
			else intRes = 1;
			break;

		case COLUMN_PORT:	
			if( mbox0.m_intPort == mbox1.m_intPort ) intRes = 0;
			else if( mbox0.m_intPort < mbox1.m_intPort ) intRes = -1;
			else intRes = 1;
			break;

		case COLUMN_POLL:	
			if( mbox0.m_intPoll == mbox1.m_intPoll ) intRes = 0;
			else if( mbox0.m_intPoll < mbox1.m_intPoll ) intRes = -1;
			else intRes = 1;
			break;
	}

	return theApp.intMVSortAscend ? intRes : -intRes;
}  

void CMailboxView::OnRclick(NMHDR* , LRESULT* ) 
{
	POINT pt = {0};
	VERIFY( GetCursorPos( &pt ) );

	ShowContextMenu( pt );
}

void CMailboxView::OnDblclk(NMHDR* , LRESULT* pResult) 
{
	if (GetListCtrl().GetSelectedCount()==1)
	{
		int nPos = GetListCtrl().GetNextItem( -1, LVNI_SELECTED );
		CMailbox* mbox = (CMailbox*) GetListCtrl().GetItemData( nPos );
		if(mbox && mbox->IsFolder())
		{
			GetDocument()->ToggleFolder(mbox);
			SortItems();
			return;
		}
	}

	if (theApp.m_dwFlags & MMF_CHECK_DBL)
		PostMessage( WM_COMMAND, ID_VIEW_REFRESH );
	else
		SendMessage( WM_COMMAND, ID_COMMAND );
	*pResult = 0;
}


void CMailboxView::OnEditSelectAll() 
{
	CListCtrl &list = GetListCtrl();
	for( int i = list.GetItemCount(); i; --i ) list.SetItemState( i-1, LVIS_SELECTED, LVIS_SELECTED );
}

void CMailboxView::OnUpdateEditSelectAll(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable( GetListCtrl().GetItemCount() );
}

void CMailboxView::OnItemChanged(NMHDR* pNMHDR, LRESULT* pResult ) 
{
	ASSERT( GetDocument() );

	NM_LISTVIEW *pnmv = (NM_LISTVIEW*) pNMHDR; 
	if( ( pnmv->uOldState ^ pnmv->uNewState ) & LVIS_SELECTED )
	{
		CListCtrl &list = GetListCtrl();

		switch( list.GetSelectedCount() )
		{
			case 0:
			{
				for( int i = list.GetItemCount(); i; --i )
				{
					CMailbox &mbox = *(CMailbox*) list.GetItemData( i-1 );
					mbox.SetSelection(1);
				}
			}
			break;

			case 1:
			{
				for( int i = list.GetItemCount(); i; --i )
				{
					CMailbox &mbox = *(CMailbox*) list.GetItemData( i-1 );
					mbox.SetSelection(0);
				}
			}

			default:
			{
				CMailbox &mbox = *(CMailbox*) GetListCtrl().GetItemData( pnmv->iItem );
				mbox.SetSelection(!!( LVIS_SELECTED & pnmv->uNewState ));
			}
		}

	}

	*pResult = 0;
}

void CMailboxView::OnViewRefresh() 
{
	CMagicDoc *doc = (CMagicDoc*) GetDocument();
	BOOL bForce = (GetListCtrl().GetSelectedCount()==1);
	if( doc ) 
		doc->Check(bForce);
}

void CMailboxView::OnEditDelete() 
{
	if ( IDYES != AfxMessageBox( IDS_MBOX_DELETE_CNF, MB_YESNO | MB_ICONSTOP | MB_DEFBUTTON2 ) ) 
		return;
	
	CListCtrl &list = GetListCtrl();
	CArray<CMailbox*> aFolders;
	CArray< CMailbox* > arrayMailbox;

	for( int i = list.GetItemCount(); i; --i )
	{
		if( LVNI_SELECTED == list.GetItemState( i-1, LVNI_SELECTED ) )
		{
			CMailbox* pM = (CMailbox*) list.GetItemData(i-1) ;
			if (!pM)
				continue;
			if (pM->IsFolder())
				aFolders.Add(pM);
			else
				arrayMailbox.Add(pM);
		}
	}
	for (int i=0; i<arrayMailbox.GetSize(); i++)
	{
		GetDocument()->DeleteMailbox( arrayMailbox[i] );
	}
	if (arrayMailbox.GetSize())
		return;
	for (int i=0; i<aFolders.GetSize(); i++)
	{
		GetDocument()->DeleteMailbox( aFolders[i] );
	}
}

void CMailboxView::OnMarkRead()
{
	CArray<CMailbox*> ar;
	if (!FindSelectedMailboxes(ar))
		return;
	for (int i=0; i<ar.GetSize(); i++)
	{
		ar[i]->MarkRead(TRUE);
	}
}
void CMailboxView::OnMarkUnread() 
{
	CArray<CMailbox*> ar;
	if (!FindSelectedMailboxes(ar))
		return;
	for (int i=0; i<ar.GetSize(); i++)
	{
		ar[i]->MarkRead(FALSE);
	}
}
void CMailboxView::OnRename() 
{
	CListCtrl &list = GetListCtrl();
	CArray<CMailbox*> aMailboxes;

	int i = -1;
	CString sStart;
	while( -1 != ( i = list.GetNextItem( i, LVNI_SELECTED ) ) )
	{
		CMailbox* pM = (CMailbox*) list.GetItemData(i) ;
		if (!pM)
			continue;
		aMailboxes.Add(pM);
		if (sStart.IsEmpty())
			sStart = pM->m_strAlias;
	}
	if (DoAskText(_T("MMM"), _T("${Name}:"), sStart))
	{
		for (int i=0; i<aMailboxes.GetSize(); i++)
		{
			aMailboxes[i]->m_strAlias = sStart;
			aMailboxes[i]->Change(COLUMN_ALIAS);
			GetDocument()->UpdateItem(aMailboxes[i]);
		}
		GetDocument()->SetModifiedFlag();
	}
}
int CMailboxView::FindSelectedMailboxes(CArray<CMailbox*>& ar) 
{
	CListCtrl &list = GetListCtrl();
	int i = -1;
	while( -1 != ( i = list.GetNextItem( i, LVNI_SELECTED ) ) )
	{
		CMailbox* pM = (CMailbox*) list.GetItemData(i) ;
		if (!pM)
			continue;
		if (pM->IsFolder())
		{
			CMailboxFolder* pFolder = (CMailboxFolder*)pM;
			for (int m=0; m<pFolder->m_aBoxes.GetSize(); m++)
			{
				BOOL bFound = FALSE;
				for (int f=0; f<ar.GetSize(); f++)
				{
					if (ar[f]==pFolder->m_aBoxes[m])
					{
						bFound = true;
						break;
					}
				}
				if (!bFound)
					ar.Add(pFolder->m_aBoxes[m]);
			}
		}
		else
			ar.Add(pM);
	}
	return ar.GetSize();
}
void CMailboxView::OnEditProperties() 
{
	CArray<CMailbox* > arrayMailbox;
	FindSelectedMailboxes(arrayMailbox);
	if (arrayMailbox.GetSize())
		GetDocument()->MailboxProperties( arrayMailbox );
}

void CMailboxView::OnEditNew() 
{
	GetDocument()->NewMailbox();
}

void CMailboxView::OnCommand() 
{
	CListCtrl &list = GetListCtrl();

	for( int i = list.GetItemCount(); i; --i )
	{
		if( list.GetItemState( i-1, LVIS_FOCUSED ) )
		{
			theApp.m_pMainWnd->SendMessage( VM_START_COMMAND, 0, LPARAM( list.GetItemData( i-1 ) ) );
			break;
		}
	}
}

void CMailboxView::OnUpdateIfSelectedCount(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable( GetListCtrl().GetSelectedCount() );
}


void CMailboxView::OnUpdateIfItemCount(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable( GetListCtrl().GetItemCount() );
}

void CMailboxView::OnUpdateIfFocusedCount(CCmdUI* pCmdUI) 
{
	CListCtrl &list = GetListCtrl();

	for( int i = list.GetItemCount(); i; --i )
	{
		if( list.GetItemState( i-1, LVIS_FOCUSED ) ) 
		{
			pCmdUI->Enable( TRUE );
			return;
		}
	}
	pCmdUI->Enable( FALSE );
}

void CMailboxView::OnUpdateIndicatorObjects( CCmdUI *pCmdUI )
{
	CListCtrl &list = GetListCtrl();
    pCmdUI->Enable(); 

	UINT uintSelected = list.GetSelectedCount();
	UINT uintTotal = list.GetItemCount();
	UINT uintReported = uintSelected ? uintSelected : uintTotal;

	CString strTmp1, strTmp2; 
	strTmp1.LoadString( uintReported-1 ? IDP_MAILBOXES : IDP_MAILBOX );
	StrTranslate(strTmp1);
	if( uintSelected ) strTmp2.LoadString( IDP_SELECTED );
	StrTranslate(strTmp2);

    CString strObjects;
    strObjects.Format( _T("%d %s %s"), uintReported, strTmp1, strTmp2); 
    pCmdUI->SetText( strObjects ); 
}

void CMailboxView::OnUpdateIndicatorContents( CCmdUI *pCmdUI )
{
	CListCtrl &list = GetListCtrl();
    pCmdUI->Enable();
	UINT uintCount = 0;
	UINT uSize = 0;

	BOOL bNoneSelected = !list.GetSelectedCount();

	for( int i = list.GetItemCount(); i; --i )
	{
		if( bNoneSelected || list.GetItemState( i-1, LVIS_SELECTED ) ) 
		{
			CMailbox* pM = (CMailbox*) list.GetItemData( i-1 );
			if (!pM)
				continue;
			uintCount += pM->m_arrayExcerpt.GetSize();
			for (int e=0; e<pM->m_arrayExcerpt.GetSize(); e++)
			{
				if (pM->m_arrayExcerpt[e])
					uSize += pM->m_arrayExcerpt[e]->m_intSize;
			}
		}
	}
	
	CString strContents, strTmp;
	strTmp.LoadString( uintCount-1 ? IDP_MESSAGES : IDP_MESSAGE );
	StrTranslate(strTmp);
	CString strSize;
	BytesToString( uSize, strSize);
	strContents.Format( _T("%d %s - %s"), uintCount, strTmp, strSize );
    pCmdUI->SetText( strContents ); 
}

void CMailboxView::OnStopChecking() 
{
	CMagicDoc *doc = (CMagicDoc*) GetDocument();
	if( doc ) doc->StopChecking();
}

void CMailboxView::OnKeydown(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_KEYDOWN* pLVKeyDown = (LV_KEYDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here

	*pResult = 0;

	if( pLVKeyDown->wVKey == VK_APPS )
	{
		CListCtrl &list = GetListCtrl();
		
		int iFocusedItem = list.GetNextItem( -1, LVNI_FOCUSED );

		POINT pt = {0};
		if( iFocusedItem != -1 )
		{
			RECT rc = {0};
			list.GetItemRect( iFocusedItem, &rc, LVIR_BOUNDS );

			pt.x = rc.left;
			pt.y = rc.bottom;
		}

		ClientToScreen( &pt );
		ShowContextMenu( pt );
	}
	else if( pLVKeyDown->wVKey == VK_TAB )
	{
		CMagicFrame *frame = (CMagicFrame*) GetParentFrame();

		if( frame )
		{
			frame->SwitchPane();
		}
	}
}

void CMailboxView::ShowContextMenu( const POINT& pt ) 
{
	CMenu menuPopup;	
	VERIFY( menuPopup.LoadMenu( IDR_MAILBOX_POPUP_MENU ) );

	MENUITEMINFO info;
	info.cbSize = sizeof( info );
	info.fMask = MIIM_STATE;
	info.fState = MFS_DEFAULT;
	
	UINT nDef = ID_COMMAND;
	if (theApp.m_dwFlags & MMF_CHECK_DBL)
		nDef = ID_VIEW_REFRESH;
	VERIFY( SetMenuItemInfo( menuPopup.m_hMenu, nDef, FALSE, &info ) );

	MenuTranslate(menuPopup.GetSubMenu(0)->m_hMenu);
	menuPopup.GetSubMenu(0)->TrackPopupMenu
	( 
		TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON, 
		pt.x, pt.y, this/*AfxGetMainWnd()*/, NULL 
	);
}

LRESULT CMailboxView::OnLanguage(WPARAM, LPARAM)
{
	CString strColumn;
	
	int nCols = sizeof(anMBCols)/sizeof(anMBCols[0]);
	for (int i=0; i<nCols; i++)
	{
		LOAD_STRING(strColumn, anMBCols[i]);
		SetColTitle(i, strColumn);
	}
	return 0;
}

void CMailboxView::OnFilters() 
{
	DFilters dlg((CMagicDoc*)GetDocument());
	dlg.DoModal();
}

void CMailboxView::OnHeaderEndDrag(NMHDR* /*pHeader*/, LRESULT* pResult)
{
	RedrawWindow(NULL, NULL, RDW_INTERNALPAINT | RDW_INVALIDATE);
	*pResult = 0;
}

void CMailboxView::GetListSettings(void)
{
	CListCtrl &list = GetListCtrl();
	LVCOLUMN lvc;
	lvc.mask=LVCF_WIDTH | LVCF_ORDER;
	list.GetColumn(COLUMN_ALIAS, &lvc);
	theApp.intMCAliasWidth = lvc.cx;
	theApp.intMCAliasPos = lvc.iOrder;
	list.GetColumn(COLUMN_USER, &lvc);
	theApp.intMCUserWidth = lvc.cx;
	theApp.intMCUserPos = lvc.iOrder;
	list.GetColumn(COLUMN_HOST, &lvc);
	theApp.intMCHostWidth = lvc.cx;
	theApp.intMCHostPos = lvc.iOrder;
	list.GetColumn(COLUMN_MAIL, &lvc);
	theApp.intMCMailWidth = lvc.cx;
	theApp.intMCMailPos = lvc.iOrder;
	list.GetColumn(COLUMN_STATE, &lvc);
	theApp.intMCStatWidth = lvc.cx;
	theApp.intMCStatPos = lvc.iOrder;
	list.GetColumn(COLUMN_ELAPSED, &lvc);
	theApp.intMCElapsedWidth = lvc.cx;
	theApp.intMCElapsedPos = lvc.iOrder;
	list.GetColumn(COLUMN_PORT, &lvc);
	theApp.intMCPortWidth = lvc.cx;
	theApp.intMCPortPos = lvc.iOrder;
	list.GetColumn(COLUMN_POLL, &lvc);
	theApp.intMCPollWidth = lvc.cx;
	theApp.intMCPollPos = lvc.iOrder;
}
void CMailboxView::GetCustomRect(int nItem,CRect& rcItem, int LVIR_x)
{
	CListViewEx::GetCustomRect(nItem, rcItem, LVIR_x);
	CMailbox* mbox = (CMailbox*) GetListCtrl().GetItemData( nItem );
	if (LVIR_x == LVIR_LABEL || LVIR_x == LVIR_ICON)
	{
		if (mbox->InFolder() && !mbox->IsFolder())
			rcItem.left += 20;
	}
}

void CMailboxView::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (!lpDrawItemStruct)
		return;
	int nItem = lpDrawItemStruct->itemID;
	CDC* pDC=CDC::FromHandle(lpDrawItemStruct->hDC);
	CMailbox &mbox = *(CMailbox*) GetListCtrl().GetItemData( nItem );
	int nNew = (mbox.m_nUnread > 0);

	// select bold font for unread messages
	if (!m_fntBold.m_hObject)
	{
		CFont* pF = GetFont();
		if (pF)
		{
			pF->GetObject(sizeof(LOGFONT), &m_lfBold);
			m_lfBold.lfWeight = 800;
			m_fntBold.CreateFontIndirect(&m_lfBold);
		}
	}
	CFont* pOldF = NULL;
	if (m_fntBold.m_hObject && nNew)
	{
		pOldF = pDC->SelectObject(&m_fntBold);
	}

	CListViewEx::DrawItem(lpDrawItemStruct);

	if (pOldF)
		pDC->SelectObject(pOldF);
}

CMailBoxClip MyClip;

void CMailboxView::OnMailboxCopy() 
{
	CListCtrl &list = GetListCtrl();
	if (!list.GetSelectedCount())
		return;

	CArray<CMailbox*, CMailbox*> aSel;
	for( int i = list.GetItemCount(); i; --i )
	{
		if( list.GetItemState( i-1, LVIS_SELECTED ) ) 
		{
			CMailbox* pM = (CMailbox*) list.GetItemData( i-1 );
			if (!pM)
				continue;
			aSel.Add(pM);
		}
	}
	MyClip.SaveData(aSel.GetData(), aSel.GetSize());
}

void CMailboxView::OnUpdateMailboxCopy(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(GetListCtrl().GetSelectedCount()>0);	
}


void CMailboxView::OnMailboxPaste() 
{
	int nCount=0;
	CMailbox** pData = MyClip.GetData(nCount);
	if (!pData)
		return;
	for (int i=0; i<nCount; i++, pData++)
		GetDocument()->CopyMailBox(*pData);
	ASSERT(AfxCheckMemory());
}

void CMailboxView::OnUpdateMailboxPaste(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(MyClip.IsPossiblePaste());
}
void CMailboxView::OnInitMenuPopup(CMenu* pPopupMenu,UINT nIndex,BOOL bSysMenu )
{
	CListViewEx::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
	if (!pPopupMenu)
		return;
	if (pPopupMenu->GetMenuItemID(0)!=ID_FOLDER_LIST)
		return;
	CMagicDoc &doc = *(CMagicDoc*) GetDocument();
	pPopupMenu->RemoveMenu(0, MF_BYPOSITION);
	CStringArray as;
	int nCount = doc.GetFoldersList(as);
	if (!nCount)
		pPopupMenu->AppendMenu(MF_STRING|MF_GRAYED|MF_DISABLED, ID_FOLDER_LIST, _T("none"));
	else
	{
		nCount = min(nCount, ID_FOLDER_LIST2-ID_FOLDER_LIST-1);
		for (int i=0; i<nCount; i++)
			pPopupMenu->AppendMenu(MF_STRING, ID_FOLDER_LIST+i, as[as.GetSize()-1-i]);
		if (nCount<as.GetSize())
			pPopupMenu->AppendMenu(MF_STRING, ID_FOLDER_LIST2, _T("${Select}..."));
		pPopupMenu->AppendMenu(MF_SEPARATOR);
		pPopupMenu->AppendMenu(MF_STRING, ID_FOLDER_REMOVE, _T("${none}"));
	}
	MenuTranslate(pPopupMenu->m_hMenu);
}
void CMailboxView::OnRemoveFromFolder()
{
	CMagicDoc &doc = *(CMagicDoc*) GetDocument();
	CListCtrl &list = GetListCtrl();
	int i = -1;
	while( -1 != ( i = list.GetNextItem( i, LVNI_SELECTED ) ) )
	{
		CMailbox* pBox =  (CMailbox*) list.GetItemData(i) ;
		if (pBox->IsFolder() || !pBox->InFolder())
			continue;
		int nOldPos = doc.FindBox(pBox);
		if (nOldPos<0)
			return;
		doc.m_aMailboxes.RemoveAt(nOldPos);
		doc.m_aMailboxes.InsertAt(0, pBox);
		pBox->MakeInFolder(FALSE);
	}
	doc.UpdateFolderStats();
	SortItems();
	GetDocument()->SetModifiedFlag();
}
int CMailboxView::SelectFolder()
{
	CStringArray as;
	CMagicDoc &doc = *(CMagicDoc*) GetDocument();
	doc.GetFoldersList(as);
	int nRet = 0;
	if (!DoAskList(_T("MMM"), _T("${Folder}:"), nRet, as))
		return -1;
	return nRet;

}
void CMailboxView::OnSelectFolder(UINT nID)
{
	CMagicDoc &doc = *(CMagicDoc*) GetDocument();
	int nFolderIndex = -1;
	if (nID==ID_FOLDER_LIST2)	// make selection
	{
		nFolderIndex = SelectFolder();
	}
	else
	{
		CStringArray as;
		doc.GetFoldersList(as);
		nFolderIndex = nID-ID_FOLDER_LIST;
		nFolderIndex = as.GetSize()-1-nFolderIndex;
	}
	if (nFolderIndex<0)
		return;

	int nFolder = doc.FindFolder(nFolderIndex);
	if (nFolder<0)
		return;
	CMailbox* pFolder = doc.m_aMailboxes[nFolder];
	CListCtrl &list = GetListCtrl();
	int i = -1;
	CArray<CMailbox*> aHandled;
	while( -1 != ( i = list.GetNextItem( i, LVNI_SELECTED ) ) )
	{
		CMailbox* pBox =  (CMailbox*) list.GetItemData(i) ;
		if (pBox->IsFolder())
			continue;
		int nOldPos = doc.FindBox(pBox);
		if (nOldPos<0)
			return;
		doc.m_aMailboxes.RemoveAt(nOldPos);
		if (nOldPos<nFolder)
			nFolder--;
		doc.m_aMailboxes.InsertAt(nFolder+1, pBox);
		pBox->MakeInFolder(TRUE);
		if (pFolder->IsClosed())
			pBox->CloseFolder();
		aHandled.Add(pBox);
	}
	for (int i=0; i<aHandled.GetSize(); i++)
		doc.UpdateItem(aHandled[i]);

	doc.UpdateFolderStats();
	doc.UpdateItem(pFolder);
	SortItems();
	GetDocument()->SetModifiedFlag();
}
