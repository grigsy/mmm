// AdvOptionsPage.cpp : implementation file
//

#include "stdafx.h"
#include "magic.h"
#include "AdvOptionsPage.h"
#include "folderdialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAdvOptionsPage property page

IMPLEMENT_DYNCREATE(CAdvOptionsPage, CPropertyPage)

CAdvOptionsPage::CAdvOptionsPage() : CPropertyPage(CAdvOptionsPage::IDD)
{
	//{{AFX_DATA_INIT(CAdvOptionsPage)
	m_bLog = (theApp.m_dwFlags & MMF_LOG_DELETED)!=0;
	m_nLogSize = theApp.m_nMaxLogSize;
	m_bColor = (theApp.m_dwFlags & MMF_COLOR_FRIEND)!=0;
	m_bSingle = (theApp.m_dwFlags & MMF_SINGLE)!=0;
	m_bAllDel = (theApp.m_dwFlags & MMF_LOG_FILTER)==0;
	m_bDbl = (theApp.m_dwFlags & MMF_CHECK_DBL)!=0;
	m_bSmartSort = (theApp.m_dwFlags & MMF_SMART_SORT)!=0;
	m_bProtectFriend = (theApp.m_dwFlags & MMF_PROTECT)!=0;
	m_bMassiveMode= (theApp.m_dwFlags & MMF_MASSIVE)!=0;
	m_nMassiveBoxes = theApp.m_nMassiveBoxes;
	m_nMassivePeriod = theApp.m_nMassivePeriod;
	//}}AFX_DATA_INIT
	m_clrFriend = theApp.m_clrFriends;
}

CAdvOptionsPage::~CAdvOptionsPage()
{
}

void CAdvOptionsPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAdvOptionsPage)
	DDX_Control(pDX, IDC_CLR_SAMPLE, m_Sample);
	DDX_Check(pDX, IDC_LOG_DEL, m_bLog);
	DDX_Text(pDX, IDC_MAX_LOG, m_nLogSize);
	DDX_Check(pDX, IDC_FRIEND_COLOR, m_bColor);
	DDX_Check(pDX, IDC_SINGLE, m_bSingle);
	DDX_Check(pDX, IDC_ALL_DEL, m_bAllDel);
	DDX_Check(pDX, IDC_DBL_CHECK, m_bDbl);
	DDX_Check(pDX, IDC_SORT_SMART, m_bSmartSort);
	DDX_Check(pDX, IDC_CHECK1, m_bProtectFriend);
	DDX_Check(pDX, IDC_OPT_MASSIVE, m_bMassiveMode);
	DDX_Text(pDX, IDC_MASSIVE_BOXES, m_nMassiveBoxes);
	DDX_Text(pDX, IDC_MASSIVE_POLL, m_nMassivePeriod);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAdvOptionsPage, CPropertyPage)
	//{{AFX_MSG_MAP(CAdvOptionsPage)
	ON_BN_CLICKED(IDC_CLR_BRS, OnClrBrs)
	ON_BN_CLICKED(IDC_FRIEND_COLOR, OnFriendColor)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAdvOptionsPage message handlers

BOOL CAdvOptionsPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	DlgTranslate(this);
	
	m_Sample.SetColor(!m_bColor, m_clrFriend);
	
	return TRUE;
}

BOOL CAdvOptionsPage::OnApply() 
{
	if (m_bLog)
		theApp.m_dwFlags |= MMF_LOG_DELETED;
	else
		theApp.m_dwFlags &= ~MMF_LOG_DELETED;
	if (m_bAllDel)
		theApp.m_dwFlags &= ~MMF_LOG_FILTER;
	else
		theApp.m_dwFlags |= MMF_LOG_FILTER;

	theApp.m_nMaxLogSize = m_nLogSize;
	theApp.m_nMassiveBoxes= m_nMassiveBoxes;
	theApp.m_nMassivePeriod= m_nMassivePeriod;
	if (m_bMassiveMode)
		theApp.m_dwFlags |= MMF_MASSIVE;
	else
		theApp.m_dwFlags &= ~MMF_MASSIVE;
	if (m_bColor)
		theApp.m_dwFlags |= MMF_COLOR_FRIEND;
	else
		theApp.m_dwFlags &= ~MMF_COLOR_FRIEND;
	if (m_bProtectFriend)
		theApp.m_dwFlags |= MMF_PROTECT;
	else
		theApp.m_dwFlags &= ~MMF_PROTECT;
	if (m_bSingle)
		theApp.m_dwFlags |= MMF_SINGLE;
	else
		theApp.m_dwFlags &= ~MMF_SINGLE;
	if (m_bSmartSort)
		theApp.m_dwFlags |= MMF_SMART_SORT;
	else
		theApp.m_dwFlags &= ~MMF_SMART_SORT;
	if (m_bDbl)
		theApp.m_dwFlags |= MMF_CHECK_DBL;
	else
		theApp.m_dwFlags &= ~MMF_CHECK_DBL;
	theApp.m_clrFriends = m_clrFriend;
	
	return TRUE;
}

void CAdvOptionsPage::OnClrBrs() 
{
	UpdateData();
	CColorDialog dlg(m_clrFriend);
	if (dlg.DoModal()!=IDOK)
		return;
	m_clrFriend = dlg.GetColor();
	m_Sample.SetColor(!m_bColor, m_clrFriend);
}

void CAdvOptionsPage::OnFriendColor() 
{
	UpdateData();
	m_Sample.SetColor(!m_bColor, m_clrFriend);
}


/************************************************************************/
/*                      CLocalOptionsPage                               */
/************************************************************************/

IMPLEMENT_DYNCREATE(CLocalOptionsPage, CPropertyPage)

	CLocalOptionsPage::CLocalOptionsPage() : CPropertyPage(CLocalOptionsPage::IDD)
{
	//{{AFX_DATA_INIT(CLocalOptionsPage)
	m_bFont = (theApp.m_dwFlags & MMF_FONT)!=0;;
	//}}AFX_DATA_INIT
	m_strTemp = theApp.m_strTemp;

	LOGFONT* pLF = &theApp.m_lfMain;
	if (!m_bFont)
		pLF = &theApp.m_lfDef;

	memcpy(&m_lfMain, pLF, sizeof(LOGFONT));
	m_fntMain.CreateFontIndirect(&m_lfMain);
}

CLocalOptionsPage::~CLocalOptionsPage()
{
}

void CLocalOptionsPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLocalOptionsPage)
	DDX_Check(pDX, IDC_FONT, m_bFont);
	DDX_Text(pDX, IDC_TEMP, m_strTemp);
	DDX_Control(pDX, IDC_ENC, m_cbEnc);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLocalOptionsPage, CPropertyPage)
	//{{AFX_MSG_MAP(CLocalOptionsPage)
	ON_BN_CLICKED(IDC_FONT_BRS, OnFontBrs)
	ON_BN_CLICKED(IDC_FONT, UpdateButtons)
	ON_BN_CLICKED(IDC_BRS_FOLDER, OnBrsFolder)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLocalOptionsPage message handlers
struct EncodingData
{
	LPCTSTR sEncoding;
	int     nEncoding;
};
EncodingData aEnc[]=
{
	{_T("${Default}"), 0},
	{_T("${By font}"), -1},
	{_T("${Central European}"), 1250},
	{_T("${Cyrillic}"), 1251},
	{_T("${Western European}"), 1252},
	{_T("${Baltic}"), 1257},
	{_T("${Greek}"), 1253},
	{_T("${Turkish}"), 1254},
	{_T("${Hebrew}"), 1255},
	{_T("${Arabic}"), 1256},
	{_T("${Vietnamese}"), 1258},
	{_T("${Thai}"), 874},
	{_T("${Japanese}"), 932},
	{_T("${Chinese (simplified)}"), 936},
	{_T("${Chinese (traditional)}"), 958},
	{_T("${Korean}"), 949},
};

BOOL CLocalOptionsPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	DlgTranslate(this);

	int nCur = -1;
	m_cbEnc.ResetContent();
	for (int i=0; i<_countof(aEnc); i++)
	{
		CString sText;
		if (aEnc[i].nEncoding>0)
			sText.Format(_T("%s (%d)"), aEnc[i].sEncoding, aEnc[i].nEncoding);
		else
			sText = aEnc[i].sEncoding;
		StrTranslate(sText);
		int idx = m_cbEnc.AddString(sText);
		m_cbEnc.SetItemData(idx, aEnc[i].nEncoding);
		if (aEnc[i].nEncoding == theApp.m_nEncoding)
			nCur = idx;
	}
	m_cbEnc.SetCurSel(nCur);

	UpdateButtons();

	return TRUE;
}

BOOL CLocalOptionsPage::OnApply() 
{
	theApp.m_strTemp = m_strTemp;
	if (m_bFont)
		theApp.m_dwFlags |= MMF_FONT;
	else
		theApp.m_dwFlags &= ~MMF_FONT;
	theApp.SetFont(m_lfMain);
	theApp.m_nEncoding = m_cbEnc.GetItemData(m_cbEnc.GetCurSel());

	return TRUE;
}

void CLocalOptionsPage::OnFontBrs() 
{
	if (!DoChangeFont(m_fntMain, &m_lfMain))
		return;
	UpdateButtons();	
}

void CLocalOptionsPage::UpdateButtons()
{
	UpdateData();
	GetDlgItem(IDC_FONT_BRS)->SetFont(&m_fntMain);
	GetDlgItem(IDC_FONT_BRS)->EnableWindow(m_bFont);

	CString str;
	GetFontName(m_lfMain, str);
	SetDlgItemText(IDC_FONT_BRS, str);

}

void CLocalOptionsPage::OnBrsFolder() 
{
	UpdateData();
	CString s(m_strTemp);
	if (m_strTemp.Find('%')>=0)
	{
		DWORD dwLen = ExpandEnvironmentStrings(m_strTemp, s.GetBuffer(MAX_PATH*4+1), 
			MAX_PATH*4);
		s.ReleaseBuffer();
		if (dwLen>MAX_PATH*4)
			s = m_strTemp;
	}
	CFolderDialog dlg(s);
	if (dlg.DoModal()!=IDOK)
		return;
	CString sNew = dlg.GetPathName();
	CString sShort;
	GetShortPathName(sNew, sShort.GetBuffer(MAX_PATH*2+1), MAX_PATH*2);
	sShort.ReleaseBuffer();
	if (sNew.CompareNoCase(s) &&  sShort.CompareNoCase(s))
		m_strTemp = sNew;
	UpdateData(FALSE);
}
