// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// MagicFrame.cpp : implementation of the CMagicFrame class
//
/////////////////////////////////////////////////////////////////////
//
//   File : MagicFrame.cpp
//   Description : Main frame
//
//   Modification history ( date, name, description ) : 
//		1.	17.12.2002	Igor Green, mmm3@grigsoft.com
//			Fixed toolbar problem
//
//HISTORY_ENTRY_END:1!17.12.2002
/////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Magic.h"
#include "Mailbox.h"
#include "Excerpt.h"
#include "MagicDoc.h"
#include "MailboxView.h"
#include "ExcerptView.h"
#include "OptionsPage.h"
#include "CommandPage.h"
#include "PlaybackPage.h"
#include "MagicFrame.h"
#include "resrc1.h"
#include "getwinver.h"
#include "tools.h"
#include "advoptionspage.h"
#include "enbitmap.h"
#include "actppage.h"
#include "cregexp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMagicFrame

IMPLEMENT_DYNCREATE(CMagicFrame, CFrameWnd)

const UINT WM_TASKBARCREATED = 
    ::RegisterWindowMessage(_T("TaskbarCreated"));

BEGIN_MESSAGE_MAP(CMagicFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMagicFrame)
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_COMMAND(ID_RESTORE, OnRestore)
	ON_UPDATE_COMMAND_UI(ID_RESTORE, OnUpdateRestore)
	ON_COMMAND(ID_PREFERENCES, OnPreferences)
	ON_WM_ENTERIDLE()
	ON_COMMAND(ID_QUICK_SEND, OnQuickSend)
	ON_COMMAND(ID_SUSPEND, OnSuspend)
	ON_UPDATE_COMMAND_UI(ID_SUSPEND, OnUpdateSuspend)
	ON_WM_ENDSESSION()
	ON_UPDATE_COMMAND_UI(IDC_FIRST_LANGUAGE, OnUpdateFirstLanguage)
	ON_COMMAND(ID_FILE_APPLYFILTERS, OnFileApplyfilters)
	ON_COMMAND(ID_FRIENDS, OnFriends)
	ON_WM_CLOSE()
	ON_WM_INITMENUPOPUP()
	ON_COMMAND(ID_VIEW_DEL_LOG, OnViewDelLog)
	ON_COMMAND(ID_KILL_SPAM, OnKillSpam)
	ON_COMMAND(ID_TEST_RE, OnTestRe)
	ON_COMMAND(ID_FILTERS, OnFilters)
	ON_WM_COPYDATA()
	ON_WM_QUERYENDSESSION()
	//}}AFX_MSG_MAP
	ON_COMMAND_RANGE(IDC_FIRST_LANGUAGE, IDC_LAST_LANGUAGE, OnLanguageSelect)
	ON_COMMAND_EX(ID_VIEW_STATUS_BAR, OnMyBarCheck)
	ON_COMMAND_EX(ID_VIEW_TOOLBAR, OnMyBarCheck)

	ON_WM_SYSCOMMAND()
	ON_WM_ENTERMENULOOP()
	ON_WM_EXITMENULOOP()
	ON_WM_ACTIVATE()

	ON_MESSAGE( VM_TASKBAR_NOTIFICATION, OnTaskbarNotification )
	ON_MESSAGE( VM_END_RESOLVE_HOST_BY_NAME, OnEndResolveHostByName )
	ON_MESSAGE( VM_START_COMMAND, OnStartCommand )
	ON_MESSAGE( VM_START_PLAYBACK, OnStartPlayback )
	ON_MESSAGE( VM_START_MESSAGE, OnStartMessage )
	ON_MESSAGE( VM_TEST_MESSAGE, OnTestMessage )
	
	ON_MESSAGE( VM_UPDATEITEM, OnUpdateItem )
	ON_MESSAGE( MM_MCINOTIFY, OnMciNotify )

	ON_COMMAND( ID_HELP_FINDER, CFrameWnd::OnHelpFinder )
	ON_COMMAND( ID_HELP, CFrameWnd::OnHelp )
	ON_COMMAND( ID_CONTEXT_HELP, CFrameWnd::OnContextHelp )
	ON_COMMAND( ID_DEFAULT_HELP, CFrameWnd::OnHelpFinder )

	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipText)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipText)
	ON_REGISTERED_MESSAGE( WM_TASKBARCREATED, OnTaskBarCreated )
END_MESSAGE_MAP()

static UINT indicators_when_menu[] =
{
	ID_SEPARATOR,           // status line indicator
};

static UINT indicators_when_no_menu[] =
{
	ID_INDICATOR_OBJECTS,
	ID_INDICATOR_CONTENTS,
};

/////////////////////////////////////////////////////////////////////////////
// CMagicFrame construction/destruction

CMagicFrame::CMagicFrame()
:	m_bIsAlternativeIcon( true ),
	m_uintTimer( 0 ),
	m_bFlashNewMail( false )
{
	m_bListReady = FALSE;
	m_bActive = FALSE;
}

CMagicFrame::~CMagicFrame()
{
}

LRESULT CMagicFrame::OnTaskBarCreated( WPARAM, LPARAM )
{
	// install the taskbar icon
	m_nidA.cbSize = sizeof( NOTIFYICONDATA );
	m_nidA.hWnd = m_hWnd;
	m_nidA.uID = 1;
	m_nidA.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	m_nidA.uCallbackMessage = VM_TASKBAR_NOTIFICATION;
	m_nidA.hIcon = m_hMailboxEmpty16;
	m_nidA.szTip[0] = 0;
	VERIFY( Shell_NotifyIcon( NIM_ADD, &m_nidA ) );

	return 0;
}

void CMagicFrame::SetIndicators( BOOL bMenuIsOn )
{
	UINT nID, nStyle;
	int cxWidth;

	if( bMenuIsOn )
	{
		m_wndStatusBar.SetIndicators( indicators_when_menu, sizeof(indicators_when_menu)/sizeof(UINT) );
		m_wndStatusBar.GetPaneInfo( 0, nID, nStyle, cxWidth );
		m_wndStatusBar.SetPaneInfo( 0, nID, SBPS_STRETCH | SBPS_NOBORDERS, cxWidth );
	}
	else
	{
		m_wndStatusBar.SetIndicators( indicators_when_no_menu, sizeof(indicators_when_no_menu)/sizeof(UINT));
		m_wndStatusBar.GetPaneInfo( 0, nID, nStyle, cxWidth );
		m_wndStatusBar.SetPaneInfo( 0, nID, SBPS_NORMAL, 128 );

		m_wndStatusBar.GetPaneInfo( 1, nID, nStyle, cxWidth );
		m_wndStatusBar.SetPaneInfo( 1, nID, SBPS_STRETCH | SBPS_NORMAL, 128 );
	}
}

void NormalizeRect(CRect& rc)
{
  	CSize ScreenSize(::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
  
	if (rc.Width()>ScreenSize.cx)
		rc.right = rc.left+ScreenSize.cx/2;

	if (rc.right > ScreenSize.cx)
        rc.OffsetRect(-(rc.right - ScreenSize.cx), 0);

    // Too far left?
    if (rc.left < 0)
        rc.OffsetRect( - rc.left, 0);

	if (rc.Height()>ScreenSize.cy)
		rc.bottom = rc.top + ScreenSize.cy/2;

    // Bottom falling out of screen?
    if (rc.bottom > ScreenSize.cy)
        rc.OffsetRect(0, -(rc.bottom-ScreenSize.cy));
	if (rc.top<0)
        rc.OffsetRect( 0, -rc.top);
}

CImageList ToolbarImages;
CEnBitmap	bmToolbar;
CImageList ToolbarImagesD;
int CMagicFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if ( -1 == CFrameWnd::OnCreate(lpCreateStruct) ) return -1;

	m_hMailboxFull = theApp.m_imgMailbox.ExtractIcon(II_FULL);//theApp.LoadIcon( IDI_MAILBOX_FULL );
	m_hMailboxEmpty = theApp.m_imgMailbox.ExtractIcon(II_EMPTY);//theApp.LoadIcon( IDI_MAILBOX_EMPTY );
	if (theApp.m_nWindowsVersion >= WXP)
	{
		//m_hMailboxFull16 = theApp.LoadIcon( IDI_MAILBOX_FULL );
		//m_hMailboxEmpty16 = theApp.LoadIcon( IDI_MAILBOX_EMPTY );
		m_hMailboxFull16 = theApp.m_imgMailbox.ExtractIcon(II_FULL);
		m_hMailboxEmpty16 = theApp.m_imgMailbox.ExtractIcon(II_EMPTY);
	}
	else
	{
		m_hMailboxFull16 = theApp.LoadIcon( IDI_MAILBOX_FULL_16 );
		m_hMailboxEmpty16 = theApp.LoadIcon( IDI_MAILBOX_EMPTY_16 );
	}
	m_hMailboxSuspended = theApp.LoadIcon( IDR_OVERLAYSUSPENDED );

	// status bar 
	m_wndStatusBar.Create( this );
	SetIndicators( FALSE );

	// tool bar 
	VERIFY( m_wndToolBar.Create( this ) );
	
	m_wndToolBar.ModifyStyle(0, TBSTYLE_FLAT);
	m_wndToolBar.LoadToolBar( IDR_TOOLBAR );

	CWindowDC SDC(NULL);  // screen device context
	int nRasterCaps= SDC.GetDeviceCaps(BITSPIXEL);
	if (nRasterCaps>8)
	{
		CSize sz(16, 15);
		// try loading another picture
		CString sBmpFile;
		BOOL bExtFile = FindLocalFile("mmm3glyphs.bmp", sBmpFile, FALSE);
		HANDLE hBitmap = NULL;
		int nColors = 8;
		if (bExtFile)
		{
			hBitmap = ::LoadImage(NULL, sBmpFile, IMAGE_BITMAP, 0, 0,
				LR_LOADFROMFILE);
			if (!hBitmap)
				bExtFile = FALSE;
			BITMAP bmp;
			int nObj = CEnBitmap::GetFileData(sBmpFile, &bmp);
			if (!nObj)
				bExtFile = FALSE;
			else
			{
				sz.cy = bmp.bmHeight;
				if (sz.cy <= 16)
					sz.cx = 16;
				else if (sz.cy==24)
					sz.cx = 24;
				else if (sz.cy==32)
					sz.cx = 32;
				nColors = bmp.bmBitsPixel;
			}
		}
		if (bExtFile && nColors == 32 && (theApp.m_nWindowsVersion < W2K))	// do not allow full color in 9x
			bExtFile = FALSE;
		CEnBitmap bmp;
		if (bExtFile)
		{
			bmp.Attach(CopyImage(hBitmap, IMAGE_BITMAP, 0, 0, LR_COPYRETURNORG));
			bmToolbar.Attach(hBitmap);
		}
		else
		{
			/*
			if (theApp.m_nWindowsVersion < WNTFIRST)
			{
				bmToolbar.LoadBitmap(IDB_TB_98);
				bmp.LoadBitmap(IDB_TB_98);
			}
			else
			*/
			{
				sz.cx = 24;
				sz.cy = 24;
				nColors = 24;
				bmToolbar.LoadBitmap(IDB_FULLCOLORTB);
				bmp.LoadBitmap(IDB_FULLCOLORTB);
			}
		}
		
		if (nColors == 32)
		{
			ToolbarImages.Create(sz.cx, sz.cy, ILC_COLOR32|ILC_MASK, 18, 1);
			if (bmToolbar.ApplyAlpha(GetSysColor(COLOR_BTNFACE)))
				ToolbarImages.Add(&bmToolbar, (CBitmap *)NULL);
			else
				ToolbarImages.Add(&bmToolbar, GetSysColor(COLOR_BTNFACE));
		}
		else
		{
			ToolbarImages.Create(sz.cx, sz.cy, ILC_COLOR24|ILC_MASK, 18, 1);
			ToolbarImages.Add(&bmToolbar, RGB(255, 0, 255));
		}
		CSize sizeButton(sz.cx + 7, sz.cy + 7);
		m_wndToolBar.SetSizes(sizeButton, sz);
		m_wndToolBar.GetToolBarCtrl().SetImageList(&ToolbarImages);
		// disabled images
		if (nColors>8)
			bmp.MakeDisabled32(RGB(255, 0, 255), sz.cx);
		else
			bmp.MakeDisabled(RGB(255, 0, 255), sz.cx);
		if (nColors == 32)
		{
			bmp.ApplyAlpha(GetSysColor(COLOR_BTNFACE));
			ToolbarImagesD.Create(sz.cx, sz.cy, ILC_COLOR32|ILC_MASK, 18, 1);
			ToolbarImagesD.Add(&bmp, (CBitmap *)NULL);
		}
		else
		{
			ToolbarImagesD.Create(sz.cx, sz.cy, ILC_COLOR24|ILC_MASK, 18, 1);
			ToolbarImagesD.Add(&bmp, RGB(255, 0, 255));
		}
		m_wndToolBar.GetToolBarCtrl().SetDisabledImageList(&ToolbarImagesD);
	}

	m_wndToolBar.SetBarStyle( m_wndToolBar.GetBarStyle() | 
		CBRS_TOOLTIPS);

	if (theApp.m_dwFlags & MMF_NO_STATUS)
		m_wndStatusBar.ShowWindow(FALSE);
	if (theApp.m_dwFlags & MMF_NO_TOOLBAR)
		m_wndToolBar.ShowWindow(FALSE);

	// install the taskbar icon
	OnTaskBarCreated( 0, 0 );

	m_uintTimer = SetTimer( 1, 60000, NULL );
	if( !m_uintTimer )
	{
		AfxMessageBox( IDP_TIMER_CREATE_FAILED );
	}

	// kick off the icon animation
	if( theApp.bIsSuspended )
	{
		CreateWatchDog( 1 );
	}

	MenuTranslate(GetMenu()->m_hMenu);

	if( theApp.rcWnd.right )
	{
		WINDOWPLACEMENT wp;
		wp.length = sizeof(WINDOWPLACEMENT);
		wp.flags = 0;
		wp.ptMinPosition.x = -1;
		wp.ptMinPosition.y = -1;
		wp.ptMaxPosition.x = -1;
		wp.ptMaxPosition.y = -1;
		NormalizeRect(theApp.rcWnd);
		CopyRect(&wp.rcNormalPosition, &theApp.rcWnd);
		if (theApp.intStartAlwaysHidden)
			wp.showCmd = SW_HIDE;
		else if (theApp.m_dwFlags & MMF_MAXIMIZED)
		{
			wp.showCmd = SW_HIDE;	// if we show as maximized now, it loses normal position for some reason
			theApp.m_nCmdShow = SW_SHOWMAXIMIZED;
		}
		else
			wp.showCmd = SW_NORMAL;
		if (theApp.m_dwFlags & MMF_MAXIMIZED && theApp.intStartAlwaysHidden )
			wp.flags = WPF_RESTORETOMAXIMIZED;
		SetWindowPlacement(&wp);
	}

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CMagicFrame diagnostics

#ifdef _DEBUG
void CMagicFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMagicFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMagicFrame message handlers

void CMagicFrame::SaveSettings()
{
	WINDOWPLACEMENT wndplMain;
	GetWindowPlacement( &wndplMain );
	theApp.rcWnd = wndplMain.rcNormalPosition;
	BOOL bMax = (wndplMain.showCmd == SW_SHOWMAXIMIZED) || 
				( (wndplMain.showCmd == SW_SHOWMINIMIZED) && wndplMain.flags == WPF_RESTORETOMAXIMIZED);
	SetBoolFlag(theApp.m_dwFlags,  MMF_MAXIMIZED, bMax);

	RECT rc;
	m_wndSplitter.GetPane( 0, 0 )->GetClientRect( &rc );
	theApp.intSplitterPos = rc.bottom;

	// Save/LoadBarState is not compatible, if list of bar is changed
	// Done in OnMyBarCheck
}

BOOL CMagicFrame::DestroyWindow() 
{
	VERIFY( Shell_NotifyIcon( NIM_DELETE, &m_nidA ) );
	KillTimer( m_uintTimer );

	//SaveSettings();

	return CFrameWnd::DestroyWindow();
}

void CMagicFrame::OnTimer(UINT nIDEvent) 
{
	if( m_uintTimer == nIDEvent ) 
	{
		PostMessage( WM_COMMAND, ID_TIMER_CHECK );
		return;
	}
	CFrameWnd::OnTimer( nIDEvent );
}

LRESULT CMagicFrame::OnTaskbarNotification( WPARAM /*wParam*/, LPARAM lParam )
{
	//UINT uID     = (UINT) wParam;  
	UINT uMouMsg = (UINT) lParam;  
 
	switch( uMouMsg ) 
	{
		case WM_RBUTTONDOWN:
		{
			SetForegroundWindow();
		}
		break;

		case WM_RBUTTONUP: // activate IDR_NOTIFY_MENU
		{
			CMenu menuPopup;
			VERIFY( menuPopup.LoadMenu( IDR_NOTIFY_MENU ) );
			POINT ptCur;
			VERIFY( GetCursorPos( &ptCur ) );
			MENUITEMINFO info;
			info.cbSize = sizeof( info );
			info.fMask = MIIM_STATE;
			info.fState = MFS_DEFAULT;
			VERIFY( SetMenuItemInfo( menuPopup.m_hMenu, ID_RESTORE, FALSE, &info ) );
			MenuTranslate(menuPopup.GetSubMenu(0)->m_hMenu);
			menuPopup.GetSubMenu(0)->TrackPopupMenu( 
				TPM_LEFTALIGN | TPM_LEFTBUTTON, ptCur.x, ptCur.y, this, NULL );
		}
		break;
        
		case WM_LBUTTONDBLCLK: // invoke ID_RESTORE command
		{
			if (IsWindowVisible() && !IsIconic())
			{
				ShowWindow(SW_HIDE);
			}
			else
			{
				SetForegroundWindow();
				PostMessage( WM_COMMAND, ID_RESTORE );
			}
		}
		break;
	} 
	return TRUE; 
}

void CMagicFrame::UpdateMailCount( int intTotalMailCount, int intNewMailCount )
{
	if( intNewMailCount > 0 )
	{
		if( !m_bActive)
		{
			m_bFlashNewMail = true;
		}
	}

	HICON hIcon = intTotalMailCount ? m_hMailboxFull16 : m_hMailboxEmpty16;
	m_nidA.hIcon = hIcon;

	CString strTmp;
	strTmp.LoadString( ( intTotalMailCount - 1 ) ? IDP_MESSAGES : IDP_MESSAGE );
	StrTranslate(strTmp);
	_sntprintf_s( m_nidA.szTip, sizeof( m_nidA.szTip ), sizeof(m_nidA.szTip), _T("%d %s"), intTotalMailCount, strTmp );

	if( !IsWatchdogOn() )
	{
		CreateWatchDog( 1 );
	}

	if( intNewMailCount )
	{
		if( theApp.intPopUpMainWindow )
		{
			OnRestore();
		}
	}
}

void CMagicFrame::OnRestore() 
{
	if (IsIconic())
		SendMessage( WM_SYSCOMMAND, SC_RESTORE );
	else if (theApp.m_dwFlags & MMF_MAXIMIZED)
		ShowWindow(SW_MAXIMIZE);
	else
		ShowWindow(SW_SHOW);
}

void CMagicFrame::OnActivate( UINT nState, CWnd* /*pWndOther*/, BOOL bMinimized )
{
	if( !bMinimized && (nState == WA_ACTIVE || nState == WA_CLICKACTIVE ))
	{
		m_bFlashNewMail = false;
		m_bActive = TRUE;
	}
	else
		m_bActive = FALSE;
}

void CMagicFrame::OnUpdateRestore(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable( ( !IsWindowVisible() || IsIconic() || IsZoomed() ) );
}

LRESULT CMagicFrame::OnEndResolveHostByName( WPARAM wParam, LPARAM lParam )
{
	CMagicDoc *doc = static_cast< CMagicDoc* >( GetActiveDocument() );
	ASSERT( doc );
	if( doc )
	{
		doc->EndResolveHostByName( wParam, lParam );
	}

	return 0;
}

void CMagicFrame::OnPreferences() 
{
	COptionsPage pageO;
	CAdvOptionsPage pageA;
	CActPPage pageB;
	CCommandPage pageC;
	CPlaybackPage pageP;
	CLocalOptionsPage pageL;

	CPropertySheet sheet( IDP_PREFERENCES_TITLE, this );
	sheet.AddPage( &pageO );
	sheet.AddPage( &pageA );
	sheet.AddPage( &pageL );
	sheet.AddPage( &pageB );
	sheet.AddPage( &pageC );
	sheet.AddPage( &pageP );

	sheet.DoModal();

	// this would fire even on Cancel,
	// but Apply could have been pressed before, so that's fine
	theApp.SaveSettings(NULL);
	CMagicDoc *doc = static_cast< CMagicDoc* >( GetActiveDocument() );
	if( doc )
		doc->UpdateAllViews(0, HINT_OPTIONS, 0);
}

LRESULT CMagicFrame::OnStartPlayback( WPARAM , LPARAM lParam ) 
{
	int intPlayback = theApp.intPlayback;;
	int intPlaybackDevice = theApp.intPlaybackDevice;
	CString strPlayback = theApp.strPlayback;
	UINT *puMciID = &theApp.m_uMciID;
	if( lParam )
	{
		CMailbox &mbox = *( (CMailbox*) lParam );
		ASSERT( mbox.IsKindOf( RUNTIME_CLASS( CMailbox ) ) );
		if( ACTION_DEFAULT != mbox.m_intPlayback ) 
		{
			intPlayback = mbox.m_intPlayback;
			intPlaybackDevice = mbox.m_intPlaybackDevice;
			strPlayback = mbox.m_strPlayback;
			puMciID = &mbox.m_uMciID;
		}
	}
	
	if( ACTION_NONE == intPlayback )
	{
		return 0;
	}

	if( *puMciID ) return 0;

	MCI_OPEN_PARMS mciOpenParams;
	DWORD dwFlags = 0;
	mciOpenParams.lpstrElementName = strPlayback;

	switch( intPlaybackDevice )
	{
		case PLAYBACK_DEVICE_FILE: 
			dwFlags = MCI_OPEN_ELEMENT; 
			break;
		
		case PLAYBACK_DEVICE_CDA: 
			dwFlags = MCI_OPEN_TYPE; 
			mciOpenParams.lpstrDeviceType = "cdaudio"; 
			break;

	}

	if( mciSendCommand( NULL, MCI_OPEN, dwFlags, (DWORD) &mciOpenParams ) ) 
	{
		MessageBeep( MB_ICONEXCLAMATION );
		return 0;
	}

	*puMciID = mciOpenParams.wDeviceID;
	MCI_PLAY_PARMS mciPlayParams;
	mciPlayParams.dwCallback = (DWORD) m_hWnd;

	if( mciSendCommand( *puMciID, MCI_PLAY, MCI_NOTIFY, (DWORD) &mciPlayParams ) )
	{
		MessageBeep( MB_ICONEXCLAMATION );
		*puMciID = 0;
	}
	return 1;
}

LRESULT CMagicFrame::OnTestMessage( WPARAM wParam, LPARAM lParam )
{
	return ShowMessage(wParam, lParam, TRUE);
}

LRESULT CMagicFrame::ShowMessage(WPARAM wParam, LPARAM lParam, BOOL bForce)
{
	ASSERT( lParam );
	CMailbox &mbox = *( (CMailbox*) lParam );
	ASSERT( mbox.IsKindOf( RUNTIME_CLASS( CMailbox ) ) );

	if( !bForce && ACTION_NONE == mbox.m_intMessage ) 
		return 0;
	
	CString strTmp1, strTmp2, strMessage;
	strTmp1.Format( _T("%d"), (int) wParam ); // number of 
	strTmp2.LoadString( (wParam-1) ? IDP_MESSAGES : IDP_MESSAGE ); // single/plural
	StrTranslate(strTmp2);

	//AfxFormatString2( strMessage, IDP_YOU_HAVE_NEW_2, strTmp1, strTmp2 );
	// result of AfxFormatString2 is not translatable, so:
	MAKE_STRING(strTmp3, IDP_YOU_HAVE_NEW_2);
	strTmp3.Replace("%1","%s");
	strTmp3.Replace("%2","%s");
	strMessage.Format(strTmp3, strTmp1, strTmp2);

	CString strCaption;
	strTmp1.LoadString( AFX_IDS_APP_TITLE );
	StrTranslate(strTmp1);
	strCaption.Format( _T("%s - %s"), mbox.m_strAlias, strTmp1 );

	SetForegroundWindow();
	MessageBox( strMessage, strCaption, MB_ICONINFORMATION );
	return 1;
}
LRESULT CMagicFrame::OnStartMessage( WPARAM wParam, LPARAM lParam )
{
	return ShowMessage(wParam, lParam, FALSE);
}

void CMagicFrame::OnSysCommand( UINT nID, LPARAM lParam )
{
	if( SC_CLOSE == nID ) 
	{
		if (theApp.intMarkRead == READ_MINIM || 
			theApp.intMarkRead == READ_BOTH)
			m_wndSplitter.SendMessageToDescendants(WM_COMMAND, IDC_ALL_READ, 0, FALSE, FALSE);
		SaveSettings();
		ShowWindow( SW_HIDE );
	}
	else 
		CFrameWnd::OnSysCommand( nID, lParam );
}

LRESULT CMagicFrame::OnStartCommand( WPARAM , LPARAM lParam )
{
	( (CMagicDoc*) GetActiveDocument() )->OnUpdateStopCommandAll( NULL ); 

	int intCommand = theApp.intCommand;
	int intCommandRun = theApp.intCommandRun;
	CString strCommand = theApp.strCommand;
	HANDLE *phCmdID = &theApp.m_hCmdID;
	if( lParam )
	{
		CMailbox &mbox = *( (CMailbox*) lParam );
		ASSERT( mbox.IsKindOf( RUNTIME_CLASS( CMailbox ) ) );
		if( ACTION_DEFAULT != mbox.m_intCommand )
		{
			intCommand = mbox.m_intCommand;
			intCommandRun = mbox.m_intCommandRun;
			strCommand = mbox.m_strCommand;
			phCmdID = &mbox.m_hCmdID;
		}
	}
	if( *phCmdID )
	{
		//SetForegroundProcess
		return 0;
	}

	if( ACTION_NONE == intCommand ) return 0;
	
	strCommand.TrimLeft();
	strCommand.TrimRight();

	if( strCommand.IsEmpty() ) return 0;

	CString strFile = strCommand;
	int intFileIndex = 0;
	int intDelimiter = 0;
	int intDelimiterMax = strCommand.GetLength();
	{
		BOOL bQuoted = FALSE;
		TCHAR prev = 0, curr = 0;

		for( intDelimiter = 0; intDelimiter < intDelimiterMax; ++intDelimiter )
		{
			prev = curr;
			curr = strCommand.GetAt( intDelimiter );
			strFile.SetAt( intFileIndex, curr );

			if( _T('\"') == curr && _T('\\') != prev ) { bQuoted = !bQuoted; --intFileIndex; }
			else if( ( _T('\t') == curr || _T(' ') == curr ) &&	!bQuoted ) break;
			++intFileIndex;
		}

		if( intFileIndex < intDelimiterMax ) strFile.SetAt( intFileIndex, _T('\0') );
	}

	CString strParameters = strCommand.Right( intDelimiterMax - intDelimiter - 1 );

	int intCmdShow = SW_NORMAL;
	switch( intCommandRun )
	{
		case COMMAND_RUN_HIDDEN: intCmdShow = SW_HIDE; break;
		case COMMAND_RUN_MINIMIZED: intCmdShow = SW_SHOWMINNOACTIVE; break;
		case COMMAND_RUN_MAXIMIZED: intCmdShow = SW_SHOWMAXIMIZED; break;
	}

	SHELLEXECUTEINFO sei = 
	{
		sizeof( SHELLEXECUTEINFO ),
		SEE_MASK_DOENVSUBST | SEE_MASK_NOCLOSEPROCESS,
		NULL,
		_T("open"),
		strFile,
		strParameters,
		NULL,
		intCmdShow,
		0, 0, 0, 0, 0, 0, 0
	};		
	
	ShellExecuteEx( &sei );

	*phCmdID = sei.hProcess;
	return 1;
}

BOOL CMagicFrame::OnCreateClient( LPCREATESTRUCT /*lpcs*/, CCreateContext* pContext ) 
{
	VERIFY( m_wndSplitter.CreateStatic( this, 2, 1 ) );
	VERIFY( m_wndSplitter.CreateView( 0, 0, RUNTIME_CLASS( CMailboxView ), CSize( 0, theApp.intSplitterPos ), pContext ) );
	VERIFY( m_wndSplitter.CreateView( 1, 0, RUNTIME_CLASS( CExcerptView ), CSize( 0, 0 ), pContext ) );

	return TRUE;
}

LRESULT CMagicFrame::OnMciNotify( WPARAM , LPARAM lParam )
{
	ASSERT( GetActiveDocument() );
	( (CMagicDoc*) GetActiveDocument() )->MciNotify( lParam );
	return 0;
}

void CMagicFrame::OnEnterMenuLoop( BOOL bIsTrackPopupMenu )
{
	SetIndicators( TRUE );
	CFrameWnd::OnEnterMenuLoop( bIsTrackPopupMenu );
}

void CMagicFrame::OnExitMenuLoop( BOOL bIsTrackPopupMenu )
{
	CFrameWnd::OnExitMenuLoop( bIsTrackPopupMenu );
	SetIndicators( FALSE );
}

LRESULT CMagicFrame::OnUpdateItem( WPARAM wParam, LPARAM )
{
	CMagicDoc *doc = ( (CMagicDoc*) GetActiveDocument() );
	if( !doc ) return 0;

	if( wParam ) 
		doc->UpdateItem( (CObject*) wParam );
	else 
		doc->UpdateIdle();

	return 0;
}

void CMagicFrame::OnEnterIdle(UINT nWhy, CWnd* pWho) 
{
	CFrameWnd::OnEnterIdle(nWhy, pWho);
	SendMessage( VM_UPDATEITEM );	
}

void CMagicFrame::OnQuickSend() 
{
	/* launch mailer now! */
	if( (int) ShellExecute( NULL, _T("open"), _T("mailto:"), _T(""), _T(""), SW_NORMAL ) < 32 )
	{
		CString strMessage;
		AfxFormatString1( strMessage, IDP_CANNOT_FOLLOW_THE_LINK_1, _T("mailto:") );
		AfxMessageBox( strMessage );
	}
}

void CMagicFrame::OnSuspend() 
{
	theApp.bIsSuspended = !theApp.bIsSuspended;
	if( !IsWatchdogOn() )
	{
		CreateWatchDog( 1 );
	}
}

void CMagicFrame::OnUpdateSuspend(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck( theApp.bIsSuspended );
}

void CMagicFrame::WatchDogTimedOut()
{
	if( theApp.bIsSuspended )
	{
		if( m_bIsAlternativeIcon )
		{
			m_nidB = m_nidA;
			m_nidB.hIcon = m_hMailboxSuspended;
			VERIFY( Shell_NotifyIcon( NIM_MODIFY, &m_nidB ) );
			SetMainWindowIcon( m_nidB.hIcon );
		}
		else
		{
			VERIFY( Shell_NotifyIcon( NIM_MODIFY, &m_nidA ) );
			SetMainWindowIcon( m_nidA.hIcon );
		}
		m_bIsAlternativeIcon = !m_bIsAlternativeIcon;

		CreateWatchDog( 1000 ); // 1 sec
	}
	else if( m_bFlashNewMail )
	{
		if( m_bIsAlternativeIcon )
		{
			VERIFY( Shell_NotifyIcon( NIM_MODIFY, &m_nidA ) );
			SetMainWindowIcon( m_nidA.hIcon );
		}
		else
		{
			m_nidB = m_nidA;
			m_nidB.hIcon = m_hMailboxEmpty16;
			VERIFY( Shell_NotifyIcon( NIM_MODIFY, &m_nidB ) );
			SetMainWindowIcon( m_nidB.hIcon );
		}
		m_bIsAlternativeIcon = !m_bIsAlternativeIcon;

		CreateWatchDog( 1000 ); // 1 sec
	}
	else
	{
		VERIFY( Shell_NotifyIcon( NIM_MODIFY, &m_nidA ) );
		SetMainWindowIcon( m_nidA.hIcon );
		m_bIsAlternativeIcon = true;
	}
}

void CMagicFrame::SwitchPane()
{
	m_wndSplitter.ActivateNext();
}

void CMagicFrame::OnEndSession(BOOL bEnding) 
{
	SaveSettings();

	CFrameWnd::OnEndSession(bEnding);
}

void CMagicFrame::SetMainWindowIcon( HICON hTaskbarIcon )
{
	HICON hMainWindowIcon = 0;

	if( hTaskbarIcon == m_hMailboxFull16 )
	{
		hMainWindowIcon = m_hMailboxFull;
	}
	else if( hTaskbarIcon == m_hMailboxEmpty16 )
	{
		hMainWindowIcon = m_hMailboxEmpty;
	}
	else if( hTaskbarIcon == m_hMailboxSuspended )
	{
		hMainWindowIcon = m_hMailboxSuspended;
	}
	else
	{
		ASSERT( false );
	}

	SetIcon( hMainWindowIcon, FALSE );
}

void CMagicFrame::GetMessageString( UINT nID, CString& rMessage ) const
{
	CFrameWnd::GetMessageString( nID, rMessage ) ;
	StrTranslate(rMessage);
}

void CMagicFrame::OnLanguageSelect(UINT nID) 
{
	int nLang = nID - IDC_FIRST_LANGUAGE-1;
	LPCTSTR sFile = NULL;
	if (nLang<0)
		sFile = NULL;
	else if (nLang < m_aLanguages.GetSize())
		sFile = m_aLanguages[nLang].sFile;
	else
		return;
	GetDictionary()->SetDictionary(sFile, TRUE);
	// update language change
	static CMenu menu;	// static is required to avoid bufg with Alt.
	menu.DestroyMenu();
	menu.LoadMenu(IDR_MAINFRAME);
	GetDictionary()->MenuTranslate(menu.m_hMenu);
	SetMenu(&menu);

	// send message to all windows
	m_wndSplitter.SendMessageToDescendants(VM_LANGUAGE, 0, 0, FALSE, FALSE);
	theApp.strLastDiction = sFile;

	m_aLanguages.RemoveAll();	// lng list have to be updated
}

void CMagicFrame::OnUpdateFirstLanguage(CCmdUI* pCmdUI) 
{
	if (!pCmdUI->m_pSubMenu)	
		return;
	if (m_aLanguages.GetSize())
		return;	// already done
	m_aLanguages.RemoveAll();
	GetDictionary()->FindDictions(m_aLanguages);
	if (m_aLanguages.GetSize() == 0)
		return;
	for (int i = 0; i<m_aLanguages.GetSize(); i++)
	{
		pCmdUI->m_pSubMenu->AppendMenu(MF_STRING, 
			IDC_FIRST_LANGUAGE+i+1, m_aLanguages[i].sLang);
	}
}

BOOL CMagicFrame::OnToolTipText(UINT nID, NMHDR* pNMHDR, LRESULT* pResult)
{
	// need to handle both ANSI and UNICODE versions of the message
	ASSERT(pNMHDR->code == TTN_NEEDTEXTA || pNMHDR->code == TTN_NEEDTEXTW);

	if (! CFrameWnd::OnToolTipText(nID, pNMHDR, pResult))
		return FALSE;

#ifndef _UNICODE
	if (pNMHDR->code == TTN_NEEDTEXTA)
	{
		TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
		CString s = pTTTA->szText;
		if (s.IsEmpty())
			s.LoadString(pNMHDR->idFrom);
		StrTranslate(s);
		lstrcpyn(pTTTA->szText, s, sizeof(pTTTA->szText));
	}
	else
	{
		TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
		CString str=pTTTW->szText;		// convert to ANSI
		if (str.IsEmpty())
			str.LoadString(pNMHDR->idFrom);
		StrTranslate(str);
		_mbstowcsz(pTTTW->szText, str, sizeof(pTTTW->szText));
	}
#else
	if (pNMHDR->code == TTN_NEEDTEXTW)
	{
		TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
		CString s = pTTTW->szText;
		StrTranslate(s);
		lstrcpyn(pTTTW->szText, s, sizeof(pTTTW->szText));
	}
#endif

	return TRUE;	// message was handled
}


void CMagicFrame::OnFileApplyfilters() 
{
	m_wndSplitter.SendMessageToDescendants(WM_COMMAND, ID_FILE_APPLYFILTERS, 0, FALSE, FALSE);
}

void CMagicFrame::OnFriends() 
{
	CString sFile;
	if (!FindLocalFile(FRIENDS_FILE, sFile, FALSE))
	{
		// no file? - create one
		CStdioFile file;
		if (!file.Open(sFile, CFile::modeCreate|CFile::typeText|CFile::modeWrite, NULL))
			return;
		file.WriteString(";If sender address is in this file, message is not filtered in any way\n");
		file.WriteString(";You can allow whole domains with *@domain.com\n");
		file.WriteString("\n;SourceForge addresses are not limited to @sourceforge.net : user@users.sourceforge.net\n");
		file.WriteString("*.sourceforge.net\n");
	}
	ShellExecute(NULL, "Open", sFile, NULL, NULL, SW_SHOW);
}

void CMagicFrame::OnClose() 
{
	SaveSettings();	
	CFrameWnd::OnClose();
}
// copied from MFC, to ensure correct state save
BOOL CMagicFrame::OnMyBarCheck(UINT nID)
{
	ASSERT(ID_VIEW_STATUS_BAR == AFX_IDW_STATUS_BAR);
	ASSERT(ID_VIEW_TOOLBAR == AFX_IDW_TOOLBAR);

	CControlBar* pBar = GetControlBar(nID);
	if (pBar != NULL)
	{
		BOOL bVis = ((pBar->GetStyle() & WS_VISIBLE) == 0);
		ShowControlBar(pBar, bVis, FALSE);
		if (nID == ID_VIEW_STATUS_BAR)
		{
			if (bVis)
				theApp.m_dwFlags &= ~MMF_NO_STATUS;
			else
				theApp.m_dwFlags |= MMF_NO_STATUS;
		}
		else if (nID == ID_VIEW_TOOLBAR)
		{
			if (bVis)
				theApp.m_dwFlags &= ~MMF_NO_TOOLBAR;
			else
				theApp.m_dwFlags |= MMF_NO_TOOLBAR;
		}
		return TRUE;
	}
	return FALSE;
}

void CMagicFrame::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu) 
{
	CFrameWnd::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
	
	if (pPopupMenu->GetMenuItemID(0) != ID_LIST_MIN)
		return;

	UpdateFilterLists();
	pPopupMenu->DeleteMenu(0, MF_BYPOSITION);
	CStringArray asActions;
	CString sAction;
	LOAD_STRING(sAction, IDP_COLUMN_TO);	asActions.Add(sAction);
	LOAD_STRING(sAction, IDP_COLUMN_FROM);	asActions.Add(sAction);
	LOAD_STRING(sAction, IDP_COLUMN_SUBJ);	asActions.Add(sAction);
	
	int nMax = min(49, m_asFilterLists.GetSize());

	for (int i=0; i<nMax; i++)
	{
		CMenu popup;
		popup.CreatePopupMenu();
		popup.AppendMenu(MF_STRING, ID_LIST_MIN+1+i, asActions[0]);
		popup.AppendMenu(MF_STRING, ID_LIST_MIN+50+i, asActions[1]);
		popup.AppendMenu(MF_STRING, ID_LIST_MIN+100+i, asActions[2]);
		pPopupMenu->AppendMenu(MF_POPUP, (UINT)popup.Detach(), m_asFilterLists[i]);
	}
}

void CMagicFrame::UpdateFilterLists()
{
	if (m_bListReady)
		return;
	m_asFilterLists.RemoveAll();
	for (int i=0; i<theApp.m_Filters.GetSize(); i++)
	{
		CMailFilter& mf = theApp.m_Filters.ElementAt(i);
		for (int c=0; c<CMailFilter::nConditions; c++)
		{
			if (mf.m_aCnd[c].m_sText.Find('$') != 0)
				continue;
			CString sFile = mf.m_aCnd[c].m_sText.Mid(1);
			sFile.MakeLower();
			sFile.TrimRight();
			for (int j=0; j<m_asFilterLists.GetSize(); j++)
			{
				if (m_asFilterLists[j] == sFile)
				{
					sFile.Empty();
					break;
				}
			}
			if (!sFile.IsEmpty())
				m_asFilterLists.Add(sFile);
		}
	}
	m_bListReady = TRUE;
}

void CMagicFrame::OnViewDelLog() 
{
	CString sPath;
	if (!FindLocalFile(DELLOG_FILE, sPath, TRUE))
		return;
	CString sTxtApp;
	GetTextHandler(sTxtApp, sPath);

	WinExec(sTxtApp, SW_SHOW);
}

void CMagicFrame::OnKillSpam() 
{
	m_wndSplitter.SendMessageToDescendants(WM_COMMAND, ID_KILL_SPAM, 0, FALSE, FALSE);
}
/////////////////////////////////////////////////////////////////////////////
// DReTester dialog


DReTester::DReTester(CWnd* pParent /*=NULL*/)
	: CDialog(DReTester::IDD, pParent)
{
	//{{AFX_DATA_INIT(DReTester)
	m_sRE = _T("");
	m_sText = _T("");
	//}}AFX_DATA_INIT
}


void DReTester::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DReTester)
	DDX_Text(pDX, IDC_RE, m_sRE);
	DDX_Text(pDX, IDC_TEXT, m_sText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DReTester, CDialog)
	//{{AFX_MSG_MAP(DReTester)
	ON_BN_CLICKED(IDOK, OnOk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DReTester message handlers

void DReTester::OnOk() 
{
	UpdateData();
	CString s(m_sRE);
	if (s.GetLength()<=0)
		return;
	if (s[0]!='/')
		s.Format("/%s/i", m_sRE);
	CRegExp re;
	if (!re.SetExpr((LPTSTR)(LPCTSTR)s))
	{
		AfxMessageBox("Bad formed!");
		GetDlgItem(IDC_RE)->SetFocus();
		return;
	}
	SMatches m;
	if (re.Parse((LPTSTR)(LPCTSTR)m_sText, &m))
	{
		SendDlgItemMessage(IDC_TEXT, EM_SETSEL, m.s[0], m.e[0]);
		GetDlgItem(IDC_TEXT)->SetFocus();
	}
	else
	{
		SendDlgItemMessage(IDC_TEXT, EM_SETSEL, 0, 0);
		AfxMessageBox("Not Found!");
	}
}
void CMagicFrame::OnTestRe() 
{
	DReTester dlg;
	dlg.DoModal();
}

CMagicDoc* GetDoc()
{
	POSITION pos = AfxGetApp()->GetFirstDocTemplatePosition();
	CDocTemplate* pDT = AfxGetApp()->GetNextDocTemplate(pos);
	if (!pDT)
		return NULL;
	pos = pDT->GetFirstDocPosition();
	return (CMagicDoc*)pDT->GetNextDoc(pos);
}

void CMagicFrame::OnFilters() 
{
	CMagicDoc* pDoc = GetDoc();
	if (!pDoc)
		return;

	DFilters dlg(pDoc);
	dlg.DoModal();
	m_bListReady = FALSE;
}

BOOL CMagicFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
	if (pCopyDataStruct && pCopyDataStruct->dwData==1)
	{
		OnRestore();
	}
	return CFrameWnd::OnCopyData(pWnd, pCopyDataStruct);
}

BOOL CMagicFrame::OnQueryEndSession() 
{
	CMagicDoc* pDoc = GetDoc();
	if (pDoc && pDoc->IsModified())
	{
		SendMessage(WM_COMMAND, ID_FILE_SAVE);
	}
	if (!CFrameWnd::OnQueryEndSession())
		return FALSE;
	

	return TRUE;
}
