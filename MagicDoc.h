// Copyright (C) 1997-2002 Valeriy Ovechkin
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef MAGIC_DOC_INCLUDED
#define MAGIC_DOC_INCLUDED

class CMailbox;
template < class BASE_CLASS, class TYPE > class CTypedPtrArray;

class CMagicDoc : public CDocument
{
protected: // create from serialization only
	CMagicDoc();
	DECLARE_DYNCREATE(CMagicDoc)
	int		m_nLastMassiveBox;
	int		m_nMassivePoll;

// Attributes
public:
	CString		m_sPassword;	// file protection
	CArray<CMailbox* > m_aMailboxes;

	void EndResolveHostByName( WPARAM, LPARAM );
	void UpdateItem( CObject* );
	void UpdateIdle();
	void NewMailbox();
	void OnNewFolder();
	void CopyMailBox(CMailbox* pCopy);
	void DeleteMailbox( CMailbox* );
	BOOL MailboxProperties( CArray< CMailbox* > &arrayMailbox );
	void MciNotify( UINT );
	void Check(BOOL bForce = FALSE);
	void StopChecking();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMagicDoc)
	public:
	virtual void Serialize(CArchive& ar);
	virtual void DeleteContents();
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnNewDocument();
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMagicDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	int GetFoldersList(CStringArray&);
	int FindFolder(int nIdx);
	int FindBox( CMailbox* pMailbox );
	int FindFolder(LPCTSTR sName, BOOL bCreate = FALSE);
	int FindRoot(CMailbox* pMailbox);
	void ToggleFolder(CMailbox* pBox);
	void UpdateFolderStats();
	void UpdateFolder(CMailbox* pBox);
	CMailbox* GetBox(int idx);

protected:
	BOOL UpdateRequired();
public:
	//{{AFX_MSG(CMagicDoc)
	afx_msg void OnTimerCheck();
	afx_msg void OnStopCommandAll();
	afx_msg void OnStopPlaybackAll();
	afx_msg void OnUpdateStopPlaybackAll(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStopCommandAll(CCmdUI* pCmdUI);
	afx_msg void OnPassword();
	afx_msg void OnUpdatePassword(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg void OnImport();
};

#endif
